/*------------------------------------------------------------------------------
 * Bureau of Meteorology C++ Utility Library
 *
 * Copyright 2014 Mark Curtis
 * Copyright 2016 Commonwealth of Australia, Bureau of Meteorology
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *----------------------------------------------------------------------------*/
#include "config.h"
#include "timestamp.h"
#include "trace.h"
#include "unit_test.h"

using namespace bom;

/* require a minimum resolution of milliseconds
 * the clock may not actually give us this precision, but we want to store values at it regardless to make it
 * easy to use this class with timebase */
static_assert(
      std::ratio_less_equal<timestamp::clock::period, std::chrono::milliseconds::period>::value
    , "timestamp clock resolution insufficient");

timestamp::timestamp(int year, int month, int day, int hour, int minute, int second)
{
  struct tm tmm;
  tmm.tm_year = year - 1900;
  tmm.tm_mon = month - 1;
  tmm.tm_mday = day;
  tmm.tm_hour = hour;
  tmm.tm_min = minute;
  tmm.tm_sec = second;
  time_ = clock::from_time_t(timegm(&tmm));
}

auto timestamp::breakdown(int& year, int& month, int& day) const -> void
{
  auto time = as_time_t();
  struct tm tmm;
  if (gmtime_r(&time, &tmm) == nullptr)
    throw std::runtime_error("gmtime_r failed");
  year = tmm.tm_year + 1900;
  month = tmm.tm_mon + 1;
  day = tmm.tm_mday;
}

auto timestamp::breakdown(int& year, int& month, int& day, int& hour, int& minute, int& second) const -> void
{
  auto time = as_time_t();
  struct tm tmm;
  if (gmtime_r(&time, &tmm) == nullptr)
    throw std::runtime_error("gmtime_r failed");
  year = tmm.tm_year + 1900;
  month = tmm.tm_mon + 1;
  day = tmm.tm_mday;
  hour = tmm.tm_hour;
  minute = tmm.tm_min;
  second = tmm.tm_sec;
}

auto bom::operator<<(std::ostream& lhs, const timestamp& rhs) -> std::ostream&
{
  auto time = rhs.as_time_t();

  struct tm tmm;
  if (gmtime_r(&time, &tmm) == nullptr)
    throw std::runtime_error("gmtime_r failed");

  char buf[32];
  auto len = strftime(buf, sizeof(buf), "%Y-%m-%dT%H:%M:%SZ", &tmm);
  if (len <= 0)
    throw std::runtime_error("strftime failed");

  return lhs.write(buf, len);
}

auto string_conversions<timestamp>::read_string(char const* str, size_t len) -> timestamp
{
  // sanity check
  if (len == 0)
    throw string_conversion_error{"timestamp", ""};

  // is it a time_t directly?
  if (*str == '@')
  {
    try
    {
      return timestamp{string_conversions<time_t>::read_string(str + 1, len - 1)};
    }
    catch (...)
    {
      std::throw_with_nested(string_conversion_error{"timestamp", string(str, len)});
    }
  }

  // okay, try to parse ISO8601
  struct tm tms;
  auto frac = timestamp::clock::duration::zero();
  char t, tz_sign = '+';
  int tz_hh = 0, tz_mm = 0;

  // zero out our broken down time
  memset(&tms, 0, sizeof(struct tm));
  tms.tm_mon = 1;
  tms.tm_mday = 1;

  // build the expected format string according to string length, and note expected return value of sscanf
  char const* fmt;
  int eret;
  switch (len)
  {
  case 10: eret = 3;  fmt = "%04d-%02d-%02d"; break;
  case 13: eret = 5;  fmt = "%04d-%02d-%02d%c%02d"; break;
  case 16: eret = 6;  fmt = "%04d-%02d-%02d%c%02d:%02d"; break;
  case 19: eret = 7;  fmt = "%04d-%02d-%02d%c%02d:%02d:%02d"; break;
  default: eret = 7;  fmt = "%04d-%02d-%02d%c%02d:%02d:%02d"; break;
  }

  // try to parse as ISO 8601 with delimeters
  // here we only deal with up to the end of the integer part of the seconds
  // NOTE: if no timezone is specified we assume UTC _not_ local time
  int ret = sscanf(str, fmt, &tms.tm_year, &tms.tm_mon, &tms.tm_mday, &t, &tms.tm_hour, &tms.tm_min, &tms.tm_sec);

  // verify that we at least got the date components
  if (   ret != eret
      || ret < 3
      || (ret >= 4 && t != 'T' && t != ' '))
    throw string_conversion_error{"timestamp", string(str, len)};

  if (len > 19)
  {
    auto pos = 19;

    // process fractional seconds
    if (str[pos] == '.' || str[pos] == ',')
    {
      int n;
      float frac_sec;
      int ret = sscanf(str + pos, "%f%n", &frac_sec, &n);
      if (ret != 1)
        throw string_conversion_error{"timestamp", string(str, len)};
      pos += n;

      // convert fractions of a second into the native duration type
      frac = std::chrono::duration_cast<timestamp::clock::duration>(std::chrono::duration<float>(frac_sec));
    }

    // skip spaces (not technically valid for iso8601, but common
    while (pos < len && str[pos] == ' ')
      ++pos;

    // parse the time zone
    // NOTE: if no timezone is specified we assume UTC _not_ local time
    switch (len - pos)
    {
    case 0: eret = 0; fmt = ""; break;
    case 1: eret = 1; fmt = "%c"; break;
    case 3: eret = 2; fmt = "%c%02d"; break;
    case 5: eret = 3; fmt = "%c%02d%02d"; break;
    case 6: eret = 3; fmt = "%c%02d:%02d"; break;
    default:
      throw string_conversion_error{"timestamp", string(str, len)};
    }
    ret = sscanf(&str[pos], fmt, &tz_sign, &tz_hh, &tz_mm);
    if (   ret != eret
        || (ret == 1 && tz_sign != 'Z')
        || (ret >= 2 && tz_sign != '-' && tz_sign != '+'))
      throw string_conversion_error{"timestamp", string(str, len)};
  }

  // adjust tm structure offsets
  tms.tm_year -= 1900;
  tms.tm_mon -= 1;

  // determine the timezone adjustment
  int tz_delta = (tz_sign == '-' ? -1 : 1) * (tz_hh * 3600 + tz_mm * 60);

  // convert struct tm and offset to unix time
  // note: portability issue here - see man timegm
  return timestamp{timegm(&tms) - tz_delta} + frac;
}

auto string_conversions<timestamp>::write_string(timestamp val, string& str) -> void
{
  auto time = val.as_time_t();

  struct tm tmm;
  if (gmtime_r(&time, &tmm) == nullptr)
    throw std::runtime_error("gmtime_r failed");

  char buf[32];
  auto len = strftime(buf, sizeof(buf), "%Y-%m-%dT%H:%M:%SZ", &tmm);
  if (len <= 0)
    throw std::runtime_error("strftime failed");

  str.append(buf, len);
}

// multipliers used to convert time units into seconds
// TODO - could calculate these ratio automatically at compile time and use clock::duration instead
//        it would be a marginal speed improvement possibly
static constexpr double time_unit_factors[] =
{
  1.0, 60.0, 3600.0, 86400.0
};

auto bom::duration_from_time_units(double val, time_unit units) -> duration
{
  return std::chrono::seconds{static_cast<std::chrono::seconds::rep>(val * time_unit_factors[static_cast<int>(units)])};
}

auto bom::time_units_from_duration(duration val, time_unit units) -> double
{
  return std::chrono::duration_cast<std::chrono::seconds>(val).count() / time_unit_factors[static_cast<int>(units)];
}

// LCOV_EXCL_START
#include <sstream>
TEST_CASE("timestamp")
{
  // constructor
  CHECK(timestamp{}.as_time_point().time_since_epoch() == 0s);
  CHECK(timestamp{timestamp::clock::time_point{}}.as_time_point().time_since_epoch() == 0s);
  auto t1 = timestamp(1983, 3, 25, 0, 10, 43);
  CHECK(t1.as_time_t() == 417399043);

  // conversion operators
  CHECK((static_cast<timestamp::clock::time_point>(t1) == t1.as_time_point()));
  CHECK(static_cast<time_t>(t1) == 417399043);

  // as_julian
  CHECK(t1.as_julian() == approx(2445418.507442));

  // operator += and -=
  CHECK((t1 += 2h) == timestamp{1983, 3, 25, 2, 10, 43});
  CHECK((t1 -= 10min) == timestamp{1983, 3, 25, 2, 0, 43});
  CHECK((t1 -= 110min) == timestamp{1983, 3, 25, 0, 10, 43});

  // relational operators
  CHECK(t1 != timestamp{1983, 3, 25, 0, 10, 42});
  CHECK(t1 < timestamp{1983, 3, 25, 0, 10, 44});
  CHECK(t1 > timestamp{1983, 3, 25, 0, 10, 42});
  CHECK(t1 <= timestamp{1983, 3, 25, 0, 10, 43});
  CHECK(t1 <= timestamp{1983, 3, 25, 0, 10, 44});
  CHECK(t1 >= timestamp{1983, 3, 25, 0, 10, 43});
  CHECK(t1 >= timestamp{1983, 3, 25, 0, 10, 42});
  CHECK(t1 + 10s == timestamp{1983, 3, 25, 0, 10, 53});
  CHECK(t1 - 10s == timestamp{1983, 3, 25, 0, 10, 33});
  CHECK(t1 - timestamp{1983, 3, 25, 0, 10, 53} == -10s);

  // now()
  // in theory this could fail spuriously - but it would be astoundingly rare
  CHECK(timestamp::now().as_time_t() == time(NULL));

  // breakdown (date)
  int year, month, day, hour, minute, second;
  CHECK_NOTHROW(t1.breakdown(year, month, day));
  CHECK(year == 1983);
  CHECK(month == 3);
  CHECK(day == 25);

  // breakdown (date and time)
  CHECK_NOTHROW(t1.breakdown(year, month, day, hour, minute, second));
  CHECK(year == 1983);
  CHECK(month == 3);
  CHECK(day == 25);
  CHECK(hour == 0);
  CHECK(minute == 10);
  CHECK(second == 43);

  // truncate time
  CHECK(timestamp{2019, 3, 25, 12, 10, 5}.truncate_time() == timestamp{2019, 3, 25});

  // operator<<
  std::ostringstream oss;
  oss << t1;
  CHECK(oss.str() == "1983-03-25T00:10:43Z");

  // read string
  CHECK(from_string<timestamp>("@417399043") == timestamp{1983, 3, 25, 0, 10, 43});
  CHECK(from_string<timestamp>("1983-03-25T00:10:43.000+0005") == timestamp{1983, 3, 25, 0, 5, 43});
  CHECK(from_string<timestamp>("1983-03-25T00:10:43.403+0005") == timestamp{1983, 3, 25, 0, 5, 43} + 403ms);
  CHECK(from_string<timestamp>("1983-03-25T00:10:43+0005") == timestamp{1983, 3, 25, 0, 5, 43});
  CHECK(from_string<timestamp>("1983-03-25T00:10:43-1015") == timestamp{1983, 3, 25, 10, 25, 43});
  CHECK(from_string<timestamp>("1983-03-25T00:10:43+08") == timestamp{1983, 3, 24, 16, 10, 43});
  CHECK(from_string<timestamp>("1983-03-25T00:10:43-08") == timestamp{1983, 3, 25, 8, 10, 43});
  CHECK(from_string<timestamp>("1983-03-25T00:10:43Z") == timestamp{1983, 3, 25, 0, 10, 43});
  CHECK(from_string<timestamp>("1983-03-25T00:10:43") == timestamp{1983, 3, 25, 0, 10, 43});
  CHECK(from_string<timestamp>("1983-03-25T00:10") == timestamp{1983, 3, 25, 0, 10});
  CHECK(from_string<timestamp>("1983-03-25T00") == timestamp{1983, 3, 25, 0});
  CHECK(from_string<timestamp>("1983-03-25") == timestamp{1983, 3, 25});
  CHECK_THROWS_AS(from_string<timestamp>(""), string_conversion_error const&);
  CHECK_THROWS_AS(from_string<timestamp>("@bad"), string_conversion_error const&);
  CHECK_THROWS_AS(from_string<timestamp>("bad"), string_conversion_error const&);
  CHECK_THROWS_AS(from_string<timestamp>("1983-03-xx"), string_conversion_error const&);
  CHECK_THROWS_AS(from_string<timestamp>("1983-03-25X"), string_conversion_error const&);
  CHECK_THROWS_AS(from_string<timestamp>("1983-03-25T00:10:43X"), string_conversion_error const&);
  CHECK_THROWS_AS(from_string<timestamp>("1983-03-25T00:10:43X0000"), string_conversion_error const&);

  // write string
  CHECK(to_string(t1) == "1983-03-25T00:10:43Z");

  // format
  CHECK(fmt::format("{}", t1) == "1983-03-25T00:10:43Z");
  CHECK(fmt::format("{:%Y-%j}", t1) == "1983-084");

  // abs()
  CHECK(abs(duration{10s}) == 10s);
  CHECK(abs(duration{-10s}) == 10s);

  // duration conversions
  CHECK(duration_from_time_units(10.0, time_unit::seconds) == 10s);
  CHECK(duration_from_time_units(10.0, time_unit::minutes) == 10min);
  CHECK(duration_from_time_units(10.0, time_unit::hours) == 10h);
  CHECK(duration_from_time_units(10.0, time_unit::days) == 240h);

  CHECK(time_units_from_duration(10s, time_unit::seconds) == approx(10.0));
  CHECK(time_units_from_duration(10min, time_unit::minutes) == approx(10.0));
  CHECK(time_units_from_duration(10h, time_unit::hours) == approx(10.0));
  CHECK(time_units_from_duration(240h, time_unit::days) == approx(10.0));
}
// LCOV_EXCL_STOP
