/*------------------------------------------------------------------------------
 * Bureau of Meteorology C++ Utility Library
 *
 * Copyright 2014 Mark Curtis
 * Copyright 2016 Commonwealth of Australia, Bureau of Meteorology
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *----------------------------------------------------------------------------*/
#pragma once

#include "angle.h"
#include "string_utils.h"
#include "traits.h"

#include <cmath>
#include <istream>
#include <ostream>

namespace bom
{
  template <typename T>
  struct vec4
  {
    using value_type = T;

    T x, y, z, w;

    vec4() noexcept = default;
    constexpr vec4(T x, T y, T z, T w) : x(x), y(y), z(z), w(w) { }

    constexpr vec4(vec4 const& rhs) = default;
    constexpr vec4(vec4&& rhs) noexcept = default;
    auto operator=(vec4 const& rhs) -> vec4& = default;
    auto operator=(vec4&& rhs) noexcept -> vec4& = default;
    ~vec4() noexcept = default;

    template <typename U>
    constexpr operator vec4<U>() const
    {
      return vec4<U>(static_cast<U>(x), static_cast<U>(y), static_cast<U>(z), static_cast<U>(w));
    }

    template <typename U = T, typename = typename std::enable_if<!std::is_floating_point<U>::value>::type>
    constexpr auto operator==(vec4 const& rhs) const -> bool
    {
      return x == rhs.x && y == rhs.y && z == rhs.z && w == rhs.w;
    }

    template <typename U = T, typename = typename std::enable_if<!std::is_floating_point<U>::value>::type>
    constexpr auto operator!=(vec4 const& rhs) const -> bool
    {
      return x != rhs.x || y != rhs.y || z != rhs.z || w != rhs.w;
    }

    auto operator+=(vec4 const& rhs) -> vec4&
    {
      x += rhs.x;
      y += rhs.y;
      z += rhs.z;
      w += rhs.w;
      return *this;
    }

    auto operator-=(vec4 const& rhs) -> vec4&
    {
      x -= rhs.x;
      y -= rhs.y;
      z -= rhs.z;
      w -= rhs.w;
      return *this;
    }

    template <typename U>
    auto operator*=(U const& rhs) -> vec4&
    {
      x *= rhs;
      y *= rhs;
      z *= rhs;
      w *= rhs;
      return *this;
    }

    template <typename U>
    auto operator/=(U const& rhs) -> vec4&
    {
      x /= rhs;
      y /= rhs;
      z /= rhs;
      w /= rhs;
      return *this;
    }

    auto normalize() -> vec4&
    {
      if (auto l = length())
      {
        x /= l;
        y /= l;
        z /= l;
        w /= l;
      }
      else
      {
        x = 0.0;
        y = 0.0;
        z = 0.0;
        w = 0.0;
      }
      return *this;
    }

    auto normal() const -> vec4
    {
      vec4 ret;
      if (auto l = length())
      {
        ret.x = x / l;
        ret.y = y / l;
        ret.z = z / l;
        ret.w = w / l;
      }
      else
      {
        ret.x = 0;
        ret.y = 0;
        ret.z = 0;
        ret.w = 0;
      }
      return ret;
    }

    constexpr auto dot(vec4 const& rhs) const -> T
    {
      return x * rhs.x + y * rhs.y + z * rhs.z + w * rhs.w;
    }

    constexpr auto length_sqr() const -> T
    {
      return x * x + y * y + z * z + w * w;
    }

    auto length() const -> decltype(std::sqrt(x*x + y*y + z*z))
    {
      return std::sqrt(x * x + y * y + z * z + w * w);
    }

    auto angle(const vec4& rhs) const -> angle
    {
      return acos(dot(rhs) / (length() * rhs.length()));
    }

    template <typename T1, typename T2>
    friend constexpr auto operator+(vec4<T1> const& lhs, vec4<T2> const& rhs) -> vec4<decltype(lhs.x + rhs.x)>;

    template <typename T1, typename T2>
    friend constexpr auto operator-(vec4<T1> const& lhs, vec4<T2> const& rhs) -> vec4<decltype(lhs.x - rhs.x)>;

    template <typename T1, typename U>
    friend constexpr auto operator*(vec4<T1> const& lhs, U rhs) -> vec4<decltype(lhs.x * rhs)>;

    template <typename T1, typename U>
    friend constexpr auto operator*(U lhs, vec4<T1> const& rhs) -> vec4<decltype(lhs * rhs.x)>;

    template <typename T1, typename U>
    friend constexpr auto operator/(vec4<T1> const& lhs, U rhs) -> vec4<decltype(lhs.x / rhs)>;

    template <typename T1>
    friend auto operator>>(std::istream& lhs, vec4<T1>& rhs) -> std::istream&;

    template <typename T1>
    friend auto operator<<(std::ostream& lhs, vec4<T1> const& rhs) -> std::ostream&;
  };

  template <typename T1, typename T2>
  inline constexpr auto operator+(vec4<T1> const& lhs, vec4<T2> const& rhs) -> vec4<decltype(lhs.x + rhs.x)>
  {
    return vec4<decltype(lhs.x + rhs.x)>(lhs.x + rhs.x, lhs.y + rhs.y, lhs.z + rhs.z, lhs.w + rhs.w);
  }

  template <typename T1, typename T2>
  inline constexpr auto operator-(vec4<T1> const& lhs, vec4<T2> const& rhs) -> vec4<decltype(lhs.x - rhs.x)>
  {
    return vec4<decltype(lhs.x - rhs.x)>(lhs.x - rhs.x, lhs.y - rhs.y, lhs.z - rhs.z, lhs.w - rhs.w);
  }

  template <typename T1, typename U>
  inline constexpr auto operator*(vec4<T1> const& lhs, U rhs) -> vec4<decltype(lhs.x * rhs)>
  {
    return vec4<decltype(lhs.x * rhs)>(lhs.x * rhs, lhs.y * rhs, lhs.z * rhs, lhs.w * rhs);
  }

  template <typename T1, typename U>
  inline constexpr auto operator*(U lhs, vec4<T1> const& rhs) -> vec4<decltype(lhs * rhs.x)>
  {
    return vec4<decltype(lhs * rhs.x)>(lhs * rhs.x, lhs * rhs.y, lhs * rhs.z, lhs * rhs.w);
  }

  template <typename T1, typename U>
  inline constexpr auto operator/(vec4<T1> const& lhs, U rhs) -> vec4<decltype(lhs.x / rhs)>
  {
    return vec4<decltype(lhs.x / rhs)>(lhs.x / rhs, lhs.y / rhs, lhs.z / rhs, lhs.w / rhs);
  }

  template <typename T1>
  inline auto operator>>(std::istream& lhs, vec4<T1>& rhs) -> std::istream&
  {
    return lhs >> rhs.x >> rhs.y >> rhs.z >> rhs.w;
  }

  template <typename T1>
  inline auto operator<<(std::ostream& lhs, vec4<T1> const& rhs) -> std::ostream&
  {
    return lhs << rhs.x << ' ' << rhs.y << ' ' << rhs.z << ' ' << rhs.w;
  }

  template <typename T>
  struct string_conversions<vec4<T>>
  {
    static auto read_string(char const* str, size_t len) -> vec4<T>
    try
    {
      auto end = str;
      while (end < str + len && !std::isspace(*end))
        ++end;

      auto next = end;
      while (next < str + len && std::isspace(*next))
        ++next;

      auto end2 = next;
      while (end2 < str + len && !std::isspace(*end2))
        ++end2;

      auto next2 = end2;
      while (next2 < str + len && std::isspace(*next2))
        ++next2;

      auto end3 = next2;
      while (end3 < str + len && !std::isspace(*end3))
        ++end3;

      auto next3 = end3;
      while (next3 < str + len && std::isspace(*next3))
        ++next3;

      return {
          string_conversions<T>::read_string(str, end - str)
        , string_conversions<T>::read_string(next, end2 - next)
        , string_conversions<T>::read_string(next2, end3 - next2)
        , string_conversions<T>::read_string(next3, len - (next3 - str))
        };
    }
    catch (...)
    {
      std::throw_with_nested(string_conversion_error{"vec4", string(str, len)});
    }

    static auto write_string(vec4<T> val, string& str) -> void
    {
      string_conversions<T>::write_string(val.x, str);
      str.push_back(' ');
      string_conversions<T>::write_string(val.y, str);
      str.push_back(' ');
      string_conversions<T>::write_string(val.z, str);
      str.push_back(' ');
      string_conversions<T>::write_string(val.w, str);
    }
  };

  template <typename T>
  struct allow_simple_string_conversions<vec4<T>>
  {
    static constexpr bool value = true;
  };

  using vec4i = vec4<int>;
  using vec4z = vec4<size_t>;
  using vec4f = vec4<float>;
  using vec4d = vec4<double>;
}

namespace fmt
{
  template <typename T, typename Char>
  struct formatter<bom::vec4<T>, Char> : formatter<T, Char>
  {
    template <typename FormatContext>
    auto format(bom::vec4<T> val, FormatContext& ctx)
    {
      formatter<T, Char>::format(val.x, ctx);
      fmt::format_to(ctx.out(), " ");
      formatter<T, Char>::format(val.y, ctx);
      fmt::format_to(ctx.out(), " ");
      formatter<T, Char>::format(val.z, ctx);
      fmt::format_to(ctx.out(), " ");
      formatter<T, Char>::format(val.w, ctx);
      return ctx.out();
    }
  };
}
