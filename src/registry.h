/*------------------------------------------------------------------------------
 * Bureau of Meteorology C++ Utility Library
 *
 * Copyright 2014 Mark Curtis
 * Copyright 2016 Commonwealth of Australia, Bureau of Meteorology
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *----------------------------------------------------------------------------*/
#pragma once

#include "string_utils.h"
#include "traits.h"
#include <unordered_map>

namespace bom
{
  namespace detail
  {
    template <typename T>
    struct make_shared_proxy
    {
      template <typename... Args>
      auto operator()(Args&&... args) -> shared_ptr<T>
      {
        return std::make_shared<T>(std::forward<Args>(args)...);
      }
    };
  }

  /// Generic registry for named objects
  /** This template is useful for facilitating the management of resources which should be loaded on demand and
   *  then shared by any number of users.  This is perfect for items such as images, fonts, string tables etc.
   *
   *  The default behaviour of this template when initializing a new instance of a resource is to forward any
   *  arguments to the acquire() function, including the key, to std::make_shared().  By supplying a custom type
   *  for the F parameter this behaviour may be changed.  Any calls to acquire() which result in the instanciation
   *  of a resource will then be forwarded to an instance of F using the function call syntax.  The instance of
   *  F may be set using the set_factory() method.
   *
   *  struct my_factory
   *  {
   *    auto operator()(string const& name) -> shared_ptr<foo> { ... }
   *  };
   *  using foo_registry = registry<foo, string, my_factory>;
   *
   *  Resources which are not actively in use (i.e. their shared_ptr is unique) may be manually released by
   *  calling the cleanup() function.  This allows the point of destruction to be deterministic for the
   *  application.
   */
  template <typename T, typename K = string, typename F = detail::make_shared_proxy<T>>
  class registry : private std::unordered_map<K, shared_ptr<T>>
  {
  public:
    using factory_type = F;

  public:
    /// Construct a registry using the default factory function
    registry()
    { }

    /// Construct a registry using a non-default factory function
    registry(factory_type factory)
      : factory_{std::move(factory)}
    { }

    /// Acquire the resource initializing if necessary
    template <typename... Args>
    auto acquire(K const& key, Args&&... args) -> shared_ptr<T>
    {
      auto& ptr = std::unordered_map<K, shared_ptr<T>>::operator[](key);
      if (!ptr)
      {
        try
        {
          ptr = factory_(key, std::forward<Args>(args)...);
          if (!ptr)
            throw std::runtime_error{"null pointer returned by factory"};
        }
        catch (...)
        {
          std::throw_with_nested(std::runtime_error{fmt::format("failed to acquire resource: {}", key)});
        }
      }
      return ptr;
    }

    /// Cleanup the internal registry table by removing entries for expired resources
    auto cleanup() -> void
    {
      for (auto i = begin(); i != end(); )
      {
        if (i->second.unique())
          i = erase(i);
        else
          ++i;
      }
    }

    using std::unordered_map<K, shared_ptr<T>>::begin;
    using std::unordered_map<K, shared_ptr<T>>::cbegin;
    using std::unordered_map<K, shared_ptr<T>>::end;
    using std::unordered_map<K, shared_ptr<T>>::cend;

  private:
    factory_type   factory_;
  };
}
