/*------------------------------------------------------------------------------
 * Bureau of Meteorology C++ Utility Library
 *
 * Copyright 2014 Mark Curtis
 * Copyright 2016 Commonwealth of Australia, Bureau of Meteorology
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *----------------------------------------------------------------------------*/
#pragma once

#include "angle.h"
#include "string_utils.h"

namespace bom
{
  struct latlon
  {
    enum class format
    {
        decimal
      , dms
    };

    angle lat, lon;

    latlon() noexcept  = default;
    latlon(angle lat, angle lon) noexcept : lat(lat), lon(lon) { }

    latlon(latlon const& rhs) noexcept = default;
    latlon(latlon&& rhs) noexcept = default;

    auto operator=(latlon const& rhs) noexcept -> latlon& = default;
    auto operator=(latlon&& rhs) noexcept -> latlon& = default;

    ~latlon() noexcept = default;
  };

  auto operator<<(std::ostream& lhs, latlon const& rhs) -> std::ostream&;

  template <>
  struct string_conversions<latlon>
  {
    static auto read_string(char const* str, size_t len) -> latlon;
  };

  template <>
  struct allow_simple_string_conversions<latlon>
  {
    static constexpr bool value = true;
  };

  struct latlonalt : public latlon
  {
    double alt;

    latlonalt() noexcept  = default;
    latlonalt(angle lat, angle lon, double alt) noexcept : latlon{lat, lon}, alt{alt} { }
    latlonalt(latlon ll, double alt) noexcept : latlon{ll}, alt{alt} { }

    latlonalt(latlonalt const& rhs) noexcept = default;
    latlonalt(latlonalt&& rhs) noexcept = default;

    auto operator=(latlonalt const& rhs) noexcept -> latlonalt& = default;
    auto operator=(latlonalt&& rhs) noexcept -> latlonalt& = default;

    ~latlonalt() noexcept = default;
  };

  auto operator<<(std::ostream& lhs, latlonalt const& rhs) -> std::ostream&;

  template <>
  struct string_conversions<latlonalt>
  {
    static auto read_string(char const* str, size_t len) -> latlonalt;
  };

  template <>
  struct allow_simple_string_conversions<latlonalt>
  {
    static constexpr bool value = true;
  };
}

namespace fmt
{
  template <typename Char>
  struct formatter<bom::latlon, Char> : formatter<bom::angle, Char>
  {
    template <typename FormatContext>
    auto format(bom::latlon val, FormatContext& ctx)
    {
      if (formatter<bom::angle, Char>::style == formatter<bom::angle, Char>::out_style::dms)
      {
        formatter<bom::angle, Char>::format(val.lat.abs(), ctx);
        fmt::format_to(ctx.out(), "{} ", val.lat.radians() < 0.0 ? "S" : "N");
        formatter<bom::angle, Char>::format(val.lon.abs(), ctx);
        fmt::format_to(ctx.out(), "{}", val.lon.radians() < 0.0 ? "W" : "E");
      }
      else
      {
        formatter<bom::angle, Char>::format(val.lat, ctx);
        fmt::format_to(ctx.out(), " ");
        formatter<bom::angle, Char>::format(val.lon, ctx);
      }
      return ctx.out();
    }
  };

  template <typename Char>
  struct formatter<bom::latlonalt, Char> : formatter<bom::angle, Char>
  {
    template <typename FormatContext>
    auto format(bom::latlonalt val, FormatContext& ctx)
    {
      if (formatter<bom::angle, Char>::style == formatter<bom::angle, Char>::out_style::dms)
      {
        formatter<bom::angle, Char>::format(val.lat.abs(), ctx);
        fmt::format_to(ctx.out(), "{} ", val.lat.radians() < 0.0 ? "S" : "N");
        formatter<bom::angle, Char>::format(val.lon.abs(), ctx);
        fmt::format_to(ctx.out(), "{} ", val.lon.radians() < 0.0 ? "W" : "E");
        fmt::format_to(ctx.out(), "{:.0f}m", val.alt);
      }
      else
      {
        formatter<bom::angle, Char>::format(val.lat, ctx);
        fmt::format_to(ctx.out(), " ");
        formatter<bom::angle, Char>::format(val.lon, ctx);
        fmt::format_to(ctx.out(), " ");
        formatter<double, Char>::format(val.alt, ctx);
      }
      return ctx.out();
    }
  };
}
