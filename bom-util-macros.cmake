#-------------------------------------------------------------------------------
# Bureau of Meteorology C++ Utitlity Library
#
# Copyright 2014 Mark Curtis
# Copyright 2016 Commonwealth of Australia, Bureau of Meteorology
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#-------------------------------------------------------------------------------

# extract a release tag from the project if possible, assumes we are using git
# TODO - allow specification of alternative output variable
macro(determine_release_tag)
  find_package(Git)
  if(GIT_FOUND)
    execute_process(
      COMMAND "${GIT_EXECUTABLE}" describe --dirty
      WORKING_DIRECTORY "${PROJECT_SOURCE_DIR}"
      OUTPUT_VARIABLE PROJECT_RELEASE_TAG
      OUTPUT_STRIP_TRAILING_WHITESPACE
      ERROR_QUIET)
  endif()
  if(NOT DEFINED PROJECT_RELEASE_TAG OR "${PROJECT_RELEASE_TAG}" STREQUAL "")
    set(PROJECT_RELEASE_TAG "unknown")
  endif()
endmacro()

# setup our standard C++ compilation settings
function(setup_cplusplus)
  cmake_parse_arguments(PARSE_ARGV 0 arg "" "STANDARD" "")

  # default to C++ 23 by default
  if (NOT DEFINED arg_STANDARD)
    set(arg_STANDARD 23)
  endif()

  # require desired c++ standard and disable compiler extensions by default
  set(CMAKE_CXX_STANDARD ${arg_STANDARD} PARENT_SCOPE)
  set(CMAKE_CXX_STANDARD_REQUIRED ON PARENT_SCOPE)
  set(CMAKE_CXX_EXTENSIONS OFF PARENT_SCOPE)

  # set a high warning level
  add_compile_options(-Wall -pedantic -Wextra -Wno-unused-parameter)
endfunction()

# setup unit test and code coverage targets
# can't use a function here because enable_testing() has to be in global scope (it seems)
macro(setup_testing)

  # user options
  option(ENABLE_TESTING   "Enable unit test generation (make test)" ON)
  option(ENABLE_COVERAGE  "Enable code coverage report generation (make coverage)" OFF)

  if (ENABLE_COVERAGE AND NOT ENABLE_TESTING)
    message(FATAL_ERROR "Code coverage requires unit test generation (set ENABLE_TESTING=ON)")
  endif()

  if (ENABLE_TESTING)
    enable_testing()

    # we dynamically create the file containing the 'main' for doctest that is needed by each unit test executable
    file(WRITE ${PROJECT_BINARY_DIR}/unit_test.cc "#define DOCTEST_CONFIG_IMPLEMENT_WITH_MAIN\n")
    if (PROJECT_NAME STREQUAL "bom-util")
      file(APPEND ${PROJECT_BINARY_DIR}/unit_test.cc "#include \"${PROJECT_SOURCE_DIR}/src/unit_test.h\"\n")
    else()
      file(APPEND ${PROJECT_BINARY_DIR}/unit_test.cc "#include <bom/unit_test.h>\n")
    endif()

    if (ENABLE_COVERAGE)
      if (NOT CMAKE_BUILD_TYPE STREQUAL "Debug")
        message(FATAL_ERROR "Code coverage must performed on a Debug build")
      endif()
      if (NOT CMAKE_COMPILER_IS_GNUCXX)
        message(FATAL_ERROR "Code coverage requires use of gcc as compiler")
      endif()

      # this generates the coverage report itself, the target_unit_test() function is what actually
      # instruments the unit test binaries and adds dependency targets on this one to run them
      add_custom_target(coverage
        COMMAND mkdir -p ${PROJECT_BINARY_DIR}/coverage
        COMMAND gcovr
            --root=${PROJECT_SOURCE_DIR}
            --object-directory=${PROJECT_BINARY_DIR}
            --exclude='.*/doctest.h'
            --html
            --html-details
            --print-summary
            --output=${PROJECT_BINARY_DIR}/coverage/coverage.html
        COMMAND gcovr
            --root=${PROJECT_SOURCE_DIR}
            --object-directory=${PROJECT_BINARY_DIR}
            --exclude='.*/doctest.h'
            --html
            --xml-pretty
            --print-summary
            --output=${PROJECT_BINARY_DIR}/coverage.xml
        )
    endif()
  endif()

endmacro()

# setup unit tests for a single user defined target
function(target_unit_test target)

  # bail if unit tests are disabled
  if (NOT ENABLE_TESTING)
    return()
  endif()

  # fetch relevant properties from the user supplied target
  get_target_property(sources ${target} SOURCES)
  get_target_property(compile_options ${target} COMPILE_OPTIONS)
  get_target_property(include_directories ${target} INCLUDE_DIRECTORIES)
  get_target_property(link_libraries ${target} LINK_LIBRARIES)

  # duplicate user target and set unit test compilation tags
  add_executable(ut-${target} ${sources} ${PROJECT_BINARY_DIR}/unit_test.cc)
  target_compile_definitions(ut-${target} PRIVATE BOM_UNIT_TEST)
  foreach (option ${compile_options})
    target_compile_options(ut-${target} PRIVATE ${option})
  endforeach()
  if (include_directories)
    target_include_directories(ut-${target} PRIVATE ${include_directories})
  endif()
  if (link_libraries)
    target_link_libraries(ut-${target} PRIVATE ${link_libraries})
  endif()

  # add calling this executable as a cmake test
  add_test(NAME "${target}" COMMAND ut-${target} --reporters=junit --out=unit-test-report-${target}.xml)

  # setup for coverage testing if desired
  if (ENABLE_COVERAGE)
    # instrument our unit test binary for coverage testing
    target_compile_options(ut-${target} PRIVATE --coverage)
    target_link_libraries(ut-${target} PRIVATE gcov)

    # add a custom target to run unit tests as part of coverage target
    add_custom_target(cov-${target} COMMAND ut-${target})
    add_dependencies(coverage cov-${target})
  endif()

endfunction()

macro(setup_package_exports)
  if (NOT EXISTS "${PROJECT_SOURCE_DIR}/${PROJECT_NAME}-config.cmake.in")
    message(FATAL_ERROR "no package config template file supplied (${PROJECT_NAME}-config.cmake.in)")
  endif()
  include(CMakePackageConfigHelpers)
  write_basic_package_version_file(
    "${PROJECT_BINARY_DIR}/${PROJECT_NAME}-config-version.cmake"
    COMPATIBILITY SameMajorVersion
  )
  export(
    EXPORT ${PROJECT_NAME}-targets
    FILE "${PROJECT_BINARY_DIR}/${PROJECT_NAME}-targets.cmake"
    NAMESPACE ${PROJECT_NAME}::
  )
  configure_package_config_file(
    ${PROJECT_NAME}-config.cmake.in "${PROJECT_BINARY_DIR}/${PROJECT_NAME}-config.cmake"
    INSTALL_DESTINATION "${CMAKE_INSTALL_LIBDIR}/cmake/${PROJECT_NAME}"
  )
  install(
    EXPORT ${PROJECT_NAME}-targets
    FILE ${PROJECT_NAME}-targets.cmake
    DESTINATION "${CMAKE_INSTALL_LIBDIR}/cmake/${PROJECT_NAME}"
    NAMESPACE ${PROJECT_NAME}::
    COMPONENT devel
  )
  install(
    FILES
      "${PROJECT_BINARY_DIR}/${PROJECT_NAME}-config.cmake"
      "${PROJECT_BINARY_DIR}/${PROJECT_NAME}-config-version.cmake"
    DESTINATION
      "${CMAKE_INSTALL_LIBDIR}/cmake/${PROJECT_NAME}"
    COMPONENT
      devel
  )
endmacro()
