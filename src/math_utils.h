/*------------------------------------------------------------------------------
 * Bureau of Meteorology C++ Utility Library
 *
 * Copyright 2014 Mark Curtis
 * Copyright 2016 Commonwealth of Australia, Bureau of Meteorology
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *----------------------------------------------------------------------------*/
#pragma once

#include "traits.h"
#include <cmath>
#include <limits>
#include <ratio>

namespace bom
{
  namespace detail {
    /* this union is based on those in ieee754.h, however a number of things
     * are different:
     * - it is templated to reduce duplication of nan/is_nan style functions
     * - a constructor is added (with consistent argument order regarless of 
     *   endian-ness) to allow use within a constexpr context
     * - we split out 16 bits of the mantissa as a custom payload.  16 bits 
     *   are used because this is the maximum sized integer that will fit
     *   within the space of the 32 bit precision type.  it also avoids
     *   complicated bit shifting when needing to use mantissa0 and mantissa1
     *   in the larger types.
     */
    template <typename T, int S = sizeof(T), int digits = std::numeric_limits<T>::digits>
    union ieee754_nan;

    // 32 bit floating point types
    // 23 bit mantissa assumption
    template <typename T>
    union ieee754_nan<T, 4, 24>
    {
      static_assert(std::numeric_limits<T>::is_iec559, "floating point type is not ieee754");

      struct splitter
      {
#if __BYTE_ORDER__ == __ORDER_BIG_ENDIAN__
        unsigned int negative:1;
        unsigned int exponent:8;
        unsigned int quiet_nan:1;
        unsigned int payload:16;
        unsigned int mantissa:6;
        constexpr splitter(std::uint16_t payload)
          : negative(0), exponent(255), quiet_nan(1), payload(payload), mantissa(0)
        { }
#else
        unsigned int mantissa:6;
        unsigned int payload:16;
        unsigned int quiet_nan:1;
        unsigned int exponent:8;
        unsigned int negative:1;
        constexpr splitter(std::uint16_t payload)
          : mantissa(0), payload(payload), quiet_nan(1), exponent(255), negative(0)
        { }
#endif
      };

      splitter  split;
      T         value;

      constexpr ieee754_nan(std::uint16_t payload)
        : split(payload)
      { }
      constexpr ieee754_nan(T val)
        : value(val)
      { }
    };

    // 64 bit floating point types
    // 52 bit mantissa assumption
    template <typename T>
    union ieee754_nan<T, 8, 53>
    {
      static_assert(std::numeric_limits<T>::is_iec559, "floating point type is not ieee754");

      struct splitter
      {
#if __BYTE_ORDER__ == __ORDER_BIG_ENDIAN__
        unsigned int negative:1;
        unsigned int exponent:11;
        unsigned int quiet_nan:1;
        unsigned int payload:16;
        unsigned int mantissa0:3;
        unsigned int mantissa1:32;
        constexpr splitter(std::uint16_t payload)
          : negative(0), exponent(2047), quiet_nan(1), payload(payload), mantissa0(0), mantissa1(0)
        { }
#else
#if __FLOAT_WORD_ORDER__ == __ORDER_BIG_ENDIAN__
        unsigned int mantissa0:3;
        unsigned int payload:16;
        unsigned int quiet_nan:1;
        unsigned int exponent:11;
        unsigned int negative:1;
        unsigned int mantissa1:32;
        constexpr splitter(std::uint16_t payload)
          : mantissa0(0), payload(payload), quiet_nan(1), exponent(2047), negative(0), mantissa1(0)
        { }
#else
        unsigned int mantissa1:32;
        unsigned int mantissa0:3;
        unsigned int payload:16;
        unsigned int quiet_nan:1;
        unsigned int exponent:11;
        unsigned int negative:1;
        constexpr splitter(std::uint16_t payload)
          : mantissa1(0), mantissa0(0), payload(payload), quiet_nan(1), exponent(2047), negative(0)
        { }
#endif
#endif
      };

      splitter  split;
      T         value;

      constexpr ieee754_nan(std::uint16_t payload)
        : split(payload)
      { }
      constexpr ieee754_nan(T val)
        : value(val)
      { }
    };

    // 80 bit extended precision floating point types (long double)
    template <typename T>
    union ieee754_nan<T, 16, 64>
    {
      static_assert(std::numeric_limits<T>::is_iec559, "floating point type is not ieee754");

      struct splitter
      {
#if __BYTE_ORDER__ == __ORDER_BIG_ENDIAN__
        unsigned int negative:1;
        unsigned int exponent:15;
        unsigned int empty:16;
        unsigned int one:1;
        unsigned int quiet_nan:1;
        unsigned int payload:16;
        unsigned int mantissa0:14;
        unsigned int mantissa1:32;
        constexpr splitter(std::uint16_t payload)
          : negative(0), exponent(32767), empty(0), one(1), quiet_nan(1), payload(payload), mantissa0(0), mantissa1(0)
        { }
#else
#if __FLOAT_WORD_ORDER__ == __ORDER_BIG_ENDIAN__
        unsigned int exponent:15;
        unsigned int negative:1;
        unsigned int empty:16;
        unsigned int mantissa0:14;
        unsigned int payload:16;
        unsigned int quiet_nan:1;
        unsigned int one:1;
        unsigned int mantissa1:32;
        constexpr splitter(std::uint16_t payload)
          : exponent(32767), negative(0), empty(0), mantissa0(0), payload(payload), quiet_nan(1), one(1), mantissa1(0)
        { }
#else
        unsigned int mantissa1:32;
        unsigned int mantissa0:14;
        unsigned int payload:16;
        unsigned int quiet_nan:1;
        unsigned int one:1;
        unsigned int exponent:15;
        unsigned int negative:1;
        unsigned int empty:16;
        constexpr splitter(std::uint16_t payload)
          : mantissa1(0), mantissa0(0), payload(payload), quiet_nan(1), one(1), exponent(32767), negative(0), empty(0)
        { }
#endif
#endif
      };

      splitter  split;
      T         value;

      constexpr ieee754_nan(std::uint16_t payload)
        : split(payload)
      { }
      constexpr ieee754_nan(T val)
        : value(val)
      { }
    };

    // future work - 128 bit (quad precision) layouts if needed
  }

  /// NaN value for a type
  template <typename T>
  inline constexpr auto nan() -> T
  {
    return std::numeric_limits<T>::quiet_NaN();
  }

  /// NaN value for a type with a specific payload
  template <typename T>
  inline constexpr auto nan(uint16_t payload) -> T
  {
    return detail::ieee754_nan<T>{payload}.value;
  }

  /// Extract the payload from a NaN value
  template <typename T>
  inline constexpr auto nan_payload(T val) -> uint16_t
  {
    return detail::ieee754_nan<T>{val}.split.payload;
  }

  /// Is a value NaN?
  template <typename T>
  inline constexpr auto is_nan(T val) -> bool
  {
    return std::isnan(val);
  }

  /// Linear interpolation between two quantities
  template <typename T>
  inline constexpr auto lerp(T const& lhs, T const& rhs, double fraction) -> T
  {
    return lhs + fraction * (rhs - lhs);
  }
  template <typename T>
  inline constexpr auto lerp(T const& lhs, T const& rhs, float fraction) -> T
  {
    return lhs + fraction * (rhs - lhs);
  }

  /// Round a floating point value to a desired decimal precision
  template <typename ratio, typename T>
  inline constexpr auto round_to_precision(T val) -> T
  {
    return (ratio::num * std::round((val * ratio::den) / ratio::num)) / ratio::den;
  }

  /// Find the greatest common divisor of two integers
  // TODO - replace this with c++17 std::gcd when available
  auto gcd(int a, int b) -> int;

  /// Convert linear value to decibels (power quantities)
  inline auto to_db_pwr(double val) -> double
  {
    return 10.0 * std::log10(val);
  }
  inline auto to_db_pwr(float val) -> float
  {
    return 10.0f * std::log10(val);
  }

  /// Convert decibels to linear value (power quantities)
  inline auto from_db_pwr(double val) -> double
  {
    return std::pow(10.0, 0.1 * val);
  }
  inline auto from_db_pwr(float val) -> float
  {
    return std::pow(10.0f, 0.1f * val);
  }

  /// Convert linear value to decibels (amplitude/field quantities)
  inline auto to_db_amp(double val) -> double
  {
    return 20.0 * std::log10(val);
  }
  inline auto to_db_amp(float val) -> float
  {
    return 20.0f * std::log10(val);
  }

  /// Convert decibels to linear value (amplitude/field quantities)
  inline auto from_db_amp(double val) -> double
  {
    return std::pow(10.0, 0.05 * val);
  }
  inline auto from_db_amp(float val) -> float
  {
    return std::pow(10.0f, 0.05f * val);
  }
}
