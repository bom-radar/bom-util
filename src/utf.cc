/*------------------------------------------------------------------------------
 * Bureau of Meteorology C++ Utility Library
 *
 * Copyright 2014 Mark Curtis
 * Copyright 2016 Commonwealth of Australia, Bureau of Meteorology
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *----------------------------------------------------------------------------*/
#include "config.h"
#include "utf.h"
#include "trace.h"

using namespace bom;

#define isutf(c) (((c)&0xC0)!=0x80)

static constexpr char32_t utf8_offsets[6] =
{
    0x00000000ul
  , 0x00003080ul
  , 0x000e2080ul
  , 0x03c82080ul
  , 0xfa082080ul
  , 0x82082080ul
};

auto bom::utf8_size(char const* utf8) -> size_t
{
  size_t i = 0;
  while (utf8_next_character(utf8))
    ++i;
  return i;
}

auto bom::utf8_next_character(char const*& utf8) -> char32_t
{
  // TODO - replace this with something nicer
  char32_t ret = 0;
  int sz = 0;
  int i = 0;
  while (utf8[i])
  {
    ret <<= 6;
    ret += (unsigned char)utf8[i++];
    if (isutf(utf8[i]))
      break;
    sz++;
  }
  ret -= utf8_offsets[sz];
  utf8 += i;
  return ret;
}

