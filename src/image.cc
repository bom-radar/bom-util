/*------------------------------------------------------------------------------
 * Bureau of Meteorology C++ Utility Library
 *
 * Copyright 2014 Mark Curtis
 * Copyright 2016 Commonwealth of Australia, Bureau of Meteorology
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *----------------------------------------------------------------------------*/
#include "config.h"
#include "image.h"

using namespace bom;

auto image::overlay(image const& img, vec2i position) -> void
{
  for (size_t y = std::max(0, -position.y); y < std::min(extents().y - position.y, img.extents().y); ++y)
  {
    for (size_t x = std::max(0, -position.x); x < std::min(extents().x - position.x, img.extents().x); ++x)
    {
      auto fg = img[y][x];
      auto blend = fg.a / 255.0f;
      fg.a = 255;

      auto& bg = (*this)[position.y + y][position.x + x];
      bg = colour::lerp_rgb(bg, fg, blend);
    }
  }
}
