/*------------------------------------------------------------------------------
 * Bureau of Meteorology C++ Utility Library
 *
 * Copyright 2014 Mark Curtis
 * Copyright 2016 Commonwealth of Australia, Bureau of Meteorology
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *----------------------------------------------------------------------------*/
#pragma once

#include "../string_utils.h"
#include "../traits.h"
#include <vector>

namespace bom::io
{
  /// Internal configuration file parser
  class configuration
  {
  public:
    /// Configuration file node types
    enum class node_type
    {
        null
      , string
      , array
      , object
    };
    using string_type = bom::string;
    using array_type = std::vector<configuration>;
    using object_type = std::vector<std::pair<string_type, configuration>>;

    /// Error thrown when user requests a type that conflicts with type in file
    class type_mismatch : public std::runtime_error
    {
    public:
      type_mismatch(node_type expected, node_type found);

      auto expected() const -> node_type  { return expected_; }
      auto found() const -> node_type     { return found_; }

    private:
      node_type expected_;
      node_type found_;
    };

    /// Tag type used to indicate streaming should include top level tags
    struct top_level_tag {};

    /// Constant used in stream constructor and write function to request top level tags
    constexpr static top_level_tag top_level = top_level_tag{};

    /// Null configuration typically used as second argument to optional()
    static configuration const null;

  public:
    /// Initialize a configuration object as a null
    configuration()
      : type_{node_type::null}
    { }

    /// Initialize a configuration object as a string from a scalar type
    template <typename T, typename = typename std::enable_if<allow_simple_string_conversions<T>::value>::type>
    explicit configuration(T val)
      : type_{node_type::string}
    {
      new (&string_) string_type{to_string(val)};
    }

    /// Initialize a configuration object as a string
    explicit configuration(char const* val)
      : type_{node_type::string}
    {
      new (&string_) string_type{val};
    }

    /// Initialize a configuration object as a string
    explicit configuration(string_type val)
      : type_{node_type::string}
    {
      new (&string_) string_type{std::move(val)};
    }

    /// Initialize a configuration object as an array
    explicit configuration(array_type val)
      : type_{node_type::array}
    {
      new (&array_) array_type{std::move(val)};
    }

    /// Initialize a configuration object as an object
    explicit configuration(object_type val)
      : type_{node_type::object}
    {
      new (&object_) object_type{std::move(val)};
    }

    /// Parse a configuration object from a stream
    /** This assumes that the configuration is to be an object and that the stream will contain individual
     *  key/value pairs without any top-level braces.  The stream will be read until EOF.  If you
     *  wish to load only a single value from the stream and then stop processing use the version
     *  of this constructor which accepts the top_level tag. */
    explicit configuration(std::istream& in);
    explicit configuration(std::istream&& in) : configuration{in} { }

    explicit configuration(std::istream& in, top_level_tag);
    explicit configuration(std::istream&& in, top_level_tag) : configuration{in, top_level} { }

    configuration(configuration const& rhs);
    configuration(configuration&& rhs) noexcept;

    /// Destroy the configuration object
    ~configuration();

    auto operator=(configuration const& rhs) -> configuration&;
    auto operator=(configuration&& rhs) noexcept -> configuration&;
    template <typename T>
    auto operator=(T&& val) -> configuration&
    {
      return *this = configuration{std::forward<T>(val)};
    }

    auto write(std::ostream& out, indent in = {}) const -> void;
    auto write(std::ostream&& out, indent in = {}) const -> void          { write(out, in); }

    auto write(std::ostream& out, top_level_tag, indent in = {}) const -> void;
    auto write(std::ostream&& out, top_level_tag, indent in = {}) const -> void  { write(out, top_level, in); }

    auto clear() -> void;

    auto type() const -> node_type                                        { return type_; }

    auto string() const -> string_type const&;
    auto array() const -> array_type const&;
    auto object() const -> object_type const&;

    template <typename T, typename = typename std::enable_if<allow_simple_string_conversions<T>::value>::type>
    operator T() const                                                    { return from_string<T>(string()); }
    operator string_type const&() const                                   { return string(); }
    operator array_type const&() const                                    { return array(); }
    operator object_type const&() const                                   { return object(); }

    auto size() const -> size_t;

    /// Indexed access
    auto operator[](int i) const -> configuration const&                  { return operator[](static_cast<size_t>(i)); }
    auto operator[](int i) -> configuration&                              { return const_cast<configuration&>(static_cast<configuration const*>(this)->operator[](i)); }
    auto operator[](size_t i) const -> configuration const&;
    auto operator[](size_t i) -> configuration&                           { return const_cast<configuration&>(static_cast<configuration const*>(this)->operator[](i)); }
    auto at(size_t i) const -> configuration const&;
    auto at(size_t i) -> configuration&                                   { return const_cast<configuration&>(static_cast<configuration const*>(this)->at(i)); }

    /// Associative access
    /** The non-const versions will insert an empty configuration and return it if no match is found
     *  this allows std::map style access when building a configuration */
    auto operator[](char const* key) const -> configuration const&;
    auto operator[](char const* key) -> configuration&;
    auto operator[](bom::string const& key) const -> configuration const& { return operator[](key.c_str()); }
    auto operator[](bom::string const& key) -> configuration&             { return operator[](key.c_str()); }

    /// Object searching
    auto find(char const* key) const -> configuration const*;
    auto find(char const* key) -> configuration*                          { return const_cast<configuration*>(static_cast<configuration const*>(this)->find(key)); }
    auto find(bom::string const& key) const -> configuration const*       { return find(key.c_str()); }
    auto find(bom::string const& key) -> configuration*                   { return find(key.c_str()); }

    /// Object lookup (optional)
    template <typename T>
    auto optional(char const* key, T const& def) const -> T
    {
      if (auto p = find(key))
        return *p;
      return def;
    }
    auto optional(char const* key, configuration const& def) const -> configuration const&
    {
      if (auto p = find(key))
        return *p;
      return def;
    }
    auto optional(char const* key, char const* def) const -> char const*
    {
      if (auto p = find(key))
        return p->string().c_str();
      return def;
    }

  private:
    node_type type_;
    union
    {
      string_type string_;
      array_type  array_;
      object_type object_;
    };
  };

  inline auto operator>>(std::istream& lhs, configuration& rhs) -> std::istream&
  {
    rhs = configuration(lhs);
    return lhs;
  }
  inline auto operator<<(std::ostream& lhs, configuration const& rhs) -> std::ostream&
  {
    rhs.write(lhs);
    return lhs;
  }
}

namespace bom
{
  BOM_DECLARE_ENUM_TRAITS(io::configuration::node_type, null, string, array, object);
}
