/*------------------------------------------------------------------------------
 * Bureau of Meteorology C++ Utility Library
 *
 * Copyright 2014 Mark Curtis
 * Copyright 2016 Commonwealth of Australia, Bureau of Meteorology
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *----------------------------------------------------------------------------*/
#pragma once

#include "string_utils.h"
#include <chrono>

namespace bom
{
  /// Allow use of standard UDL for chrono types
  using namespace std::chrono_literals;

  /// UTC timestamp
  class timestamp
  {
  public:
    using clock = std::chrono::system_clock;

  public:
    /// Initialize timestamp with system clock epoch
    constexpr timestamp() noexcept
    { }

    /// Initialize timestamp with given time point
    constexpr timestamp(clock::time_point time) noexcept
      : time_(time)
    { }

    /// Initialize timestamp with given unix time
    explicit timestamp(time_t time) noexcept
      : time_(clock::from_time_t(time))
    { }

    /// Initialize timestamp from broken down UTC date/time components
    timestamp(int year, int month, int day, int hour = 0, int minute = 0, int second = 0);

    timestamp(const timestamp& rhs) noexcept = default;
    timestamp(timestamp&& rhs) noexcept = default;
    auto operator=(const timestamp& rhs) noexcept -> timestamp& = default;
    auto operator=(timestamp&& rhs) noexcept -> timestamp& = default;
    ~timestamp() noexcept = default;

    /// Convert the timestamp into the underlying time_point representation
    constexpr operator clock::time_point() const noexcept
    {
      return time_;
    }

    /// Convert the timestamp into a UNIX timestamp
    explicit operator time_t() const noexcept
    {
      return clock::to_time_t(time_);
    }

    /// Get the timestamp value as a time_point
    constexpr auto as_time_point() const noexcept -> clock::time_point
    {
      return time_;
    }

    /// Get the timestamp value as a UNIX timestamp
    auto as_time_t() const noexcept -> time_t
    {
      return clock::to_time_t(time_);
    }

    /// Get the timestamp as a julian date
    auto as_julian() const noexcept -> double
    {
      return 2440587.5 + clock::to_time_t(time_) / 86400.0;
    }

    /// Add a duration to this timestamp
    auto operator+=(clock::duration rhs) noexcept -> timestamp&
    {
      time_ += rhs;
      return *this;
    }

    /// Subtract a duration from this timestamp
    auto operator-=(clock::duration rhs) noexcept -> timestamp&
    {
      time_ -= rhs;
      return *this;
    }

    /// Retrieve the year, month and day components of the timestamp
    auto breakdown(int& year, int& month, int& day) const -> void;

    /// Retrieve the year, month, day, hour, minute and second components of the timestamp
    auto breakdown(int& year, int& month, int& day, int& hour, int& minute, int& second) const -> void;

    /// Round down to nearest 00Z
    auto truncate_time() const -> timestamp
    {
      auto t = as_time_t();
      return timestamp{t - (t % 86400)};
    }

    /// Get the earliest representable timestamp
    static constexpr auto min() noexcept -> timestamp
    {
      return timestamp{clock::time_point::min()};
    }

    /// Get the latest representable timestamp
    static constexpr auto max() noexcept -> timestamp
    {
      return timestamp{clock::time_point::max()};
    }

    /// Get the timestamp representing the current system time
    static auto now() noexcept -> timestamp
    {
      return timestamp{clock::now()};
    }

  private:
    clock::time_point time_;

    friend auto operator<=>(const timestamp&, const timestamp&) = default;
  };

  /// Standard duration type for use in arithmetic operations with timestamps
  using duration = timestamp::clock::duration;

  constexpr inline auto operator+(const timestamp& lhs, const duration& rhs) -> timestamp
  {
    return lhs.as_time_point() + rhs;
  }
  constexpr inline auto operator-(const timestamp& lhs, const timestamp& rhs) -> decltype(lhs.as_time_point() - rhs.as_time_point())
  {
    return lhs.as_time_point() - rhs.as_time_point();
  }
  constexpr inline auto operator-(const timestamp& lhs, const duration& rhs) -> timestamp
  {
    return lhs.as_time_point() - rhs;
  }

  /// Insert a timestamp to an output stream in ISO8601 format
  auto operator<<(std::ostream& lhs, const timestamp& rhs) -> std::ostream&;

  template <>
  struct string_conversions<timestamp>
  {
    static auto read_string(char const* str, size_t len) -> timestamp;
    static auto write_string(timestamp val, string& str) -> void;
  };

  template <>
  struct allow_simple_string_conversions<timestamp>
  {
    static constexpr bool value = true;
  };

  inline constexpr auto lerp(timestamp from, timestamp till, float fraction) -> timestamp
  {
    // note: use std::chrono::round() once we have c++17 support
    return from + std::chrono::duration_cast<duration>(fraction * std::chrono::duration_cast<std::chrono::duration<float, duration::period>>(till - from));
  }

  inline constexpr auto lerp(timestamp from, timestamp till, double fraction) -> timestamp
  {
    // note: use std::chrono::round() once we have c++17 support
    return from + std::chrono::duration_cast<duration>(fraction * std::chrono::duration_cast<std::chrono::duration<double, duration::period>>(till - from));
  }

  /// Time units
  /* Note: if you change this enum you must also update the time_unit_factors array */
  enum class time_unit
  {
      seconds
    , minutes
    , hours
    , days
  };

  /// Convert a quantity in a time based unit into a timestamp compatible duration
  auto duration_from_time_units(double val, time_unit units) -> duration;

  /// Convert a timestamp compatible duration into a quantity in a time based unit
  auto time_units_from_duration(duration val, time_unit units) -> double;

  /// Pair of timestamps representing a continuous range of times
  struct timespan
  {
    timestamp from;
    timestamp till;

    auto length() const -> duration
    {
      return till - from;
    }

    auto include(timestamp val) -> void
    {
      if (val < from)
        from = val;
      if (val >= till)
        till = val + duration{1};
    }

    auto intersects(timespan const& val) const -> bool
    {
      return val.from < till && val.till >= from;
    }

    auto clamp(timestamp val) const -> timestamp
    {
      return
        val < from ? from :
        val > till ? till :
        val;
    }

    static constexpr auto empty() noexcept -> timespan
    {
      return { timestamp::max(), timestamp::min() };
    }
  };

  constexpr inline bool operator==(const timespan& lhs, const timespan& rhs)
  {
    return lhs.from == rhs.from && lhs.till == rhs.till;
  }

  constexpr inline bool operator!=(const timespan& lhs, const timespan& rhs)
  {
    return lhs.from != rhs.from || lhs.till != rhs.till;
  }
}

namespace fmt
{
  template <typename Char>
  struct formatter<bom::timestamp, Char> : formatter<std::tm, Char>
  {
    // the correct way to set the default spec seems to change with each libfmt version
#if FMT_VERSION >= 100000
    constexpr formatter()
    {
      this->format_str = detail::string_literal<Char, '%', 'F', 'T', '%', 'T', 'Z'>{};
    }
#elif FMT_VERSION >= 90000
    constexpr formatter()
    {
      basic_string_view<Char> default_specs = detail::string_literal<Char, '%', 'F', 'T', '%', 'T', 'Z'>{};
      this->do_parse(default_specs.begin(), default_specs.end());
    }
#elif FMT_VERSION >= 80000
    static constexpr const std::array<Char, 6> default_specs{'%', 'F', 'T', '%', 'T', 'Z'};
    constexpr formatter()
    {
      this->do_parse(default_specs.begin(), default_specs.end());
    }
    template <typename ParseContext>
    FMT_CONSTEXPR auto parse(ParseContext& ctx) -> decltype(ctx.begin()) {
      return this->do_parse(ctx.begin(), ctx.end(), true);
    }
#else
    static constexpr const std::array<Char, 6> default_specs{'%', 'F', 'T', '%', 'T', 'Z'};
    template <typename ParseContext>
    auto parse(ParseContext& ctx) -> decltype(ctx.begin()) {
      if (ctx.begin() == ctx.end() || *ctx.begin() == '}')
      {
        formatter<std::tm, Char>::parse(default_specs);
        return ctx.begin();
      }
      return formatter<std::tm, Char>::parse(ctx);
    }
#endif

    template <typename FormatContext>
    auto format(bom::timestamp val, FormatContext& ctx)
    {
      auto time = val.as_time_t();
      struct tm tmm;
      if (gmtime_r(&time, &tmm) == nullptr)
        throw std::runtime_error("gmtime_r failed");
      return formatter<std::tm, Char>::format(tmm, ctx);
    }
  };
}
