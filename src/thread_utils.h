/*------------------------------------------------------------------------------
 * Bureau of Meteorology C++ Utility Library
 *
 * Copyright 2014 Mark Curtis
 * Copyright 2016 Commonwealth of Australia, Bureau of Meteorology
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *----------------------------------------------------------------------------*/
#pragma once

#include <signal.h>
#include <condition_variable>
#include <mutex>
#include <system_error>
#include <thread>

namespace bom
{
  /// Launch a thread in which all signals are blocked
  template <typename Function, typename... Args>
  auto launch_thread_no_signals(Function&& f, Args&&... args) -> std::thread
  {
    sigset_t all, orig;
    sigfillset(&all);
    if (pthread_sigmask(SIG_SETMASK, &all, &orig))
      throw std::system_error{errno, std::generic_category()};
    std::thread thread{std::forward<Function>(f), std::forward<Args>(args)...};
    if (pthread_sigmask(SIG_SETMASK, &orig, nullptr))
      throw std::system_error{errno, std::generic_category()};
    return thread;
  }
}
