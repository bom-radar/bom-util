/*------------------------------------------------------------------------------
 * Bureau of Meteorology C++ Utility Library
 *
 * Copyright 2014 Mark Curtis
 * Copyright 2016 Commonwealth of Australia, Bureau of Meteorology
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *----------------------------------------------------------------------------*/
#pragma once

#include <list>
#include <map>
#include <memory>
#include <optional>
#include <set>
#include <tuple>
#include <utility>
#include <vector>
#include <cstddef>

namespace bom
{
  /// Alternative name for std::move to reduce verbosity
  using std::move;

  /// Alternative name for std::unique_ptr to reduce verbosity
  template <typename T>
  using unique_ptr = std::unique_ptr<T>;

  /// Alternative name for std::shared_ptr to reduce verbosity
  template <typename T>
  using shared_ptr = std::shared_ptr<T>;

  /// Alternative name for std::weak_ptr to reduce verbosity
  template <typename T>
  using weak_ptr = std::weak_ptr<T>;

  /// Alternative name for std::vector to reduce verbosity
  template <typename T>
  using vector = std::vector<T>;

  /// Alternative name for std::vector<std::unique_ptr<T>> to reduce verbosity
  template <typename T>
  using vector_uptr = std::vector<unique_ptr<T>>;

  /// Alternative name for std::vector<std::shared_ptr<T>> to reduce verbosity
  template <typename T>
  using vector_sptr = std::vector<shared_ptr<T>>;

  /// Alternative name for std::vector<std::weak_ptr<T>> to reduce verbosity
  template <typename T>
  using vector_wptr = std::vector<weak_ptr<T>>;

  /// Aternative name for std::pair to reduce verbosity
  template <typename T, typename U>
  using pair = std::pair<T, U>;

  /// Alternative name for std::set to reduce verbosity
  template <typename T>
  using set = std::set<T>;

  /// Alternative name for std::map to reduce verbosity
  template <typename K, typename V>
  using map = std::map<K, V>;

  /// Alternative name for std::list to reduce verbosity
  template <typename T>
  using list = std::list<T>;

  /// Alternative name for std::optional to reduce verbosity
  template <typename T>
  using optional = std::optional<T>;
  using std::nullopt;

  /// Utility function to cast anything to const
  using std::as_const;

  /// Unique resource handle (ie: unique_ptr for non-pointer types)
  /** \note We use std::tuple to store the pointer and deleter here because std::tuple is free to employ empty base
   *        optimization so that an empty deleter class consumes no space in our unique_hnd object.  The std::tuple
   *        class is not required to perform this optimization, but has been observed to under GCC. */
  template <typename H, typename D, H nullval = H{}>
  class unique_hnd
  {
  public:
    /// Construct a null handle
    constexpr unique_hnd() noexcept
      : data_(nullval, D{})
    { }

    /// Construct a handle specifying the underlying handle to manage
    explicit unique_hnd(H handle, D deleter = {}) noexcept
      : data_(std::move(handle), std::move(deleter))
    { }

    /// Move constructor
    unique_hnd(unique_hnd&& rhs) noexcept
      : data_(rhs.data_)
    {
      std::get<0>(rhs.data_) = nullval;
    }

    /// Move assignment
    auto operator=(unique_hnd&& rhs) noexcept -> unique_hnd&
    {
      using std::swap;
      swap(data_, rhs.data_);
      return *this;
    }

    /// Destroy a resource
    ~unique_hnd()
    {
      if (std::get<0>(data_) != nullval)
        std::get<1>(data_)(std::get<0>(data_));
    }

    /// Return true if the handle is not the null handle
    operator bool() const noexcept
    {
      return std::get<0>(data_) != nullval;
    }

    /// Return the native handle
    operator H() const noexcept
    {
      return std::get<0>(data_);
    }

    /// Member access (valid if handle is a pointer type)
    auto operator->() const noexcept -> H
    {
      return std::get<0>(data_);
    }

    /// Manually release the resource
    auto release() noexcept -> H
    {
      auto ret = std::get<0>(data_);
      std::get<0>(data_) = nullval;
      return ret;
    }

    /// Reset the resource with a new value
    auto reset(H handle = nullval) noexcept -> void
    {
      if (std::get<0>(data_) != nullval)
        std::get<1>(data_)(std::get<0>(data_));
      std::get<0>(data_) = handle;
    }

    /// Explicitly get the handle
    auto get() const noexcept -> H
    {
      return std::get<0>(data_);
    }

    /// Get a reference to the underlying handle itself
    /** This is useful when dealing with APIs where you need to pass a pointer to the handle, typically
     *  so that the handle can be initialized while the function returns a separate error code. */
    auto ptr() noexcept -> H*
    {
      return &std::get<0>(data_);
    }

    auto ptr() const noexcept -> H const*
    {
      return &std::get<0>(data_);
    }

  public:
    /// The null handle value for this handle type
    static constexpr H nullhnd = nullval;

  private:
    std::tuple<H, D> data_;
  };

  /// Check if two shared (or weak) pointers point to the samed owned object
  /** This function is different to comparing two shared_ptr<> objects using the standard equality comparison
   *  operator.  Due to multiple/virtual inheritance it is possible for operator== to return false on two
   *  shared_ptrs which point to different sub-objects of the same owned piece of memory.  This function
   *  compares whether the pointers own the same piece of memory, not whether they expose the same part of it
   *  publicly.
   *
   *  The advantage of this approach is when weak_ptrs are involved, they do not need to be locked.
   *
   *  \note This function is currently implemented using the strick weak ordering provided by the ptr.owner_before()
   *        member of the smart pointer classes.  This is a less efficient approach than would be possible by
   *        simply comparing the underlying control block addresses, but alas, the C++ standard provides no such
   *        functionality.  This is believed to be an oversight and will hopefully be addressed in a future standard.
   *        At such a time, the implementation of this function should be adjusted to use owner_equals() or whatever
   *        mechanism is introduced to address the lack. */
  template <typename L, typename R>
  auto owner_equals(shared_ptr<L> const& lhs, shared_ptr<R> const& rhs) -> bool
  {
    return !lhs.owner_before(rhs) && !rhs.owner_before(lhs);
  }
  template <typename L, typename R>
  auto owner_equals(shared_ptr<L> const& lhs, weak_ptr<R> const& rhs) -> bool
  {
    return !lhs.owner_before(rhs) && !rhs.owner_before(lhs);
  }
  template <typename L, typename R>
  auto owner_equals(std::weak_ptr<L> const& lhs, shared_ptr<R> const& rhs) -> bool
  {
    return !lhs.owner_before(rhs) && !rhs.owner_before(lhs);
  }
  template <typename L, typename R>
  auto owner_equals(std::weak_ptr<L> const& lhs, weak_ptr<R> const& rhs) -> bool
  {
    return !lhs.owner_before(rhs) && !rhs.owner_before(lhs);
  }

  /// Deleter for in-place constructed objects
  struct placement_deleter
  {
    template <typename T>
    void operator()(T* ptr) const
    {
      ptr->~T();
    }
  };

  /// Alternative name for std::make_unique to reduce verbosity
  using std::make_unique;

  /// Function to build unique_ptr to object whose memory is managed with placement new and delete
  template <typename T, typename... Args>
  auto make_unique_placement(void* where, Args&&... args) -> std::unique_ptr<T, placement_deleter>
  {
    return std::unique_ptr<T, placement_deleter>{::new (where) T(std::forward<Args>(args)...)};
  }

  /// Alternative name for std::make_shared to reduce verbosity
  using std::make_shared;

  /// Custom cast used to static_cast the contents of a unique_ptr
  template <typename T, typename F>
  auto static_unique_ptr_cast(unique_ptr<F>&& from) -> unique_ptr<T>
  {
    return unique_ptr<T>{static_cast<T*>(from.release())};
  }

  /// Allocator template used to generate allocators that are friend of a specific class
  /** The primary purpose of this template is to allow the use of std::make_shared type functionality with a class
   *  which only has private constructors.  Typically, this is to allow a static factory function within the class
   *  to use std::make_shared.
   *
   *  Since std::make_shared does not allow the specification of an allocator you should actually use
   *  std::allocate_shared.  This does exactly the same work as std::make_shared except that it allows you to
   *  specify a custom allocator.
   *
   *  Example usage:
   *
   *  class foo
   *  {
   *  public:
   *    static auto create(int bar) -> shared_ptr<foo>;
   *
   *  private:
   *    foo(int bar);
   *    friend class private_allocator<foo>;
   *  };
   *
   *  auto foo::create(int bar) -> shared_ptr<foo>
   *  {
   *    return std::allocate_shared<foo>(private_allocator<foo>{}, bar);
   *  }
   *
   *  You can also supply a second type parameter to specify the class which is allowed to use the allocator
   *  separately from the type being created.  This is usefull when the factory class is not the same as the
   *  created class:
   *
   *  class factory;
   *
   *  class foo
   *  {
   *    foo(int bar);
   *    friend class private_allocator<foo, factory>;
   *  };
   *
   *  class factory
   *  {
   *    auto create_foo(int bar) -> shared_ptr<foo>
   *    {
   *      return std::allocate_shared<foo>(private_allocator<foo, factory>{}, bar);
   *    }
   *  };
   *
   *  \note The functionality described depends on std::allocate_shared calling the construct() method of the
   *  allocator which is passed in.  Technically, the C++ 2011 specification states that construct() will NOT
   *  be called and that std::allocate_shared will use placement new directly.  This is considered a bug in the
   *  standard and tested compilers (gcc/clang) all actually call construct() as is reasonably expected.
   */
  template <typename T, typename F = T>
  struct private_allocator : std::allocator<T>
  {
    template <typename T2>
    private_allocator(private_allocator<T2, F> const&) { }

    template <typename U>
    struct rebind
    {
      typedef private_allocator<U, F> other;
    };

    template <typename U, class... Args>
    void construct(U* p, Args&&... args)
    {
      ::new(p) T(std::forward<Args>(args)...);
    }

  private:
    private_allocator() = default;
    friend F;
  };
}
