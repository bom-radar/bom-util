/*------------------------------------------------------------------------------
 * Bureau of Meteorology C++ Utility Library
 *
 * Copyright 2014 Mark Curtis
 * Copyright 2016 Commonwealth of Australia, Bureau of Meteorology
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *----------------------------------------------------------------------------*/
#include "config.h"
#include "file_utils.h"
#include "trace.h"
#include "unit_test.h"

#include <sys/socket.h>
#include <alloca.h>
#include <fcntl.h>
#include <utime.h>
#include <algorithm>
#include <cstdio>
#include <fstream>
#include <mutex>
#include <stdexcept>
#include <system_error>

using namespace bom;

/* Thinking of close() as a system call:
 *
 * It can be tempting to check for EINTR from close() and retry.  NEVER EVER EVER DO THIS!  The state of the
 * fd is undefined after EINTR on close() and it may now be invalid or even reallocated under a different
 * thread.  Retrying close() can result in you closing a random file descriptor owned by another thread!
 *
 * In linux close is guaranteed to always release the file descriptor first before it performs the final
 * flush().  If EINTR occurs it will be because the underlying flush() was interrupted which means you may
 * have lost some data.  But since your fd is now release there's nothing you could do about it anyway. */

BOM_DEFINE_ENUM_TRAITS(bom::io_mode);

auto bom::shutdown_close_socket(int fd) -> void
{
  shutdown(fd, SHUT_RDWR);
  close(fd);
}

auto bom::is_absolute_path(char const* path) -> bool
{
  return path[0] == '/';
}

auto bom::stat_path(string const& path, file_options::follow_links follow_links) -> file_metadata
{
  file_metadata ret;
  if (follow_links == file_options::follow_links::yes ? stat(path.c_str(), ret.data()) : lstat(path.c_str(), ret.data()) != 0)
    std::memset(ret.data(), 0, sizeof(struct stat));
  return ret;
}

auto bom::stat_path(dir_stream_hnd const& dir, char const* name, file_options::follow_links follow_links) -> file_metadata
{
  file_metadata ret;
  if (fstatat(dirfd(dir), name, ret.data(), follow_links == file_options::follow_links::yes ? 0 : AT_SYMLINK_NOFOLLOW) != 0)
    std::memset(ret.data(), 0, sizeof(struct stat));
  return ret;
}

// recurse down a path until we find an extant directory then create directories on the way back up
static auto make_directory_recursive(char* path) -> void
{
  struct stat sb;

  // if we can't stat the path assume it doesn't exist yet
  // it may also be an access rights issue, but ignore this and let mkdir fail for more relevant error message
  if (stat(path, &sb) != 0)
  {
    // truncate the end part of our path and recurse to create parent directories as needed
    auto ptr = strrchr(path, '/');
    if (ptr)
    {
      // ensure we ignore repeated '/'s
      while (ptr != path && ptr[-1] == '/')
        --ptr;

      *ptr = '\0';
      make_directory_recursive(path);
      *ptr = '/';
    }

    // parents now exist, so create the directory at this level
    /* if it fails double check presence with stat.  it is possible that the directory was created by another
     * thread between our stat and mkdir calls.  this is inefficient, but rare and not a problem. */
    if (mkdir(path, 0777) != 0)
    {
      auto err = errno;
      if (   err != EEXIST
          || stat(path, &sb) != 0
          || !S_ISDIR(sb.st_mode))
        throw std::system_error{err, std::generic_category(), "mkdir"};
    }
  }
  // if path exists but is not a directory fail out
  else if (!S_ISDIR(sb.st_mode))
    throw std::runtime_error{"component of path is not a directory"};
}

auto bom::make_directory(string const& path, file_options::parents_only parents_only) -> void
try
{
  auto size = path.size();

  // ignore final component if desired
  if (parents_only == file_options::parents_only::yes)
  {
    while (size > 0 && path[size - 1] != '/')
      --size;
  }

  // ignore trailing slashes
  while (size > 0 && path[size - 1] == '/')
    --size;

  // ignore empty path
  if (size == 0)
    return;

  // save a string allocation, potentially trash the stack... hey, it's a low risk trade-off
  char* buffer = static_cast<char*>(alloca(size + 1));
  memcpy(buffer, path.c_str(), size);
  buffer[size] = '\0';

  // recursively create each component
  make_directory_recursive(buffer);
}
catch (...)
{
  std::throw_with_nested(std::runtime_error{fmt::format(
                "make_directory failed: {}{}"
              , path
              , parents_only == file_options::parents_only::yes ? " (parents only)" : "")});
}

auto bom::iterate_directory(
      string const& path
    , std::function<bool(dir_stream_hnd const&, char const*)> const& func
    ) -> bool
{
  // open the directory
  auto dir_iter = dir_stream_hnd{opendir(path.c_str())};
  if (!dir_iter)
    throw std::system_error{errno, std::generic_category(), fmt::format("opendir: {}", path)};

  // iterate over entries
  while (auto entry = (errno = 0, readdir(dir_iter)))
  {
    if (   strcmp(entry->d_name, ".") == 0
        || strcmp(entry->d_name, "..") == 0)
      continue;
    if (!func(dir_iter, entry->d_name))
      return false;
  }
  if (errno != 0)
    throw std::system_error{errno, std::generic_category(), "readdir"};

  return true;
}

auto bom::iterate_directory(
      dir_stream_hnd const& dir
    , char const* name
    , std::function<bool(dir_stream_hnd const&, char const*)> const& func
    ) -> bool
{
  // open the directory
  auto dir_iter = dir_stream_hnd{fdopendir(openat(dirfd(dir), name, O_DIRECTORY))};
  if (!dir_iter)
    throw std::system_error{errno, std::generic_category(), "fdopendir"};

  // iterate over entries
  while (auto entry = (errno = 0, readdir(dir_iter)))
  {
    if (   strcmp(entry->d_name, ".") == 0
        || strcmp(entry->d_name, "..") == 0)
      continue;
    if (!func(dir_iter, entry->d_name))
      return false;
  }
  if (errno != 0)
    throw std::system_error{errno, std::generic_category(), "readdir"};

  return true;
}

static auto delete_directory_recursive(dir_stream_hnd& dir, string const& path) -> void
{
  // get the file descriptor from the directory stream (needed for fstatat)
  int fd = dirfd(dir);
  if (fd == -1)
    throw std::system_error{errno, std::generic_category(), "dirfd"};

  // iterate over each directory entry
  bool unlinked = false;
  struct stat sb;
  while (true)
  {
    // read the next entry
    auto entry = (errno = 0, readdir(dir));
    if (errno != 0)
      throw std::system_error{errno, std::generic_category(), "readdir"};

    // no more entries available
    /* if we have performed any unlinks, then we _must_ rewind and restart the scan.  this is because some
     * files systems (HFS on Mac) do not ensure that readdir will still hit every entry when some are unlinked
     * during traversal. */
    if (entry == nullptr)
    {
      if (unlinked)
      {
        rewinddir(dir);
        unlinked = false;
        continue;
      }
      break;
    }

    // ignore the self and parent entries
    if (strcmp(entry->d_name, ".") == 0 || strcmp(entry->d_name, "..") == 0)
      continue;

    // is the entry a file or directory?
    if (fstatat(fd, entry->d_name, &sb, AT_SYMLINK_NOFOLLOW) != 0)
      throw std::system_error{errno, std::generic_category(), "fstatat"};

    if (S_ISDIR(sb.st_mode))
    {
      auto subdir = dir_stream_hnd{fdopendir(openat(fd, entry->d_name, O_DIRECTORY))};
      delete_directory_recursive(subdir, path);
      if (unlinkat(fd, entry->d_name, AT_REMOVEDIR) != 0)
        throw std::system_error{errno, std::generic_category(), "unlinkat (dir)"};
    }
    else
    {
      if (unlinkat(fd, entry->d_name, 0) != 0)
        throw std::system_error{errno, std::generic_category(), "unlinkat (file)"};
    }
  }
}

auto bom::delete_directory(string const& path, file_options::recursive recursive) -> void
try
{
  if (recursive == file_options::recursive::yes)
  {
    auto subdir = dir_stream_hnd{opendir(path.c_str())};
    if (!subdir)
      throw std::system_error{errno, std::generic_category(), "opendir"};
    delete_directory_recursive(subdir, path);
  }

  if (unlinkat(AT_FDCWD, path.c_str(), AT_REMOVEDIR) != 0)
    throw std::system_error{errno, std::generic_category(), "unlinkat (dir)"};
}
catch (...)
{
  std::throw_with_nested(std::runtime_error{fmt::format(
                "delete_directory failed: {}{}"
              , path
              , recursive == file_options::recursive::yes ? " (recursive)" : "")});
}

auto bom::delete_directory(dir_stream_hnd const& dir, char const* name, file_options::recursive recursive) -> void
try
{
  auto fd = dirfd(dir);

  if (recursive == file_options::recursive::yes)
  {
    auto subdir = dir_stream_hnd{fdopendir(openat(fd, name, O_DIRECTORY))};
    if (!subdir)
      throw std::system_error{errno, std::generic_category(), "fdopendir"};
    delete_directory_recursive(subdir, name);
  }

  if (unlinkat(fd, name, AT_REMOVEDIR) != 0)
    throw std::system_error{errno, std::generic_category(), "unlinkat (dir)"};
}
catch (...)
{
  std::throw_with_nested(std::runtime_error{fmt::format(
                "delete_directory failed: <dir>/{}{}"
              , name
              , recursive == file_options::recursive::yes ? " (recursive)" : "")});
}

auto bom::copy_file(string const& source, string const& destination) -> void
try
{
  // open the source file
  file_desc_hnd fdsrc{open(source.c_str(), O_RDONLY)};
  if (!fdsrc)
    throw std::system_error{errno, std::generic_category(), "open (src)"};

  // determine source permissions because we attempt to preserve them in the new file (umask permitting)
  file_metadata md;
  if (fstat(fdsrc, md.data()) != 0)
    throw std::system_error{errno, std::generic_category(), "fstat"};

  // ensure that we are attempting to copy a plain file
  if (!md.is_file())
    throw std::runtime_error{"source not a regular file"};

  // open the file
  file_desc_hnd fddst{open(destination.c_str(), O_CREAT | O_WRONLY | O_TRUNC, md.data()->st_mode)};
  if (!fddst)
    throw std::system_error{errno, std::generic_category(), "open (dst)"};

  // copy data from source to destination in large (hopefully optimal) chunks
  // note: we use 64K chunks here as it seems to be the best length
  constexpr size_t buffer_size = 64 * 1024;
  std::unique_ptr<char[]> buf{new char[buffer_size]};
  while (auto read_size = read(fdsrc, buf.get(), buffer_size))
  {
    // if read failed due to signal interruption try again, otherwise throw
    if (read_size < 0)
    {
      if (errno == EINTR)
        continue;
      throw std::system_error{errno, std::generic_category(), "read"};
    }

    // write the block out
    decltype(read_size) write_size = 0;
    while (write_size < read_size)
    {
      auto size = write(fddst, buf.get() + write_size, read_size - write_size);
      if (size < 0)
      {
        if (errno == EINTR)
          continue;
        throw std::system_error{errno, std::generic_category(), "write"};
      }
      write_size += size;
    }
  }
}
catch (...)
{
  std::throw_with_nested(std::runtime_error{fmt::format("copy_file failed: {} to {}", source, destination)});
}

auto bom::move_file(string const& source, string const& destination, file_options::require_atomic require_atomic) -> void
try
{
  // try to perform a simple atomic rename to move the file
  if (rename(source.c_str(), destination.c_str()) == 0)
    return;

  // if we fail for any reason other than files being on different file systems propagate the error as an exception
  if (errno != EXDEV)
    throw std::system_error{errno, std::generic_category(), "rename"};

  if (require_atomic == file_options::require_atomic::yes)
    throw std::runtime_error{"cannot atomically move file (different file system)"};

  // attempt to copy the source to destination
  copy_file(source, destination);

  // and now remove the source
  if (unlink(source.c_str()) != 0)
    throw std::system_error{errno, std::generic_category(), "unlink"};
}
catch (...)
{
  std::throw_with_nested(std::runtime_error{fmt::format("move_file failed: {} to {}", source, destination)});
}

auto bom::delete_file(string const& path) -> void
try
{
  if (unlinkat(AT_FDCWD, path.c_str(), 0) != 0)
    throw std::system_error{errno, std::generic_category(), "unlinkat (file)"};
}
catch (...)
{
  std::throw_with_nested(std::runtime_error{fmt::format("delete_file failed: {}", path)});
}

auto bom::delete_file(dir_stream_hnd const& dir, char const* name) -> void
try
{
  if (unlinkat(dirfd(dir), name, 0) != 0)
    throw std::system_error{errno, std::generic_category(), "unlinkat (file)"};
}
catch (...)
{
  std::throw_with_nested(std::runtime_error{fmt::format("delete_file failed: <dir>/{}", name)});
}

auto bom::update_file_modification_time(string const& path) -> void
{
  if (utime(path.c_str(), nullptr) != 0)
  {
    std::error_code ec{errno, std::generic_category()};
    throw std::system_error{ec, fmt::format("update_file_modification time: {}", path)};
  }
}

/// \todo Fix slow method of reading a file into a string
auto bom::read_file_into_string(string const& path) -> string
{
  std::ifstream ifs(path);
  if (ifs.fail())
    throw std::runtime_error{"failed to open file: " + path};
  return {std::istreambuf_iterator<char>(ifs.rdbuf()), std::istreambuf_iterator<char>()};
}

/// \todo Fix slow method of reading a file into a string
auto bom::read_file_into_vector(string const& path) -> vector<char>
{
  std::ifstream ifs(path);
  if (ifs.fail())
    throw std::runtime_error{"failed to open file: " + path};
  return {std::istreambuf_iterator<char>(ifs.rdbuf()), std::istreambuf_iterator<char>()};
}

auto bom::write_file_from_vector(string const& path, vector<char> const& data) -> void
{
  std::ofstream ofs(path);
  if (ofs.fail())
    throw std::runtime_error{"failed to open file: " + path};
  ofs.write(data.data(), data.size());
}

static string const default_temporary_file_base = "tmp";

temporary_file::temporary_file()
  : temporary_file{default_temporary_file_base}
{ }

temporary_file::temporary_file(string const& base)
{
  // build the local path template
  char* path = static_cast<char*>(alloca(base.size() + 7 + 1));
  strcpy(path, base.c_str());
  strcpy(path + base.size(), ".XXXXXX");

  // generate a unique temporary path
  file_desc_hnd fd{mkstemp(path)};
  if (!fd)
    throw std::system_error{errno, std::generic_category(), "mkstemp"};

  // sucess, so assign the path
  path_.assign(path, base.size() + 7);
}

temporary_file::temporary_file(temporary_file&& rhs) noexcept
  : path_(std::move(rhs.path_))
{
  rhs.path_.clear();
}

auto temporary_file::operator=(temporary_file&& rhs) noexcept -> temporary_file&
{
  if (&rhs != this)
  {
    try
    {
      reset();
    }
    catch (std::system_error& err)
    {
      trace::error("failed to remove temporary file {}: {}", path_, format_exception(err));
    }

    path_ = std::move(rhs.path_);
    rhs.path_.clear();
  }
  return *this;
}

temporary_file::~temporary_file()
{
  try
  {
    reset();
  }
  catch (std::system_error& err)
  {
    trace::error("failed to remove temporary file {}: {}", path_, format_exception(err));
  }
}

auto temporary_file::reset() -> void
{
  if (!path_.empty())
  {
    /* TODO is there a bug here if:
     * - the temporary file is created using a relative path
     * - the process changes working directory while the temporary file is 'alive' */
    if (unlink(path_.c_str()) != 0)
      throw std::system_error{errno, std::generic_category(), "unlink"};
    path_.clear();
  }
}

auto temporary_file::release() -> void
{
  path_.clear();
}

// LCOV_EXCL_START
TEST_CASE("file_utils")
{
  vector<char> dat(5 * 64 * 1024);
  for (size_t i = 0; i < dat.size(); ++i)
    dat[i] = i % 256;

  CHECK(is_absolute_path("foo/bar") == false);
  CHECK(is_absolute_path("/foo/bar") == true);

  // create new, existing/new, existing/new/new
  CHECK_NOTHROW(make_directory("foo"));
  CHECK_NOTHROW(make_directory("foo/bar/file", file_options::parents_only::yes));

  // create a file
  CHECK_THROWS(write_file_from_vector("bad/bad/file", dat));
  CHECK_NOTHROW(write_file_from_vector("foo/bar/file", dat));

  // another directory as a move target
  CHECK_THROWS(make_directory("foo/bar/file"));
  CHECK_NOTHROW(make_directory("foo//abc/def/"));

  // move it
  CHECK_THROWS(move_file("foo/bar/file", "bad/bad/file", file_options::require_atomic::no));
  CHECK_NOTHROW(move_file("foo/bar/file", "foo/abc/file", file_options::require_atomic::no));

  // copy it
  // TODO do we check the return of the write open in copy_file????
  CHECK_THROWS(copy_file("bad/abc/file", "bad/bad/file"));
  CHECK_THROWS(copy_file("foo/abc/file", "bad/bad/file"));
  CHECK_THROWS(copy_file("foo/abc", "bad/bad/file"));
  CHECK_NOTHROW(copy_file("foo/abc/file", "foo/bar/copy"));
  CHECK_NOTHROW(copy_file("foo/abc/file", "foo/bar/zzzz"));

  // read it back in as a vector
  vector<char> buf1;
  CHECK_THROWS(buf1 = read_file_into_vector("bad/bad/copy"));
  CHECK_NOTHROW(buf1 = read_file_into_vector("foo/bar/copy"));
  CHECK(buf1 == dat);

  // read it back in as a string
  string buf2;
  CHECK_THROWS(buf2 = read_file_into_string("bad/bad/copy"));
  CHECK_NOTHROW(buf2 = read_file_into_string("foo/bar/copy"));
  CHECK(buf2.size() == dat.size());
  auto compare_buf2 = [&]()
  {
    for (size_t i = 0; i < dat.size(); ++i)
      if (buf2[i] != dat[i])
        return false;
    return true;
  };
  CHECK(compare_buf2());

  // try updating modification time
  CHECK_NOTHROW(update_file_modification_time("foo/bar/copy"));

  // iterate over directory to get list of files
  vector<string> items;
  auto store_name = [&](dir_stream_hnd const&, char const* name) -> bool
  {
    items.emplace_back(name);
    return true;
  };
  CHECK_NOTHROW(iterate_directory("foo/bar", store_name));
  std::sort(items.begin(), items.end());
  CHECK(items.size() == 2);
  CHECK(items[0] == "copy");
  CHECK(items[1] == "zzzz");

  // delete one of the files
  CHECK_THROWS(delete_file("bad/bad/file"));
  CHECK_NOTHROW(delete_file("foo/bar/copy"));

  // create a temporary file
  temporary_file t1;
  CHECK(stat_path(t1.get()));

  temporary_file t2{"foo/tmp"};
  CHECK(t2.get().compare(0, 7, "foo/tmp") == 0);

  temporary_file t3{std::move(t2)};
  CHECK(t2.get().empty());

  t3 = std::move(t1);
  CHECK(t1.get().empty());

  auto path = t3.get();
  t3.release();
  CHECK(stat_path(path));
  CHECK_NOTHROW(delete_file(path));

  temporary_file t4;
  path = t4.get();
  CHECK(stat_path(path));
  CHECK_NOTHROW(t4.reset());
  CHECK(!stat_path(path));
  CHECK(t4.get().empty());

  // delete the directory
  CHECK_THROWS(delete_directory("bad/bad", file_options::recursive::no));
  CHECK_THROWS(delete_directory("bad/bad", file_options::recursive::yes));
  CHECK_NOTHROW(delete_directory("foo", file_options::recursive::yes));
}
// LCOV_EXCL_STOP
