/*------------------------------------------------------------------------------
 * Bureau of Meteorology C++ Utility Library
 *
 * Copyright 2014 Mark Curtis
 * Copyright 2016 Commonwealth of Australia, Bureau of Meteorology
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *----------------------------------------------------------------------------*/
#pragma once

#include "angle.h"
#include "vec4.h"

namespace bom
{
  template <typename T>
  struct mat4
  {
    static_assert(std::is_floating_point<T>::value, "mat4 requires floating point type parameter");

    using value_type = T;

    T m[4][4];

    mat4() noexcept = default;
    constexpr mat4(mat4 const& rhs) = default;
    constexpr mat4(mat4&& rhs) noexcept = default;
    auto operator=(mat4 const& rhs) -> mat4& = default;
    auto operator=(mat4&& rhs) noexcept -> mat4& = default;
    ~mat4() noexcept = default;

    operator T const*() const
    {
      return &m[0][0];
    }
    operator T*()
    {
      return &m[0][0];
    }

    auto operator[](size_t i) -> T*
    {
      return m[i];
    }
    auto operator[](size_t i) const -> T const*
    {
      return m[i];
    }

    auto set_inverse_of(mat4 const& m) -> bool;

    // temp - may move these out later on...
    auto set_projection_orthographic(T left, T right, T bottom, T top, T near, T far) -> mat4&;
    auto set_projection_perspective(angle fovy, T aspect_ratio, T near, T far) -> mat4&;
  };

  template <typename T>
  auto operator*(mat4<T> const& lhs, mat4<T> const& rhs) -> mat4<T>
  {
    mat4<T> m;
    m[0][0] = lhs[0][0] * rhs[0][0] + lhs[1][0] * rhs[0][1] + lhs[2][0] * rhs[0][2] + lhs[3][0] * rhs[0][3];
    m[0][1] = lhs[0][1] * rhs[0][0] + lhs[1][1] * rhs[0][1] + lhs[2][1] * rhs[0][2] + lhs[3][1] * rhs[0][3];
    m[0][2] = lhs[0][2] * rhs[0][0] + lhs[1][2] * rhs[0][1] + lhs[2][2] * rhs[0][2] + lhs[3][2] * rhs[0][3];
    m[0][3] = lhs[0][3] * rhs[0][0] + lhs[1][3] * rhs[0][1] + lhs[2][3] * rhs[0][2] + lhs[3][3] * rhs[0][3];
    m[1][0] = lhs[0][0] * rhs[1][0] + lhs[1][0] * rhs[1][1] + lhs[2][0] * rhs[1][2] + lhs[3][0] * rhs[1][3];
    m[1][1] = lhs[0][1] * rhs[1][0] + lhs[1][1] * rhs[1][1] + lhs[2][1] * rhs[1][2] + lhs[3][1] * rhs[1][3];
    m[1][2] = lhs[0][2] * rhs[1][0] + lhs[1][2] * rhs[1][1] + lhs[2][2] * rhs[1][2] + lhs[3][2] * rhs[1][3];
    m[1][3] = lhs[0][3] * rhs[1][0] + lhs[1][3] * rhs[1][1] + lhs[2][3] * rhs[1][2] + lhs[3][3] * rhs[1][3];
    m[2][0] = lhs[0][0] * rhs[2][0] + lhs[1][0] * rhs[2][1] + lhs[2][0] * rhs[2][2] + lhs[3][0] * rhs[2][3];
    m[2][1] = lhs[0][1] * rhs[2][0] + lhs[1][1] * rhs[2][1] + lhs[2][1] * rhs[2][2] + lhs[3][1] * rhs[2][3];
    m[2][2] = lhs[0][2] * rhs[2][0] + lhs[1][2] * rhs[2][1] + lhs[2][2] * rhs[2][2] + lhs[3][2] * rhs[2][3];
    m[2][3] = lhs[0][3] * rhs[2][0] + lhs[1][3] * rhs[2][1] + lhs[2][3] * rhs[2][2] + lhs[3][3] * rhs[2][3];
    m[3][0] = lhs[0][0] * rhs[3][0] + lhs[1][0] * rhs[3][1] + lhs[2][0] * rhs[3][2] + lhs[3][0] * rhs[3][3];
    m[3][1] = lhs[0][1] * rhs[3][0] + lhs[1][1] * rhs[3][1] + lhs[2][1] * rhs[3][2] + lhs[3][1] * rhs[3][3];
    m[3][2] = lhs[0][2] * rhs[3][0] + lhs[1][2] * rhs[3][1] + lhs[2][2] * rhs[3][2] + lhs[3][2] * rhs[3][3];
    m[3][3] = lhs[0][3] * rhs[3][0] + lhs[1][3] * rhs[3][1] + lhs[2][3] * rhs[3][2] + lhs[3][3] * rhs[3][3];
    return m;
  }

  template <typename T>
  auto operator*(mat4<T> const& lhs, vec4<T> const& rhs) -> vec4<T>
  {
    return
    {
        lhs[0][0] * rhs.x + lhs[1][0] * rhs.y + lhs[2][0] * rhs.z + lhs[3][0] * rhs.w
      , lhs[0][1] * rhs.x + lhs[1][1] * rhs.y + lhs[2][1] * rhs.z + lhs[3][1] * rhs.w
      , lhs[0][2] * rhs.x + lhs[1][2] * rhs.y + lhs[2][2] * rhs.z + lhs[3][2] * rhs.w
      , lhs[0][3] * rhs.x + lhs[1][3] * rhs.y + lhs[2][3] * rhs.z + lhs[3][3] * rhs.w
    };
  }

  using mat4f = mat4<float>;
  using mat4d = mat4<double>;

  template <typename T>
  auto mat4<T>::set_inverse_of(mat4 const& v) -> bool
  {
    m[0][0] = v[1][1] * v[2][2] * v[3][3] - 
              v[1][1] * v[2][3] * v[3][2] - 
              v[2][1] * v[1][2] * v[3][3] + 
              v[2][1] * v[1][3] * v[3][2] +
              v[3][1] * v[1][2] * v[2][3] - 
              v[3][1] * v[1][3] * v[2][2];

    m[1][0] = -v[1][0] * v[2][2] * v[3][3] + 
              v[1][0] * v[2][3] * v[3][2] + 
              v[2][0] * v[1][2] * v[3][3] - 
              v[2][0] * v[1][3] * v[3][2] - 
              v[3][0] * v[1][2] * v[2][3] + 
              v[3][0] * v[1][3] * v[2][2];

    m[2][0] = v[1][0] * v[2][1] * v[3][3] - 
              v[1][0] * v[2][3] * v[3][1] - 
              v[2][0] * v[1][1] * v[3][3] + 
              v[2][0] * v[1][3] * v[3][1] + 
              v[3][0] * v[1][1] * v[2][3] - 
              v[3][0] * v[1][3] * v[2][1];

    m[3][0] = -v[1][0] * v[2][1] * v[3][2] + 
              v[1][0] * v[2][2] * v[3][1] +
              v[2][0] * v[1][1] * v[3][2] - 
              v[2][0] * v[1][2] * v[3][1] - 
              v[3][0] * v[1][1] * v[2][2] + 
              v[3][0] * v[1][2] * v[2][1];

    m[0][1] = -v[0][1] * v[2][2] * v[3][3] + 
              v[0][1] * v[2][3] * v[3][2] + 
              v[2][1] * v[0][2] * v[3][3] - 
              v[2][1] * v[0][3] * v[3][2] - 
              v[3][1] * v[0][2] * v[2][3] + 
              v[3][1] * v[0][3] * v[2][2];

    m[1][1] = v[0][0] * v[2][2] * v[3][3] - 
              v[0][0] * v[2][3] * v[3][2] - 
              v[2][0] * v[0][2] * v[3][3] + 
              v[2][0] * v[0][3] * v[3][2] + 
              v[3][0] * v[0][2] * v[2][3] - 
              v[3][0] * v[0][3] * v[2][2];

    m[2][1] = -v[0][0] * v[2][1] * v[3][3] + 
              v[0][0] * v[2][3] * v[3][1] + 
              v[2][0] * v[0][1] * v[3][3] - 
              v[2][0] * v[0][3] * v[3][1] - 
              v[3][0] * v[0][1] * v[2][3] + 
              v[3][0] * v[0][3] * v[2][1];

    m[3][1] = v[0][0] * v[2][1] * v[3][2] - 
              v[0][0] * v[2][2] * v[3][1] - 
              v[2][0] * v[0][1] * v[3][2] + 
              v[2][0] * v[0][2] * v[3][1] + 
              v[3][0] * v[0][1] * v[2][2] - 
              v[3][0] * v[0][2] * v[2][1];

    m[0][2] = v[0][1] * v[1][2] * v[3][3] - 
              v[0][1] * v[1][3] * v[3][2] - 
              v[1][1] * v[0][2] * v[3][3] + 
              v[1][1] * v[0][3] * v[3][2] + 
              v[3][1] * v[0][2] * v[1][3] - 
              v[3][1] * v[0][3] * v[1][2];

    m[1][2] = -v[0][0] * v[1][2] * v[3][3] + 
              v[0][0] * v[1][3] * v[3][2] + 
              v[1][0] * v[0][2] * v[3][3] - 
              v[1][0] * v[0][3] * v[3][2] - 
              v[3][0] * v[0][2] * v[1][3] + 
              v[3][0] * v[0][3] * v[1][2];

    m[2][2] = v[0][0] * v[1][1] * v[3][3] - 
              v[0][0] * v[1][3] * v[3][1] - 
              v[1][0] * v[0][1] * v[3][3] + 
              v[1][0] * v[0][3] * v[3][1] + 
              v[3][0] * v[0][1] * v[1][3] - 
              v[3][0] * v[0][3] * v[1][1];

    m[3][2] = -v[0][0] * v[1][1] * v[3][2] + 
              v[0][0] * v[1][2] * v[3][1] + 
              v[1][0] * v[0][1] * v[3][2] - 
              v[1][0] * v[0][2] * v[3][1] - 
              v[3][0] * v[0][1] * v[1][2] + 
              v[3][0] * v[0][2] * v[1][1];

    m[0][3] = -v[0][1] * v[1][2] * v[2][3] + 
              v[0][1] * v[1][3] * v[2][2] + 
              v[1][1] * v[0][2] * v[2][3] - 
              v[1][1] * v[0][3] * v[2][2] - 
              v[2][1] * v[0][2] * v[1][3] + 
              v[2][1] * v[0][3] * v[1][2];

    m[1][3] = v[0][0] * v[1][2] * v[2][3] - 
              v[0][0] * v[1][3] * v[2][2] - 
              v[1][0] * v[0][2] * v[2][3] + 
              v[1][0] * v[0][3] * v[2][2] + 
              v[2][0] * v[0][2] * v[1][3] - 
              v[2][0] * v[0][3] * v[1][2];

    m[2][3] = -v[0][0] * v[1][1] * v[2][3] + 
              v[0][0] * v[1][3] * v[2][1] + 
              v[1][0] * v[0][1] * v[2][3] - 
              v[1][0] * v[0][3] * v[2][1] - 
              v[2][0] * v[0][1] * v[1][3] + 
              v[2][0] * v[0][3] * v[1][1];

    m[3][3] = v[0][0] * v[1][1] * v[2][2] - 
              v[0][0] * v[1][2] * v[2][1] - 
              v[1][0] * v[0][1] * v[2][2] + 
              v[1][0] * v[0][2] * v[2][1] + 
              v[2][0] * v[0][1] * v[1][2] - 
              v[2][0] * v[0][2] * v[1][1];

    auto det = v[0][0] * m[0][0] + v[0][1] * m[1][0] + v[0][2] * m[2][0] + v[0][3] * m[3][0];
    if (det == 0)
      return false;

    det = 1 / det;
    for (size_t i = 0; i < 4; i++)
      for (size_t j = 0; j < 4; j++)
        m[i][j] *= det;

    return true;
  }

  template <typename T>
  auto mat4<T>::set_projection_orthographic(T left, T right, T bottom, T top, T near, T far) -> mat4&
  {
    m[0][0] = T(2.0) / (right - left);
    m[0][1] = 0.0;
    m[0][2] = 0.0;
    m[0][3] = 0.0;

    m[1][0] = 0.0;
    m[1][1] = T(2.0) / (top - bottom);
    m[1][2] = 0.0;
    m[1][3] = 0.0;

    m[2][0] = 0.0;
    m[2][1] = 0.0;
    m[2][2] = T(2.0) / (near - far);
    m[2][3] = 0.0;

    m[3][0] = (right + left) / (left - right);
    m[3][1] = (top + bottom) / (bottom - top);
    m[3][2] = (far + near) / (near - far);
    m[3][3] = 1.0;

    return *this;
  }

  template <typename T>
  auto mat4<T>::set_projection_perspective(angle fovy, T aspect_ratio, T near, T far) -> mat4&
  {
    T f = 1.0 / tan(fovy * 0.5);

    m[0][0] = f / aspect_ratio;
    m[0][1] = 0.0;
    m[0][2] = 0.0;
    m[0][3] = 0.0;

    m[1][0] = 0.0;
    m[1][1] = f;
    m[1][2] = 0.0;
    m[1][3] = 0.0;

    m[2][0] = 0.0;
    m[2][1] = 0.0;
    m[2][2] = (far + near) / (near - far);
    m[2][3] = -1.0;

    m[3][0] = 0.0;
    m[3][1] = 0.0;
    m[3][2] = (T(2.0) * far * near) / (near - far);
    m[3][3] = 0.0;

    return *this;
  }
}
