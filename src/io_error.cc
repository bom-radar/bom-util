/*------------------------------------------------------------------------------
 * Bureau of Meteorology C++ Utility Library
 *
 * Copyright 2014 Mark Curtis
 * Copyright 2016 Commonwealth of Australia, Bureau of Meteorology
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *----------------------------------------------------------------------------*/
#include "config.h"
#include "io_error.h"
#include "unit_test.h"

using namespace bom;

io_error::io_error(string desc, string path)
  : desc_{std::move(desc)}
  , path_{std::move(path)}
{ }

auto io_error::what() const noexcept -> char const*
{
  try
  {
    if (path_.empty())
      what_ = fmt::format("io error: {}", desc_);
    else
      what_ = fmt::format("io error at path {}: {}", path_, desc_);
    return what_.c_str();
  }
  catch (...)
  {
    return "io error";
  }
}

// LCOV_EXCL_START
TEST_CASE("io_error")
{
  io_error err1{"description"};
  CHECK(string(err1.what()) == "io error: description");
  CHECK(err1.description() == "description");
  CHECK(err1.path().empty());

  io_error err2{"description", "path"};
  CHECK(string(err2.what()) == "io error at path path: description");
  CHECK(err2.description() == "description");
  CHECK(err2.path() == "path");

  err2.set_path("new path");
  CHECK(string(err2.what()) == "io error at path new path: description");
  CHECK(err2.path() == "new path");
}
// LCOV_EXCL_STOP
