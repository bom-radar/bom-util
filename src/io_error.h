/*------------------------------------------------------------------------------
 * Bureau of Meteorology C++ Utility Library
 *
 * Copyright 2014 Mark Curtis
 * Copyright 2016 Commonwealth of Australia, Bureau of Meteorology
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *----------------------------------------------------------------------------*/
#pragma once

#include "string_utils.h"
#include <exception>

namespace bom
{
  /// Generic I/O error
  class io_error : public std::exception
  {
  public:
    /// Create an I/O error
    io_error(string desc, string path = {});

    /// Get the full error detail string
    /** Note: this function is currently NOT thread safe.  Do not call what() on the same exception from multiple
     *  threads at the same time.  (Almost impossible to do accidentally anyway) */
    virtual auto what() const noexcept -> char const*;

    /// Get the error description
    auto description() const -> string const&
    {
      return desc_;
    }

    /// Get the file on which the I/O error occurred
    auto path() const -> string const&
    {
      return path_;
    }

    /// Set the path for which the I/O error occurred
    auto set_path(string const& path) -> void
    {
      path_ = path;
    }

  private:
    string  desc_;
    string  path_;
    mutable string what_;
  };
}
