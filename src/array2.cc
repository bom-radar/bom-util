/*------------------------------------------------------------------------------
 * Bureau of Meteorology C++ Utility Library
 *
 * Copyright 2014 Mark Curtis
 * Copyright 2016 Commonwealth of Australia, Bureau of Meteorology
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *----------------------------------------------------------------------------*/
#include "config.h"
#include "array2.h"
#include "unit_test.h"

using namespace bom;

// LCOV_EXCL_START
TEST_CASE("array2")
{
  SUBCASE("basic")
  {
    array2z array;

    CHECK(array.size() == 0);
    CHECK(array.extents() == vec2z{0, 0});
    array.resize((vec2z{10, 10}));
    CHECK(array.size() == 100);
    CHECK(array.extents() == vec2z{10, 10});
    CHECK(array.data() == &array[0][0]);
    CHECK(array.begin() == &array[0][0]);
    CHECK(array.end() == &array[9][10]);

    array.fill(0);
    for (auto& val : array)
      CHECK(val == 0);

    array.fill(7);
    for (auto& val : array)
      CHECK(val == 7);

    array2z copy{array};
    for (auto& val : copy)
      CHECK(val == 7);

    auto ptr = array.data();
    array2z move{std::move(array)};
    CHECK(move.data() == ptr);
  }
}
// LCOV_EXCL_STOP
