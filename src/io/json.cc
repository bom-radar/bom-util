/*------------------------------------------------------------------------------
 * Bureau of Meteorology C++ Utility Library
 *
 * Copyright 2014 Mark Curtis
 * Copyright 2016 Commonwealth of Australia, Bureau of Meteorology
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *----------------------------------------------------------------------------*/
#include "config.h"
#include "json.h"
#include "../parse_error.h"
#include "../unit_test.h"
#include <stdexcept>

using namespace bom;
using namespace bom::io;

static inline auto skip_whitespace(std::istream& in) -> void
{
  while (std::isspace(in.peek()))
    in.ignore();
}

static auto parse_string(std::istream& in) -> json::string_type
{
  if (in.get() != '"')
    throw parse_error{"expected '\"'", in};

  json::string_type ret;
  for (char cur = in.get(); cur != '"'; cur = in.get())
  {
    if (cur == std::char_traits<char>::eof())
      throw parse_error{"unterminated string", in};

    if (cur == '\\')
    {
      switch (cur = in.get())
      {
      case '"': ret.push_back('\"'); break;
      case '\\': ret.push_back('\\'); break;
      case '/': ret.push_back('/'); break;
      case 'b': ret.push_back('\b'); break;
      case 'f': ret.push_back('\f'); break;
      case 'n': ret.push_back('\n'); break;
      case 'r': ret.push_back('\r'); break;
      case 't': ret.push_back('\t'); break;
      case 'u':
        throw parse_error{"unicode escape sequences unsupported", in};
      default:
        throw parse_error{"unknown escape sequence", in};
      }
    }
    else
      ret.push_back(cur);
  }
  return ret;
}

json::json()
  : type_{type::null}
{ }

json::json(double val)
  : type_{type::number}
  , value_number_{val}
{ }

json::json(string_type val)
  : type_{type::string}
{
  new (&value_string_) string_type{std::move(val)};
}

json::json(bool val)
  : type_{type::boolean}
  , value_boolean_{val}
{ }

json::json(array_type val)
  : type_{type::array}
{
  new (&value_array_) array_type{std::move(val)};
}

json::json(object_type val)
  : type_{type::object}
{
  new (&value_object_) object_type{std::move(val)};
}

json::json(std::istream& in)
{
  skip_whitespace(in);

  auto next = in.peek();
  if (next == '{')
  {
    type_ = type::object;
    new (&value_object_) object_type();
    in.ignore();
    skip_whitespace(in);
    if (in.peek() != '}')
    {
      while (true)
      {
        auto key(parse_string(in));
        skip_whitespace(in);
        if (in.get() != ':')
          throw parse_error{"expected ':'", in};
        value_object_.emplace_back(std::move(key), json{in});
        skip_whitespace(in);
        next = in.peek();
        if (next == '}')
          break;
        if (next != ',')
          throw parse_error{"expected '}' or ','", in};
        in.ignore();
        skip_whitespace(in);
      }
    }
    in.ignore();
  }
  else if (next == '[')
  {
    type_ = type::array;
    new (&value_array_) array_type();
    in.ignore();
    skip_whitespace(in);
    if (in.peek() != ']')
    {
      while (true)
      {
        value_array_.emplace_back(in);
        skip_whitespace(in);
        next = in.peek();
        if (next == ']')
          break;
        if (next != ',')
          throw parse_error{"expected ','", in};
        in.ignore();
      }
    }
    in.ignore();
  }
  else if (next == '"')
  {
    type_ = type::string;
    new (&value_string_) string_type(parse_string(in));
  }
  else if (next == 't')
  {
    if (in.get() != 't' || in.get() != 'r' || in.get() != 'u' || in.get() != 'e')
      throw parse_error{"unexpected value token", in};
    type_ = type::boolean;
    value_boolean_ = true;
  }
  else if (next == 'f')
  {
    if (in.get() != 'f' || in.get() != 'a' || in.get() != 'l' || in.get() != 's' || in.get() != 'e')
      throw parse_error{"unexpected value token", in};
    type_ = type::boolean;
    value_boolean_ = false;
  }
  else if (next == 'n')
  {
    if (in.get() != 'n' || in.get() != 'u' || in.get() != 'l' || in.get() != 'l')
      throw parse_error{"unexpected value token", in};
    type_ = type::null;
  }
  else if (next == '-' || std::isdigit(next))
  {
    type_ = type::number;
    in >> value_number_;
  }
  else
    throw parse_error{"unexpected value token", in};
}

json::json(json const& rhs)
  : type_{rhs.type_}
{
  switch (type_)
  {
  case type::number:
    value_number_ = rhs.value_number_;
    break;
  case type::string:
    new (&value_string_) string_type(rhs.value_string_);
    break;
  case type::boolean:
    value_boolean_ = rhs.value_boolean_;
    break;
  case type::array:
    new (&value_array_) array_type(rhs.value_array_);
    break;
  case type::object:
    new (&value_object_) object_type(rhs.value_object_);
    break;
  case type::null:
    break;
  }
}

json::json(json&& rhs) noexcept
  : type_{rhs.type_}
{
  switch (type_)
  {
  case type::number:
    value_number_ = rhs.value_number_;
    break;
  case type::string:
    new (&value_string_) string_type(std::move(rhs.value_string_));
    break;
  case type::boolean:
    value_boolean_ = rhs.value_boolean_;
    break;
  case type::array:
    new (&value_array_) array_type(std::move(rhs.value_array_));
    break;
  case type::object:
    new (&value_object_) object_type(std::move(rhs.value_object_));
    break;
  case type::null:
    break;
  }
}

json::~json()
{
  clear();
}

auto json::operator=(json const& rhs) -> json&
{
  clear();
  switch (rhs.type_)
  {
  case type::number:
    value_number_ = rhs.value_number_;
    break;
  case type::string:
    new (&value_string_) string_type(rhs.value_string_);
    break;
  case type::boolean:
    value_boolean_ = rhs.value_boolean_;
    break;
  case type::array:
    new (&value_array_) array_type(rhs.value_array_);
    break;
  case type::object:
    new (&value_object_) object_type(rhs.value_object_);
    break;
  case type::null:
    break;
  }
  type_ = rhs.type_;
  return *this;
}

auto json::operator=(json&& rhs) noexcept -> json&
{
  clear();
  switch (rhs.type_)
  {
  case type::number:
    value_number_ = rhs.value_number_;
    break;
  case type::string:
    new (&value_string_) string_type(std::move(rhs.value_string_));
    break;
  case type::boolean:
    value_boolean_ = rhs.value_boolean_;
    break;
  case type::array:
    new (&value_array_) array_type(std::move(rhs.value_array_));
    break;
  case type::object:
    new (&value_object_) object_type(std::move(rhs.value_object_));
    break;
  case type::null:
    break;
  }
  type_ = rhs.type_;
  return *this;
}

auto json::write(std::ostream& out, int indent) const -> void
{
  switch (type_)
  {
  case type::number:
    out << value_number_;
    break;
  case type::string:
    // TODO - string escaping
    out << '\"' << value_string_ << '\"';
    break;
  case type::boolean:
    out << (value_boolean_ ? "true" : "false");
    break;
  case type::array:
    if (value_array_.empty())
      out << "[]";
    else
    {
      out << '[';
      value_array_.front().write(out, indent);
      for (auto i = value_array_.begin() + 1; i != value_array_.end(); ++i)
      {
        out << ", ";
        i->write(out, indent);
      }
      out << ']';
    }
    break;
  case type::object:
    if (value_object_.empty())
      out << "{}";
    else
    {
      // TODO - string escaping
      out << "{\n";
      for (int i = 0; i < indent + 1; ++i)
        out << ' ' ;
      out << "\"" << value_object_.front().first << "\": ";
      value_object_.front().second.write(out, indent + 1);
      for (auto i = value_object_.begin() + 1; i != value_object_.end(); ++i)
      {
        out << ",\n";
        for (int j = 0; j < indent + 1; ++j)
          out << ' ';
        out << '\"' << i->first << "\": ";
        i->second.write(out, indent + 1);
      }
      out << '\n';
      for (int i = 0; i < indent; ++i)
        out << ' ' ;
      out << '}';
    }
    break;
  case type::null:
    out << "null";
    break;
  }
}

auto json::clear() -> void
{
  switch (type_)
  {
  case type::number:
    break;
  case type::string:
    value_string_.~basic_string();
    break;
  case type::boolean:
    break;
  case type::array:
    value_array_.~vector();
    break;
  case type::object:
    value_object_.~vector();
    break;
  case type::null:
    break;
  }
  type_ = type::null;
}

auto json::size() const -> size_t
{
  if (type_ == type::array)
    return value_array_.size();
  if (type_ == type::object)
    return value_object_.size();
  throw std::runtime_error{"TOOD"};
}

auto json::number() const -> double
{
  if (type_ == type::number)
    return value_number_;
  throw std::runtime_error{"TODO"};
}

auto json::string() const -> string_type const&
{
  if (type_ == type::string)
    return value_string_;
  throw std::runtime_error{"TODO"};
}

auto json::boolean() const -> bool
{
  if (type_ == type::boolean)
    return value_boolean_;
  throw std::runtime_error{"TODO"};
}

auto json::array() const -> array_type const&
{
  if (type_ == type::array)
    return value_array_;
  throw std::runtime_error{"TODO"};
}

auto json::object() const -> object_type const&
{
  if (type_ == type::object)
    return value_object_;
  throw std::runtime_error{"TODO"};
}

auto json::operator[](size_t i) const -> json const&
{
  if (type_ == type::array)
    return value_array_[i];
  if (type_ == type::object)
    return value_object_[i].second;
  throw std::runtime_error{"TODO"};
}

auto json::operator[](size_t i) -> json&
{
  if (type_ == type::array)
    return value_array_[i];
  if (type_ == type::object)
    return value_object_[i].second;
  throw std::runtime_error{"TODO"};
}

auto json::operator[](char const* key) const -> json const&
{
  if (type_ == type::object)
  {
    for (auto& val : value_object_)
      if (val.first == key)
        return val.second;
    throw std::runtime_error{"TODO"};
  }
  throw std::runtime_error{"TODO"};
}

auto json::operator[](char const* key) -> json&
{
  if (type_ == type::object)
  {
    for (auto& val : value_object_)
      if (val.first == key)
        return val.second;

    // if the value is not found, insert a new null value and return it
    value_object_.emplace_back(key, json{});
    return value_object_.back().second;
  }
  throw std::runtime_error{"TODO"};
}

auto json::find(char const* key) const -> json const*
{
  if (type_ == type::object)
  {
    for (auto& val : value_object_)
      if (val.first == key)
        return &val.second;
    return nullptr;
  }
  throw std::runtime_error{"TODO"};
}

auto json::find(char const* key) -> json*
{
  if (type_ == type::object)
  {
    for (auto& val : value_object_)
      if (val.first == key)
        return &val.second;
    return nullptr;
  }
  throw std::runtime_error{"TODO"};
}

// LCOV_EXCL_START
#include <sstream>
TEST_CASE("json")
{
  std::istringstream ss;
  auto iss = [&](char const* str) -> std::istringstream&
  {
    ss.str(str);
    ss.clear();
    return ss;
  };

  SUBCASE("skip_whitespace")
  {
    CHECK_NOTHROW(skip_whitespace(iss("  foo")));
    CHECK(ss.peek() == 'f');
  }

  SUBCASE("parse_string")
  {
    CHECK(parse_string(iss("\"foo\"")) == "foo");
    CHECK(parse_string(iss("\"foo bar\"")) == "foo bar");
    CHECK(parse_string(iss("\"\"")) == "");

    CHECK(parse_string(iss("\"\\\"\"")) == "\"");
    CHECK(parse_string(iss("\"\\\\\"")) == "\\");
    CHECK(parse_string(iss("\"\\/\"")) == "/");
    CHECK(parse_string(iss("\"\\b\"")) == "\b");
    CHECK(parse_string(iss("\"\\f\"")) == "\f");
    CHECK(parse_string(iss("\"\\n\"")) == "\n");
    CHECK(parse_string(iss("\"\\r\"")) == "\r");
    CHECK(parse_string(iss("\"\\t\"")) == "\t");

    // expected failure - unicode not implemented yet
    CHECK_THROWS(parse_string(iss("\"\\u00b0\"")) == "\u00b0");

    CHECK_THROWS_AS(parse_string(iss("foo")), parse_error const&);
    CHECK_THROWS_AS(parse_string(iss("\"foo")), parse_error const&);
    CHECK_THROWS_AS(parse_string(iss("\"\\Q\"")), parse_error const&);
  }

  SUBCASE("constructors")
  {
    CHECK(json{}.value_type() == json::type::null);
    CHECK(json{123.45}.value_type() == json::type::number);
    CHECK(json{123.45}.number() == approx(123.45));
    CHECK(json{"foo"s}.value_type() == json::type::string);
    CHECK(json{"foo"s}.string() == "foo");
    CHECK(json{false}.value_type() == json::type::boolean);
    CHECK(json{false}.boolean() == false);
    CHECK(json{json::array_type{json{}, json{}, json{}}}.value_type() == json::type::array);
    CHECK(json{json::array_type{json{}, json{}, json{}}}.array().size() == 3);
    CHECK(json{json::object_type{std::make_pair("a"s, json{}), std::make_pair("b"s, json{})}}.value_type() == json::type::object);
    CHECK(json{json::object_type{std::make_pair("a"s, json{}), std::make_pair("b"s, json{})}}.object().size() == 2);
  }

  auto test_str = R"(
    {
      "foo": "bar",
      "deep" :
      {
        "deeper": [ 1, 2, 3 ] ,
        "flag": true
      },
      " bl ah ": 123.4,
      "false": false,
      "empty": null,
      "empty_array": [ ],
      "empty_obj": { }
    }
  )";
  SUBCASE("parse")
  {
    json j1{iss(test_str)};

    // size()
    CHECK(j1.size() == 7);
    CHECK(j1["deep"]["deeper"].size() == 3);
    CHECK_THROWS(j1["foo"].size());

    // number
    CHECK(j1[" bl ah "].number() == approx(123.4));
    CHECK_THROWS(j1["foo"].number());

    // string
    CHECK(j1["foo"].string() == "bar");
    CHECK_THROWS(j1["deep"].string());

    // boolean
    CHECK(j1["false"].boolean() == false);
    CHECK_THROWS(j1["foo"].boolean());

    // array
    CHECK(j1["deep"]["deeper"].array().size() == 3);
    CHECK_THROWS(j1["foo"].array());

    // object
    CHECK(j1["deep"].object().size() == 2);
    CHECK_THROWS(j1["foo"].object());

    // operaor[] (index)
    auto const& c1 = j1;
    CHECK(j1[3].boolean() == false);
    CHECK(c1[3].boolean() == false);
    CHECK(j1["deep"]["deeper"][1].number() == approx(2));
    CHECK(c1["deep"]["deeper"][1].number() == approx(2));
    CHECK_THROWS(j1["foo"][0]);
    CHECK_THROWS(c1["foo"][0]);

    // operator[] (string)
    CHECK_THROWS(j1["foo"]["bar"]);
    CHECK(j1["not_found"].value_type() == json::type::null);
    CHECK_THROWS(c1["foo"]["bar"]);
    CHECK_THROWS(c1["not_found2"]);

    // find
    CHECK(j1.find("deep") == &j1["deep"]);
    CHECK(j1.find("not_found3") == nullptr);
    CHECK(c1.find("deep") == &c1["deep"]);
    CHECK(c1.find("not_found3") == nullptr);
    CHECK_THROWS(j1["foo"].find("bad"));
    CHECK_THROWS(c1["foo"].find("bad"));

    // parse errors
    CHECK_THROWS_AS(json{iss(R"({"bad" 123})")}, parse_error const&);
    CHECK_THROWS_AS(json{iss(R"({"bad": 123 x})")}, parse_error const&);
    CHECK_THROWS_AS(json{iss(R"([123 x])")}, parse_error const&);
    CHECK_THROWS_AS(json{iss(R"(tXue)")}, parse_error const&);
    CHECK_THROWS_AS(json{iss(R"(trXe)")}, parse_error const&);
    CHECK_THROWS_AS(json{iss(R"(truX)")}, parse_error const&);
    CHECK_THROWS_AS(json{iss(R"(fXlse)")}, parse_error const&);
    CHECK_THROWS_AS(json{iss(R"(faXse)")}, parse_error const&);
    CHECK_THROWS_AS(json{iss(R"(falXe)")}, parse_error const&);
    CHECK_THROWS_AS(json{iss(R"(falsX)")}, parse_error const&);
    CHECK_THROWS_AS(json{iss(R"(nXll)")}, parse_error const&);
    CHECK_THROWS_AS(json{iss(R"(nuXl)")}, parse_error const&);
    CHECK_THROWS_AS(json{iss(R"(nulX)")}, parse_error const&);
    CHECK_THROWS_AS(json{iss(R"(bad)")}, parse_error const&);
  }

  SUBCASE("copy/move")
  {
    json j1{iss(test_str)};

    // copy constructor
    json j2{j1};
    // move constructor
    json j3{std::move(j1)};

    json j4, j5;
    // copy assign TODO
    CHECK((j4 = j2).value_type() == json::type::object);
    CHECK((j4 = j2[" bl ah "]).number() == approx(123.4));
    CHECK((j4 = j2["foo"]).string() == "bar");
    CHECK((j4 = j2["false"]).boolean() == false);
    CHECK((j4 = j2["deep"]["deeper"]).array().size() == 3);
    CHECK((j4 = j2["empty"]).value_type() == json::type::null);
    // move assign TODO
    j5 = std::move(j3);
    CHECK((j4 = json{123.0}).number() == approx(123.0));
    CHECK((j4 = json{"foo"s}).string() == "foo");
    CHECK((j4 = json{true}).boolean() == true);
    CHECK((j4 = json{json::array_type{}}).value_type() == json::type::array);
    CHECK((j4 = json{json::object_type{}}).value_type() == json::type::object);
    CHECK((j4 = json{}).value_type() == json::type::null);
  }

  SUBCASE("write")
  {
    json j1{iss(test_str)};

    std::ostringstream oss;

    j1.write(oss, 6);

    auto test_str2 = R"({
       "foo": "bar",
       "deep": {
        "deeper": [1, 2, 3],
        "flag": true
       },
       " bl ah ": 123.4,
       "false": false,
       "empty": null,
       "empty_array": [],
       "empty_obj": {}
      })"s;
    CHECK(oss.str() == test_str2);
  }
}
// LCOV_EXCL_STOP
