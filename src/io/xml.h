/*------------------------------------------------------------------------------
 * Bureau of Meteorology C++ Utility Library
 *
 * Copyright 2016 Commonwealth of Australia, Bureau of Meteorology
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *----------------------------------------------------------------------------*/
#pragma once

#include "../raii.h"
#include "../string_utils.h"

#include <unordered_set>

namespace bom::io::xml
{
  class node;
  class attribute;

  using string_store = std::unordered_set<string>;

  class path_error : public std::exception
  {
  public:
    path_error(char const* name, bool is_attribute);

    virtual auto what() const noexcept -> char const*;

    auto name() const -> string const&          { return name_; }
    auto is_attribute() const -> bool           { return is_attribute_; }

  private:
    string          name_;
    bool            is_attribute_;
    mutable string  description_;
  };

  class attribute_store : private vector<attribute>
  {
  private:
    using base = vector<attribute>;

  public:
    using iterator = base::iterator;
    using const_iterator = base::const_iterator;
    using reverse_iterator = base::reverse_iterator;
    using const_reverse_iterator = base::const_reverse_iterator;

  public:
    attribute_store(attribute_store const& rhs) = delete;
    attribute_store(attribute_store&& rhs) noexcept;

    auto operator=(attribute_store const& rhs) -> attribute_store& = delete;
    auto operator=(attribute_store&& rhs) noexcept -> attribute_store&;

    ~attribute_store() = default;

    using base::begin;
    using base::end;
    using base::rbegin;
    using base::rend;
    using base::cbegin;
    using base::cend;
    using base::crbegin;
    using base::crend;

    using base::size;
    using base::empty;
    using base::reserve;

    using base::operator[];
    auto operator[](char const* name) -> attribute&;
    auto operator[](char const* name) const -> attribute const&;
    auto operator[](string const& name) -> attribute&;
    auto operator[](string const& name) const -> attribute const&;

    auto find(char const* name) -> iterator;
    auto find(char const* name) const -> const_iterator;
    auto find(string const& name) -> iterator;
    auto find(string const& name) const -> const_iterator;

    auto insert(string const& name) -> iterator;
    auto insert(string&& name) -> iterator;

    auto find_or_insert(char const* name) -> iterator;
    auto find_or_insert(string const& name) -> iterator;

    using base::erase;
    using base::clear;

  private:
    attribute_store(string_store* strings);

  private:
    string_store* strings_;

    friend class node;
    friend class node_store;
  };

  class node_store : private vector<node>
  {
  private:
    using base = vector<node>;

  public:
    using iterator = base::iterator;
    using const_iterator = base::const_iterator;
    using reverse_iterator = base::reverse_iterator;
    using const_reverse_iterator = base::const_reverse_iterator;

  public:
    node_store(node_store const& rhs) = delete;
    node_store(node_store&& rhs) noexcept;

    auto operator=(node_store const& rhs) -> node_store& = delete;
    auto operator=(node_store&& rhs) noexcept -> node_store&;

    ~node_store() = default;

    using base::begin;
    using base::end;
    using base::rbegin;
    using base::rend;
    using base::cbegin;
    using base::cend;
    using base::crbegin;
    using base::crend;

    using base::size;
    using base::empty;
    using base::reserve;

    using base::operator[];
    using base::front;
    using base::back;

    using base::clear;

    auto operator[](char const* name) -> node&;
    auto operator[](char const* name) const -> node const&;
    auto operator[](string const& name) -> node&;
    auto operator[](string const& name) const -> node const&;

    auto find(char const* name) -> iterator;
    auto find(char const* name) const -> const_iterator;
    auto find(string const& name) -> iterator;
    auto find(string const& name) const -> const_iterator;

    auto push_back(string const& name) -> iterator;
    auto push_back(string&& name) -> iterator;
    using base::pop_back;

    auto insert(iterator position, string const& name) -> iterator;
    auto insert(iterator position, string&& name) -> iterator;
    using base::erase;

    auto find_or_insert(char const* name) -> iterator;
    auto find_or_insert(string const& name) -> iterator;

  private:
    node_store(node* owner);

  private:
    node* owner_;

    friend class node;
  };

  class attribute
  {
  public:
    attribute(attribute const& rhs) = delete;
    attribute(attribute&& rhs) noexcept = default;

    auto operator=(attribute const& rhs) -> attribute& = delete;
    auto operator=(attribute&& rhs) noexcept -> attribute& = default;

    ~attribute() = default;

    auto name() const -> string const&           { return *name_; }
    auto set_name(string const& val) -> void;

    auto get() const -> string const&            { return *value_; }
    auto set(string const& val) -> void;

    template <typename T, typename = typename std::enable_if<allow_simple_string_conversions<T>::value>::type>
    operator T() const                           { return from_string<T>(*value_); }
    operator string const&() const               { return *value_; }

    template <typename T, typename = typename std::enable_if<allow_simple_string_conversions<T>::value>::type>
    auto set(T&& val) -> void                    { set(to_string(std::forward<T>(val))); }
    auto set(char const* val) -> void            { set(string(val)); }

  private:
    // this allows us to use a constructor in a container's emplace methods while
    // still ensuring that it uncallable (private) from the user's point of view
    struct private_constructor { };

  public:
    attribute(
          string_store* strings
        , char const*& src
        , string& tmp
        , private_constructor);
    attribute(
          string_store* strings
        , attribute const& rhs
        , private_constructor);
    attribute(
          string_store* strings
        , const string* name
        , private_constructor);

    auto write(std::ostream& out) const -> void;

  private:
    string_store*  strings_;
    const string*  name_;
    const string*  value_;

    friend class node;
    friend class attribute_store;
  };

  // outside declarations to avoid use of incomplete types due to foward declaration of attribute (clang fails)
  inline attribute_store::attribute_store(string_store* strings) : strings_(strings) { }

  /* mixed mode elements are emulated using elements with an empty name */
  class node
  {
  public:
    enum class type
    {
        element_simple      //!< empty, or simple text contents element
      , element_compound    //!< element containing children (elements and/or text)
      , text_block          //!< text only block (child of a mixed element)
    };

  public:
    node(node const& rhs) = delete;
    node(node&& rhs) noexcept;

    auto operator=(node const& rhs) -> node& = delete;
    auto operator=(node&& rhs) noexcept -> node&;

    ~node() = default;

    auto write(std::ostream& out) const -> void;

    auto node_type() const -> enum type;

    auto name() const -> string const&                             { return *name_; }
    auto set_name(string const& val) -> void;

    // attribute access: only valid for 'element_simple' and 'element_compund' nodes
    auto attributes() -> attribute_store&                          { return attributes_; }
    auto attributes() const -> attribute_store const&              { return attributes_; }
    auto operator()(char const* name) -> attribute&                { return attributes_[name]; }
    auto operator()(char const* name) const -> attribute const&    { return attributes_[name]; }
    auto operator()(string const& name) -> attribute&              { return attributes_[name]; }
    auto operator()(string const& name) const -> attribute const&  { return attributes_[name]; }

    // optional attribute access:
    template <typename T, typename = typename std::enable_if<allow_simple_string_conversions<std::decay_t<T>>::value>::type>
    auto operator()(char const* name, T&& def) const -> std::decay_t<T>
    {
      auto i = attributes_.find(name);
      return i != attributes_.end() ? *i : std::forward<T>(def);
    }

    auto operator()(char const* name, string const& def) const -> string const&
    {
      auto i = attributes_.find(name);
      return i != attributes_.end() ? i->get() : def;
    }

    auto operator()(char const* name, char const* def) const -> char const*
    {
      auto i = attributes_.find(name);
      return i != attributes_.end() ? i->get().c_str() : def;
    }

    // child access: only valid for 'element_compound' nodes
    auto children() -> node_store&                                { return children_; }
    auto children() const -> node_store const&                    { return children_; }
    auto operator[](char const* name) -> node&                    { return children_[name]; }
    auto operator[](char const* name) const -> node const&        { return children_[name]; }
    auto operator[](string const& name) -> node&                  { return children_[name]; }
    auto operator[](string const& name) const -> node const&      { return children_[name]; }

    auto get() const -> string const&                             { return *value_; }
    auto set(string const& val) -> void;

    template <typename T, typename = typename std::enable_if<allow_simple_string_conversions<T>::value>::type>
    operator T() const                                            { return from_string<T>(*value_); }
    operator string const&() const                                { return *value_; }

    template <typename T, typename = typename std::enable_if<allow_simple_string_conversions<T>::value>::type>
    auto set(T&& val) -> void                                     { set(to_string(std::forward<T>(val))); }
    auto set(char const* val) -> void                             { set(string(val)); }

  private:
    // this allows us to use a constructor in a container's emplace methods while
    // still ensuring that it uncallable (private) from the user's point of view
    struct private_constructor { };

  public:
    node(private_constructor);
    node(
          string_store* strings
        , const string* value
        , private_constructor);
    node(
          string_store* strings
        , char const*& src
        , string& tmp
        , private_constructor);
    node(
          string_store* strings
        , node const& rhs
        , private_constructor);
    node(
          string_store* strings
        , const string* name
        , bool // dummy flag to distinguish constructor
        , private_constructor);

  private:
    attribute_store attributes_;  // contains a pointer to the strings store
    string const*   name_;
    string const*   value_;       // only meaningful if a text node
    node_store      children_;

    friend class document;
    friend class node_store;
  };

  // outside declarations to avoid use of incomplete types due to foward declaration of node (clang fails)
  inline node_store::node_store(node* owner) : owner_(owner) { }

  class document
  {
  public:
    /// Create an empty document with an empty root node
    document();
    /// Parse a document from an input stream
    document(std::istream& source);
    /// Parse a document from an input stream
    document(std::istream&& source);
    /// Parse a document from a string
    document(string const& source);
    /// Parse a document from a string
    document(char const* source);
    /// Create a document by copying a node from another document as the root
    document(const xml::node& root);

    document(const document& rhs);
    document(document&& rhs) noexcept;

    auto operator=(const document& rhs) -> document&;
    auto operator=(document&& rhs) noexcept -> document&;

    ~document() = default;

    auto write(std::ostream& out) -> void;
    auto write(std::ostream&& out) -> void;

    auto root() -> node&             { return root_; }
    auto root() const -> node const& { return root_; }

  private:
    string_store  strings_;
    node          root_;
  };
}
