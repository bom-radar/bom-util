/*------------------------------------------------------------------------------
 * Bureau of Meteorology C++ Utility Library
 *
 * Copyright 2014 Mark Curtis
 * Copyright 2016 Commonwealth of Australia, Bureau of Meteorology
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *----------------------------------------------------------------------------*/
#include "config.h"
#include "parse_error.h"
#include "unit_test.h"
#include <limits>

using namespace bom;

parse_error::parse_error(string desc, char const* where)
  : desc_(std::move(desc))
  , where_(where)
  , line_(0)
  , column_(0)
{
  build_what_string();
}

parse_error::parse_error(string desc, int line, int column)
  : desc_(std::move(desc))
  , where_(nullptr)
  , line_(line)
  , column_(column)
{
  build_what_string();
}

parse_error::parse_error(string desc, std::istream& in)
  : desc_(std::move(desc))
  , where_(nullptr)
  , line_(0)
  , column_(0)
{
  // the clear and check of pos is needed to cope with end of file (when we want a line/column count)
  // and missing files / broken streams etc (when we can't get a line/column count)
  in.clear();
  std::streamoff pos = in.tellg();
  if (pos != -1)
  {
    in.seekg(0, std::ios_base::beg);
    for (decltype(pos) i = 0; i < pos; ++i)
    {
      char c = in.get();
      if (c == std::char_traits<char>::eof())
        break;
      if (c == '\n')
      {
        ++line_;
        column_ = 0;
      }
      else
        ++column_;
    }
    ++line_;
    ++column_;
  }
  build_what_string();
}

auto parse_error::what() const noexcept -> char const*
{
  return what_.c_str();
}

auto parse_error::determine_line_column(char const* src) -> void
{
  if (where_ && src)
  {
    for (line_ = column_ = 1; src < where_; ++src)
    {
      if (*src == '\n')
      {
        ++line_;
        column_ = 1;
      }
      else
        ++column_;
    }
    build_what_string();
  }
}

auto parse_error::build_what_string() -> void
{
  if (line_ || column_)
    what_ = fmt::format("parse error at line {} column {}: {}", line_, column_, desc_);
  else
    what_ = fmt::format("parse error: {}", desc_);
}

// LCOV_EXCL_START
#include <sstream>

TEST_CASE("parse_error")
{
  char const* text = "123456\nabcdef\n\nline 4";

  // where string constructor
  SUBCASE("where_constructor")
  {
    parse_error err{"desc", text + 16};
    CHECK_NOTHROW(err.determine_line_column(text));
    CHECK(err.description() == "desc");
    CHECK(err.where() == text + 16);
    CHECK(err.line() == 4);
    CHECK(err.column() == 2);
  }

  // manual line column constructor
  SUBCASE("manual_line")
  {
    parse_error err{"desc", 4, 2};
    CHECK(err.description() == "desc");
    CHECK(err.line() == 4);
    CHECK(err.column() == 2);
  }

  // istream constructor
  SUBCASE("istream")
  {
    std::istringstream iss{text};
    iss.ignore(100, '\n');
    iss.ignore(100, '\n');
    iss.ignore(100, '\n');
    iss.ignore(1);
    parse_error err{"desc", iss};
    CHECK(err.description() == "desc");
    CHECK(err.line() == 4);
    CHECK(err.column() == 2);
  }
}
// LCOV_EXCL_STOP
