/*------------------------------------------------------------------------------
 * Bureau of Meteorology C++ Utility Library
 *
 * Copyright 2014 Mark Curtis
 * Copyright 2016 Commonwealth of Australia, Bureau of Meteorology
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *----------------------------------------------------------------------------*/
#pragma once

#include "string_utils.h"
#include "raii.h"
#include "trace.h"
#include <memory>
#include <stdexcept>
#include <typeinfo>
#include <unordered_map>

namespace bom
{
  /// Generic factory class template
  /** The T parameter indicates the base type to be produced.  The P type indicates the type of pointer to be
   *  returned, and the F type indicates the factory function type.
   *
   *  For example:
   *  using my_factory = factory<my_type, std::unique_ptr<my_type>, configuration const&>;
   */
  template <typename T, typename P = unique_ptr<T>, typename... Args>
  class factory
  {
  public:
    using base_type = T;
    using pointer_type = P;
    using function_type = P(Args...);

  public:
    /// Register a concrete type with the factory
    /*  If the name provided is already registered, then this function will check whether the concrete type and the
     *  factory function match.  If so, a warning will be issued to trace but the function will return as if the
     *  registration had been successful.  If either the concrete type or the factory function differ from the
     *  existing registration then an exception will be thrown. */
    template <typename D>
    auto register_type(string name, function_type* func) -> void
    {
      auto type = &typeid(D);
      auto ret = map_.emplace(std::move(name), registration{func, type});
      if (!ret.second)
      {
        auto str = fmt::format("duplicate factory registration for type: {}", ret.first->first);
        if (ret.first->second.func == func && ret.first->second.type == type)
          trace::warning("{}", ret.first->first);
        else
          throw std::runtime_error{str};
      }
    }

    /// Register a concrete type with the factory
    /** This performs the same work as the overload which takes an explicit pointer to a factory function with the
     *  difference that a default factory function will be created for you.  This function will new to create an
     *  object forwarding the Args parameters to the consructor.
     *
     *  The function is SFINAE overloaded to ensure that std::make_shared is used in cases where the factory shall
     *  output a shared_ptr. */
    template <typename D, typename P1 = P>
    auto register_type(string name) -> typename std::enable_if<!is_instance_of<P1, shared_ptr>::value>::type
    {
      register_type<D>(std::move(name), [](Args... args) { return P{new D(std::forward<Args>(args)...)}; });
    }
    template <typename D, typename P1 = P>
    auto register_type(string name) -> typename std::enable_if<is_instance_of<P1, shared_ptr>::value>::type
    {
      register_type<D>(std::move(name), [](Args... args) -> P { return make_shared<D>(std::forward<Args>(args)...); });
    }

    /// Create an instance of the named concrete type
    template <typename... Args2>
    auto create(string const& name, Args2&&... args) -> pointer_type
    {
      auto i = map_.find(name);
      if (i == map_.end())
        throw std::runtime_error{"unknown concrete type requested: " + name};
      return i->second.func(std::forward<Args2>(args)...);
    }

  private:
    struct registration
    {
      registration()
        : func(nullptr), type(nullptr)
      { }
      registration(function_type* func, std::type_info const* type)
        : func(func), type(type)
      { }
      function_type* func;
      std::type_info const* type;
    };

  private:
    std::unordered_map<string, registration> map_;
  };
}
