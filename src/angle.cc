/*------------------------------------------------------------------------------
 * Bureau of Meteorology C++ Utility Library
 *
 * Copyright 2014 Mark Curtis
 * Copyright 2016 Commonwealth of Australia, Bureau of Meteorology
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *----------------------------------------------------------------------------*/
#include "config.h"
#include "angle.h"
#include "unit_test.h"

using namespace bom;

auto angle::dms() const -> std::tuple<int, int, double>
{
  std::tuple<int, int, double> ret;

  double rem;
  std::get<0>(ret) = rem = std::abs(degrees());
  std::get<1>(ret) = rem = (rem - std::get<0>(ret)) * 60.0;
  std::get<2>(ret) = rem = (rem - std::get<1>(ret)) * 60.0;

  // fix rare case of floating point stuff up
  if (std::get<2>(ret) >= 60.0)
  {
    std::get<1>(ret)++;
    std::get<2>(ret) -= 60.0;
  }

  // ensure correct sign on degrees
  if (rads_ < 0.0)
    std::get<0>(ret) = -std::get<0>(ret);

  return ret;
}

// LCOV_EXCL_START
#include <sstream>
#include <iomanip>

TEST_CASE("angle")
{
  auto a = 90_deg;

  // _rad UDL
  CHECK((1.57079633_rad).radians() == approx(1.57079633));
  CHECK((1.57079633_rad).degrees() == approx(90.0));

  // _deg UDL
  CHECK((90_deg).radians() == approx(1.57079633));
  CHECK((90_deg).degrees() == approx(90.0));

  // relational operators
  CHECK(89.999_deg < 90.000_deg);
  CHECK_FALSE(90.001_deg < 90.000_deg);
  CHECK(90.000_deg <= 90.000_deg);
  CHECK_FALSE(90.001_deg <= 90.000_deg);
  CHECK(90.001_deg > 90.000_deg);
  CHECK_FALSE(89.999_deg > 90.000_deg);
  CHECK(90.000_deg >= 90.000_deg);
  CHECK_FALSE(89.999_deg >= 90.000_deg);

  // binary operator
  CHECK((a + 10_deg).degrees() == approx(100.0));
  CHECK((a - 10_deg).degrees() == approx(80.0));
  CHECK((a * 2.0).degrees() == approx(180.0));
  CHECK((a / 2.0).degrees() == approx(45.0));
  CHECK(a / 45_deg == approx(2.0));

  // compound assignment operators
  CHECK((a += 10_deg).degrees() == approx(100.0));
  CHECK((a -= 10_deg).degrees() == approx(90.0));
  CHECK((a *= 2.0).degrees() == approx(180.0));
  CHECK((a /= 2.0).degrees() == approx(90.0));

  // set_degrees() and set_radians()
  a.set_degrees(120.0);
  CHECK(a.radians() == approx(2.09439510));
  CHECK(a.degrees() == approx(120.0));
  a.set_radians(2.0);
  CHECK(a.radians() == approx(2.0));
  CHECK(a.degrees() == approx(114.591559));

  // dms()
  auto dms = (-90.123_deg).dms();
  CHECK(std::get<0>(dms) == -90);
  CHECK(std::get<1>(dms) == 7);
  CHECK(std::abs(std::get<2>(dms) - 22.8) < 0.1);

  // set_dms()
  a.set_dms(std::make_tuple(90, 7, 22.8));
  CHECK(a.degrees() == approx(90.123));
  a.set_dms(std::make_tuple(-45, 30, 45.13));
  CHECK(a.degrees() == approx(-45.512536));

  // abs()
  CHECK(a.abs().degrees() == approx(45.512536));
  CHECK(abs(a).degrees() == approx(45.512536));

  // normalize()
  CHECK((0_deg).normalize().degrees() == approx(0.0));
  CHECK((360_deg).normalize().degrees() == approx(0.0));
  CHECK((450_deg).normalize().degrees() == approx(90.0));
  CHECK((-90_deg).normalize().degrees() == approx(270.0));

  // nan<angle>() / is_nan()
  CHECK(is_nan(nan<angle>().radians()));
  CHECK(is_nan(nan<angle>().degrees()));
  CHECK(is_nan(nan<angle>()));
  CHECK(is_nan(nan<double>() * 1_deg));

  // trig overloads
  CHECK(cos(0_deg) == approx(1.0));
  CHECK(cos(45_deg) == approx(0.70710678118));
  CHECK(cos(90_deg) == approx(0.0));
  CHECK(sin(0_deg) == approx(0.0));
  CHECK(sin(45_deg) == approx(0.70710678118));
  CHECK(sin(90_deg) == approx(1.0));
  CHECK(tan(0_deg) == approx(0.0));
  CHECK(tan(45_deg) == approx(1.0));
  CHECK(tan(80_deg) == approx(5.67128181962));
  CHECK(bom::acos(1.0).degrees() == approx(0.0));
  CHECK(bom::acos(0.70710678118).degrees() == approx(45.0));
  CHECK(bom::acos(0.0).degrees() == approx(90.0));
  CHECK(bom::asin(0.0).degrees() == approx(0.0));
  CHECK(bom::asin(0.70710678118).degrees() == approx(45.0));
  CHECK(bom::asin(1.0).degrees() == approx(90.0));
  CHECK(bom::atan(0.0).degrees() == approx(0.0));
  CHECK(bom::atan(1.0).degrees() == approx(45.0));
  CHECK(bom::atan(5.67128181962).degrees() == approx(80.0));
  CHECK(bom::atan2(0.0, 1.0).degrees() == approx(0.0));
  CHECK(bom::atan2(1.0, 1.0).degrees() == approx(45.0));
  CHECK(bom::atan2(1.0, 0.0).degrees() == approx(90.0));
  CHECK(bom::atan2(1.0, -1.0).degrees() == approx(135.0));
  CHECK(bom::atan2(0.0, -1.0).degrees() == approx(180.0));
  CHECK(bom::atan2(-1.0, -1.0).degrees() == approx(-135.0));
  CHECK(bom::atan2(-1.0, 0.0).degrees() == approx(-90.0));
  CHECK(bom::atan2(-1.0, 1.0).degrees() == approx(-45.0));

  // interpolation
  CHECK(lerp(359.0_deg, 1.0_deg, 0.5f).normalize().degrees() == approx(0.0));
  CHECK(lerp(359.0_deg, 1.0_deg, 0.5).normalize().degrees() == approx(0.0));
  CHECK(lerp(1.0_deg, 359.0_deg, 0.5).normalize().degrees() == approx(0.0));
  CHECK(lerp(180.0_deg, 900.0_deg, 0.5).normalize().degrees() == approx(180.0));
  CHECK(lerp(-1.0_deg, 1.0_deg, 0.5).normalize().degrees() == approx(0.0));
  CHECK(lerp(-10.0_deg, 10.0_deg, 0.5).normalize().degrees() == approx(0.0));
  CHECK(lerp(-89.0_deg, 89.0_deg, 0.5).normalize().degrees() == approx(0.0));
  CHECK(lerp(-91.0_deg, 91.0_deg, 0.5).normalize().degrees() == approx(180.0));
  CHECK(lerp(91.0_deg, -91.0_deg, 0.5).normalize().degrees() == approx(180.0));
  CHECK(lerp(-180.0_deg, 180.0_deg, 0.5).normalize().degrees() == approx(180.0));

  // write_string
  CHECK(to_string(-35.1_deg) == "-35.1");

  // read string
  CHECK(from_string<angle>("-45.512536").degrees() == approx(-45.512536));

  // format
  CHECK(fmt::format("{}", -35.1_deg) == "-35.1");
  CHECK(fmt::format("{:}", -45.512537_deg) == "-45.512537");
  CHECK(fmt::format("{:dms}", -45.512536_deg) == "-45\u00b030'45\"");
  CHECK(fmt::format("{:rad}", -45.512536_deg) == "-0.79434360413245");
  CHECK(fmt::format("{:02.0f}", -45.512536_deg) == "-46");

  // operator<<
  std::ostringstream oss;
  oss << std::setprecision(4) << std::fixed << 90_deg;
  CHECK(oss.str().substr(0, 7) == "90.0000");
}
// LCOV_EXCL_STOP
