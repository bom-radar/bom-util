/*------------------------------------------------------------------------------
 * Bureau of Meteorology C++ Utility Library
 *
 * Copyright 2014 Mark Curtis
 * Copyright 2016 Commonwealth of Australia, Bureau of Meteorology
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *----------------------------------------------------------------------------*/
#pragma once

#include "angle.h"
#include "string_utils.h"
#include "traits.h"

#include <cmath>
#include <istream>
#include <ostream>

namespace bom
{
  template <typename T>
  struct vec2
  {
    using value_type = T;

    T x, y;

    constexpr vec2() = default;

    constexpr vec2(T x, T y) : x(x), y(y) { }

    template <typename U>
    constexpr vec2(vec2<U> const& rhs) : x(rhs.x), y(rhs.y) { }

    template <typename U = T, typename = typename std::enable_if<!std::is_floating_point<U>::value>::type>
    constexpr auto operator==(vec2 const& rhs) const
    {
      return x == rhs.x && y == rhs.y;
    }

    template <typename U = T, typename = typename std::enable_if<!std::is_floating_point<U>::value>::type>
    constexpr auto operator!=(vec2 const& rhs) const
    {
      return x != rhs.x || y != rhs.y;
    }

    auto& operator+=(vec2 const& rhs)
    {
      x += rhs.x;
      y += rhs.y;
      return *this;
    }

    auto& operator-=(vec2 const& rhs)
    {
      x -= rhs.x;
      y -= rhs.y;
      return *this;
    }

    template <typename U>
    auto& operator*=(U const& rhs)
    {
      x *= rhs;
      y *= rhs;
      return *this;
    }

    template <typename U>
    auto& operator/=(U const& rhs)
    {
      x /= rhs;
      y /= rhs;
      return *this;
    }

    auto& normalize()
    {
      if (auto l = length())
      {
        x /= l;
        y /= l;
      }
      else
      {
        x = 0.0;
        y = 0.0;
      }
      return *this;
    }

    auto normal() const
    {
      vec2 ret;
      if (auto l = length())
      {
        ret.x = x / l;
        ret.y = y / l;
      }
      else
      {
        ret.x = 0;
        ret.y = 0;
      }
      return ret;
    }

    constexpr auto dot(vec2 const& rhs) const
    {
      return x * rhs.x + y * rhs.y;
    }

    constexpr auto scale(vec2 const& rhs) const
    {
      return vec2{x * rhs.x, y * rhs.y};
    }

    constexpr auto length_sqr() const
    {
      return x * x + y * y;
    }

    auto length() const
    {
      return std::sqrt(x * x + y * y);
    }

    auto angle(const vec2& rhs) const
    {
      return acos(dot(rhs) / (length() * rhs.length()));
    }

    template <typename T1, typename T2>
    friend constexpr auto operator+(vec2<T1> const& lhs, vec2<T2> const& rhs) -> vec2<decltype(lhs.x + rhs.x)>;

    template <typename T1, typename T2>
    friend constexpr auto operator-(vec2<T1> const& lhs, vec2<T2> const& rhs) -> vec2<decltype(lhs.x - rhs.x)>;

    template <typename T1, typename U>
    friend constexpr auto operator*(vec2<T1> const& lhs, U rhs) -> vec2<decltype(lhs.x * rhs)>;

    template <typename T1, typename U>
    friend constexpr auto operator*(U lhs, vec2<T1> const& rhs) -> vec2<decltype(lhs * rhs.x)>;

    template <typename T1, typename U>
    friend constexpr auto operator/(vec2<T1> const& lhs, U rhs) -> vec2<decltype(lhs.x / rhs)>;

    template <typename T1>
    friend auto operator>>(std::istream& lhs, vec2<T1>& rhs) -> std::istream&;

    template <typename T1>
    friend auto operator<<(std::ostream& lhs, vec2<T1> const& rhs) -> std::ostream&;
  };

  template <typename T1, typename T2>
  inline constexpr auto operator+(vec2<T1> const& lhs, vec2<T2> const& rhs) -> vec2<decltype(lhs.x + rhs.x)>
  {
    return vec2<decltype(lhs.x + rhs.x)>(lhs.x + rhs.x, lhs.y + rhs.y);
  }

  template <typename T1, typename T2>
  inline constexpr auto operator-(vec2<T1> const& lhs, vec2<T2> const& rhs) -> vec2<decltype(lhs.x - rhs.x)>
  {
    return vec2<decltype(lhs.x - rhs.x)>(lhs.x - rhs.x, lhs.y - rhs.y);
  }

  template <typename T1, typename U>
  inline constexpr auto operator*(vec2<T1> const& lhs, U rhs) -> vec2<decltype(lhs.x * rhs)>
  {
    return vec2<decltype(lhs.x * rhs)>(lhs.x * rhs, lhs.y * rhs);
  }

  template <typename T1, typename U>
  inline constexpr auto operator*(U lhs, vec2<T1> const& rhs) -> vec2<decltype(lhs * rhs.x)>
  {
    return vec2<decltype(lhs * rhs.x)>(lhs * rhs.x, lhs * rhs.y);
  }

  template <typename T1, typename U>
  inline constexpr auto operator/(vec2<T1> const& lhs, U rhs) -> vec2<decltype(lhs.x / rhs)>
  {
    return vec2<decltype(lhs.x / rhs)>(lhs.x / rhs, lhs.y / rhs);
  }

  template <typename T1>
  inline auto operator>>(std::istream& lhs, vec2<T1>& rhs) -> std::istream&
  {
    return lhs >> rhs.x >> rhs.y;
  }

  template <typename T1>
  inline auto operator<<(std::ostream& lhs, vec2<T1> const& rhs) -> std::ostream&
  {
    return lhs << rhs.x << ' ' << rhs.y;
  }

  template <typename T>
  struct string_conversions<vec2<T>>
  {
    static auto read_string(char const* str, size_t len) -> vec2<T>
    try
    {
      auto end = str;
      while (end < str + len && !std::isspace(*end))
        ++end;

      auto next = end;
      while (next < str + len && std::isspace(*next))
        ++next;

      return {
          string_conversions<T>::read_string(str, end - str)
        , string_conversions<T>::read_string(next, len - (next - str))
        };
    }
    catch (...)
    {
      std::throw_with_nested(string_conversion_error{"vec2", string(str, len)});
    }

    static auto write_string(vec2<T> val, string& str) -> void
    {
      string_conversions<T>::write_string(val.x, str);
      str.push_back(' ');
      string_conversions<T>::write_string(val.y, str);
    }

    static auto format_string(vec2<T> val, string& str, char const* fmt_begin = nullptr, char const* fmt_end = nullptr) -> void
    {
      // TODO - obey format string
      string_conversions<T>::write_string(val.x, str);
      str.push_back(' ');
      string_conversions<T>::write_string(val.y, str);
    }
  };

  template <typename T>
  struct allow_simple_string_conversions<vec2<T>>
  {
    static constexpr bool value = true;
  };

  using vec2i = vec2<int>;
  using vec2z = vec2<size_t>;
  using vec2f = vec2<float>;
  using vec2d = vec2<double>;
}

namespace fmt
{
  template <typename T, typename Char>
  struct formatter<bom::vec2<T>, Char> : formatter<T, Char>
  {
    template <typename FormatContext>
    auto format(bom::vec2<T> val, FormatContext& ctx)
    {
      formatter<T, Char>::format(val.x, ctx);
      fmt::format_to(ctx.out(), " ");
      formatter<T, Char>::format(val.y, ctx);
      return ctx.out();
    }
  };
}
