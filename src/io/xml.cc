/*------------------------------------------------------------------------------
 * Bureau of Meteorology C++ Utility Library
 *
 * Copyright 2016 Commonwealth of Australia, Bureau of Meteorology
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *----------------------------------------------------------------------------*/
#include "config.h"
#include "xml.h"
#include "../parse_error.h"
#include "../unit_test.h"

using namespace bom;
using namespace bom::io;
using namespace bom::io::xml;

static_assert(
      std::is_nothrow_move_constructible<attribute>::value
    , "attribute not move constructible");
static_assert(
      std::is_nothrow_move_assignable<attribute>::value
    , "attribute not move assignable");
static_assert(
      std::is_nothrow_move_constructible<node>::value
    , "node not move constructible");
static_assert(
      std::is_nothrow_move_assignable<node>::value
    , "node not move assignable");

// this is thread safe because we only ever use it in comparisons
static string const empty_string;

static auto skip_pi(char const*& src) -> void
{
  // loop until we find a '?>'
  for (char c = *++src; c != '\0'; c = *++src)
  {
    if (c != '?')
      continue;

    c = *++src;
    if (c != '>')
      continue;

    ++src;
    return;
  }

  throw parse_error("unterminated processing instruction '<?'", src);
}

static auto skip_comment(char const*& src) -> void
{
  // check that we have the '--'
  if (*++src != '-')
    throw parse_error("invalid comment", src);
  if (*++src != '-')
    throw parse_error("invalid comment", src);

  // loop until we find a '--'
  for (char c = *++src; c != '\0'; c = *++src)
  {
    if (c != '-')
      continue;

    c = *++src;
    if (c != '-')
      continue;

    c = *++src;
    if (c != '>')
      throw parse_error("comment body contains '--'", src);

    ++src;
    return;
  }

  throw parse_error("unterminated comment", src);
}

/* this function is currently only able to skip simple DOCTYPE declarations.
 * if you need to skip declarations that contain internal subsets you are in
 * for some fun... */
static auto skip_doctype(char const*& src) -> void
{
  ++src;
  if (   src[0] != 'D' || src[1] != 'O' || src[2] != 'C'
      || src[3] != 'T' || src[4] != 'Y' || src[5] != 'P' || src[6] != 'E')
    throw parse_error("invalid DOCTYPE", src);
  src += 7;

  char quote_char = '\0';
  for (char c = *++src; c != '\0'; c = *++src)
  {
    if (quote_char == '\0')
    {
      if (c == '>')
      {
        ++src;
        return;
      }
      if (c == '"' || c == '\'')
        quote_char = c;
    }
    else if (c == quote_char)
      quote_char = '\0';
  }

  throw parse_error("unterminated DOCTYPE", src);
}

/* where_end should point to the character after 'src' in the stream. it
 * is only used to provide location details when an exception is thrown */
static auto resolve_entities(string& src, char const* where_end) -> void
{
  // search backward through the string for an '&'
  for (auto i = src.rbegin(); i != src.rend(); ++i)
  {
    if (*i != '&')
      continue;

    // got an entity, now loop forward until the ';'
    string::iterator s = i.base() - 1;
    string::iterator e = s;
    while (e != src.end() && *e != ';')
      ++e;
    if (e == src.end())
      throw parse_error(
            "unterminated character entity"
          , where_end - (i - src.rbegin() + 1));

    // determine which entity we have found.  if we wanted to support
    // custom entities, they would need to be detected here
    auto el = e - s;
    if (el == 3 && *(s + 1) == 'l' && *(s + 2) == 't')
      *e = '<';
    else if (el == 3 && *(s + 1) == 'g' && *(s + 2) == 't')
      *e = '>';
    else if (el == 4 && *(s + 1) == 'a' && *(s + 2) == 'm' && *(s + 3) == 'p')
      *e = '&';
    else if (el == 5 && *(s + 1) == 'a' && *(s + 2) == 'p' && *(s + 3) == 'o' && *(s + 4) == 's')
      *e = '\'';
    else if (el == 5 && *(s + 1) == 'q' && *(s + 2) == 'u' && *(s + 3) == 'o' && *(s + 4) == 't')
      *e = '"';
    else
      throw parse_error(
            "unknown character entity"
          , where_end - (i - src.rbegin() + 1));

    // erase the entity
    src.erase((i.base() - 1), e);
  }
}

path_error::path_error(char const* name, bool is_attribute)
  : name_(name)
  , is_attribute_(is_attribute)
{

}

auto path_error::what() const noexcept -> char const*
{
  description_.assign("xml path error: requested child ");
  if (is_attribute_)
    description_.append("attribute '");
  else
    description_.append("node '");
  description_.append(name_);
  description_.append("' does not exist");
  return description_.c_str();
}

attribute_store::attribute_store(attribute_store&& rhs) noexcept
  : base(std::move(rhs))
  , strings_(rhs.strings_)
{ }

auto attribute_store::operator=(attribute_store&& rhs) noexcept -> attribute_store&
{
  base::operator=(std::move(rhs));
  strings_ = rhs.strings_;
  return *this;
}

auto attribute_store::operator[](char const* name) -> attribute&
{
  for (auto& i : *this)
    if (i.name() == name)
      return i;
  throw path_error(name, true);
}

auto attribute_store::operator[](char const* name) const -> attribute const&
{
  for (auto& i : *this)
    if (i.name() == name)
      return i;
  throw path_error(name, true);
}

auto attribute_store::operator[](string const& name) -> attribute&
{
  for (auto& i : *this)
    if (i.name() == name)
      return i;
  throw path_error(name.c_str(), true);
}

auto attribute_store::operator[](string const& name) const -> attribute const&
{
  for (auto& i : *this)
    if (i.name() == name)
      return i;
  throw path_error(name.c_str(), true);
}

auto attribute_store::find(char const* name) -> iterator
{
  auto i = begin();
  while (i != end() && i->name() != name)
    ++i;
  return i;
}

auto attribute_store::find(char const* name) const -> const_iterator
{
  auto i = begin();
  while (i != end() && i->name() != name)
    ++i;
  return i;
}

auto attribute_store::find(string const& name) -> iterator
{
  auto i = begin();
  while (i != end() && i->name() != name)
    ++i;
  return i;
}

auto attribute_store::find(string const& name) const -> const_iterator
{
  auto i = begin();
  while (i != end() && i->name() != name)
    ++i;
  return i;
}

auto attribute_store::insert(string const& name) -> iterator
{
  emplace_back(
        strings_
      , &*strings_->insert(name).first
      , attribute::private_constructor());
  return end() - 1;
}

auto attribute_store::insert(string&& name) -> iterator
{
  emplace_back(
        strings_
      , &*strings_->insert(std::move(name)).first
      , attribute::private_constructor());
  return end() - 1;
}

auto attribute_store::find_or_insert(char const* name) -> iterator
{
  auto i = find(name);
  return i != end() ? i : insert(name);
}

auto attribute_store::find_or_insert(string const& name) -> iterator
{
  auto i = find(name);
  return i != end() ? i : insert(name);
}

node_store::node_store(node_store&& rhs) noexcept
  : base(std::move(rhs))
  , owner_(rhs.owner_)
{

}

auto node_store::operator=(node_store&& rhs) noexcept -> node_store&
{
  base::operator=(std::move(rhs));
  owner_ = rhs.owner_;
  return *this;
}

auto node_store::operator[](char const* name) -> node&
{
  for (auto& i : *this)
    if (i.name() == name)
      return i;
  throw path_error(name, false);
}

auto node_store::operator[](char const* name) const -> node const&
{
  for (auto& i : *this)
    if (i.name() == name)
      return i;
  throw path_error(name, false);
}

auto node_store::operator[](string const& name) -> node&
{
  for (auto& i : *this)
    if (i.name() == name)
      return i;
  throw path_error(name.c_str(), false);
}

auto node_store::operator[](string const& name) const -> node const&
{
  for (auto& i : *this)
    if (i.name() == name)
      return i;
  throw path_error(name.c_str(), false);
}

auto node_store::find(char const* name) -> iterator
{
  auto i = begin();
  while (i != end() && i->name() != name)
    ++i;
  return i;
}

auto node_store::find(char const* name) const -> const_iterator
{
  auto i = begin();
  while (i != end() && i->name() != name)
    ++i;
  return i;
}

auto node_store::find(string const& name) -> iterator
{
  auto i = begin();
  while (i != end() && i->name() != name)
    ++i;
  return i;
}

auto node_store::find(string const& name) const -> const_iterator
{
  auto i = begin();
  while (i != end() && i->name() != name)
    ++i;
  return i;
}

auto node_store::push_back(string const& name) -> iterator
{
  if (!owner_->value_->empty())
    throw std::logic_error("xml logic error: adding child node to non-empty element_simple");
  emplace_back(
        owner_->attributes_.strings_
      , &*owner_->attributes_.strings_->insert(name).first
      , true
      , node::private_constructor());
  return end() - 1;
}

auto node_store::push_back(string&& name) -> iterator
{
  if (!owner_->value_->empty())
    throw std::logic_error("xml logic error: adding child node to non-empty element_simple");
  emplace_back(
        owner_->attributes_.strings_
      , &*owner_->attributes_.strings_->insert(std::move(name)).first
      , true
      , node::private_constructor());
  return end() - 1;
}

auto node_store::insert(iterator position, string const& name) -> iterator
{
  if (!owner_->value_->empty())
    throw std::logic_error("xml logic error: adding child node to non-empty element_simple");
  return emplace(
        position
      , owner_->attributes_.strings_
      , &*owner_->attributes_.strings_->insert(name).first
      , true
      , node::private_constructor());
}

auto node_store::insert(iterator position, string&& name) -> iterator
{
  if (!owner_->value_->empty())
    throw std::logic_error("xml logic error: adding child node to non-empty element_simple");
  return emplace(
        position
      , owner_->attributes_.strings_
      , &*owner_->attributes_.strings_->insert(std::move(name)).first
      , true
      , node::private_constructor());
}

auto node_store::find_or_insert(char const* name) -> iterator
{
  auto i = find(name);
  return i != end() ? i : insert(end(), name);
}

auto node_store::find_or_insert(string const& name) -> iterator
{
  auto i = find(name);
  return i != end() ? i : insert(end(), name);
}

// this constructor is used to parse an attribute from a string
attribute::attribute(
      string_store* strings
    , char const*& src
    , string& tmp
    , private_constructor)
  : strings_(strings)
{
  size_t len;
  char c = *src;

  // extract the name
  len = 0;
  while (c != '\0' && c != '=' && c != ' ' && c != '\t' && c != '\n')
    c = *++src, ++len;
  if (len == 0)
    throw parse_error("attribute missing name", src);
  tmp.assign(src - len, len);
  name_ = &*strings_->insert(tmp).first;

  // extract the =
  while (c != '\0' && (c == ' ' || c == '\t' || c == '\n'))
    c = *++src;
  if (c != '=')
    throw parse_error("attribute missing '='", src);
  c = *++src;

  // extract the leading "
  while (c != '\0' && (c == ' ' || c == '\t' || c == '\n'))
    c = *++src;
  if (c != '"')
    throw parse_error("attribute missing leading '\"'", src);
  c = *++src;

  // extract the value
  len = 0;
  while (c != '\0' && c != '"')
    c = *++src, ++len;
  tmp.assign(src - len, len);
  resolve_entities(tmp, src);
  value_ = &*strings_->insert(tmp).first;

  // extract the trailing "
  if (c != '"')
    throw parse_error("attribute missing '\"'", src);
  ++src;
}

// this constructor is used to copy an attribute between documents
attribute::attribute(string_store* strings, attribute const& rhs, private_constructor)
  : strings_(strings)
  , name_(&*strings_->insert(*rhs.name_).first)
  , value_(&*strings_->insert(*rhs.value_).first)
{

}

// this constructor is used by attribute_store when appending a new attribute
attribute::attribute(string_store* strings, string const* name, private_constructor)
  : strings_(strings)
  , name_(name)
  , value_(&empty_string)
{

}

auto attribute::write(std::ostream& out) const -> void
{
  out << *name_ << "=\"";
  for (auto c : *value_)
    if (c == '<')
      out << "&lt;";
    else if (c == '&')
      out << "&amp;";
    else if (c == '"')
      out << "&quot;";
    else
      out << c;
  out << "\" ";
}

auto attribute::set_name(string const& val) -> void
{
  name_ = &*strings_->insert(val).first;
}

auto attribute::set(string const& val) -> void
{
  value_ = &*strings_->insert(val).first;
}

// this constructor is used to initialize a text_block
node::node(
      string_store* strings
    , string const* value
    , private_constructor)
  : attributes_(strings)
  , name_(&empty_string)
  , value_(value)
  , children_(this)
{ }

// this constructor is used to parse an element from a string
node::node(
      string_store* strings
    , char const*& src
    , string& tmp
    , private_constructor)
  : attributes_(strings)
  , value_(&empty_string)
  , children_(this)
{
  size_t len;
  char c = *src;

  // extract the name
  len = 0;
  while (c != '\0' && c != ' ' && c != '>' && c != '/' && c != '\t' && c != '\n')
    c = *++src, ++len;
  if (len == 0)
    throw parse_error("element missing name", src);
  tmp.assign(src - len, len);
  name_ = &*attributes_.strings_->insert(tmp).first;

  // extract any attributes
  while (c != '\0')
  {
    if (c == ' ' || c == '\t' || c == '\n')
      c = *++src;
    else if (c == '/' || c == '>')
      break;
    else
    {
      attributes_.emplace_back(attributes_.strings_, src, tmp, attribute::private_constructor());
      c = *src;
    }
  }

  // is it an empty element?
  if (c == '/')
  {
    c = *++src;
    if (c != '>')
      throw parse_error("invalid empty element tag", src);
    ++src;
    return;
  }
  if (c != '>')
    throw parse_error("invalid start element tag", src);
  c = *++src;

  // extract children
  while (c != '\0')
  {
    if (c == ' ' || c == '\t' || c == '\n')
      c = *++src;
    else if (c == '<')
    {
      // end-tag, comment, processing instruction or child element?
      c = *++src;
      if (c == '/')
        break;
      if (c == '\0')
        throw parse_error("unexpected end of file", src);
      if (c == ' ' || c == '\t' || c == '\n')
        throw parse_error("unexpected whitespace in tag", src);
      if (c == '!')
        /* Note: CDATA will also take this path (but cause a parse error) */
        skip_comment(src);
      else if (c == '?')
        skip_pi(src);
      else
      {
        // if we get an element subsequent to initialzing as a text only element
        // then upgrade us to a mixed content element first
        if (value_ != &empty_string)
        {
          children_.emplace_back(attributes_.strings_, value_, private_constructor());
          value_ = &empty_string;
        }
        children_.emplace_back(attributes_.strings_, src, tmp, private_constructor());
      }
      c = *src;
    }
    else
    {
      // child text

      // if we are already a text node, we must have encountered a comment or
      // processing instruction after text. upgrade us to a mixed content element
      if (value_ != &empty_string)
        children_.emplace_back(attributes_.strings_, value_, private_constructor());

      // extract until end of the text
      len = 0;
      while (c != '\0' && c != '<')
        c = *++src, ++len;

      // strip trailing whitespace
      size_t strip = 0;
      for (; strip < len; ++strip)
      {
        char cc = *(src - strip - 1);
        if (cc != ' ' && cc != '\t' && cc != '\n')
          break;
      }
      tmp.assign(src - len, len - strip);
      resolve_entities(tmp, src);
      value_ = &*attributes_.strings_->insert(tmp).first;

      // is this a mixed content element?
      if (!children_.empty())
      {
        children_.emplace_back(attributes_.strings_, value_, private_constructor());
        value_ = &empty_string;
      }
    }
  }
  if (c == '\0')
    throw parse_error("unexpected end of file", src);
  c = *++src;

  // extract end tag
  len = 0;
  while (c != '\0' && c != '>' && c != ' ' && c != '\t' && c != '\n')
    c = *++src, ++len;
  if (len == 0)
    throw parse_error("invalid element end tag", src);
  tmp.assign(src - len, len);
  if (tmp != *name_)
    throw parse_error("element end tag mismatch", src);

  // extract the trailing >
  while (c != '\0' && (c == ' ' || c == '\t' || c == '\n'))
    c = *++src;
  if (c != '>')
    throw parse_error("invalid element end tag", src);
  ++src;
}

// this constructor is used when copying a node to a new document
node::node(string_store* strings, node const& rhs, private_constructor)
  : attributes_(strings)
  , name_(&*attributes_.strings_->insert(*rhs.name_).first)
  , value_(&*attributes_.strings_->insert(*rhs.value_).first)
  , children_(this)
{
  attributes_.reserve(rhs.attributes_.size());
  for (auto& i : rhs.attributes_)
    attributes_.emplace_back(strings, i, attribute::private_constructor());

  children_.reserve(rhs.children_.size());
  for (auto& i : rhs.children_)
    children_.emplace_back(strings, i, private_constructor());
}

// this constructor is used to insert a new node
node::node(
      string_store* strings
    , string const* name
    , bool
    , private_constructor)
  : attributes_(strings)
  , name_(name)
  , value_(&empty_string)
  , children_(this)
{ }

node::node(node&& rhs) noexcept
  : attributes_(std::move(rhs.attributes_))
  , name_(rhs.name_)
  , value_(rhs.value_)
  , children_(std::move(rhs.children_))
{
  children_.owner_ = this;
}

auto node::operator=(node&& rhs) noexcept -> node&
{
  attributes_ = std::move(rhs.attributes_);
  name_ = rhs.name_;
  value_ = rhs.value_;
  children_ = std::move(rhs.children_);
  children_.owner_ = this;
  return *this;
}

auto node::write(std::ostream& out) const -> void
{
  if (!children_.empty())
  {
    out << "<" << *name_ << " ";
    for (auto& i : attributes_)
      i.write(out);
    out << ">";
    for (auto& i : children_)
      i.write(out);
    out << "</" << *name_ << ">";
  }
  else if (!name_->empty())
  {
    out << "<" << *name_ << " ";
    for (auto& i : attributes_)
      i.write(out);
    if (value_->empty())
      out << "/>";
    else
    {
      out << ">";
      for (auto c : *value_)
        if (c == '<')
          out << "&lt;";
        else if (c == '>')
          out << "&gt;";
        else if (c == '&')
          out << "&amp;";
        else
          out << c;
      out << "</" << *name_ << ">";
    }
  }
  else
  {
    for (auto c : *value_)
      if (c == '<')
        out << "&lt;";
      else if (c == '>')
        out << "&gt;";
      else if (c == '&')
        out << "&amp;";
      else
        out << c;
  }
}

auto node::node_type() const -> enum type
{
  if (!children_.empty())
    return type::element_compound;
  if (!name_->empty())
    return type::element_simple;
  return type::text_block;
}

auto node::set_name(string const& val) -> void
{
  name_ = &*attributes_.strings_->insert(val).first;
}

auto node::set(string const& val) -> void
{
  if (!children_.empty())
    throw std::logic_error("xml logic error: attempt to set simple contents in element_compound");
  value_ = &*attributes_.strings_->insert(val).first;
}

document::document()
  : root_(&strings_, &*strings_.insert("root").first, true, node::private_constructor())
{

}

document::document(std::istream& source)
  : document(string(
            std::istreambuf_iterator<char>(source)
          , std::istreambuf_iterator<char>()).c_str())
{

}

document::document(std::istream&& source)
  : document(string(
            std::istreambuf_iterator<char>(source)
          , std::istreambuf_iterator<char>()).c_str())
{

}

document::document(string const& source)
  : document{source.c_str()}
{ }

document::document(char const* source) try
  : root_(&strings_, &empty_string, node::private_constructor())
{
  char const* src = source;
  string tmp;
  char c = *src;

#if 0 // enable to force checking of <?xml ... ?> at start of document
  // skip the xml declaration
  if (src[0] != '<' || src[1] != '?' || src[2] != 'x' || src[3] != 'm' || src[4] != 'l')
    throw parse_error("missing xml declaration", src);
  src += 5;
  skip_pi(src);
#endif

  // skip comments, processing instructions and the document type declaration
  while (c != '\0')
  {
    if (c == ' ' || c == '\t' || c == '\n')
    {
      c = *++src;
      continue;
    }

    if (c != '<')
      throw parse_error("unexpected character", src);

    c = *++src;
    if (c == '?')
      skip_pi(src);
    else if (c == '!' && src[1] == '-')
      skip_comment(src);
    else if (c == '!')
      skip_doctype(src);
    else
    {
      root_ = node(&strings_, src, tmp, node::private_constructor());
      return;
    }
    c = *src;
  }

  throw parse_error("missing root element", src);
}
catch (parse_error& err)
{
  err.determine_line_column(source);
  throw;
}

document::document(node const& root)
  : root_(&strings_, root, node::private_constructor())
{ }

document::document(document const& rhs)
  : strings_(rhs.strings_)
  , root_(&strings_, rhs.root_, node::private_constructor())
{ }

document::document(document&& rhs) noexcept
  : root_(std::move(rhs.root_))
{
  // only a.swap(b) is guarenteed to _not_ perform any copy/move/assignment on the
  // items held by a container. this ensures that the string pointers held by the
  // nodes and attributes shall not be invalidated (c++ 23.2.1 item 8)
  strings_.swap(rhs.strings_);
}

auto document::operator=(document&& rhs) noexcept -> document&
{
  strings_.swap(rhs.strings_);
  root_ = std::move(rhs.root_);
  return *this;
}

auto document::write(std::ostream& out) -> void
{
  out << "<?xml version=\"1.0\" encoding=\"UTF-8\" ?>";
  root_.write(out);
}

auto document::write(std::ostream&& out) -> void
{
  write(out);
}

// LCOV_EXCL_START
TEST_CASE("xml")
{
  SUBCASE("skip_pi")
  {
    auto test_skip_pi = [](char const* str)
    {
      auto pos = str;
      skip_pi(pos);
      return pos - str;
    };

    CHECK(test_skip_pi("foo?>bar") == 5);
    CHECK_THROWS_AS(test_skip_pi("foo? >"), parse_error const&);
  }

  SUBCASE("skip_comment")
  {
    auto test_skip_comment = [](char const* str)
    {
      auto pos = str;
      skip_comment(pos);
      return pos - str;
    };

    CHECK(test_skip_comment("<--foo-->bar") == 9);
    CHECK(test_skip_comment("<--foo >-< -->bar") == 14);
    CHECK_THROWS_AS(test_skip_comment("<a-foo-->bar"), parse_error const&);
    CHECK_THROWS_AS(test_skip_comment("<-afoo-->bar"), parse_error const&);
    CHECK_THROWS_AS(test_skip_comment("<--foo-- -->bar"), parse_error const&);
    CHECK_THROWS_AS(test_skip_comment("<--foo bar"), parse_error const&);
  }

  SUBCASE("skip_doctype")
  {
    auto test_skip_doctype = [](char const* str)
    {
      auto pos = str;
      skip_doctype(pos);
      return pos - str;
    };

    CHECK(test_skip_doctype("!DOCTYPE > after") == 10);
    CHECK(test_skip_doctype("!DOCTYPE foo 'ba\"r' \"a'bc\"> after") == 27);
    CHECK_THROWS_AS(test_skip_doctype("! OCTYPE > after"), parse_error const&);
    CHECK_THROWS_AS(test_skip_doctype("!D CTYPE > after"), parse_error const&);
    CHECK_THROWS_AS(test_skip_doctype("!DO TYPE > after"), parse_error const&);
    CHECK_THROWS_AS(test_skip_doctype("!DOC YPE > after"), parse_error const&);
    CHECK_THROWS_AS(test_skip_doctype("!DOCT PE > after"), parse_error const&);
    CHECK_THROWS_AS(test_skip_doctype("!DOCTY E > after"), parse_error const&);
    CHECK_THROWS_AS(test_skip_doctype("!DOCTYP  > after"), parse_error const&);
    CHECK_THROWS_AS(test_skip_doctype("!DOCTYPE noterm"), parse_error const&);
  }

  SUBCASE("resolve_entities")
  {
    auto test_resolve_entities = [](string str)
    {
      resolve_entities(str, nullptr);
      return str;
    };

    CHECK(test_resolve_entities("foo&lt;bar") == "foo<bar");
    CHECK(test_resolve_entities("foo&gt;bar") == "foo>bar");
    CHECK(test_resolve_entities("foo&amp;bar") == "foo&bar");
    CHECK(test_resolve_entities("foo&apos;bar") == "foo'bar");
    CHECK(test_resolve_entities("foo&quot;bar") == "foo\"bar");
    CHECK_THROWS_AS(test_resolve_entities("foo&bad"), parse_error const&);
    CHECK_THROWS_AS(test_resolve_entities("foo&bad;bar"), parse_error const&);
  }

  SUBCASE("path_error")
  {
    io::xml::path_error err1{"mynode", false};
    CHECK(err1.name() == "mynode");
    CHECK(err1.is_attribute() == false);
    CHECK(string(err1.what()) == "xml path error: requested child node 'mynode' does not exist");

    io::xml::path_error err2{"myatt", true};
    CHECK(err2.name() == "myatt");
    CHECK(err2.is_attribute() == true);
    CHECK(string(err2.what()) == "xml path error: requested child attribute 'myatt' does not exist");
  }

  SUBCASE("attribute_store")
  {
    io::xml::document doc;
    auto& attrs = doc.root().attributes();
    auto const& cattrs = attrs;

    // insert()
    CHECK_NOTHROW(attrs.insert("foo")->set("bar"));
    CHECK_NOTHROW(attrs.insert("abc"s)->set("def"));

    // operator[]
    CHECK(attrs["foo"].get() == "bar");
    CHECK(cattrs["abc"].get() == "def");
    CHECK(attrs["foo"s].get() == "bar");
    CHECK(cattrs["abc"s].get() == "def");
    CHECK_THROWS_AS(attrs["bad"], io::xml::path_error const&);
    CHECK_THROWS_AS(cattrs["bad"], io::xml::path_error const&);
    CHECK_THROWS_AS(attrs["bad"s], io::xml::path_error const&);
    CHECK_THROWS_AS(cattrs["bad"s], io::xml::path_error const&);

    // find()
    CHECK(attrs.find("foo") != attrs.end());
    CHECK(cattrs.find("abc") != attrs.end());
    CHECK(attrs.find("foo"s) != attrs.end());
    CHECK(cattrs.find("abc"s) != attrs.end());
    CHECK(attrs.find("bad") == attrs.end());
    CHECK(cattrs.find("bad") == attrs.end());
    CHECK(attrs.find("bad"s) == attrs.end());
    CHECK(cattrs.find("bad"s) == attrs.end());

    // find_or_insert()
    auto iabc = attrs.find("abc");
    CHECK(attrs.find_or_insert("abc") == iabc);
    CHECK_NOTHROW(attrs.find_or_insert("new")->set("val"));
    CHECK(attrs["new"].get() == "val");

    iabc = attrs.find("abc");
    CHECK(attrs.find_or_insert("abc"s) == iabc);
    CHECK_NOTHROW(attrs.find_or_insert("new2"s)->set("val"));
    CHECK(attrs["new2"].get() == "val");
  }

  SUBCASE("node_store")
  {
    io::xml::document doc;
    auto& nodes = doc.root().children();
    auto const& cnodes = nodes;

    // push_back()
    CHECK_NOTHROW(nodes.push_back("foo"));
    CHECK_NOTHROW(nodes.push_back(static_cast<string const&>("abc"s))->set("def"));
    CHECK_THROWS(nodes["abc"].children().push_back("bad"));
    CHECK_THROWS(nodes["abc"].children().push_back(static_cast<string const&>("bad"s)));

    // operator[]
    CHECK(nodes["foo"].name() == "foo");
    CHECK(cnodes["abc"].get() == "def");
    CHECK(nodes["foo"s].name() == "foo");
    CHECK(cnodes["abc"s].get() == "def");
    CHECK_THROWS_AS(nodes["bad"], io::xml::path_error const&);
    CHECK_THROWS_AS(cnodes["bad"], io::xml::path_error const&);
    CHECK_THROWS_AS(nodes["bad"s], io::xml::path_error const&);
    CHECK_THROWS_AS(cnodes["bad"s], io::xml::path_error const&);

    // find()
    CHECK(nodes.find("foo") != nodes.end());
    CHECK(cnodes.find("abc") != nodes.end());
    CHECK(nodes.find("foo"s) != nodes.end());
    CHECK(cnodes.find("abc"s) != nodes.end());
    CHECK(nodes.find("bad") == nodes.end());
    CHECK(cnodes.find("bad") == nodes.end());
    CHECK(nodes.find("bad"s) == nodes.end());
    CHECK(cnodes.find("bad"s) == nodes.end());

    // insert()
    io::xml::node_store::iterator iins;
    CHECK_NOTHROW((iins = nodes.insert(nodes.begin(), "start")));
    CHECK(iins == nodes.begin());
    CHECK_NOTHROW((iins = nodes.insert(nodes.end(), "end")));
    CHECK(iins == nodes.end() - 1);
    CHECK_THROWS(nodes["abc"].children().insert(nodes["abc"].children().begin(), "bad"));
    CHECK_THROWS(nodes["abc"].children().insert(nodes["abc"].children().end(), static_cast<string const&>("bad"s)));

    // find_or_insert()
    auto iabc = nodes.find("abc");
    CHECK(nodes.find_or_insert("abc") == iabc);
    CHECK_NOTHROW(nodes.find_or_insert("new")->set("val"));
    CHECK(nodes["new"].get() == "val");

    iabc = nodes.find("abc");
    CHECK(nodes.find_or_insert("abc"s) == iabc);
    CHECK_NOTHROW(nodes.find_or_insert("new2"s)->set("val"));
    CHECK(nodes["new2"].get() == "val");
  }

  SUBCASE("parse")
  {
    // document
    CHECK_THROWS_AS(xml::document{""}, parse_error const&);                         // missing root element
    CHECK_THROWS_AS(xml::document{"bad"}, parse_error const&);                      // unexpected character

    // attribute
    CHECK_THROWS_AS(xml::document{"<root =\"bad\"></root>"}, parse_error const&);   // attribute missing name
    CHECK_THROWS_AS(xml::document{"<root bad></root>"}, parse_error const&);        // attribute missing =
    CHECK_THROWS_AS(xml::document{"<root foo=bad></root>"}, parse_error const&);    // attribute missing start "
    CHECK_THROWS_AS(xml::document{"<root foo=\"bad></root>"}, parse_error const&);  // attribute missing end "

    // node
    CHECK_THROWS_AS(xml::document{"<root>< foo /></root>"}, parse_error const&);    // unexpected whitespace in tag
    CHECK_THROWS_AS(xml::document{"<root><foo / ></root>"}, parse_error const&);    // invalid empty element
    CHECK_THROWS_AS(xml::document{"<root><foo<something"}, parse_error const&);     // invalid start tag
    CHECK_THROWS_AS(xml::document{"<root><foo><"}, parse_error const&);             // unexpected EOF
    CHECK_THROWS_AS(xml::document{"<root><foo>asdf"}, parse_error const&);          // unexpected EOF
    CHECK_THROWS_AS(xml::document{"<root><foo></>"}, parse_error const&);           // invalid end tag
    CHECK_THROWS_AS(xml::document{"<root><foo></ foo>"}, parse_error const&);       // invalid end tag
    CHECK_THROWS_AS(xml::document{"<root><foo></\tfoo>"}, parse_error const&);      // invalid end tag
    CHECK_THROWS_AS(xml::document{"<root><foo></\nfoo>"}, parse_error const&);      // invalid end tag
    CHECK_THROWS_AS(xml::document{"<root><foo></bar>"}, parse_error const&);        // end tag mismatch
    CHECK_THROWS_AS(xml::document{"<root><foo></foo bad>"}, parse_error const&);    // invalid end tag

    auto test_str = R"(
      <?xml version="1.0" ?>
      <!DOCTYPE blah>
      <!-- comment -->
      <root>
        <foo hello="world" abc = "def" funny="sad">
          <!-- inner comment -->
        </foo>
        <bar empty="attribute" />
        <deep>
          <deeper>
            <deepest>text mode</deepest>
          </deeper>
          <blah></blah >
          <mixed>some text<embedded foo="bar"/>more text</mixed>
          <mixed2>text<!--comment -->text</mixed2>
        </deep>
      </root>)";

    xml::document doc{test_str};

    CHECK(doc.root()["deep"]["deeper"]["deepest"].get() == "text mode");
    CHECK(doc.root()["foo"]("hello").get() == "world");
  }
}
// LCOV_EXCL_STOP
