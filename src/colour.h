/*------------------------------------------------------------------------------
 * Bureau of Meteorology C++ Utility Library
 *
 * Copyright 2014 Mark Curtis
 * Copyright 2016 Commonwealth of Australia, Bureau of Meteorology
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *----------------------------------------------------------------------------*/
#pragma once

#include "angle.h"
#include "string_utils.h"

namespace bom
{
  struct colour
  {
    using value_type = uint8_t;

    uint8_t r, g, b, a;

    /// Allow uninitialized construction
    colour() noexcept = default;

    /// Construct a colour from r, g, b, a values
    constexpr colour(uint8_t r, uint8_t g, uint8_t b, uint8_t a) noexcept : r(r), g(g), b(b), a(a) { }

    explicit operator uint8_t const*() const
    {
      return &r;
    }

    /// Convert a colour to a 32 bit RGBA byte array
    auto as_uint8s(uint8_t rgba[4]) const -> void;

    /// Convert a colour to HSV values
    auto as_hsv(angle& hue, float& saturation, float& value, float& alpha) const -> void;

    /// Construct a colour from HSV parameters
    static auto hsv(angle hue, float saturation, float value, float alpha = 1.0f) -> colour;

    /// Interpolate between colours in RGB space
    static auto lerp_rgb(colour const& lhs, colour const& rhs, float fraction) -> colour;

    /// Interpolate between colours in HSV space
    static auto lerp_hsv(colour const& lhs, colour const& rhs, float fraction) -> colour;

    // TODO - it would be nice to be able to interpolate in HCL as well
    //      - HCL gives better intermediate colours for human vision
  };

  inline auto operator==(colour lhs, colour rhs) -> bool
  {
    return lhs.r == rhs.r && lhs.g == rhs.g && lhs.b == rhs.b && lhs.a == rhs.a;
  }

  inline auto operator!=(colour lhs, colour rhs) -> bool
  {
    return lhs.r != rhs.r || lhs.g != rhs.g || lhs.b != rhs.b || lhs.a != rhs.a;
  }

  template <>
  struct string_conversions<colour>
  {
    static auto read_string(char const* str, size_t len) -> colour;
    static auto write_string(colour const& val, string& str) -> void;
  };

  template <>
  struct allow_simple_string_conversions<colour>
  {
    static constexpr bool value = true;
  };

  namespace standard_colours
  {
    constexpr colour clear   = { 0x00, 0x00, 0x00, 0x00 };
    constexpr colour white   = { 0xff, 0xff, 0xff, 0xff };
    constexpr colour black   = { 0x00, 0x00, 0x00, 0xff };
    constexpr colour red     = { 0xff, 0x00, 0x00, 0xff };
    constexpr colour green   = { 0x00, 0xff, 0x00, 0xff };
    constexpr colour blue    = { 0x00, 0x00, 0xff, 0xff };
    constexpr colour cyan    = { 0x00, 0xff, 0xff, 0xff };
    constexpr colour magenta = { 0xff, 0x00, 0xff, 0xff };
    constexpr colour yellow  = { 0xff, 0xff, 0x00, 0xff };
  }
}

namespace fmt
{
  template <>
  struct formatter<bom::colour>
  {
    template <typename ParseContext>
    constexpr auto parse(ParseContext& ctx)
    {
      return ctx.begin();
    }

    template <typename FormatContext>
    auto format(bom::colour val, FormatContext& ctx) -> decltype(ctx.out())
    {
      // this generates warnings with libfmt 9.x, but I have no idea why since it seems to work fine
      fmt::format_to(ctx.out(), "{:02x}{:02x}{:02x}{:02x}", val.r, val.g, val.b, val.a);
      return ctx.out();
    }
  };
}
