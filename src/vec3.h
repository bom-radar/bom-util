/*------------------------------------------------------------------------------
 * Bureau of Meteorology C++ Utility Library
 *
 * Copyright 2014 Mark Curtis
 * Copyright 2016 Commonwealth of Australia, Bureau of Meteorology
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *----------------------------------------------------------------------------*/
#pragma once

#include "angle.h"
#include "string_utils.h"
#include "traits.h"

#include <cmath>
#include <istream>
#include <ostream>

namespace bom
{
  template <typename T>
  struct vec3
  {
    using value_type = T;

    T x, y, z;

    constexpr vec3() = default;

    constexpr vec3(T x, T y, T z) : x(x), y(y), z(z) { }

    template <typename U>
    constexpr operator vec3<U>() const
    {
      return vec3<U>(static_cast<U>(x), static_cast<U>(y), static_cast<U>(z));
    }

    template <typename U = T, typename = typename std::enable_if<!std::is_floating_point<U>::value>::type>
    constexpr auto operator==(vec3 const& rhs) const
    {
      return x == rhs.x && y == rhs.y && z == rhs.z;
    }

    template <typename U = T, typename = typename std::enable_if<!std::is_floating_point<U>::value>::type>
    constexpr auto operator!=(vec3 const& rhs) const
    {
      return x != rhs.x || y != rhs.y || z != rhs.z;
    }

    auto& operator+=(vec3 const& rhs)
    {
      x += rhs.x;
      y += rhs.y;
      z += rhs.z;
      return *this;
    }

    auto& operator-=(vec3 const& rhs)
    {
      x -= rhs.x;
      y -= rhs.y;
      z -= rhs.z;
      return *this;
    }

    template <typename U>
    auto& operator*=(U const& rhs)
    {
      x *= rhs;
      y *= rhs;
      z *= rhs;
      return *this;
    }

    template <typename U>
    auto& operator/=(U const& rhs)
    {
      x /= rhs;
      y /= rhs;
      z /= rhs;
      return *this;
    }

    auto& normalize()
    {
      if (auto l = length())
      {
        x /= l;
        y /= l;
        z /= l;
      }
      else
      {
        x = 0.0;
        y = 0.0;
        z = 0.0;
      }
      return *this;
    }

    auto normal() const
    {
      vec3 ret;
      if (auto l = length())
      {
        ret.x = x / l;
        ret.y = y / l;
        ret.z = z / l;
      }
      else
      {
        ret.x = 0;
        ret.y = 0;
        ret.z = 0;
      }
      return ret;
    }

    constexpr auto dot(vec3 const& rhs) const
    {
      return x * rhs.x + y * rhs.y + z * rhs.z;
    }

    constexpr auto cross(vec3 const& rhs) const
    {
      return vec3(y * rhs.z - z * rhs.y, z * rhs.x - x * rhs.z, x * rhs.y - y * rhs.x);
    }

    constexpr auto scale(vec3 const& rhs) const
    {
      return vec3(x * rhs.x, y * rhs.y, z * rhs.z);
    }

    constexpr auto length_sqr() const
    {
      return x * x + y * y + z * z;
    }

    auto length() const
    {
      return std::sqrt(x * x + y * y + z * z);
    }

    auto angle(const vec3& rhs) const
    {
      return acos(dot(rhs) / (length() * rhs.length()));
    }

    template <typename T1, typename T2>
    friend constexpr auto operator+(vec3<T1> const& lhs, vec3<T2> const& rhs) -> vec3<decltype(lhs.x + rhs.x)>;

    template <typename T1, typename T2>
    friend constexpr auto operator-(vec3<T1> const& lhs, vec3<T2> const& rhs) -> vec3<decltype(lhs.x - rhs.x)>;

    template <typename T1, typename U>
    friend constexpr auto operator*(vec3<T1> const& lhs, U rhs) -> vec3<decltype(lhs.x * rhs)>;

    template <typename T1, typename U>
    friend constexpr auto operator*(U lhs, vec3<T1> const& rhs) -> vec3<decltype(lhs * rhs.x)>;

    template <typename T1, typename U>
    friend constexpr auto operator/(vec3<T1> const& lhs, U rhs) -> vec3<decltype(lhs.x / rhs)>;

    template <typename T1>
    friend auto operator>>(std::istream& lhs, vec3<T1>& rhs) -> std::istream&;

    template <typename T1>
    friend auto operator<<(std::ostream& lhs, vec3<T1> const& rhs) -> std::ostream&;
  };

  template <typename T1, typename T2>
  inline constexpr auto operator+(vec3<T1> const& lhs, vec3<T2> const& rhs) -> vec3<decltype(lhs.x + rhs.x)>
  {
    return vec3<decltype(lhs.x + rhs.x)>(lhs.x + rhs.x, lhs.y + rhs.y, lhs.z + rhs.z);
  }

  template <typename T1, typename T2>
  inline constexpr auto operator-(vec3<T1> const& lhs, vec3<T2> const& rhs) -> vec3<decltype(lhs.x - rhs.x)>
  {
    return vec3<decltype(lhs.x - rhs.x)>(lhs.x - rhs.x, lhs.y - rhs.y, lhs.z - rhs.z);
  }

  template <typename T1, typename U>
  inline constexpr auto operator*(vec3<T1> const& lhs, U rhs) -> vec3<decltype(lhs.x * rhs)>
  {
    return vec3<decltype(lhs.x * rhs)>(lhs.x * rhs, lhs.y * rhs, lhs.z * rhs);
  }

  template <typename T1, typename U>
  inline constexpr auto operator*(U lhs, vec3<T1> const& rhs) -> vec3<decltype(lhs * rhs.x)>
  {
    return vec3<decltype(lhs * rhs.x)>(lhs * rhs.x, lhs * rhs.y, lhs * rhs.z);
  }

  template <typename T1, typename U>
  inline constexpr auto operator/(vec3<T1> const& lhs, U rhs) -> vec3<decltype(lhs.x / rhs)>
  {
    return vec3<decltype(lhs.x / rhs)>(lhs.x / rhs, lhs.y / rhs, lhs.z / rhs);
  }

  template <typename T1>
  inline auto operator>>(std::istream& lhs, vec3<T1>& rhs) -> std::istream&
  {
    return lhs >> rhs.x >> rhs.y >> rhs.z;
  }

  template <typename T1>
  inline auto operator<<(std::ostream& lhs, vec3<T1> const& rhs) -> std::ostream&
  {
    return lhs << rhs.x << ' ' << rhs.y << ' ' << rhs.z;
  }

  template <typename T>
  struct string_conversions<vec3<T>>
  {
    static auto read_string(char const* str, size_t len) -> vec3<T>
    try
    {
      auto end = str;
      while (end < str + len && !std::isspace(*end))
        ++end;

      auto next = end;
      while (next < str + len && std::isspace(*next))
        ++next;

      auto end2 = next;
      while (end2 < str + len && !std::isspace(*end2))
        ++end2;

      auto next2 = end2;
      while (next2 < str + len && std::isspace(*next2))
        ++next2;

      return {
          string_conversions<T>::read_string(str, end - str)
        , string_conversions<T>::read_string(next, end2 - next)
        , string_conversions<T>::read_string(next2, len - (next2 - str))
        };
    }
    catch (...)
    {
      std::throw_with_nested(string_conversion_error{"vec3", string(str, len)});
    }

    static auto write_string(vec3<T> val, string& str) -> void
    {
      string_conversions<T>::write_string(val.x, str);
      str.push_back(' ');
      string_conversions<T>::write_string(val.y, str);
      str.push_back(' ');
      string_conversions<T>::write_string(val.z, str);
    }
  };

  template <typename T>
  struct allow_simple_string_conversions<vec3<T>>
  {
    static constexpr bool value = true;
  };

  using vec3i = vec3<int>;
  using vec3z = vec3<size_t>;
  using vec3f = vec3<float>;
  using vec3d = vec3<double>;
}

namespace fmt
{
  template <typename T, typename Char>
  struct formatter<bom::vec3<T>, Char> : formatter<T, Char>
  {
    template <typename FormatContext>
    auto format(bom::vec3<T> val, FormatContext& ctx)
    {
      formatter<T, Char>::format(val.x, ctx);
      fmt::format_to(ctx.out(), " ");
      formatter<T, Char>::format(val.y, ctx);
      fmt::format_to(ctx.out(), " ");
      formatter<T, Char>::format(val.z, ctx);
      return ctx.out();
    }
  };
}
