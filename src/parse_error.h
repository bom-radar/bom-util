/*------------------------------------------------------------------------------
 * Bureau of Meteorology C++ Utility Library
 *
 * Copyright 2014 Mark Curtis
 * Copyright 2016 Commonwealth of Australia, Bureau of Meteorology
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *----------------------------------------------------------------------------*/
#pragma once

#include "string_utils.h"
#include <exception>
#include <istream>

namespace bom
{
  /// Generic parser error
  class parse_error : public std::exception
  {
  public:
    /// Create a parser error
    /**
     * \param desc  Error description
     * \param where Optional pointer to location of error in string being parsed
     */
    parse_error(string desc, char const* where = nullptr);

    /// Create a parser error
    parse_error(string desc, int line, int column = 0);

    /// Create a parser error
    parse_error(string desc, std::istream& in);

    /// Get the full error detail string
    virtual auto what() const noexcept -> char const*;

    /// Get the error description
    auto description() const -> string const&
    {
      return desc_;
    }

    /// Get the pointer to the error location
    auto where() const -> char const*
    {
      return where_;
    }

    /// Get the line number of the parse error location (0 if not set)
    auto line() const -> int
    {
      return line_;
    }

    /// Get the column number of the parse error location (0 if not set)
    auto column() const -> int
    {
      return column_;
    }

    /// Determine the line and column number corresponding to the location of the error
    /**
     * If set, 'where' must point to a character of 'str'.  Violating this condition results in
     * undefined behaviour.
     *
     * \param src  Original string into which 'where' is a pointer
     */
    auto determine_line_column(char const* src) -> void;

  private:
    auto build_what_string() -> void;

  private:
    string      desc_;
    char const* where_;
    int         line_;
    int         column_;
    string      what_;
  };
}
