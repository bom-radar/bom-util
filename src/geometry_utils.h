/*------------------------------------------------------------------------------
 * Bureau of Meteorology C++ Utility Library
 *
 * Copyright 2014 Mark Curtis
 * Copyright 2016 Commonwealth of Australia, Bureau of Meteorology
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *----------------------------------------------------------------------------*/
#pragma once

#include "raii.h"
#include "vec2.h"

#include <vector>

namespace bom
{
  /// Determine the minimum distance between a point and a line segment
  template <typename T>
  auto minimum_distance_to_line_segment_sqr(T const& line_a, T const& line_b, T const& point) -> typename T::value_type
  {
    auto line_delta = line_b - line_a;
    auto l2 = line_delta.length_sqr();

    // special case - line ends are coincident
    // potential floating point compare here is okay - just avoiding divide by 0
    if (l2 == 0)
      return (point - line_b).length_sqr();

    // find proportionally how far along the line segment the point is
    auto t = (point - line_a).dot(line_b - line_a) / static_cast<float>(l2);
    if (t < 0.0f)
      return (point - line_a).length_sqr();
    if (t > 1.0f)
      return (point - line_b).length_sqr();

    // find point at proportional distance along the line and return distance to it
    auto p = line_a + t * line_delta;
    return (point - p).length_sqr();
  }

  /// Simple 2D polygon
  class polygon
  {
  public:
    /// Winding direction for polygon points
    enum class point_winding
    {
        clockwise           ///< Polygon is wound clockwise
      , counter_clockwise   ///< Polygon is wound counter-clockwise
    };

  public:
    polygon()
    { }

    polygon(std::initializer_list<vec2f> points)
      : points_{std::move(points)}
    { }

    auto size() const -> size_t { return points_.size(); }

    auto points() -> std::vector<vec2f>&  { return points_; }

    vec2f& operator[](size_t i) { return points_[i]; }
    vec2f const& operator[](size_t i) const { return points_[i]; }

    /// Caculate the direction in which the polygon points are wound
    auto winding() const -> point_winding;

    /// Ensure the polygon points are wound in the specified direction (inverts them if needed)
    auto set_winding(point_winding dir) -> void;

    /// Remove a hole from the polygon by introducing a zero width cut between an outer vertex and the hole
    /** If you are removing multiple holes, it is best to remove them such that the hole with the largest X
     *  coordinate in any of its points is removed first.
     *
     *  NOTE: The hole polygon must be wound in the opposite direction to this polygon.  Since checking and changing
     *  the winding order is potentially expensive, this is left as a responsibility for the user to ensure.
     */
    auto remove_hole(polygon const& hole) -> bool;

    /// Triangulate the polygon into an index buffer
    /** This function is implemented via the ear clipping method.  It's not particularly fast but it is relatively
     *  simple and produces acceptable results.  Feel free to implement something better!
     *
     *  The base_index parameter will be added to all of the produced index values.  This allows for easy packing
     *  of multiple polygons into a single index buffer.
     *
     *  The function returns false if the triangulation fails.
     */
    auto triangulate(std::vector<uint16_t>& index, uint16_t base_index) -> bool;

  private:
    std::vector<vec2f> points_;
  };

  namespace detail
  {
    template <typename T>
    inline auto min_dist_sqr(T s, T e, T p) -> typename T::value_type
    {
      auto const l2 = (e - s).length_sqr();
      if (l2 == 0.0)
        return (p - e).length_sqr();
      auto const t = (p - s).dot(e - s) / l2;
      if (t < 0.0)
        return (p - s).length_sqr();
      else if (t > 1.0)
        return (p - e).length_sqr();
      else
        return (p - (s + t * (e - s))).length_sqr();
    }

    template <typename Iter, typename T, typename GetPoint>
    auto douglas_peucker(Iter begin, GetPoint get_point, bool* keep, T max_sqr, size_t l, size_t r) -> void
    {
      // mark left and right as keepers
      keep[l] = true;
      keep[r] = true;

      // find point with greatest distance from segment l -> r
      auto max_d = 0.0;
      auto max_i = size_t(0);
      for (size_t i = l + 1; i <= r - 1; ++i)
      {
        auto d = min_dist_sqr(get_point(begin + l), get_point(begin + r), get_point(begin + i));
        if (d > max_d)
        {
          max_d = d;
          max_i = i;
        }
      }

      // if the worst point is too far away, recurse
      if (max_d > max_sqr)
      {
        douglas_peucker(begin, get_point, keep, max_sqr, l, max_i);
        douglas_peucker(begin, get_point, keep, max_sqr, max_i, r);
      }
    }
  }

  /// Reduce the number of points in a line string / line loop / polygon
  /** This function uses the Douglas Peucker algorithm to optimally remove points from a polygon such that the
   *  deviation from the original shape is limited.  The maximum deviation allowed is set by the max_deviation
   *  parameter.
   *
   *  The function takes an iterator to the begin and end of the sequence of the points.  These iterators must
   *  be random access iterators.  The function transforms the range [begin, end) so that it reflects the
   *  optimized sequence and returns an iterator pointing to the new end of the sequence.  Values that fall between
   *  the returned iterator and the original end are left in a valid but unspecified state and may be safely
   *  removed from the owning container.  (ie: This function works like std::remove).
   *
   *  The third type parameter may be used to provide a functor which is used to extract the point vector from
   *  the iterator type.  The default is a functor which simply dereferences the iterator.  */
  template <typename Iter, typename T, typename GetPoint = dereference<Iter>>
  auto reduce_points(Iter begin, Iter end, T max_deviation, GetPoint get_point = {}) -> Iter
  {
    // determine points to keep
    size_t size = end - begin;
    unique_ptr<bool[]> keep{new bool[size]};
    std::fill(keep.get(), keep.get() + size, false);
    detail::douglas_peucker(begin, get_point, keep.get(), max_deviation * max_deviation, 0, size - 1);

    // shift them to the start of the container
    size_t o = 0;
    for (size_t i = 0; i < size; ++i)
    {
      if (keep[i])
      {
        *(begin + o) = std::move(*(begin + i));
        ++o;
      }
    }
    return begin + o;
  }
}
