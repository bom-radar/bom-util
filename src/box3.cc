/*------------------------------------------------------------------------------
 * Bureau of Meteorology C++ Utility Library
 *
 * Copyright 2014 Mark Curtis
 * Copyright 2016 Commonwealth of Australia, Bureau of Meteorology
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *----------------------------------------------------------------------------*/
#include "config.h"
#include "box3.h"
#include "unit_test.h"

using namespace bom;

// LCOV_EXCL_START
TEST_CASE("box3")
{
  SUBCASE("basic")
  {
    box3i a{vec3i{0, 0, 0}, vec3i{10, 10, 10}};
    CHECK(a.min == vec3i{0, 0, 0});
    CHECK(a.max == vec3i{10, 10, 10});

    CHECK(a.contains(vec3i{5,5,5}));
    CHECK(!a.contains(vec3i{-5,-5,-5}));

    a.expand(vec3i{100, 100, 100});
    CHECK((a.min == vec3i{0, 0, 0}));
    CHECK((a.max == vec3i{100, 100, 100}));

    a.expand(vec3i{-100, -100, -100});
    CHECK((a.min == vec3i{-100, -100, -100}));
    CHECK((a.max == vec3i{100, 100, 100}));

    CHECK(!a.contains(vec3i{-101,-101,-101}));
    CHECK(!a.contains(vec3i{-101,-101,0}));
    CHECK(!a.contains(vec3i{-101,-101,101}));
    CHECK(!a.contains(vec3i{-101,0,-101}));
    CHECK(!a.contains(vec3i{-101,0,0}));
    CHECK(!a.contains(vec3i{-101,0,101}));
    CHECK(!a.contains(vec3i{-101,101,-101}));
    CHECK(!a.contains(vec3i{-101,101,0}));
    CHECK(!a.contains(vec3i{-101,101,101}));

    CHECK(!a.contains(vec3i{0,-101,-101}));
    CHECK(!a.contains(vec3i{0,-101,0}));
    CHECK(!a.contains(vec3i{0,-101,101}));
    CHECK(!a.contains(vec3i{0,0,-101}));
    CHECK(a.contains(vec3i{0,0,0}));
    CHECK(!a.contains(vec3i{0,0,101}));
    CHECK(!a.contains(vec3i{0,101,-101}));
    CHECK(!a.contains(vec3i{0,101,0}));
    CHECK(!a.contains(vec3i{0,101,101}));

    CHECK(!a.contains(vec3i{101,-101,-101}));
    CHECK(!a.contains(vec3i{101,-101,0}));
    CHECK(!a.contains(vec3i{101,-101,101}));
    CHECK(!a.contains(vec3i{101,0,-101}));
    CHECK(!a.contains(vec3i{101,0,0}));
    CHECK(!a.contains(vec3i{101,0,101}));
    CHECK(!a.contains(vec3i{101,101,-101}));
    CHECK(!a.contains(vec3i{101,101,0}));
    CHECK(!a.contains(vec3i{101,101,101}));

    CHECK(!box3i::empty_box().contains(vec3i{0,0,0}));

    box3i b = box3i::empty_box();
    b.expand(vec3i{10,0,0});
    b.expand(vec3i{0,10,0});
    b.expand(vec3i{0,0,10});
    CHECK(b.min == vec3i{0, 0, 0});
    CHECK(b.max == vec3i{10, 10, 10});
  }
}
// LCOV_EXCL_STOP
