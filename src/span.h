/*------------------------------------------------------------------------------
 * Bureau of Meteorology C++ Utility Library
 *
 * Copyright 2016 Commonwealth of Australia, Bureau of Meteorology
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *----------------------------------------------------------------------------*/
#pragma once

/* The contents of this file is a cut-down version of the span template from Microsoft's
 * Guidelines Support Library (github.com/Microsoft/gsl).  The original copyright notice
 * and license is preserved below.  Remaining parts of the code are heavily modified.
 *
 * If the span template is integrated into the standard library, then this implementation
 * should be removed in favour of a simple "using span = std::span;" to bring it into
 * the bom namespace.
 */

// Copyright (c) 2015 Microsoft Corporation. All rights reserved.
//
// This code is licensed under the MIT License (MIT).
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

#include <array>
#include <iterator>
#include <limits>
#include <memory>
#include <stdexcept>
#include <type_traits>
#include <utility>

namespace bom
{
  constexpr const std::ptrdiff_t dynamic_extent = -1;

  template <class ElementType, std::ptrdiff_t Extent = dynamic_extent>
  class span;

  namespace detail
  {
    template <class T>
    struct is_span_oracle : std::false_type
    { };

    template <class ElementType, std::ptrdiff_t Extent>
    struct is_span_oracle<span<ElementType, Extent>> : std::true_type
    { };

    template <class T>
    struct is_span : public is_span_oracle<std::remove_cv_t<T>>
    { };

    template <class T>
    struct is_std_array_oracle : std::false_type
    { };

    template <class ElementType, std::size_t Extent>
    struct is_std_array_oracle<std::array<ElementType, Extent>> : std::true_type
    { };

    template <class T>
    struct is_std_array : public is_std_array_oracle<std::remove_cv_t<T>>
    { };

    template <std::ptrdiff_t From, std::ptrdiff_t To>
    struct is_allowed_extent_conversion : public std::integral_constant<bool, From == To || From == dynamic_extent || To == dynamic_extent>
    { };

    template <class From, class To>
    struct is_allowed_element_type_conversion : public std::integral_constant<bool, std::is_convertible<From (*)[], To (*)[]>::value>
    { };

    template <class Span, bool IsConst>
    class span_iterator
    {
    private:
      using element_type_ = typename Span::element_type;

    public:
      using iterator_category = std::random_access_iterator_tag;
      using value_type = std::remove_cv_t<element_type_>;
      using difference_type = typename Span::index_type;

      using reference = std::conditional_t<IsConst, const element_type_, element_type_>&;
      using pointer = std::add_pointer_t<reference>;

      span_iterator() = default;

      constexpr span_iterator(const Span* span, typename Span::index_type index) noexcept
        : span_(span), index_(index)
      { }

      friend span_iterator<Span, true>;
      template<bool B, std::enable_if_t<!B && IsConst>* = nullptr>
        constexpr span_iterator(const span_iterator<Span, B>& other) noexcept
        : span_iterator(other.span_, other.index_)
      { }

      constexpr reference operator*() const noexcept
      {
        return (*span_)[index_];
      }

      constexpr pointer operator->() const noexcept
      {
        return span_->data() + index_;
      }

      constexpr span_iterator& operator++() noexcept
      {
        ++index_;
        return *this;
      }

      constexpr span_iterator operator++(int) noexcept
      {
        auto ret = *this;
        ++(*this);
        return ret;
      }

      constexpr span_iterator& operator--() noexcept
      {
        --index_;
        return *this;
      }

      constexpr span_iterator operator--(int) noexcept
      {
        auto ret = *this;
        --(*this);
        return ret;
      }

      constexpr span_iterator operator+(difference_type n) const noexcept
      {
        auto ret = *this;
        return ret += n;
      }

      constexpr span_iterator& operator+=(difference_type n) noexcept
      {
        index_ += n;
        return *this;
      }

      constexpr span_iterator operator-(difference_type n) const noexcept
      {
        auto ret = *this;
        return ret -= n;
      }

      constexpr span_iterator& operator-=(difference_type n) noexcept { return *this += -n; }

      constexpr difference_type operator-(const span_iterator& rhs) const noexcept
      {
        return index_ - rhs.index_;
      }

      constexpr reference operator[](difference_type n) const noexcept
      {
        return *(*this + n);
      }

      constexpr friend bool operator==(const span_iterator& lhs, const span_iterator& rhs) noexcept
      {
        return lhs.span_ == rhs.span_ && lhs.index_ == rhs.index_;
      }

      constexpr friend bool operator!=(const span_iterator& lhs, const span_iterator& rhs) noexcept
      {
        return !(lhs == rhs);
      }

      constexpr friend bool operator<(const span_iterator& lhs, const span_iterator& rhs) noexcept
      {
        return lhs.index_ < rhs.index_;
      }

      constexpr friend bool operator<=(const span_iterator& lhs, const span_iterator& rhs) noexcept
      {
        return !(rhs < lhs);
      }

      constexpr friend bool operator>(const span_iterator& lhs, const span_iterator& rhs) noexcept
      {
        return rhs < lhs;
      }

      constexpr friend bool operator>=(const span_iterator& lhs, const span_iterator& rhs) noexcept
      {
        return !(rhs > lhs);
      }

    protected:
      const Span* span_ = nullptr;
      std::ptrdiff_t index_ = 0;
    };

    template <class Span, bool IsConst>
    inline constexpr span_iterator<Span, IsConst>
    operator+(typename span_iterator<Span, IsConst>::difference_type n, const span_iterator<Span, IsConst>& rhs) noexcept
    {
      return rhs + n;
    }

    template <class Span, bool IsConst>
    inline constexpr span_iterator<Span, IsConst>
    operator-(typename span_iterator<Span, IsConst>::difference_type n, const span_iterator<Span, IsConst>& rhs) noexcept
    {
      return rhs - n;
    }

    template <std::ptrdiff_t Ext>
    class extent_type
    {
    public:
      using index_type = std::ptrdiff_t;

      static_assert(Ext >= 0, "A fixed-size span must be >= 0 in size.");

      constexpr extent_type() noexcept {}

      template <index_type Other>
      constexpr extent_type(extent_type<Other> ext)
      {
        static_assert(Other == Ext || Other == dynamic_extent, "Mismatch between fixed-size extent and size of initializing data.");
      }

      constexpr extent_type(index_type size) { }

      constexpr index_type size() const noexcept { return Ext; }
    };

    template <>
    class extent_type<dynamic_extent>
    {
    public:
      using index_type = std::ptrdiff_t;

      template <index_type Other>
      explicit constexpr extent_type(extent_type<Other> ext) : size_(ext.size())
      {
      }

      explicit constexpr extent_type(index_type size) : size_(size) { }

      constexpr index_type size() const noexcept { return size_; }

    private:
      index_type size_;
    };
  }

  template <class ElementType, std::ptrdiff_t Extent>
  class span
  {
  public:
    using element_type = ElementType;
    using value_type = std::remove_cv_t<ElementType>;
    using index_type = std::ptrdiff_t;
    using pointer = element_type*;
    using reference = element_type&;

    using iterator = detail::span_iterator<span<ElementType, Extent>, false>;
    using const_iterator = detail::span_iterator<span<ElementType, Extent>, true>;
    using reverse_iterator = std::reverse_iterator<iterator>;
    using const_reverse_iterator = std::reverse_iterator<const_iterator>;

    using size_type = index_type;

    constexpr static const index_type extent = Extent;

    template <bool Dependent = false, class = std::enable_if_t<(Dependent || Extent <= 0)>>
    constexpr span() noexcept : storage_(nullptr, detail::extent_type<0>())
    { }

    constexpr span(std::nullptr_t) noexcept : span() {}

    constexpr span(pointer ptr, index_type count) : storage_(ptr, count) {}

    constexpr span(pointer firstElem, pointer lastElem)
      : storage_(firstElem, std::distance(firstElem, lastElem))
    { }

    template <std::size_t N>
    constexpr span(element_type (&arr)[N]) noexcept
      : storage_(&arr[0], detail::extent_type<N>())
    { }

    template <std::size_t N, class ArrayElementType = std::remove_const_t<element_type>>
    constexpr span(std::array<ArrayElementType, N>& arr) noexcept
      : storage_(&arr[0], detail::extent_type<N>())
    { }

    template <std::size_t N>
    constexpr span(const std::array<std::remove_const_t<element_type>, N>& arr) noexcept
      : storage_(&arr[0], detail::extent_type<N>())
    { }

    template <class ArrayElementType = std::add_pointer<element_type>>
    constexpr span(const std::unique_ptr<ArrayElementType>& ptr, index_type count)
      : storage_(ptr.get(), count)
    { }

    constexpr span(const std::unique_ptr<ElementType>& ptr) : storage_(ptr.get(), ptr.get() ? 1 : 0)
    { }
    constexpr span(const std::shared_ptr<ElementType>& ptr) : storage_(ptr.get(), ptr.get() ? 1 : 0)
    { }

    // NB: the SFINAE here uses .data() as a incomplete/imperfect proxy for the requirement
    // on Container to be a contiguous sequence container.
    template <class Container, class = std::enable_if_t<
         !detail::is_span<Container>::value
      && !detail::is_std_array<Container>::value
      && std::is_convertible<typename Container::pointer, pointer>::value
      && std::is_convertible<typename Container::pointer, decltype(std::declval<Container>().data())>::value>>
    constexpr span(Container& cont) : span(cont.data(), cont.size())
    { }

    template <class Container, class = std::enable_if_t<
         std::is_const<element_type>::value && !detail::is_span<Container>::value
      && std::is_convertible<typename Container::pointer, pointer>::value
      && std::is_convertible<typename Container::pointer, decltype(std::declval<Container>().data())>::value>>
    constexpr span(const Container& cont) : span(cont.data(), cont.size())
    { }

    constexpr span(const span& other) noexcept = default;
    constexpr span(span&& other) noexcept = default;

    template <class OtherElementType, std::ptrdiff_t OtherExtent, class = std::enable_if_t<
         detail::is_allowed_extent_conversion<OtherExtent, Extent>::value
      && detail::is_allowed_element_type_conversion<OtherElementType, element_type>::value>>
    constexpr span(const span<OtherElementType, OtherExtent>& other)
      : storage_(other.data(), detail::extent_type<OtherExtent>(other.size()))
    { }

    template <class OtherElementType, std::ptrdiff_t OtherExtent, class = std::enable_if_t<
         detail::is_allowed_extent_conversion<OtherExtent, Extent>::value
      && detail::is_allowed_element_type_conversion<OtherElementType, element_type>::value>>
    constexpr span(span<OtherElementType, OtherExtent>&& other)
      : storage_(other.data(), detail::extent_type<OtherExtent>(other.size()))
    { }

    ~span() noexcept = default;
    constexpr span& operator=(const span& other) noexcept = default;

    constexpr span& operator=(span&& other) noexcept = default;

    template <std::ptrdiff_t Count>
    constexpr span<element_type, Count> first() const
    {
      return {data(), Count};
    }

    template <std::ptrdiff_t Count>
    constexpr span<element_type, Count> last() const
    {
      return {data() + (size() - Count), Count};
    }

    template <std::ptrdiff_t Offset, std::ptrdiff_t Count = dynamic_extent>
    constexpr span<element_type, Count> subspan() const
    {
      return {data() + Offset, Count == dynamic_extent ? size() - Offset : Count};
    }

    constexpr span<element_type, dynamic_extent> first(index_type count) const
    {
      return {data(), count};
    }

    constexpr span<element_type, dynamic_extent> last(index_type count) const
    {
      return {data() + (size() - count), count};
    }

    constexpr span<element_type, dynamic_extent> subspan(index_type offset, index_type count = dynamic_extent) const
    {
      return {data() + offset, count == dynamic_extent ? size() - offset : count};
    }

    constexpr index_type length() const noexcept { return size(); }
    constexpr index_type size() const noexcept { return storage_.size(); }
    constexpr index_type length_bytes() const noexcept { return size_bytes(); }
    constexpr index_type size_bytes() const noexcept
    {
      return size() * sizeof(element_type);
    }
    constexpr bool empty() const noexcept { return size() == 0; }

    constexpr reference operator[](index_type idx) const
    {
      return data()[idx];
    }

    constexpr reference at(index_type idx) const { return this->operator[](idx); }
    constexpr reference operator()(index_type idx) const { return this->operator[](idx); }
    constexpr pointer data() const noexcept { return storage_.data(); }

    iterator begin() const noexcept { return {this, 0}; }
    iterator end() const noexcept { return {this, length()}; }

    const_iterator cbegin() const noexcept { return {this, 0}; }
    const_iterator cend() const noexcept { return {this, length()}; }

    reverse_iterator rbegin() const noexcept { return reverse_iterator{end()}; }
    reverse_iterator rend() const noexcept { return reverse_iterator{begin()}; }

    const_reverse_iterator crbegin() const noexcept { return const_reverse_iterator{cend()}; }
    const_reverse_iterator crend() const noexcept { return const_reverse_iterator{cbegin()}; }

  private:
    // this implementation detail class lets us take advantage of the
    // empty base class optimization to pay for only storage of a single
    // pointer in the case of fixed-size spans
    template <class ExtentType>
    class storage_type : public ExtentType
    {
    public:
      template <class OtherExtentType>
      constexpr storage_type(pointer data, OtherExtentType ext) : ExtentType(ext), data_(data)
      { }

      constexpr pointer data() const noexcept { return data_; }

    private:
      pointer data_;
    };

    storage_type<detail::extent_type<Extent>> storage_;
  };

  template <class ElementType, std::ptrdiff_t FirstExtent, std::ptrdiff_t SecondExtent>
  inline constexpr bool operator==(const span<ElementType, FirstExtent>& l, const span<ElementType, SecondExtent>& r)
  {
    return std::equal(l.begin(), l.end(), r.begin(), r.end());
  }

  template <class ElementType, std::ptrdiff_t Extent>
  inline constexpr bool operator!=(const span<ElementType, Extent>& l, const span<ElementType, Extent>& r)
  {
    return !(l == r);
  }

  template <class ElementType, std::ptrdiff_t Extent>
  inline constexpr bool operator<(const span<ElementType, Extent>& l, const span<ElementType, Extent>& r)
  {
    return std::lexicographical_compare(l.begin(), l.end(), r.begin(), r.end());
  }

  template <class ElementType, std::ptrdiff_t Extent>
  inline constexpr bool operator<=(const span<ElementType, Extent>& l, const span<ElementType, Extent>& r)
  {
    return !(l > r);
  }

  template <class ElementType, std::ptrdiff_t Extent>
  inline constexpr bool operator>(const span<ElementType, Extent>& l, const span<ElementType, Extent>& r)
  {
    return r < l;
  }

  template <class ElementType, std::ptrdiff_t Extent>
  inline constexpr bool operator>=(const span<ElementType, Extent>& l, const span<ElementType, Extent>& r)
  {
    return !(l < r);
  }

  namespace detail
  {
    // if we only supported compilers with good constexpr support then
    // this pair of classes could collapse down to a constexpr function
    template <class ElementType, std::ptrdiff_t Extent>
    struct calculate_byte_size : std::integral_constant<std::ptrdiff_t, static_cast<std::ptrdiff_t>(sizeof(ElementType) * static_cast<std::size_t>(Extent))>
    { };

    template <class ElementType>
    struct calculate_byte_size<ElementType, dynamic_extent> : std::integral_constant<std::ptrdiff_t, dynamic_extent>
    { };
  }

  template <class ElementType, std::ptrdiff_t Extent>
  span<const unsigned char, detail::calculate_byte_size<ElementType, Extent>::value> as_bytes(span<ElementType, Extent> s) noexcept
  {
    return {reinterpret_cast<const unsigned char*>(s.data()), s.size_bytes()};
  }

  template <class ElementType, std::ptrdiff_t Extent, class = std::enable_if_t<!std::is_const<ElementType>::value>>
  span<unsigned char, detail::calculate_byte_size<ElementType, Extent>::value> as_writeable_bytes(span<ElementType, Extent> s) noexcept
  {
    return {reinterpret_cast<unsigned char*>(s.data()), s.size_bytes()};
  }

  template <class ElementType>
  span<ElementType> make_span(ElementType* ptr, typename span<ElementType>::index_type count)
  {
    return span<ElementType>(ptr, count);
  }

  template <class ElementType>
  span<ElementType> make_span(ElementType* firstElem, ElementType* lastElem)
  {
    return span<ElementType>(firstElem, lastElem);
  }

  template <class ElementType, std::size_t N>
  span<ElementType, N> make_span(ElementType (&arr)[N])
  {
    return span<ElementType, N>(arr);
  }

  template <class Container>
  span<typename Container::value_type> make_span(Container& cont)
  {
    return span<typename Container::value_type>(cont);
  }

  template <class Container>
  span<const typename Container::value_type> make_span(const Container& cont)
  {
    return span<const typename Container::value_type>(cont);
  }

  template <class Ptr>
  span<typename Ptr::element_type> make_span(Ptr& cont, std::ptrdiff_t count)
  {
    return span<typename Ptr::element_type>(cont, count);
  }

  template <class Ptr>
  span<typename Ptr::element_type> make_span(Ptr& cont)
  {
    return span<typename Ptr::element_type>(cont);
  }
}
