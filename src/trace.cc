/*------------------------------------------------------------------------------
 * Bureau of Meteorology C++ Utility Library
 *
 * Copyright 2014 Mark Curtis
 * Copyright 2016 Commonwealth of Australia, Bureau of Meteorology
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *----------------------------------------------------------------------------*/
#include "config.h"
#include "trace.h"
#include "timestamp.h"

#include <atomic>
#include <iostream>
#include <map>
#include <mutex>
#include <thread>

using namespace bom::trace;

BOM_DEFINE_ENUM_TRAITS(bom::trace::level);

static constexpr level_marker_set def_markers = { "XX ", "** ", "EE ", "WW ", "-- ", "DD " };
static constexpr char const*      def_terminator = "\n";

static std::atomic<level>                   min_level_{level::log};
static std::atomic_bool                     prefix_timestamp_{true};
static std::atomic_bool                     identify_threads_{true};
static std::atomic<level_marker_set const*> level_markers_{&def_markers};
static std::atomic<char const*>             terminator_{def_terminator};
static std::mutex                           target_mutex_;
static std::ostream*                        target_{&std::cerr};

static auto make_thread_id() -> size_t
{
  static std::mutex mutex;
  std::lock_guard<std::mutex> lock(mutex);
  static std::map<std::thread::id, size_t> ids;
  return ids.insert(std::make_pair(std::this_thread::get_id(), ids.size())).first->second;
}

auto bom::trace::min_level() -> level
{
  return min_level_;
}

auto bom::trace::set_min_level(level min) -> level
{
  return min_level_.exchange(min);
}

auto bom::trace::prefix_timestamp() -> bool
{
  return prefix_timestamp_;
}

auto bom::trace::set_prefix_timestamp(bool timestamp) -> void
{
  prefix_timestamp_ = timestamp;
}

auto bom::trace::identify_threads() -> bool
{
  return identify_threads_;
}

auto bom::trace::set_identify_threads(bool identify) -> void
{
  identify_threads_ = identify;
}

auto bom::trace::level_markers() -> level_marker_set const*
{
  return level_markers_;
}

auto bom::trace::set_level_markers(level_marker_set const* markers) -> void
{
  level_markers_ = markers;
}

auto bom::trace::message_terminator() -> char const*
{
  return terminator_;
}

auto bom::trace::set_message_terminator(char const* terminator) -> void
{
  terminator_ = terminator;
}

auto bom::trace::target() -> std::ostream&
{
  std::lock_guard<std::mutex> lock(target_mutex_);
  return *target_;
}

auto bom::trace::set_target(std::ostream& target) -> void
{
  std::lock_guard<std::mutex> lock(target_mutex_);
  target_ = &target;
}

auto bom::trace::level_active(level lvl) -> bool
{
  return static_cast<int>(lvl) <= static_cast<int>(min_level_.load());
}

auto bom::trace::vtrace(level lvl, char const* format_str, fmt::format_args const& args) -> void
{
  // skip trace if we level is not sufficient
  /* unless the user called this function directly, this check will have already been performed by the inline function
   * that wraps this one, but its cheap to recheck here compared to the substantial cost of the actual formatting. */
  if (static_cast<int>(lvl) > static_cast<int>(min_level_.load()))
    return;

  // allocate a string to build our message in
  /* if performance of tracing ever becomes an issue, you could keep a list of strings around as buffers and
   * reuse them as needed instead of generating a new string here every time. */
  string out;

  // build the message prefix
  if (prefix_timestamp_)
  {
    string_conversions<timestamp>::write_string(timestamp::now(), out);
    out.push_back(' ');
  }
  if (identify_threads_)
  {
    thread_local string thread_id = to_string(make_thread_id());
    out.append(thread_id);
    out.push_back(' ');
  }
  out.append((*level_markers_)[static_cast<int>(lvl)]);

  // append any prefixes
  scope_prefix::format(out);

  // format the main message string
  out.append(fmt::vformat(format_str, args));

  // append the terminator
  out.append(terminator_);

  // output to the target stream in a thread safe manner
  std::lock_guard<std::mutex> lock(target_mutex_);
  target_->write(out.data(), out.size());
}

static thread_local scope_prefix* prefix_stack_head_ = nullptr;

scope_prefix::scope_prefix(char const* prefix)
  : prefix_{prefix}
  , parent_{prefix_stack_head_}
{
  prefix_stack_head_ = this;
}

scope_prefix::~scope_prefix()
{
  if (prefix_stack_head_ == this)
  {
    // this is normal - destruction in same order as construction
    prefix_stack_head_ = parent_;
  }
  else
  {
    // abnormal - destroyed in different order to constructed
    auto p = prefix_stack_head_;
    while (p && p->parent_ != this)
      p = p->parent_;

    if (p)
      // abnormal but okay - destoryed in different order to construction
      p->parent_ = parent_;
    else
    {
      // terminate here since this is a non-recoverable error
      // another thread now has a dangling pointer in its prefix stack
      {
        std::lock_guard<std::mutex> lock{target_mutex_};
        *target_ << "FATAL ERROR: trace::scope_prefix destroyed from wrong thread";
      }
      std::terminate();
    }
  }
}

inline auto scope_prefix::format(string& out) -> void
{
  if (prefix_stack_head_)
  {
    prefix_stack_head_->format_impl(out);
    out.push_back(' ');
  }
}

auto scope_prefix::format_impl(string& out) const -> void
{
  if (parent_)
  {
    parent_->format_impl(out);
    out.push_back('.');
  }
  out.append(prefix_);
}
