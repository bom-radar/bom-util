/*------------------------------------------------------------------------------
 * Bureau of Meteorology C++ Utility Library
 *
 * Copyright 2014 Mark Curtis
 * Copyright 2016 Commonwealth of Australia, Bureau of Meteorology
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *----------------------------------------------------------------------------*/
#include "config.h"
#include "string_utils.h"
#include "unit_test.h"

#include <algorithm>
#include <cstring>
#include <limits>
#include <cstdlib>
#include <strings.h>

using namespace bom;

/**
 * \todo Allow specification of extra parameters to the read_string functions for built in types to control
 *       precision, width, base etc.
 */

string_conversion_error::string_conversion_error(string type)
  : std::runtime_error(fmt::format("to_string error ({})", type))
  , type_(std::move(type))
  , has_from_(false)
{

}

string_conversion_error::string_conversion_error(string type, string from)
  : std::runtime_error(fmt::format("from_string error ({}, {})", type, from))
  , type_(std::move(type))
  , from_(std::move(from))
  , has_from_(true)
{

}

auto bom::ends_with(string const& target, string const& ending) -> bool
{
  if (target.length() >= ending.length())
    return target.compare(target.length() - ending.length(), ending.length(), ending) == 0;
  return false;
}

auto bom::ends_with(string const& target, char const* ending) -> bool
{
  auto elen = std::strlen(ending);
  if (target.length() >= elen)
    return target.compare(target.length() - elen, elen, ending) == 0;
  return false;
}

auto bom::ltrim(string& str) -> void
{
  str.erase(str.begin(), std::find_if(str.begin(), str.end(), [](char c) { return !std::isspace(c); }));
}

auto bom::rtrim(string& str) -> void
{
  str.erase(std::find_if(str.rbegin(), str.rend(), [](char c) { return !std::isspace(c); }).base(), str.end());
}

auto bom::trim(string& str) -> void
{
  rtrim(str);
  ltrim(str);
}

auto bom::operator<<(std::ostream& os, const indent& val) -> std::ostream&
{
  for (int i = 0; i < val.size(); ++i)
    os.put(' ');
  return os;
}

auto string_conversions<indent>::read_string(char const* str, size_t len) -> indent
{
  for (size_t i = 0; i < len; ++i)
    if (!std::isspace(str[i]))
      throw string_conversion_error{"indent", string(str, len)};
  return len;
}

static auto format_nested_exception(string& str, std::exception const& err, indent prefix) -> void
try
{
  std::rethrow_if_nested(err);
}
catch (std::exception const& sub)
{
  str.append(1, '\n').append(prefix.size(), ' ').append("-> ").append(sub.what());
  format_nested_exception(str, sub, prefix);
}
catch (...)
{
  str.append(1, '\n').append(prefix.size(), ' ').append("-> <unknown exception>");
}

auto bom::format_exception(std::exception const& err, indent prefix) -> string
{
  string ret(err.what());
  format_nested_exception(ret, err, prefix);
  return ret;
}

template <>
auto bom::tokenize<string>(const string& str, char const* delims) -> std::vector<string>
{
  std::vector<string> ret;
  size_t pos = 0;
  while ((pos = str.find_first_not_of(delims, pos)) != string::npos)
  {
    size_t end = str.find_first_of(delims, pos + 1);
    size_t len = (end == string::npos ? str.size() : end) - pos;
    ret.emplace_back(str, pos, len);
    pos += len;
  }
  return ret;
}

auto string_conversions<bool>::read_string(char const* str, size_t len) -> bool
{
  if (len == 4 && strncmp(str, "true", 4) == 0)
    return true;
  if (len == 5 && strncmp(str, "false", 5) == 0)
    return false;
  throw string_conversion_error{"bool", string(str, len)};
}

auto string_conversions<char>::read_string(char const* str, size_t len) -> char
{
  if (len != 1)
    throw string_conversion_error{"char", string(str, len)};
  return str[0];
}

#define INT_CONVERSION(type) \
auto string_conversions<type>::read_string(char const* str, size_t len, int base) -> type \
{ \
  type val; \
  auto [end, ec] = std::from_chars(str, str + len, val, base); \
  if (end != str + len || ec != std::errc()) \
    throw string_conversion_error{#type, string(str, len)}; \
  return val; \
}

INT_CONVERSION(signed char)
INT_CONVERSION(unsigned char)
INT_CONVERSION(short)
INT_CONVERSION(unsigned short)
INT_CONVERSION(int)
INT_CONVERSION(unsigned int)
INT_CONVERSION(long)
INT_CONVERSION(unsigned long)
INT_CONVERSION(long long)
INT_CONVERSION(unsigned long long)

auto string_conversions<float>::read_string(char const* str, size_t len) -> float
{
  float val;
  auto [end, ec] = std::from_chars(str, str + len, val);
  if (end != str + len || ec != std::errc())
    throw string_conversion_error{"float", string(str, len)};
  return val;
}

auto string_conversions<double>::read_string(char const* str, size_t len) -> double
{
  double val;
  auto [end, ec] = std::from_chars(str, str + len, val);
  if (end != str + len || ec != std::errc())
    throw string_conversion_error{"double", string(str, len)};
  return val;
}

auto string_conversions<long double>::read_string(char const* str, size_t len) -> long double
{
  long double val;
  auto [end, ec] = std::from_chars(str, str + len, val);
  if (end != str + len || ec != std::errc())
    throw string_conversion_error{"long double", string(str, len)};
  return val;
}

// LCOV_EXCL_START
#include <sstream>

TEST_CASE("ends_with")
{
  CHECK(ends_with("foobar", "bar"s) == true);
  CHECK(ends_with("foobarbaz", "bar"s) == false);
  CHECK(ends_with("foo", "longer"s) == false);

  CHECK(ends_with("foobar", "bar") == true);
  CHECK(ends_with("foobarbaz", "bar") == false);
  CHECK(ends_with("foo", "longer") == false);
}

TEST_CASE("trim")
{
  auto str = " \t\nfoo \t\nbar\n\t "s;

  SUBCASE("ltrim")
  {
    ltrim(str);
    CHECK(str == "foo \t\nbar\n\t ");
  }
  SUBCASE("rtrim")
  {
    rtrim(str);
    CHECK(str == " \t\nfoo \t\nbar");
  }
  SUBCASE("trim")
  {
    trim(str);
    CHECK(str == "foo \t\nbar");
  }
}

TEST_CASE("string_conversion_error")
{
  auto err = string_conversion_error{"foo"};
  CHECK(err.type() == "foo");
  CHECK(err.from() == "");
  CHECK(err.has_from() == false);
  CHECK(string(err.what()) == "to_string error (foo)");

  auto err2 = string_conversion_error{"foo", "bar"};
  CHECK(err2.type() == "foo");
  CHECK(err2.from() == "bar");
  CHECK(err2.has_from() == true);
  CHECK(string(err2.what()) == "from_string error (foo, bar)");
}

TEST_CASE("indent")
{
  std::ostringstream oss;

  auto in = indent{};
  CHECK(in.size() == 0);

  oss << in;
  CHECK(oss.str() == "");
  oss.str("");

  oss << in + 4;
  CHECK(oss.str() == "    ");
  oss.str("");

  oss << (in + 4) - 2;
  CHECK(oss.str() == "  ");

  in += 10;
  CHECK(in.size() == 10);

  in -= 3;
  CHECK(in.size() == 7);

  CHECK(from_string<indent>("   ").size() == 3);
  CHECK_THROWS(from_string<indent>("  foo"));         // invalid character

  CHECK(to_string(indent{3}) == "   ");
}

TEST_CASE("fmt_bytes")
{
  CHECK(to_string(fmt_bytes{1023ul}) == "1023B");
  CHECK(to_string(fmt_bytes{1024ul}) == "1KB");
  CHECK(to_string(fmt_bytes{3ul * 1024 * 1024 + 400}) == "3MB");
  CHECK(to_string(fmt_bytes{7ul * 1024 * 1024 * 1024 + 400}) == "7GB");
  CHECK(to_string(fmt_bytes{2ul * 1024 * 1024 * 1024 * 1024 + 400}) == "2TB");
  CHECK(to_string(fmt_bytes{8ul * 1024 * 1024 * 1024 * 1024 * 1024 + 400}) == "8PB");

  CHECK(fmt::format("{}", fmt_bytes{2048}) == "2KB");
}

/**
 * \todo Add checks that overflow and underflow throw.  This is not so easy to generate overflow values in 
 *       string form without hard coding them...
 * \todo Test floating point conversions
 * \todo Test indent class
 * \todo Test tokenization functions
 */
template <typename T>
void test_integral_type(
        T vmin
      , char const* smin
      , T vmax
      , char const* smax
//      , char const* sunder
//      , char const* sover
      )
{
  CHECK(from_string<T>(smin) == vmin);
  CHECK(from_string<T>(smax) == vmax);
//  CHECK_THROWS(from_string<T>(sunder));
//  CHECK_THROWS(from_string<T>(sover));
  CHECK_THROWS(from_string<T>(" 10"));
  CHECK_THROWS(from_string<T>("10 "));
  CHECK(from_string_n<T>("10 ", 2) == 10);

  if constexpr (std::is_unsigned_v<T>)
    CHECK_THROWS(from_string<T>("-1"));
  if constexpr (std::is_signed_v<T>)
    CHECK_NOTHROW(from_string<T>("-1"));


  CHECK(to_string(vmin) == string(smin));
  CHECK(to_string(vmax) == string(smax));

  auto val = T{108};
  #if 0
  CHECK(to_string(val, 10) == "108");
  CHECK(to_string(val, 16) == "6c");
  CHECK(to_string(val, 8) == "154");
  CHECK_THROWS(to_string(val, 4));
  #endif

  CHECK(fmt::format("{}", val) == "108");
  CHECK(fmt::format("{:d}", val) == "108");
  CHECK(fmt::format("{:x}", val) == "6c");
  CHECK(fmt::format("{:o}", val) == "154");
}

#define INTEGRAL_TEST(t, min, max, suf) test_integral_type<t>(min##suf, #min, max##suf, #max)
TEST_CASE("string_utils<schar>")  { INTEGRAL_TEST(signed char, -127, 127, ); }
TEST_CASE("string_utils<uchar>")  { INTEGRAL_TEST(unsigned char, 0, 255, ); }
TEST_CASE("string_utils<short>")  { INTEGRAL_TEST(short, -32767, 32767, ); }
TEST_CASE("string_utils<ushort>") { INTEGRAL_TEST(unsigned short, 0, 65535, u); }
TEST_CASE("string_utils<int>")    { INTEGRAL_TEST(int, -32767, 32767, ); }
TEST_CASE("string_utils<uint>")   { INTEGRAL_TEST(unsigned int, 0, 65535, u); }
TEST_CASE("string_utils<long>")   { INTEGRAL_TEST(long, -2147483647, 2147483647, l); }
TEST_CASE("string_utils<ulong>")  { INTEGRAL_TEST(unsigned long, 0, 4294967295, ul); }
TEST_CASE("string_utils<llong>")  { INTEGRAL_TEST(long long, -9223372036854775807, 9223372036854775807, ll); }
TEST_CASE("string_utils<ullong>") { INTEGRAL_TEST(unsigned long long, 0, 18446744073709551615, ull); }

TEST_CASE("string_utils<float>")
{
  CHECK(from_string<float>("123.45") == approx(123.45));

  CHECK_THROWS(from_string<float>("12k.45"));         // invalid character
  CHECK_THROWS(from_string<float>(" 123.45"));        // leading space
  CHECK_THROWS(from_string<float>("12 3.45"));        // middle space
  CHECK_THROWS(from_string<float>("123.45 "));        // trailing space

  CHECK(to_string(123.50f) == "123.5");

  CHECK(fmt::format("{}", 123.50f) == "123.5");
  CHECK(fmt::format("{:06.1f}", 123.46f) == "0123.5");
}

TEST_CASE("string_utils<double>")
{
  CHECK(from_string<double>("123.45") == approx(123.45));

  CHECK_THROWS(from_string<double>("12k.45"));        // invalid character
  CHECK_THROWS(from_string<double>(" 123.45"));       // leading space
  CHECK_THROWS(from_string<double>("12 3.45"));       // middle space
  CHECK_THROWS(from_string<double>("123.45 "));       // trailing space

  CHECK(to_string(123.50) == "123.5");

  CHECK(fmt::format("{}", 123.50) == "123.5");
  CHECK(fmt::format("{:06.1f}", 123.46) == "0123.5");
}

TEST_CASE("string_utils<bool>")
{
  CHECK(from_string<bool>("true") == true);
  CHECK(from_string<bool>("false") == false);
  CHECK_THROWS(from_string<bool>("truebad"));
  CHECK_THROWS(from_string<bool>(" true "));
  CHECK(from_string_n<bool>("truebad", 4) == true);
  CHECK(from_string_n<bool>("falsebad", 5) == false);
  CHECK(to_string(true) == "true");
  CHECK(to_string(false) == "false");

  CHECK(fmt::format("{}", true) == "true");
  CHECK(fmt::format("{}", false) == "false");
}

TEST_CASE("string_utils<char>")
{
  CHECK(from_string<char>("a") == 'a');
  CHECK_THROWS(from_string<char>("ab"));
  CHECK(to_string<char>('a') == "a");
}

TEST_CASE("string_utils<void const*>")
{
  CHECK(to_string(reinterpret_cast<void*>(0x90)) == "0x90");
}

TEST_CASE("format")
{
  CHECK(fmt::format("1{}3", 2) == "123");
  CHECK(fmt::format("1{0}3", 2) == "123");
  CHECK(fmt::format("1{:}3", 2) == "123");
  CHECK(fmt::format("1{0:}3", 2) == "123");

  CHECK(fmt::format("{11}", 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, "eleven") == "eleven");

  CHECK(fmt::format("1{}{}3", 2, 2) == "1223");
  CHECK(fmt::format("1{0}{1}3", 2, 2) == "1223");
  CHECK(fmt::format("1{1}3", 2, 22) == "1223");
  CHECK(fmt::format("1{1:}3", 2, 22) == "1223");

  CHECK(fmt::format("hello {{ world }}") == "hello { world }");
  CHECK(fmt::format("}}") == "}");

  CHECK(fmt::format("1{}3", 2, 20) == "123");

  CHECK(fmt::format("a{hello}b", "hello"_a = 10) == "a10b");
  CHECK(fmt::format("a{gg} {hello:}b", "hello"_a = 10, "gg"_a = 0) == "a0 10b");

  // some different types
#if 0
  CHECK(fmt::format("hello{}world", (signed char) -10) == "hello-10world");
  CHECK(fmt::format("yes{}yes no{}no ten{}ten", true, false, 10) == "yestrueyes nofalseno ten10ten");
  CHECK(fmt::format("float{}float", 45.0) == "float45.000000float");
#endif

  // the c-string variants are trouble makers... test each one (char*, char const* and literal string)
  char const* foo = "hello";
  CHECK(fmt::format("foo{}bar", foo) == "foohellobar");
  CHECK(fmt::format("foo{named}bar", "named"_a = foo) == "foohellobar");

  char buf[16];
  strcpy(buf, "hello");
  char * out = &buf[0];
  CHECK(fmt::format("out={}", out) == "out=hello");
  CHECK(fmt::format("out={named}", "named"_a = out) == "out=hello");

  CHECK(fmt::format("cstring {}", "here") == "cstring here");
  CHECK(fmt::format("cstring {named}", "named"_a = "here") == "cstring here");
}

TEST_CASE("tokenize")
{
  auto tokens = tokenize<int>("1,2.3|40,,,", ",.|");
  REQUIRE(tokens.size() == 4);
  CHECK(tokens[0] == 1);
  CHECK(tokens[1] == 2);
  CHECK(tokens[2] == 3);
  CHECK(tokens[3] == 40);
}
TEST_CASE("tokenize<string>")
{
  auto tokens = tokenize<string>("a,b.c|ee,,,", ",.|");
  REQUIRE(tokens.size() == 4);
  CHECK(tokens[0] == "a");
  CHECK(tokens[1] == "b");
  CHECK(tokens[2] == "c");
  CHECK(tokens[3] == "ee");
}
// LCOV_EXCL_STOP
