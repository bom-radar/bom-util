/*------------------------------------------------------------------------------
 * Bureau of Meteorology C++ Utility Library
 *
 * Copyright 2014 Mark Curtis
 * Copyright 2016 Commonwealth of Australia, Bureau of Meteorology
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *----------------------------------------------------------------------------*/
#pragma once

#include "math_utils.h"
#include "string_utils.h"

#include <cmath>
#include <istream>
#include <ostream>
#include <tuple>

namespace bom
{
  class angle
  {
  public:
    constexpr static double pi = M_PI;
    constexpr static double two_pi = M_PI * 2.0;
    constexpr static double deg_to_rad = M_PI / 180.0;
    constexpr static double rad_to_deg = 180.0 / M_PI;

  public:
    angle() noexcept = default;

    constexpr angle(std::tuple<int, int, double> dms)
      : rads_{std::copysign(
                std::abs(std::get<0>(dms)) + (std::get<1>(dms) / 60.0) + (std::get<2>(dms) / 3600.0)
              , std::get<0>(dms)) * deg_to_rad}
    { }

    constexpr angle(angle const& rhs) noexcept = default;
    constexpr angle(angle&& rhs) noexcept = default;

    auto operator=(angle const& rhs) noexcept -> angle& = default;
    auto operator=(angle&& rhs) noexcept -> angle& = default;

    ~angle() noexcept = default;

    constexpr auto operator<(angle rhs) const -> bool
    {
      return rads_ < rhs.rads_;
    }

    constexpr auto operator<=(angle rhs) const -> bool
    {
      return rads_ <= rhs.rads_;
    }

    constexpr auto operator>(angle rhs) const -> bool
    {
      return rads_ > rhs.rads_;
    }

    constexpr auto operator>=(angle rhs) const -> bool
    {
      return rads_ >= rhs.rads_;
    }

    auto operator+=(angle rhs) -> angle&
    {
      rads_ += rhs.rads_;
      return *this;
    }

    auto operator-=(angle rhs) -> angle&
    {
      rads_ -= rhs.rads_;
      return *this;
    }

    auto operator*=(double rhs) -> angle&
    {
      rads_ *= rhs;
      return *this;
    }

    auto operator/=(double rhs) -> angle&
    {
      rads_ /= rhs;
      return *this;
    }

    constexpr auto degrees() const -> double
    {
      return rads_ * rad_to_deg;
    }

    auto set_degrees(double val) -> void
    {
      rads_ = val * deg_to_rad;
    }

    constexpr auto radians() const -> double
    {
      return rads_;
    }

    auto set_radians(double val) -> void
    {
      rads_ = val;
    }

    /// return the angle as a tuple of integer degrees, minutes and floating point seconds
    auto dms() const -> std::tuple<int, int, double>;

    /// set the angle based on integer degrees, minutes and floating point seconds
    auto set_dms(std::tuple<int, int, double> val) -> void
    {
      rads_ = angle{val}.rads_;
    }

    constexpr auto abs() const -> angle
    {
      return angle(std::abs(rads_));
    }

    // normalize the angle to [0..360) degrees
    auto normalize() const -> angle
    {
      return angle(rads_ - two_pi * std::floor(rads_ / two_pi));
    }

  public:
    friend constexpr auto operator"" _deg(unsigned long long val) -> angle;
    friend constexpr auto operator"" _deg(long double val) -> angle;
    friend constexpr auto operator"" _rad(unsigned long long val) -> angle;
    friend constexpr auto operator"" _rad(long double val) -> angle;

    friend auto operator>>(std::istream& lhs, angle& rhs) -> std::istream&;
    friend auto operator<<(std::ostream& lhs, angle const& rhs) -> std::ostream&;

    friend constexpr auto operator-(angle lhs) -> angle;
    friend constexpr auto abs(angle lhs) -> angle;

    friend constexpr auto operator+(angle lhs, angle rhs) -> angle;
    friend constexpr auto operator-(angle lhs, angle rhs) -> angle;
    friend constexpr auto operator*(double lhs, angle rhs) -> angle;
    friend constexpr auto operator*(angle lhs, double rhs) -> angle;
    friend constexpr auto operator/(angle lhs, angle rhs) -> double;
    friend constexpr auto operator/(angle lhs, double rhs) -> angle;

    friend constexpr auto lerp(angle lhs, angle rhs, double fraction) -> angle;

    friend auto cos(angle x) -> double;
    friend auto sin(angle x) -> double;
    friend auto tan(angle x) -> double;
    friend auto acos(double x) -> angle;
    friend auto asin(double x) -> angle;
    friend auto atan(double x) -> angle;
    friend auto atan2(double y, double x) -> angle;

  private:
    explicit constexpr angle(double rads) : rads_(rads) { }

  private:
    double rads_;
  };

  // user defined literals (use 'val * 1_deg' to convert a built-in to an angle)
  inline constexpr auto operator"" _deg(unsigned long long val) -> angle
  {
    return angle(val * angle::deg_to_rad);
  }

  inline constexpr auto operator"" _deg(long double val) -> angle
  {
    return angle(val * angle::deg_to_rad);
  }

  inline constexpr auto operator"" _rad(unsigned long long val) -> angle
  {
    return angle(val);
  }

  inline constexpr auto operator"" _rad(long double val) -> angle
  {
    return angle(val);
  }

  // special nan type for angles
  template <>
  inline constexpr auto nan<angle>() -> angle
  {
    return angle(nan<double>() * 1_rad);
  }

  template <>
  inline constexpr auto is_nan(angle val) -> bool
  {
    return is_nan(val.radians());
  }

  // unary operators
  inline constexpr auto operator-(angle lhs) -> angle
  {
    return angle(-lhs.rads_);
  }

  inline constexpr auto abs(angle lhs) -> angle
  {
    return angle(std::abs(lhs.rads_));
  }

  // binary operators
  inline constexpr auto operator+(angle lhs, angle rhs) -> angle
  {
    return angle(lhs.rads_ + rhs.rads_);
  }

  inline constexpr auto operator-(angle lhs, angle rhs) -> angle
  {
    return angle(lhs.rads_ - rhs.rads_);
  }

  inline constexpr auto operator*(double lhs, angle rhs) -> angle
  {
    return angle(lhs * rhs.rads_);
  }

  inline constexpr auto operator*(angle lhs, double rhs) -> angle
  {
    return angle(lhs.rads_ * rhs);
  }

  inline constexpr auto operator/(angle lhs, angle rhs) -> double
  {
    return lhs.rads_ / rhs.rads_;
  }

  inline constexpr auto operator/(angle lhs, double rhs) -> angle
  {
    return angle(lhs.rads_ / rhs);
  }

  // specialized linear interpolation to ensure correct wrap around at 360
  inline constexpr auto lerp(angle lhs, angle rhs, double fraction) -> angle
  {
    auto da = fmod((rhs - lhs).rads_, angle::two_pi);
    auto dist = fmod(2.0 * da, angle::two_pi) - da;
    return angle(lhs.rads_ + fraction * dist);
  }

  inline constexpr auto lerp(angle lhs, angle rhs, float fraction) -> angle
  {
    return lerp(lhs, rhs, static_cast<double>(fraction));
  }

  // stream insertion
  inline auto operator<<(std::ostream& lhs, angle const& rhs) -> std::ostream&
  {
    return lhs << rhs.degrees();
  }

  // string conversion
  template <>
  struct string_conversions<angle>
  {
    static auto read_string(char const* str, size_t len) -> angle
    {
      return string_conversions<double>::read_string(str, len) * 1_deg;
    }
  };

  // allow simple configuration usage
  template <>
  struct allow_simple_string_conversions<angle>
  {
    static constexpr bool value = true;
  };

  // overload the standard trig functions to work directly with angle
  inline auto cos(angle x) -> double
  {
    return std::cos(x.rads_);
  }

  inline auto sin(angle x) -> double
  {
    return std::sin(x.rads_);
  }

  inline auto tan(angle x) -> double
  {
    return std::tan(x.rads_);
  }

  inline auto acos(double x) -> angle
  {
    return angle(std::acos(x));
  }

  inline auto asin(double x) -> angle
  {
    return angle(std::asin(x));
  }

  inline auto atan(double x) -> angle
  {
    return angle(std::atan(x));
  }

  inline auto atan2(double y, double x) -> angle
  {
    return angle(std::atan2(y, x));
  }
}

namespace fmt
{
  template <typename Char>
  struct formatter<bom::angle, Char> : formatter<double, Char>
  {
    enum out_style { deg, dms, rad };

    out_style style = out_style::deg;

    template <typename ParseContext>
    constexpr auto parse(ParseContext& ctx)
    {
      auto i = ctx.begin();

      // see if the user set one of our special flag values 'rad' or 'dms'
      if (i != ctx.end() && std::next(i) != ctx.end() && std::next(i, 2) != ctx.end() && std::next(i, 3) != ctx.end())
      {
        if (*i == 'd' && *std::next(i) == 'm' && *std::next(i, 2) == 's' && *std::next(i, 3) == '}')
        {
          style = out_style::dms;
          return std::next(i, 3);
        }
        if (*i == 'r' && *std::next(i) == 'a' && *std::next(i, 2) == 'd' && *std::next(i, 3) == '}')
        {
          style = out_style::rad;
          return std::next(i, 3);
        }
      }

      return formatter<double, Char>::parse(ctx);
    }

    template <typename FormatContext>
    auto format(bom::angle val, FormatContext& ctx)
    {
      if (style == out_style::deg)
        return formatter<double, Char>::format(val.degrees(), ctx);
      if (style == out_style::rad)
        return formatter<double, Char>::format(val.radians(), ctx);
      auto dms = val.dms();
      fmt::format_to(ctx.out(), "{}\u00b0{:02}'{:02.0f}\"", std::get<0>(dms), std::get<1>(dms), std::get<2>(dms));
      return ctx.out();
    }
  };
}
