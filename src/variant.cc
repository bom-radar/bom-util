/*------------------------------------------------------------------------------
 * Bureau of Meteorology C++ Utility Library
 *
 * Copyright 2014 Mark Curtis
 * Copyright 2016 Commonwealth of Australia, Bureau of Meteorology
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *----------------------------------------------------------------------------*/
#include "config.h"
#include "variant.h"
#include "unit_test.h"

using namespace bom;

// LCOV_EXCL_START
#include "string_utils.h"
TEST_CASE("variant")
{
  using var = variant<bool, double, int, string>;

  // default constructor
  CHECK(var{}.which() == var::type_enum<bool>());

  // direct type constructors
  CHECK(var{true}.which() == var::type_enum<bool>());
  CHECK(var{3}.which() == var::type_enum<int>());
  CHECK(var{1.2}.which() == var::type_enum<double>());
  CHECK(var{"foo"s}.which() == var::type_enum<string>());

  // copy constructor
  var a{"foo"s};
  var b{a};
  CHECK(b.which() == var::type_enum<string>());

  // move constructor
  var c{std::move(b)};
  CHECK(c.which() == var::type_enum<string>());
  CHECK(c.get<string>() == "foo");

  // copy assignment
  var d;
  CHECK_NOTHROW(d = a); // old copy is noexcept
  CHECK(d.get<string>() == "foo");
  var e{32.0};
  CHECK_NOTHROW(d = e); // old copy is NOT noexcept
  CHECK(e.get<double>() == approx(32.0));
  CHECK_NOTHROW(e = d); // same type

  b = 32.0;
  CHECK(b.which() == var::type_enum<double>());
  CHECK(b.get<double>() == approx(32.0));

  b = "bar"s;
  CHECK(b.which() == var::type_enum<string>());
  CHECK(b.get<string>() == "bar");

  string bar;
  b = bar;
  b = 1;
  CHECK(b.get<int>() == 1);
}
// LCOV_EXCL_STOP
