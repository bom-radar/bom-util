/*------------------------------------------------------------------------------
 * Bureau of Meteorology C++ Utility Library
 *
 * Copyright 2014 Mark Curtis
 * Copyright 2016 Commonwealth of Australia, Bureau of Meteorology
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *----------------------------------------------------------------------------*/
#pragma once

#include <type_traits>

namespace bom
{
  /// Template used to build flags out of an appropriately constructed enum type
  template <typename T>
  class flagset
  {
  public:
    using value_type = typename std::underlying_type<T>::type;

  public:
    constexpr flagset() noexcept
      : val_(0)
    { }

    constexpr explicit flagset(value_type val) noexcept
      : val_(val)
    { }

    constexpr flagset(T val) noexcept
      : val_(static_cast<value_type>(val))
    { }

    constexpr operator bool() const noexcept
    {
      return val_ != 0;
    }

    constexpr auto value() const noexcept -> value_type
    {
      return val_;
    }

    auto clear() noexcept -> flagset&
    {
      val_ = 0;
      return *this;
    }

    auto operator&=(flagset const& rhs) noexcept -> flagset&
    {
      val_ &= rhs.val_;
      return *this;
    }

    auto operator|=(flagset const& rhs) noexcept -> flagset&
    {
      val_ |= rhs.val_;
      return *this;
    }

    auto operator^=(flagset const& rhs) noexcept -> flagset&
    {
      val_ ^= rhs.val_;
      return *this;
    }

    friend constexpr auto operator~(flagset const& rhs) noexcept -> flagset
    {
      return flagset{~rhs.val_};
    }

    friend constexpr auto operator&(flagset const& rhs, flagset const& lhs) noexcept -> flagset
    {
      return flagset{lhs.val_ & rhs.val_};
    }

    friend constexpr auto operator|(flagset const& rhs, flagset const& lhs) noexcept -> flagset
    {
      return flagset{lhs.val_ | rhs.val_};
    }

    friend constexpr auto operator^(flagset const& rhs, flagset const& lhs) noexcept -> flagset
    {
      return flagset{lhs.val_ ^ rhs.val_};
    }

  private:
    value_type val_;
  };
}
