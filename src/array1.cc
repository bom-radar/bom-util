/*------------------------------------------------------------------------------
 * Bureau of Meteorology C++ Utility Library
 *
 * Copyright 2014 Mark Curtis
 * Copyright 2016 Commonwealth of Australia, Bureau of Meteorology
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *----------------------------------------------------------------------------*/
#include "config.h"
#include "array1.h"
#include "unit_test.h"

using namespace bom;

// LCOV_EXCL_START
TEST_CASE_TEMPLATE("array1", T, short, int, unsigned int, size_t, float, double)
{
  using array = array1<T>;

  array a1{10};
  for (size_t i = 0; i < a1.size(); ++i)
    a1[i] = i;

  // default constructor
  CHECK(array{}.size() == 0);

  // sized constructor
  CHECK(array{10}.size() == 10);

  // copy constructor
  array a2{a1};
  CHECK(a2.size() == a1.size());
  for (size_t i = 0; i < a1.size(); ++i)
    CHECK(a1[i] == a2[i]);

  // move constructor
  auto size = a2.size();
  auto ptr = a2.data();
  array a3{std::move(a2)};
  CHECK(a2.size() == 0);
  CHECK(a2.data() == nullptr);
  CHECK(a3.size() == size);
  CHECK(a3.data() == ptr);

  // copy assignment
  CHECK(&(a3 = a1) == &a3);
  CHECK(a3.size() == a1.size());
  for (size_t i = 0; i < a1.size(); ++i)
    CHECK(a3[i] == a1[i]);

  // move assignment
  size = a3.size();
  ptr = a3.data();
  a2 = std::move(a3);
  CHECK(a3.size() == 0);
  CHECK(a3.data() == nullptr);
  CHECK(a2.size() == size);
  CHECK(a2.data() == ptr);

  // size()
  CHECK(a1.size() == 10);

  // resize()
  a1.resize(20);
  CHECK(a1.size() == 20);
  a1.resize(20); // test null change chase
  CHECK(a1.size() == 20);

  // operator[] and data()
  CHECK(&a1[0] == a1.data());
  CHECK(&a1[3] == a1.data() + 3);

  // operator[] const and data() const
  auto const& c1 = a1;
  CHECK(&c1[0] == c1.data());
  CHECK(&c1[3] == c1.data() + 3);

  // begin() / begin() const / cbegin()
  CHECK(&a1[0] == &(*a1.begin()));
  CHECK(&c1[0] == &(*c1.begin()));
  CHECK(&c1[0] == &(*c1.cbegin()));

  // end() / end() const / cend()
  CHECK(&a1[a1.size() - 1] == &(*(a1.end() - 1)));
  CHECK(&c1[a1.size() - 1] == &(*(c1.end() - 1)));
  CHECK(&a1[a1.size() - 1] == &(*(a1.cend() - 1)));

  // fill()
  a1.fill(7);
  for (auto v : a1)
    CHECK(v == approx(7));
}
// LCOV_EXCL_STOP
