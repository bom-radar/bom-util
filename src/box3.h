/*------------------------------------------------------------------------------
 * Bureau of Meteorology C++ Utility Library
 *
 * Copyright 2014 Mark Curtis
 * Copyright 2016 Commonwealth of Australia, Bureau of Meteorology
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *----------------------------------------------------------------------------*/
#pragma once

#include "vec3.h"

namespace bom
{
  template <typename T>
  struct box3
  {
    using value_type = T;

    vec3<T> min, max;

    constexpr box3(vec3<T> min, vec3<T> max) : min{min}, max{max} { }

    constexpr box3(box3 const&) noexcept = default;
    constexpr box3(box3&&) noexcept = default;
    auto operator=(box3 const&) noexcept -> box3& = default;
    auto operator=(box3&&) noexcept -> box3& = default;

    /// Expand the box to enclose a point
    auto expand(vec3<T> const& point) -> void
    {
      // can't optimize out the > tests as 'else ifs' because that fails for degenerate boxes such as the empty box
      if (point.x < min.x)
        min.x = point.x;
      if (point.x > max.x)
        max.x = point.x;
      if (point.y < min.y)
        min.y = point.y;
      if (point.y > max.y)
        max.y = point.y;
      if (point.z < min.z)
        min.z = point.z;
      if (point.z > max.z)
        max.z = point.z;
    }

    /// Is the passed point enclosed by the box
    auto contains(vec3<T> point) const -> bool
    {
      return
           point.x >= min.x && point.x <= max.x
        && point.y >= min.y && point.y <= max.y
        && point.z >= min.z && point.z <= max.z;
    }

    /// An empty box where min and max are set to their max and min values respectively
    /** This box may be used as a safe starting point when looping though a set of points and expanding the
     *  box to include each one.  Without this, you would need to initialize the box with the value of the
     *  first point and loop over the remaining points - which complicates the required logic. */
    static constexpr auto empty_box() -> box3
    {
      return
      {
          {std::numeric_limits<T>::max(), std::numeric_limits<T>::max(), std::numeric_limits<T>::max()}
        , {std::numeric_limits<T>::lowest(), std::numeric_limits<T>::lowest(), std::numeric_limits<T>::lowest()}
      };
    };
  };

  using box3i = box3<int>;
  using box3z = box3<size_t>;
  using box3f = box3<float>;
  using box3d = box3<double>;
}
