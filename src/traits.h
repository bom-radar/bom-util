/*------------------------------------------------------------------------------
 * Bureau of Meteorology C++ Utility Library
 *
 * Copyright 2014 Mark Curtis
 * Copyright 2016 Commonwealth of Australia, Bureau of Meteorology
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *----------------------------------------------------------------------------*/
#pragma once

#include "macros.h"

#include <cstring>
#include <initializer_list>
#include <stdexcept>
#include <type_traits>
#include <utility>

namespace bom
{
  /// Constexpr function to determine if all of the passed values are true
  constexpr auto all_of() { return true; }
  template <typename... Args>
  constexpr auto all_of(bool v, Args&&... args) { return v && all_of(std::forward<Args>(args)...); }

  /// Constexpr function to determine if any of the passed values are true
  constexpr auto any_of() { return false; }
  template <typename... Args>
  constexpr auto any_of(bool v, Args&&... args) { return v || any_of(std::forward<Args>(args)...); }

  /// Constexpr function to find the maximum of all passed values
  template <typename L>
  constexpr auto min_of(L l) { return l; }
  template <typename L, typename... Args>
  constexpr auto min_of(L l, Args... args) { return l < min_of(args...) ? l : min_of(args...); }

  /// Constexpr function to find the maximum of all passed values
  template <typename L>
  constexpr auto max_of(L l) { return l; }
  template <typename L, typename... Args>
  constexpr auto max_of(L l, Args... args) { return l < max_of(args...) ? max_of(args...) : l; }

  /// Constexpr function to determine whether the arguments represent an ascending sequence of integers
  constexpr auto is_sequential() { return true; }
  template <typename L>
  constexpr auto is_sequential(L l) { return true; }
  template <typename L, typename R, typename... Args>
  constexpr auto is_sequential(L l, R r, Args... args) { return l + 1 == r && is_sequential(r, args...); }

  /// Constexpr function to determine whether the arguments represent an ascending sequence of integers starting at 0
  template <typename L, typename... Args>
  constexpr auto is_index_sequence(L l, Args... args) { return l == 0 && is_sequential(l, args...); }

  /// Get type of first type in parameter pack
  template <typename F, typename... T>
  struct first_type{ typedef F type; };

  /// get the index of the first occurence of a type in a parameter pack
  template <typename S, typename F, typename... T>
  struct type_index : std::integral_constant<int, type_index<S, T...>::value + 1>
  { };
  template <typename S, typename... T>
  struct type_index<S, S, T...> : std::integral_constant<int, 0>
  { };

  /// return true if type is a member of a typelist
  template <typename S, typename... T>
  struct type_one_of : std::integral_constant<bool, any_of(std::is_same<S, T>::value...)>
  { };

  /// Macro used to define traits for an enumerate type
  /** The arguments to this macro should be the name of the enumerate (relative to the bom namespace) followed
   *  by each of the enumerate values in the same order as they are delcared in. */
  #define BOM_DECLARE_ENUM_TRAITS(x, ...) \
    template <> \
    struct enum_traits<x> \
    { \
      using type = x; \
      using value_type = std::underlying_type<x>::type; \
      static inline constexpr bool specialized = true; \
      static inline constexpr char const* name = #x; \
      static inline constexpr size_t count = BOM_NUM_ARGS(__VA_ARGS__); \
      static inline constexpr char const* labels[] = { BOM_STRINGIFY_ARGS(__VA_ARGS__) }; \
      static inline constexpr value_type values[] = { BOM_PREPEND_TYPE_ARGS(__VA_ARGS__) }; \
      static inline constexpr value_type max_value = max_of(BOM_PREPEND_TYPE_ARGS(__VA_ARGS__)); \
      static inline constexpr bool is_simple = is_index_sequence( BOM_PREPEND_TYPE_ARGS(__VA_ARGS__) ); \
    }
#if 0
  #define BOM_DEFINE_ENUM_TRAITS(x) \
    constexpr decltype(bom::enum_traits<x>::name) bom::enum_traits<x>::name; \
    constexpr decltype(bom::enum_traits<x>::count) bom::enum_traits<x>::count; \
    constexpr decltype(bom::enum_traits<x>::labels) bom::enum_traits<x>::labels; \
    constexpr decltype(bom::enum_traits<x>::values) bom::enum_traits<x>::values; \
    constexpr decltype(bom::enum_traits<x>::max_value) bom::enum_traits<x>::max_value; \
    constexpr decltype(bom::enum_traits<x>::is_simple) bom::enum_traits<x>::is_simple
#else
  #define BOM_DEFINE_ENUM_TRAITS(x)
#endif

  /// Traits template used to provide reflection of enum values and labels
  /** Traits may be defined for new enum types using the BOM_DECLARE_ENUM_TRAITS() macro.  After running this
   *  macro a specialization of the enum_traits template will exist for the enum which contains the following
   *  members:
   *    type       - the enum type itself (T)
   *    value_type - the underlying type for the enum (std::underlying_type<T>::type)
   *    name       - name of the enum type (char const*)
   *    count      - the number of enumerates defined by the enum type (size_t)
   *    labels     - an array of strings specifying the name of each enumerate (char const* [])
   *    values     - an array of values specifying the underlying value of each enumerate (value_type[])
   *    is_simple  - a boolean which is true if values are exactly [0..N)
   */
  template <typename T>
  struct enum_traits
  {
    static constexpr bool specialized = false;
  };

  /// Get the string associated with an enum value
  /** In the case of a complex enum where multiple enumerates map to the same value, the first one listed in
   *  the enum_traits for the type will be returned. */
  template <typename T>
  constexpr auto enum_label(T val) -> typename std::enable_if<enum_traits<T>::is_simple, char const*>::type
  {
    return enum_traits<T>::labels[static_cast<typename std::underlying_type<T>::type>(val)];
  }
  template <typename T>
  auto enum_label(T val) -> typename std::enable_if<!enum_traits<T>::is_simple, char const*>::type
  {
    for (size_t i = 0; i < enum_traits<T>::count; ++i)
      if (enum_traits<T>::values[i] == static_cast<typename std::underlying_type<T>::type>(val))
        return enum_traits<T>::labels[i];
    throw std::logic_error{std::string("assert - invalid enum_traits detected for type:") + enum_traits<T>::name};
  }

  /// Get the enum associated with a string
  template <typename T>
  auto enum_value(char const* str) -> T
  {
    for (size_t i = 0; i < enum_traits<T>::count; ++i)
      if (strcmp(str, enum_traits<T>::labels[i]) == 0)
        return static_cast<T>(enum_traits<T>::values[i]);
    throw std::runtime_error{std::string("unknown enumerate for type ") + enum_traits<T>::name + ": " + str};
  }

  /// Adapter that converts a pointer to a function into a type which can be invoked with operator()
  /** This adapter is particularly useful for defining predicates and deleters as used by various templates
   *  within the standard library.  For example, one could define a functor type which simply calls the standard
   *  C free() function and then use this type as the deleter in a unique_ptr.  Since the function pointer itself
   *  is part of the type of the deleter, the deleter will occupy no space or runtime overhead in the resultant
   *  unique_ptr type.
   */
  template <auto function>
  struct functor
  {
    template <typename... Args>
    auto operator()(Args&&... args)
    {
      return function(std::forward<Args>(args)...);
    }
  };

  /// Functor type which simply dereferences its input value
  template <typename T>
  struct dereference
  {
    auto operator()(T const& t) noexcept(noexcept(*std::declval<T>()))-> decltype(*t)
    {
      return *t;
    }
  };

  /// Functor type which calls the subscript operator on its first input using the second input as key
  struct subscript
  {
    template <typename T, typename K>
    inline auto operator()(T&& obj, K&& key) const -> decltype(std::forward<T>(obj)[std::forward<K>(key)])
    {
      return std::forward<T>(obj)[std::forward<K>(key)];
    }
  };

  /// Trait to determine if a type is an instance of a particular template
  template <typename, template<class> class>
  struct is_instance_of : public std::false_type {};

  template <typename T, template<class> class U>
  struct is_instance_of<U<T>, U> : public std::true_type {};
}
