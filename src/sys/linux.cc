/*------------------------------------------------------------------------------
 * Bureau of Meteorology C++ Utility Library
 *
 * Copyright 2016 Commonwealth of Australia, Bureau of Meteorology
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *----------------------------------------------------------------------------*/
#include "config.h"
#include "linux.h"
#include "../trace.h"
#include <systemd/sd-daemon.h>
#include <systemd/sd-journal.h>
#include <sys/eventfd.h>
#include <sys/inotify.h>
#include <sys/signalfd.h>
#include <unistd.h>
#include <system_error>

#if BOM_SYSTEMD_VERSION < 227
// just here for my_sd_pid_notify_with_fds
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/un.h>
#include <stddef.h>
#endif

using namespace bom;
using namespace bom::sys;

static constexpr trace::level_marker_set col_markers = { "XX ", "\033[36m** ", "\033[31;1mEE ", "\033[33mWW ", "-- ", "\033[32mDD " };
static constexpr char const*             col_terminator = "\033[0m\n";

static constexpr trace::level_marker_set sysd_markers = { "", SD_NOTICE, SD_ERR, SD_WARNING, SD_INFO, SD_DEBUG };
static constexpr char const*             sysd_terminator = "\n";

auto bom::sys::setup_trace_coloured() -> void
{
  trace::set_level_markers(&col_markers);
  trace::set_message_terminator(col_terminator);
}

auto bom::sys::setup_trace_systemd() -> void
{
  trace::set_prefix_timestamp(false);
  trace::set_level_markers(&sysd_markers);
  trace::set_message_terminator(sysd_terminator);
}

auto bom::sys::sd_notify_state(service_state state) -> void
{
  switch (state)
  {
  case service_state::ready:
    sd_notify(0, "READY=1");
    break;
  case service_state::reloading:
    sd_notify(0, "RELOADING=1");
    break;
  case service_state::stopping:
    sd_notify(0, "STOPPING=1");
    break;
  }
}

auto bom::sys::sd_notify_status(string status) -> void
{
  auto pos = status.find_first_of("\n\r");
  if (pos != string::npos)
  {
    trace::error("sd_notify_status passed multi-line input, status truncated");
    status.resize(pos);
  }

  status.insert(0, "STATUS=");
  sd_notify(0, status.c_str());
}

#if BOM_SYSTEMD_VERSION < 227
// older systemd libraries do not implement FD passing correctly
// this is a modified implementation of sd_pid_notify_with_fds taken from v227
static auto my_sd_pid_notify_with_fds(const char *state, const int *fds, unsigned n_fds) -> int
{
  if (!state)
    return -EINVAL;

  if (n_fds > 0 && !fds)
    return -EINVAL;

  auto e = getenv("NOTIFY_SOCKET");
  if (!e)
    return 0;

  /* Must be an abstract socket, or an absolute path */
  if ((e[0] != '@' && e[0] != '/') || e[1] == 0)
    return -EINVAL;

  auto fd = file_desc_hnd{socket(AF_UNIX, SOCK_DGRAM|SOCK_CLOEXEC, 0)};
  if (!fd)
    return -errno;

  sockaddr_un sockaddr;
  sockaddr.sun_family = AF_UNIX;
  strncpy(sockaddr.sun_path, e, sizeof(sockaddr.sun_path));
  if (sockaddr.sun_path[0] == '@')
    sockaddr.sun_path[0] = 0;

  iovec iov;
  iov.iov_base = (void*) state;
  iov.iov_len = strlen(state);

  msghdr msg;
  msg.msg_iov = &iov;
  msg.msg_iovlen = 1;
  msg.msg_name = &sockaddr;
  msg.msg_namelen = offsetof(struct sockaddr_un, sun_path) + strlen(e);
  if (msg.msg_namelen > sizeof(struct sockaddr_un))
    msg.msg_namelen = sizeof(struct sockaddr_un);

  if (n_fds > 0) {
    /* CMSG_SPACE(0) may return value different then zero, which results in miscalculated controllen. */
    msg.msg_controllen = CMSG_SPACE(sizeof(int) * n_fds);
    msg.msg_control = alloca(msg.msg_controllen);

    auto cmsg = CMSG_FIRSTHDR(&msg);
    cmsg->cmsg_level = SOL_SOCKET;
    cmsg->cmsg_type = SCM_RIGHTS;
    cmsg->cmsg_len = CMSG_LEN(sizeof(int) * n_fds);
    memcpy(CMSG_DATA(cmsg), fds, sizeof(int) * n_fds);
  }

  if (sendmsg(fd, &msg, MSG_NOSIGNAL) >= 0)
    return 1;

  return -errno;
}
#endif

auto bom::sys::sd_store_fds(vector<int> const& fds) -> void
{
  if (fds.empty())
    return;

#if BOM_SYSTEMD_VERSION < 227
  auto ret = my_sd_pid_notify_with_fds("FDSTORE=1", fds.data(), fds.size());
#else
  auto ret = sd_pid_notify_with_fds(0, 0, "FDSTORE=1", fds.data(), fds.size());
#endif
  if (ret < 0)
    throw std::system_error{ret, std::generic_category(), "sd_pid_notify_with_fds"};
  if (ret == 0)
    trace::log("systemd not available, fds not stored");
  trace::log("stored {} fds with systemd", fds.size());
}

auto bom::sys::sd_retrieve_fds() -> vector<int>
{
  auto ret = sd_listen_fds(0);
  if (ret < 0)
    throw std::system_error{ret, std::generic_category(), "sd_listen_fds"};
  if (ret == 0)
    trace::log("systemd not available or no fds stored, fds not retrieved");
  auto fds = vector<int>(ret);
  for (size_t i = 0; i < fds.size(); ++i)
    fds[i] = SD_LISTEN_FDS_START + i;
  return fds;
}

signalfd::signalfd(std::initializer_list<int> signals, bool non_blocking)
{
  // setup a mask of the signals we want to handle using signalfd
  sigset_t mask;
  if (sigemptyset(&mask) != 0)
    throw std::system_error{errno, std::generic_category(), "sigemptyset"};
  for (auto sig : signals)
    if (sigaddset(&mask, sig) != 0)
      throw std::system_error{errno, std::generic_category(), "sigaddset"};

  // block these signals so that we only see them via signalfd (not their default handlers)
  if (sigprocmask(SIG_BLOCK, &mask, nullptr) != 0)
    throw std::system_error{errno, std::generic_category(), "sigprocmask"};

  // initialize the socketfd system
  fd_.reset(::signalfd(-1, &mask, (non_blocking ? SFD_NONBLOCK : 0) | SFD_CLOEXEC));
  if (!fd_)
    throw std::system_error{errno, std::generic_category(), "signalfd"};
}

auto signalfd::read() -> int
{
  struct signalfd_siginfo fdsi;

  while (true)
  {
    auto len = ::read(fd_, &fdsi, sizeof(fdsi));
    if (len == -1)
    {
      // system call interrupted by signal - try again
      if (errno == EINTR)
        continue;

      // no signals waiting - terminate loop
      if (errno == EAGAIN || errno == EWOULDBLOCK)
        return 0;

      throw std::system_error{errno, std::generic_category(), "read(signalfd)"};
    }

    // partial read
    // this cannot happen in practice due to signalfd implementation in kernel
    if (len != sizeof(fdsi))
      throw std::runtime_error{"partial read of signalfd"};

    return fdsi.ssi_signo;
  }
}

eventfd::eventfd(bool non_blocking)
{
  fd_.reset(::eventfd(0, (non_blocking ? EFD_NONBLOCK : 0) | EFD_CLOEXEC));
  if (!fd_)
    throw std::system_error{errno, std::generic_category(), "eventfd"};
}

auto eventfd::signal() -> void
{
  eventfd_write(fd_, 1);
}

auto eventfd::clear() -> void
{
  eventfd_t val;
  eventfd_read(fd_, &val);
}

sd_watchdog::sd_watchdog(timestamp now)
  : next_{now}
{
  uint64_t usec;
  auto ret = sd_watchdog_enabled(0, &usec);
  if (ret < 0)
    throw std::system_error{errno, std::generic_category(), "sd_watchdog_enabled"};
  period_ = std::chrono::microseconds{ret > 0 ? usec / 2 : 0};
  trace::log("systemd watchdog {}", enabled() ? "enabled" : "disabled");
}

auto sd_watchdog::send_keepalive(timestamp now) -> void
{
  if (enabled())
  {
    next_ = now + period_;
    sd_notify(0, "WATCHDOG=1");
  }
}

auto sd_watchdog::update(timestamp now) -> void
{
  if (enabled() && next_ <= now)
  {
    next_ = now + period_;
    sd_notify(0, "WATCHDOG=1");
  }
}

inotify::inotify(bool non_blocking)
{
  // initialize the inotify system
  fd_.reset(inotify_init1((non_blocking ? IN_NONBLOCK : 0) | IN_CLOEXEC));
  if (!fd_)
    throw std::system_error{errno, std::generic_category(), "inotify_init1"};
}

auto inotify::add_watch(string const& path, uint32_t mask, callback_fn callback) -> int
{
  auto wd = inotify_add_watch(fd_, path.c_str(), mask);
  if (wd == -1)
    throw std::system_error{errno, std::generic_category(), "inotify_add_watch"};
  watches_[wd] = std::move(callback);
  return wd;
}

auto inotify::remove_watch(int wd) -> void
{
  auto iwatch = watches_.find(wd);
  if (iwatch == watches_.end())
    return;

  if (inotify_rm_watch(fd_, wd) != 0)
  {
    auto err = std::system_error{errno, std::generic_category(), "inotify_rm_watch"};
    trace::warning("{}", format_exception(err));
  }

  watches_.erase(iwatch);
}

auto inotify::dispatch_events() -> void
{
  char buf[BUFSIZ];

  while (true)
  {
    auto len = read(fd_, buf, sizeof(buf));
    if (len == -1)
    {
      // system call interrupted by signal - try again
      if (errno == EINTR)
        continue;

      // no more events waiting - terminate loop
      if (errno == EAGAIN || errno == EWOULDBLOCK)
        break;

      throw std::system_error{errno, std::generic_category(), "read(inotify)"};
    }

    // process each retrieved inotify event
    int pos = 0;
    while (pos < len)
    {
      auto event = reinterpret_cast<inotify_event*>(&buf[pos]);
      auto iwatch = watches_.find(event->wd);
      if (iwatch != watches_.end())
        iwatch->second(*event);
      else
        trace::warning("inotify event received for unknown watch descriptor");
      pos += sizeof(inotify_event) + event->len;
    }

    // partial read
    // this cannot happen in practice due to inotify implementation in kernel
    if (pos != len)
      throw std::runtime_error{"partial read of inotify"};
  }
}
