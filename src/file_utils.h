/*------------------------------------------------------------------------------
 * Bureau of Meteorology C++ Utility Library
 *
 * Copyright 2014 Mark Curtis
 * Copyright 2016 Commonwealth of Australia, Bureau of Meteorology
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *----------------------------------------------------------------------------*/
#pragma once

#include "raii.h"
#include "string_utils.h"
#include "timestamp.h"
#include "traits.h"
#include <sys/types.h>
#include <sys/stat.h>
#include <dirent.h>
#include <unistd.h>
#include <cstdio>
#include <fstream>
#include <functional>
#include <memory>
#include <vector>

namespace bom
{
  /// File I/O mode
  enum class io_mode
  {
      create
    , read_only
    , read_write
  };
  BOM_DECLARE_ENUM_TRAITS(io_mode, create, read_only, read_write);

  namespace file_options
  {
    enum class follow_links : bool { no, yes };
    enum class require_atomic : bool { no, yes };
    enum class recursive : bool { no, yes };
    enum class parents_only : bool { no, yes };
  }

  /// File system metadata for a file system entry
  /** Use the stat_path() function to obtain an instance of this class for a given path. */
  class file_metadata
  {
  public:
    /// Cast to bool as shorthand for path_exists()
    operator bool() const                 { return path_exists(); }

    /// Does the path point to a valid file system object?
    /** This will also return false if the path exists but is for some reason invisible to the user, for example
     *  if the user does not have permission to traverse the directories leading to the leaf element of the path. */
    auto path_exists() const -> bool      { return sb_.st_mode != 0; }

    /// Is the entry a regular file?
    auto is_file() const -> bool          { return S_ISREG(sb_.st_mode); }

    /// Is the entry a directory?
    auto is_directory() const -> bool     { return S_ISDIR(sb_.st_mode); }

    /// Get the size of the entry in bytes
    auto size() const -> size_t           { return sb_.st_size; }

    /// Get the modification time of the file
    auto modification_time() const -> timestamp { return timestamp{sb_.st_mtim.tv_sec}; }

    /// Access the underlying stat structure
    auto data() -> struct stat*           { return &sb_; }

  private:
    struct stat sb_;
  };

  /// RAII wrapper for a C API style file stream (ie: FILE*)
  using file_stream_hnd = unique_hnd<FILE*, functor<fclose>, nullptr>;
  /// RAII wrapper for a C API style file descriptor (ie: int)
  using file_desc_hnd = unique_hnd<int, functor<close>, -1>;
  /// RAII wrapper for a C API style directory stream (ie: DIR*)
  using dir_stream_hnd = unique_hnd<DIR*, functor<closedir>, nullptr>;

  /// Explicitly shutdown a socket and then close its file descriptor
  auto shutdown_close_socket(int fd) -> void;

  /// File descriptor handle specalized for sockets
  /** In most situations it is fine to simply use the file_desc_hnd raii type to manage file descriptors for
   *  sockets since it also calls close() on the file descriptor.  The difference is that this wrapper will
   *  call shutdown() on the socket, then close().  Under normal circumstances this is not needed.  The main
   *  reason you would want to call shutdown() is if the file descriptor for the socket has been duped to another
   *  process, but you want to close the connection in all processes.
   *
   *  The only time we actuall want to do this (so far) is when handing off sockets to the systemd service
   *  manager using the FDSTORE functionality.  When we do this there is no way to then force systemd to 'forget'
   *  about the sockets.  On restart of a service it will happily hand the file descriptors back to us, but if all
   *  we do is close() them when we want to disconnect a client then the copy that systemd is holding on to will
   *  keep the socket alive.  Because of this it is important for us to explicitly shutdown() the socket before
   *  we close() the file descriptor when using FDSTORE.  This will terminate the connection even if systemd still
   *  has a copy of the descriptor open.  Systemd will then detect the lost connection and abandon it.
   *
   *  In later versions of systemd this approach is not necessary.  There is an FDSTOREREMOVE flag for sd_notify
   *  that can be used to tell systemd to forget our descriptors after we've recived them back.  If you have done
   *  this then file_desc_hnd will suffice. */
  using socket_desc_hnd = unique_hnd<int, functor<shutdown_close_socket>, -1>;

  /// Determine whether the given path is absolute or relative
  auto is_absolute_path(char const* path) -> bool;
  inline auto is_absolute_path(string const& path) -> bool
  {
    return is_absolute_path(path.c_str());
  }

  /// Collect file system information about the file at a path
  auto stat_path(
        string const& path
      , file_options::follow_links follow_links = file_options::follow_links::yes
      ) -> file_metadata;
  auto stat_path(
        dir_stream_hnd const& dir
      , char const* name
      , file_options::follow_links follow_links = file_options::follow_links::yes
      ) -> file_metadata;

  /// Create a directory
  /** The behaviour of this function is very similar to a 'mkdir -p' bash command.  That is:
   *  - If the target directory already exists the function simply returns (ie: no-op)
   *  - Directories are created recursively along the path as needed .
   *  The parents_only option will cause the creation of all directories needed to contain a file specified by
   *  path, but will not create a directory at the path itself.  This is useful for when you need to write a file
   *  and need to ensure the target directory exists (without needing to manually strip the file component from
   *  the path).  The presence of a trailing slash in this case is important.  When this option is set:
   *  - '/foo/bar/abc' will create only foo and bar
   *  - '/foo/bar/abc/' will create foo, bar and abc
   */
  auto make_directory(
        string const& path
      , file_options::parents_only parents_only = file_options::parents_only::no
      ) -> void;

  /// Iterate over a directory calling the supplied function for each file
  /** For every entry in the directory, the callback function will be called passing a string containing the name
   *  of the entry.  If the callback returns false then no further iterations will be performed and the function
   *  will return.  This is useful for terminating a search once the desired file has been found.
   *
   *  If all entries in the directory are iterated over the function will return true.  If the iteration was
   *  terminated early due to the callback returning false, then the function will return false.  */
  auto iterate_directory(
        string const& path
      , std::function<bool(dir_stream_hnd const&, char const*)> const& func
      ) -> bool;
  auto iterate_directory(
        dir_stream_hnd const& dir
      , char const* name
      , std::function<bool(dir_stream_hnd const&, char const*)> const& func
      ) -> bool;

  /// Delete a directory
  auto delete_directory(string const& path, file_options::recursive recursive) -> void;
  auto delete_directory(dir_stream_hnd const& dir, char const* name, file_options::recursive recursive) -> void;

  /// Copy a file
  auto copy_file(string const& source, string const& destination) -> void;

  /// Move a file
  /** This function will attempt to perform a simple rename on the file which if successful will ensure an atomic
   *  move from the file system perspective.  The rename may fail if the destination path is on a different file
   *  system to the origin.  In this case the function will fall back on a copy/remove strategy which is more
   *  likely to succeed but will not appear as an atomic update to the file system. */
  auto move_file(string const& source, string const& destination, file_options::require_atomic require_atomic) -> void;

  /// Delete a file
  auto delete_file(string const& path) -> void;
  auto delete_file(dir_stream_hnd const& dir, char const* name) -> void;

  /// Update the modification and access times of a file
  auto update_file_modification_time(string const& path) -> void;

  /// Read the entire contents of a file into a string
  auto read_file_into_string(string const& path) -> string;

  /// Read the entire contents of a file into a vector
  /** WARNING - this function does NOT ensure that the buffer is terminated with a null character */
  auto read_file_into_vector(string const& path) -> vector<char>;

  /// Write the contents of a vector to a file
  auto write_file_from_vector(string const& path, vector<char> const& data) -> void;

  /// Unique path for a temporary file
  /**
   * This class is used to create a temporary file at an optionally specified location.  The created file is
   * deleted automatically when the object goes out of scope.  A user may call the release() function to release
   * ownership of the file so that it is not deleted by the destructor.
   */
  class temporary_file
  {
  public:
    /// Create a temporary file in the current working directory
    /**
     * The file will be created as 'tmp.XXXXXX' where XXXXXX is replaced by characters which will make the file
     * name unique.  It is guaranteed that the file did not exist before the call and that it was created by
     * the application. */
    temporary_file();

    /// Create a temporary file with a specified path and/or base file name
    /**
     * The file will be created as 'base.XXXXXX' where base is the passed string and XXXXXX is replaced by
     * characters which make the file name unique.  It is guaranteed that the file did not exist before the
     * call and is created by the application.
     */
    explicit temporary_file(string const& base);

    temporary_file(temporary_file const&) = delete;
    temporary_file(temporary_file&& rhs) noexcept;

    auto operator=(temporary_file const&) -> temporary_file& = delete;
    auto operator=(temporary_file&& rhs) noexcept -> temporary_file&;

    ~temporary_file();

    auto get() const -> string const& { return path_; }
    auto reset() -> void;
    auto release() -> void;

  private:
    string path_;
  };
}
