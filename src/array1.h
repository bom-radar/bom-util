/*------------------------------------------------------------------------------
 * Bureau of Meteorology C++ Utility Library
 *
 * Copyright 2014 Mark Curtis
 * Copyright 2016 Commonwealth of Australia, Bureau of Meteorology
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *----------------------------------------------------------------------------*/
#pragma once

#include "raii.h"
#include <algorithm>

namespace bom
{
  template <typename T>
  class array1
  {
  public:
    using value_type = T;
    using pointer = T*;
    using const_pointer = T const*;
    using reference = T&;
    using const_reference = const T&;
    using size_type = std::size_t;
    using difference_type = std::ptrdiff_t;
    using iterator = pointer;
    using const_iterator = const_pointer;
    using reverse_iterator = std::reverse_iterator<iterator>;
    using const_reverse_iterator = std::reverse_iterator<const_iterator>;

  public:
    array1() noexcept
      : size_{0}
    { }

    array1(size_type size)
      : size_{size}
      , data_{new T[size_]}
    { }

    array1(array1 const& rhs)
      : size_{rhs.size_}
      , data_{new T[size_]}
    {
      std::copy(rhs.data_.get(), rhs.data_.get() + size_, data_.get());
    }

    array1(array1&& rhs) noexcept
      : size_{rhs.size_}
      , data_{std::move(rhs.data_)}
    {
      rhs.size_ = 0;
    }

    auto operator=(array1 const& rhs) -> array1&
    {
      unique_ptr<T[]> copy{new T[rhs.size_]};
      std::copy(rhs.data_.get(), rhs.data_.get() + size_, copy.get());

      size_ = rhs.size_;
      data_ = std::move(copy);
      return *this;
    }

    auto operator=(array1&& rhs) noexcept -> array1&
    {
      size_ = rhs.size_;
      data_ = std::move(rhs.data_);
      rhs.size_ = 0;
      return *this;
    }

    auto resize(size_type size) -> void
    {
      if (size_ == size)
        return;
      data_.reset(new T[size]);
      size_ = size;
    }

    auto size() const noexcept -> size_type                         { return size_; }
    auto empty() const noexcept -> bool                             { return size_ == 0; }

    auto operator[](size_type i) noexcept -> reference              { return data_[i]; }
    auto operator[](size_type i) const noexcept -> const_reference  { return data_[i]; }

    auto data() const noexcept -> const_pointer                     { return data_.get(); }
    auto data() noexcept -> pointer                                 { return data_.get(); }

    auto begin() noexcept -> iterator                               { return data_.get(); }
    auto begin() const noexcept -> const_iterator                   { return data_.get(); }
    auto end() noexcept -> iterator                                 { return data_.get() + size_; }
    auto end() const noexcept -> const_iterator                     { return data_.get() + size_; }

    auto rbegin() noexcept -> reverse_iterator                      { return reverse_iterator{data_.get() + size_}; }
    auto rbegin() const noexcept -> const_reverse_iterator          { return const_reverse_iterator{data_.get() + size_}; }
    auto rend() noexcept -> reverse_iterator                        { return reverse_iterator{data_.get()}; }
    auto rend() const noexcept -> const_reverse_iterator            { return const_reverse_iterator{data_.get()}; }

    auto cbegin() const noexcept -> const_iterator                  { return data_.get(); }
    auto cend() const noexcept -> const_iterator                    { return data_.get() + size_; }
    auto crbegin() const noexcept -> const_reverse_iterator         { return const_reverse_iterator{data_.get() + size_}; }
    auto crend() const noexcept -> const_reverse_iterator           { return const_reverse_iterator{data_.get()}; }

    auto fill(T val) -> void
    {
      std::fill(data_.get(), data_.get() + size_, val);
    }

  private:
    size_type       size_;
    unique_ptr<T[]> data_;
  };

  using array1i = array1<int>;
  using array1l = array1<long>;
  using array1z = array1<size_t>;
  using array1f = array1<float>;
  using array1d = array1<double>;
}
