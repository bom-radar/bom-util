/*------------------------------------------------------------------------------
 * Bureau of Meteorology C++ Utility Library
 *
 * Copyright 2014 Mark Curtis
 * Copyright 2016 Commonwealth of Australia, Bureau of Meteorology
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *----------------------------------------------------------------------------*/
#pragma once

#include "raii.h"
#include "traits.h"

#include "fmt/format.h"
#include "fmt/chrono.h"
#include "fmt/args.h"

#include <array>
#include <cstring>
#include <istream>
#include <ostream>
#include <stdexcept>
#include <string>
#include <tuple>
#include <vector>

#include <iostream>
#include <typeinfo>

#include <string_view>

namespace bom
{
  /// Use std::string as our standard string type
  using string = std::string;

  /// Use std::string_view to unify c-string and std::string based APIs
  using string_view = std::string_view;

  /// Allow use of standard UDL for strings
  using namespace std::string_literals;

  /// Allow use of to_string provided by fmt
  using fmt::to_string;

  /// Allow use of ""_a UDLs for named format string arguments
  using namespace fmt::literals;

  /// Trait used to indicate validity of conversions and assignments via to/from string functions when serializing
  template <typename T, typename = void>
  struct allow_simple_string_conversions
  {
    static constexpr bool value = std::is_arithmetic<std::decay_t<T>>::value || enum_traits<std::decay_t<T>>::specialized;
  };

  /// Return true if target string ends with specified ending string
  auto ends_with(string const& target, string const& ending) -> bool;
  auto ends_with(string const& target, char const* ending) -> bool;

  /// Trim whitespace from left of string
  auto ltrim(string& str) -> void;

  /// Trim whitespace from right of string
  auto rtrim(string& str) -> void;

  /// Trim whitespace from both ends of string
  auto trim(string& str) -> void;

  /// Error thrown when conversion to or from a string fails
  class string_conversion_error : public std::runtime_error
  {
  public:
    string_conversion_error(string type);
    string_conversion_error(string type, string from);

    auto type() const -> const string&  { return type_; }
    auto from() const -> const string&  { return from_; }
    auto has_from() const -> bool       { return has_from_; }

  private:
    string type_;
    string from_;
    bool   has_from_;
  };

  /// Traits class which implements string conversion operations
  template <typename T, typename = void>
  struct string_conversions
  {
    // you must specialize this template to use it!
    static_assert(sizeof(T) == -1, "no string_conversions specialization supplied for type T");

    /// Convert the first len characters of a string to the type
    /**
     * The conversion must consume the exact length of the string.  Any leading whitespace or trailing
     * characters must cause a string_conversion_error to be thrown.
     *
     * Extra parameters may be added to the end of this function to implement type specific options such as
     * specifying the base for integers.  These parameters will be directly forwarded from the from_string()
     * function.
     */
    static auto read_string(char const* str, size_t len) -> T;
  };

  /// Convert a C style string to a value
  /** The entire input string must be consumed by the conversion.  Trailing characters that cannot be converted
   *  will cause the conversion to fail and throw a string_conversion_error. */
  template <typename T, typename... Args>
  inline auto from_string(char const* str, Args&&... args) -> T
  {
    return string_conversions<T>::read_string(str, std::strlen(str), std::forward<Args>(args)...);
  }

  /// Convert a string to a value
  /** The entire input string must be consumed by the conversion.  Trailing characters that cannot be converted
   *  will cause the conversion to fail and throw a string_conversion_error. */
  template <typename T, typename... Args>
  inline auto from_string(string const& str, Args&&... args) -> std::conditional_t<std::is_same<T, string>::value, string const&, T>
  {
    return string_conversions<T>::read_string(str.c_str(), str.size(), std::forward<Args>(args)...);
  }
  template <>
  inline auto from_string<string>(string const& str) -> string const&
  {
    return str;
  }

  /// Convert the first n characters of a C style string to a value
  /** The entire len characters must be consumed by the conversion.  Trailing characters that cannot be converted
   * will cause the conversion to fail and throw a string_conversion_error. */
  template <typename T, typename... Args>
  inline auto from_string_n(char const* str, size_t len, Args&&... args) -> T
  {
    return string_conversions<T>::read_string(str, len, std::forward<Args>(args)...);
  }

  /// Convert the first n characters of a string to a value
  /** The entire len characters must be consumed by the conversion.  Trailing characters that cannot be converted
   * will cause the conversion to fail and throw a string_conversion_error. */
  template <typename T, typename... Args>
  inline auto from_string_n(string const& str, size_t len, Args&&... args) -> T
  {
    return string_conversions<T>::read_string(str.c_str(), len, std::forward<Args>(args)...);
  }

  /// Simple object to ease insertion of indentation to streams
  /** Use it as if it was an integer:
   *  {
   *    indent in;
   *    cout << in << "no indent here";
   *    cout << in + 4 << "this is indented by 4 spaces";
   *    indent_will_be_2_spaces_inside_here(obj, in + 2);
   *  }
   */
  class indent
  {
  public:
    constexpr indent() : size_(0) { }
    constexpr indent(int size) : size_(size) { }

    constexpr indent operator+(int size) const  { return {size_ + size}; }
    constexpr indent operator-(int size) const  { return {size_ - size}; }

    indent& operator+=(int size)                { size_ += size; return *this; }
    indent& operator-=(int size)                { size_ -= size; return *this; }

    constexpr int size() const                  { return size_; }

  private:
    int size_;
  };
  auto operator<<(std::ostream& os, const indent& in) -> std::ostream&;
  template <>
  struct string_conversions<indent>
  {
    static auto read_string(char const* str, size_t len) -> indent;
  };

  /// Simple wrapper around a size_t used for easy formatting of integral types into disk size types (KB, MB etc)
  struct fmt_bytes
  {
    constexpr fmt_bytes(size_t value) : value(value) { }
    size_t value;
  };

  /// Format an exception description, including all nested exceptions
  auto format_exception(std::exception const& err, indent prefix = 2) -> string;

  /// Tokenize a string into a vector
  /**
   * \param str[in]     String to be tokenized
   * \param delims[in]  Character(s) used to delimit tokens within str
   * \param args...     Arguments to pass onward to from_string_n
   * \return            Vector of extracted values
   */
  template <typename T, typename... Args>
  auto tokenize(string const& str, char const* delims, Args const&... args) -> std::vector<T>
  {
    std::vector<T> ret;
    size_t pos = 0;
    while ((pos = str.find_first_not_of(delims, pos)) != string::npos)
    {
      size_t end = str.find_first_of(delims, pos + 1);
      size_t len = (end == string::npos ? str.size() : end) - pos;
      ret.push_back(from_string_n<T>(str.c_str() + pos, len, args...));
      pos += len;
    }
    return ret;
  }
  template <> auto tokenize<string>(string const& str, char const* delims) -> std::vector<string>;

  // string_conversion templates for enum_traits'ed types
  template <typename T>
  struct string_conversions<T, typename std::enable_if<std::is_enum<T>::value>::type>
  {
    // ensure that enum_traits has been specialized for this enum type
    static_assert(enum_traits<T>::specialized, "no enum_traits specialization supplied for type T");

    static auto read_string(char const* str, size_t len) -> T
    try
    {
      // TODO - this dosn't actually check for an exact length match
      return enum_value<T>(str);
    }
    catch (...)
    {
      std::throw_with_nested(string_conversion_error{enum_traits<T>::name, string(str, len)});
    }
  };

  template <>
  struct string_conversions<bool>
  {
    static auto read_string(char const* str, size_t len) -> bool;
  };

  template <>
  struct string_conversions<char>
  {
    static auto read_string(char const* str, size_t len) -> char;
  };

  template <>
  struct string_conversions<signed char>
  {
    static auto read_string(char const* str, size_t len, int base = 10) -> signed char;
  };

  template <>
  struct string_conversions<unsigned char>
  {
    static auto read_string(char const* str, size_t len, int base = 10) -> unsigned char;
  };

  template <>
  struct string_conversions<short>
  {
    static auto read_string(char const* str, size_t len, int base = 10) -> short;
  };

  template <>
  struct string_conversions<unsigned short>
  {
    static auto read_string(char const* str, size_t len, int base = 10) -> unsigned short;
  };

  template <>
  struct string_conversions<int>
  {
    static auto read_string(char const* str, size_t len, int base = 10) -> int;
  };

  template <>
  struct string_conversions<unsigned int>
  {
    static auto read_string(char const* str, size_t len, int base = 10) -> unsigned int;
  };

  template <>
  struct string_conversions<long>
  {
    static auto read_string(char const* str, size_t len, int base = 10) -> long;
  };

  template <>
  struct string_conversions<unsigned long>
  {
    static auto read_string(char const* str, size_t len, int base = 10) -> unsigned long;
  };

  template <>
  struct string_conversions<long long>
  {
    static auto read_string(char const* str, size_t len, int base = 10) -> long long;
  };

  template <>
  struct string_conversions<unsigned long long>
  {
    static auto read_string(char const* str, size_t len, int base = 10) -> unsigned long long;
  };

  template <>
  struct string_conversions<float>
  {
    static auto read_string(char const* str, size_t len) -> float;
  };

  template <>
  struct string_conversions<double>
  {
    static auto read_string(char const* str, size_t len) -> double;
  };

  template <>
  struct string_conversions<long double>
  {
    static auto read_string(char const* str, size_t len) -> long double;
  };
}

namespace fmt
{
  /// Formatter for enum types which have specialized enum_traits<T>
  /** We inherit the string formatter to allow use of padding and other string specific parameters. */
  template <typename T, typename Char>
  struct formatter<T, Char, std::enable_if_t<bom::enum_traits<T>::specialized>> : formatter<char const*>
  {
    template <typename FormatContext>
    auto format(T val, FormatContext& ctx) -> decltype(ctx.out())
    {
      return formatter<char const*>::format(bom::enum_label(val), ctx);
    }
  };

  /// Formatter for std::exception that prints nested exceptions
  template <typename T, typename Char>
  struct formatter<T, Char, std::enable_if_t<std::is_base_of<std::exception, T>::value>>
  {
    template <typename ParseContext>
    constexpr auto parse(ParseContext& ctx)
    {
      return ctx.begin();
    }

    template <typename FormatContext>
    auto format(std::exception const& err, FormatContext& ctx) -> decltype(ctx.out())
    {
      fmt::format_to(ctx.out(), "{}", err.what());
      try
      {
        std::rethrow_if_nested(err);
      }
      catch (std::exception const& nested)
      {
        fmt::format_to(ctx.out(), "\n-> ");
        format(nested, ctx);
      }
      return ctx.out();
    }
  };

  template <typename Char>
  struct formatter<bom::indent, Char>
  {
    template <typename ParseContext>
    constexpr auto parse(ParseContext& ctx)
    {
      return ctx.begin();
    }

    template <typename FormatContext>
    auto format(bom::indent val, FormatContext& ctx) -> decltype(ctx.out())
    {
      for (auto i = 0; i < val.size(); ++i)
        fmt::format_to(ctx.out(), " ");
      return ctx.out();
    }
  };

  template <typename Char>
  struct formatter<bom::fmt_bytes, Char>
  {
    template <typename ParseContext>
    constexpr auto parse(ParseContext& ctx)
    {
      return ctx.begin();
    }

    template <typename FormatContext>
    auto format(bom::fmt_bytes val, FormatContext& ctx) -> decltype(ctx.out())
    {
      static constexpr char const* const suffixes[] = { "B", "KB", "MB", "GB", "TB", "PB", "EB", "ZB", "YB" };
      int i = 0;
      while (val.value >= 1024)
      {
        val.value /= 1024;
        ++i;
      }
      fmt::format_to(ctx.out(), "{}{}", val.value, suffixes[i]);
      return ctx.out();
    }
  };
}
