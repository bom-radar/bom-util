/*------------------------------------------------------------------------------
 * Bureau of Meteorology C++ Utility Library
 *
 * Copyright 2014 Mark Curtis
 * Copyright 2016 Commonwealth of Australia, Bureau of Meteorology
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *----------------------------------------------------------------------------*/
#pragma once

#include "../string_utils.h"

#include <istream>
#include <ostream>
#include <vector>
#include <utility>

/* TODO - use a shared_ptr to an unordered_set to store strings.  individual strings should be stored as raw
 *        pointers to the strings, NOT as iterators into the set.  when the set is forced to rehash all the
 *        iterators are invalidated, but raw pointers to the internal strings are not. */
namespace bom::io
{
  /// JSON file parser
  class json
  {
  public:
    /// JSON object types
    enum class type
    {
        number
      , string
      , boolean
      , array
      , object
      , null
    };
    using string_type = bom::string;
    using array_type = std::vector<json>;
    using object_type = std::vector<std::pair<string_type, json>>;

  public:
    /// Initialize a json object as a null
    json();
    /// Initialize a json object as a number
    explicit json(double val);
    /// Initialize a json object as a string
    explicit json(string_type val);
    /// Initialize a json object as a boolean
    explicit json(bool val);
    /// Initialize a json object as an array
    explicit json(array_type val);
    /// Initialize a json object as an object
    explicit json(object_type val);

    /// Parse a json object from a stream
    explicit json(std::istream& in);
    /// Parse a json object from a stream
    explicit json(std::istream&& in) : json{in} { }

    json(json const& rhs);
    json(json&& rhs) noexcept;
    auto operator=(json const& rhs) -> json&;
    auto operator=(json&& rhs) noexcept -> json&;
    ~json();

    auto write(std::ostream& out, int indent = 0) const -> void;
    auto write(std::ostream&& out) const -> void                  { write(out); }

    auto clear() -> void;

    auto value_type() const -> type                               { return type_; }
    auto number() const -> double;
    auto string() const -> string_type const&;
    auto boolean() const -> bool;
    auto array() const -> array_type const&;
    auto object() const -> object_type const&;

    operator int() const                                          { return number(); }
    operator size_t() const                                       { return number(); }
    operator double() const                                       { return number(); }
    operator string_type const&() const                           { return string(); }
    operator bool() const                                         { return boolean(); }
    operator array_type const&() const                            { return array(); }
    operator object_type const&() const                           { return object(); }

    auto size() const -> size_t;
    auto operator[](size_t i) const -> json const&;
    auto operator[](size_t i) -> json&;
    auto operator[](int i) const -> json const&                   { return operator[](size_t(i)); }
    auto operator[](int i) -> json &                              { return operator[](size_t(i)); }

    auto operator[](char const* key) const -> json const&;
    auto operator[](char const* key) -> json&;
    auto operator[](string_type const& key) const -> json const&  { return operator[](key.c_str()); }
    auto operator[](string_type const& key) -> json&              { return operator[](key.c_str()); }

    auto find(char const* key) const -> json const*;
    auto find(char const* key) -> json*;
    auto find(string_type const& key) const -> json const*        { return find(key.c_str()); }
    auto find(string_type const& key) -> json*                    { return find(key.c_str()); }

    auto operator=(int val) -> json&                              { return *this = json{double(val)}; }
    auto operator=(double val) -> json&                           { return *this = json{val}; }
    auto operator=(string_type val) -> json&                      { return *this = json{std::move(val)}; }
    auto operator=(bool val) -> json&                             { return *this = json{val}; }
    auto operator=(array_type val) -> json&                       { return *this = json{std::move(val)}; }
    auto operator=(object_type val) -> json&                      { return *this = json{std::move(val)}; }
    auto operator=(std::nullptr_t val) -> json&                   { return *this = json{}; }

  private:
    type type_;
    union
    {
      double      value_number_;
      string_type value_string_;
      bool        value_boolean_;
      array_type  value_array_;
      object_type value_object_;
    };
  };

  inline auto operator>>(std::istream& lhs, json& rhs) -> std::istream&
  {
    rhs = json(lhs);
    return lhs;
  }
  inline auto operator<<(std::ostream& lhs, json const& rhs) -> std::ostream&
  {
    rhs.write(lhs);
    return lhs;
  }
}
