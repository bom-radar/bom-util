/*------------------------------------------------------------------------------
 * Bureau of Meteorology C++ Utility Library
 *
 * Copyright 2014 Mark Curtis
 * Copyright 2016 Commonwealth of Australia, Bureau of Meteorology
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *----------------------------------------------------------------------------*/
#pragma once

#include "string_utils.h"
#include <array>
#include <ostream>
#include <utility>

namespace bom::trace
{
  /// Trace severity levels
  enum class level
  {
      none
    , status
    , error
    , warning
    , log
    , debug
  };

  /// Type used to store a set of trace level prefix strings
  using level_marker_set = std::array<char const*, 6>;

  /// Get the current minimum trace level
  auto min_level() -> level;

  /// Set the minimum trace level and return the previous level
  auto set_min_level(level min) -> level;

  /// Get whether a timestamp will be prefixed to each trace line
  auto prefix_timestamp() -> bool;

  /// Set whether a timestamp will be prefixed to each trace line
  auto set_prefix_timestamp(bool timestamp) -> void;

  /// Get whether tread id will be prefixed to each trace line
  auto identify_threads() -> bool;

  /// Set whether to prefix each trace line with a thread identifier
  auto set_identify_threads(bool identify) -> void;

  /// Get the currentl installed trace level identifier prefixes
  auto level_markers() -> level_marker_set const *;

  /// Set the trace level identifier prefixes
  auto set_level_markers(level_marker_set const* markers) -> void;

  /// Get the string used to terminate each trace message
  auto message_terminator() -> char const*;

  /// Set the string used to terminate each trace mssage
  auto set_message_terminator(char const* terminator) -> void;

  /// Get the current trace output target
  auto target() -> std::ostream&;

  /// Set the trace output target
  auto set_target(std::ostream& target) -> void;

  /// Determine whether trace at the designated level would be output
  /** This is useful for avoiding expensive operations which are only needed for the purpose of producing
   *  trace output (which might be discarded anyway). */
  auto level_active(level l) -> bool;

  /// Write trace at the specified level with precompiled format arguments
  auto vtrace(level lvl, char const* format_str, fmt::format_args const& args) -> void;

  /// Write trace at the specified level
  template <typename... Args>
  inline auto trace(level lvl, char const* format_str, Args&&... args) -> void
  {
    if (level_active(lvl))
      vtrace(lvl, format_str, fmt::make_format_args(args...));
  }

  /// Write trace at the error level
  template <typename... Args>
  inline auto error(char const* format_str, Args&&... args) -> void
  {
    if (level_active(level::error))
      vtrace(level::error, format_str, fmt::make_format_args(args...));
  }

  /// Write trace at the status level
  template <typename... Args>
  inline auto status(char const* format_str, Args&&... args) -> void
  {
    if (level_active(level::status))
      vtrace(level::status, format_str, fmt::make_format_args(args...));
  }

  /// Write trace at the warning level
  template <typename... Args>
  inline auto warning(char const* format_str, Args&&... args) -> void
  {
    if (level_active(level::warning))
      vtrace(level::warning, format_str, fmt::make_format_args(args...));
  }

  /// Write trace at the log level
  template <typename... Args>
  inline auto log(char const* format_str, Args&&... args) -> void
  {
    if (level_active(level::log))
      vtrace(level::log, format_str, fmt::make_format_args(args...));
  }

  /// Write trace at the debug level
  template <typename... Args>
  inline auto debug(char const* format_str, Args&&... args) -> void
  {
#ifndef NDEBUG
    if (level_active(level::debug))
      vtrace(level::debug, format_str, fmt::make_format_args(args...));
#endif
  }

  /// Insert a prefix before the contents of each trace line printed during the life of this object
  /** The supplied prefix string is pushed onto a stack of prefixes upon construction and popped from the
   *  stack upon destruction.  The stack of prefixes is thread local so using these objects in multi-threaded
   *  code is safe and will behave naturally.
   *
   *  When multiple prefixes are available in the stack they will be output separated by a period character.
   *
   *  This class retains a pointer to the string provided and it is the responsibility of the user to
   *  ensure that this pointer remains valid during the lifetime of the object.  Destroying the prefix string
   *  while the scope_prefix is alive results in undefined behaviour.  This behaviour is analogous to that of
   *  std::lock_guard.
   *
   *  It is also the responsibility of the user to ensure that objects are destroyed by the same thread that
   *  created them.  This is easily ensured by only creating these objects directly on the stack.
   */
  class scope_prefix
  {
  public:
    scope_prefix(char const* prefix);

    scope_prefix(scope_prefix const&) = delete;
    scope_prefix(scope_prefix&&) = delete;
    auto operator=(scope_prefix const&) -> scope_prefix& = delete;
    auto operator=(scope_prefix&&) -> scope_prefix& = delete;

    ~scope_prefix();

  public:
    static auto format(string& out) -> void;

  private:
    auto format_impl(string& out) const -> void;

  private:
    char const*   prefix_;
    scope_prefix* parent_;
  };
}

namespace bom
{
  BOM_DECLARE_ENUM_TRAITS(trace::level, none, error, status, warning, log, debug);
}
