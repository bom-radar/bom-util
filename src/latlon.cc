/*------------------------------------------------------------------------------
 * Bureau of Meteorology C++ Utility Library
 *
 * Copyright 2014 Mark Curtis
 * Copyright 2016 Commonwealth of Australia, Bureau of Meteorology
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *----------------------------------------------------------------------------*/
#include "config.h"
#include "latlon.h"
#include "unit_test.h"

using namespace bom;

auto bom::operator<<(std::ostream& lhs, const latlon& rhs) -> std::ostream&
{
  return lhs << rhs.lat << ' ' << rhs.lon;
}

auto bom::operator<<(std::ostream& lhs, const latlonalt& rhs) -> std::ostream&
{
  return lhs << static_cast<latlon const&>(rhs) << ' ' << rhs.alt;
}

auto string_conversions<latlon>::read_string(char const* str, size_t len) -> latlon
try
{
  auto end = str;
  while (end < str + len && !std::isspace(*end))
    ++end;

  auto next = end;
  while (next < str + len && std::isspace(*next))
    ++next;

  return {
      string_conversions<angle>::read_string(str, end - str)
    , string_conversions<angle>::read_string(next, len - (next - str))
    };
}
catch (...)
{
  std::throw_with_nested(string_conversion_error{"latlon", string(str, len)});
}

auto string_conversions<latlonalt>::read_string(char const* str, size_t len) -> latlonalt
try
{
  auto end = str;
  while (end < str + len && !std::isspace(*end))
    ++end;

  auto next = end;
  while (next < str + len && std::isspace(*next))
    ++next;

  auto end2 = next;
  while (end2 < str + len && !std::isspace(*end2))
    ++end2;

  auto next2 = end2;
  while (next2 < str + len && std::isspace(*next2))
    ++next2;

  return {
      string_conversions<angle>::read_string(str, end - str)
    , string_conversions<angle>::read_string(next, end2 - next)
    , string_conversions<double>::read_string(next2, len - (next2 - str))
    };
}
catch (...)
{
  std::throw_with_nested(string_conversion_error{"latlonalt", string(str, len)});
}

// LCOV_EXCL_START
#include <sstream>
#include <iomanip>

TEST_CASE("latlon")
{
  // operator<<
  std::ostringstream oss;
  oss << std::setprecision(3) << std::fixed << latlon{-37.852_deg, 144.752_deg};
  CHECK(oss.str() == "-37.852 144.752");

  // read string
  auto ll = from_string<latlon>("-37.852 144.752");
  CHECK(ll.lat.degrees() == approx(-37.852));
  CHECK(ll.lon.degrees() == approx(144.752));
  CHECK_THROWS_AS(from_string<latlon>("invalid"), string_conversion_error const&);

  // write string
  CHECK(fmt::format("{:0.3f}", ll) == "-37.852 144.752");
  CHECK(fmt::format("{:dms}", ll) == "37\u00b051'07\"S 144\u00b045'07\"E");
}

TEST_CASE("latlonalt")
{
  // operator<<
  std::ostringstream oss;
  oss << std::setprecision(3) << std::fixed << latlonalt{-37.852_deg, 144.752_deg, 14};
  CHECK(oss.str() == "-37.852 144.752 14.000");

  // read string
  auto ll = from_string<latlonalt>("-37.852 144.752 14.000");
  CHECK(ll.lat.degrees() == approx(-37.852));
  CHECK(ll.lon.degrees() == approx(144.752));
  CHECK(ll.alt == approx(14.0));
  CHECK_THROWS_AS(from_string<latlonalt>("invalid"), string_conversion_error const&);

  // write string
  CHECK(fmt::format("{:0.3f}", ll) == "-37.852 144.752 14.000");
  CHECK(fmt::format("{:dms}", ll) == "37\u00b051'07\"S 144\u00b045'07\"E 14m");
}
// LCOV_EXCL_STOP
