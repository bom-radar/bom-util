This directory contains an embedded copy of the `{fmt}` library.  Please see the following locations:

https://github.com/fmtlib/fmt/
https://fmt.dev/

The code was extracted from `{fmt}` version `10.0.0`.

Ideally we would just use the library as an external dependency.  This is made difficult by the fact
that the version of `{fmt}` available on RHEL 7 and 8 (via EPEL) is 6.2.1.  This version is too old
for our purposes and is missing a critical feature, which is the ability to store named arguments in
a `dynamic_format_arg_store`.  I was unable to find a way to emulate this functionality using the
older 6.2.1 codebase.

Since bom-util is very widely used the burden of forcing all our applications to checkout and build
their own version of `{fmt}` prior to compiling `bom-util` is too great.  For this reason the code
has been checked in directly to our codebase and built as an internal part of the `bom-util` library.
This is an approach suggested by the `{fmt}` documentation itself.

There are two downsides to this approach:

- Minor: We now have extra third party code checked into our repo slowly going stale.
- Major: If someone tries to use `bom-util` in a project and also `fmt` directly (or via another
  dependency), then there is a high chance of very painful link errors.

Currently anything using `bom-util` tends to only depend on our other radar ecosystem applications
and a few standard C dependencies, so the major issue above is not likely to impact us.  If it does
then we will be left with two options:

1. If el7 and el8 are no longer required, simply drop support for them and remove the checked in
   version of `{fmt}` from bom-util.  This is preferable if possible.
2. At CMake time, first run a `find_package(fmt)`, and if found (with version >= 9.1.0) then
   conditionally exclude our checked in version and prefer the system headers and shared library.
   This will require adding some #defines to our config.h so that we `#include <fmt/format.h>`
   instead of `#include "fmt/format.h"`.  And since these `#include`s must occur in our headers
   we will also need to install our `config.h` file under a new name (e.g. `bom-util.config.h`)
   which we've managed to avoid doing so far.
