/*------------------------------------------------------------------------------
 * Bureau of Meteorology C++ Utility Library
 *
 * Copyright 2014 Mark Curtis
 * Copyright 2016 Commonwealth of Australia, Bureau of Meteorology
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *----------------------------------------------------------------------------*/
#include "config.h"
#include "math_utils.h"
#include "unit_test.h"

using namespace bom;

auto bom::gcd(int a, int b) -> int
{
  int t;
  while (b != 0)
  {
    t = a;
    a = b;
    b = t % b;
  }
  return a;
}

// LCOV_EXCL_START
#include "raii.h"
TEST_CASE("math_utils")
{
  SUBCASE("basic")
  {
    CHECK(round_to_precision<std::ratio<1>>(1.23) == approx(1.0));
    CHECK(round_to_precision<std::deci>(1.23) == approx(1.2));
    CHECK(round_to_precision<std::deci>(-1.23) == approx(-1.2));
    CHECK(round_to_precision<std::centi>(1.443) == approx(1.44));
    CHECK(round_to_precision<std::centi>(1.399) == approx(1.40));
    CHECK(round_to_precision<std::ratio<1>>(1.799) == approx(2.0));
    CHECK(round_to_precision<std::deca>(144.799) == approx(140.0));

    CHECK(gcd(0, 0) == 0);
    CHECK(gcd(600, 900) == 300);
  }
}
// LCOV_EXCL_STOP
