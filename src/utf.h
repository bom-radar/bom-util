/*------------------------------------------------------------------------------
 * Bureau of Meteorology C++ Utility Library
 *
 * Copyright 2014 Mark Curtis
 * Copyright 2016 Commonwealth of Australia, Bureau of Meteorology
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *----------------------------------------------------------------------------*/
#pragma once

#include <cstdlib>

namespace bom
{
  /// Determine the number of characters in a UTF8 string
  auto utf8_size(char const* utf8) -> size_t;

  /// Convert the next character in a UTF8 string into UTF32
  /** The passed pointer will be updated to point to the start of the next UTF8 character. */
  auto utf8_next_character(char const*& utf8) -> char32_t;
}
