/*------------------------------------------------------------------------------
 * Bureau of Meteorology C++ Utility Library
 *
 * Copyright 2014 Mark Curtis
 * Copyright 2016 Commonwealth of Australia, Bureau of Meteorology
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *----------------------------------------------------------------------------*/
#include "config.h"
#include "colour.h"
#include "math_utils.h"
#include "unit_test.h"
#include <cmath>
#include <cstdio>

using namespace bom;

static constexpr inline auto to_fp(uint8_t u) -> float
{
  return (1.0f / 255.0f) * u;
}

static constexpr inline auto from_fp(float f) -> uint8_t
{
  return f <= 0.0f ? 0 : f >= 1.0f ? 255 : std::floor(f * 256);
}

auto colour::as_uint8s(uint8_t rgba[4]) const -> void
{
  rgba[0] = r;
  rgba[1] = g;
  rgba[2] = b;
  rgba[3] = a;
}

auto colour::as_hsv(angle& hue, float& saturation, float& value, float& alpha) const -> void
{
  auto fr = to_fp(r), fg = to_fp(g), fb = to_fp(b), fa = to_fp(a);

  alpha = fa;
  value = std::max(fr, std::max(fg, fb));
  auto delta = value - std::min(fr, std::min(fg, fb));

  // if r == g == b then it's a greyscale colour, hue is undefined and saturation is 0
  if (delta == 0.0f)
  {
    hue = nan<angle>();
    saturation = 0.0f;
    return;
  }

  saturation = delta / value;

  float h;
  if (fr >= value)               // > is bogus, just keeps compiler happy
    h = (fg - fb) / delta;        // between yellow & magenta
  else if (fg >= value)
    h = 2.0 + (fb - fr) / delta;  // between cyan & yellow
  else
    h = 4.0 + (fr - fg) / delta;  // between magenta & cyan

  h *= 60.0;
  if (h < 0.0)
    h += 360.0;

  hue = h * 1_deg;
}

auto colour::hsv(angle hue, float saturation, float value, float alpha) -> colour
{
  // if hue is undefined it's a greyscale colour
  if (is_nan(hue))
  {
    auto v = from_fp(value);
    return {v, v, v, from_fp(alpha)};
  }

  float hp = hue.degrees();
  while (hp < 0.0f)
    hp += 360.0f;
  while (hp > 360.0f)
    hp -= 360.0f;
  hp /= 60.0f;

  float hpmod2 = hp - (float)((int)(hp/2))*2;

  float c = value * saturation;
  float x = c * (1 - std::fabs(hpmod2 - 1));
  float m = value - c;

  float r1, g1, b1;

  switch ((int) hp)
  {
  case 0:  r1 = c; g1 = x; b1 = 0; break;
  case 1:  r1 = x; g1 = c; b1 = 0; break;
  case 2:  r1 = 0; g1 = c; b1 = x; break;
  case 3:  r1 = 0; g1 = x; b1 = c; break;
  case 4:  r1 = x; g1 = 0; b1 = c; break;
  default: r1 = c; g1 = 0; b1 = x; break;
  }

  return {from_fp(r1 + m), from_fp(g1 + m), from_fp(b1 + m), from_fp(alpha)};
}

auto colour::lerp_rgb(colour const& lhs, colour const& rhs, float fraction) -> colour
{
  return
  {
      lerp(lhs.r, rhs.r, fraction)
    , lerp(lhs.g, rhs.g, fraction)
    , lerp(lhs.b, rhs.b, fraction)
    , lerp(lhs.a, rhs.a, fraction)
  };
}

auto colour::lerp_hsv(colour const& lhs, colour const& rhs, float fraction) -> colour
{
  angle lh, rh;
  float ls, rs, lv, rv, la, ra;

  // convert both colours into HSV space
  lhs.as_hsv(lh, ls, lv, la);
  rhs.as_hsv(rh, rs, rv, ra);

  // cope with undefined hue on either side (ie: greyscale colours) by using the hue from the other side
  // for the interpolation.  if both are nan, then don't worry since we are only interpolating between greys
  if (is_nan(lh))
    lh = rh;
  if (is_nan(rh))
    rh = lh;

  // interpolate H, S and V separately and convert back to RGB
  return hsv(lerp(lh, rh, fraction), lerp(ls, rs, fraction), lerp(lv, rv, fraction), lerp(la, ra, fraction));
}

auto string_conversions<colour>::read_string(char const* str, size_t len) -> colour
{
  bool okay;
  uint8_t r, g, b, a;
  switch (len)
  {
  case 3:
    okay = sscanf(str, "%1hhx%1hhx%1hhx", &r, &g, &b) == 3;
    r += r * 16;
    g += g * 16;
    b += b * 16;
    a = 0xff;
    break;
  case 4:
    okay = sscanf(str, "%1hhx%1hhx%1hhx%1hhx", &r, &g, &b, &a) == 4;
    r += r * 16;
    g += g * 16;
    b += b * 16;
    a += a * 16;
    break;
  case 6:
    okay = sscanf(str, "%2hhx%2hhx%2hhx", &r, &g, &b) == 3;
    a = 0xff;
    break;
  case 8:
    okay = sscanf(str, "%2hhx%2hhx%2hhx%2hhx", &r, &g, &b, &a) == 4;
    break;
  default:
    okay = false;
    break;
  }

  if (!okay)
    throw string_conversion_error{"colour", string(str, len)};

  return {r, g, b, a};
}

auto string_conversions<colour>::write_string(colour const& val, string& str) -> void
{
  char buf[9];
  snprintf(buf, 9, "%02x%02x%02x%02x", val.r, val.g, val.b, val.a);
  str.append(buf, 8);
}

// LCOV_EXCL_START
TEST_CASE("colour")
{
  // constructor
  CHECK(colour{0x01, 0x02, 0x03, 0x04}.r == 0x01);
  CHECK(colour{0x01, 0x02, 0x03, 0x04}.g == 0x02);
  CHECK(colour{0x01, 0x02, 0x03, 0x04}.b == 0x03);
  CHECK(colour{0x01, 0x02, 0x03, 0x04}.a == 0x04);

  // cast to uint8_t*
  CHECK(static_cast<uint8_t const*>(standard_colours::clear) + 0 == &standard_colours::clear.r);
  CHECK(static_cast<uint8_t const*>(standard_colours::clear) + 1 == &standard_colours::clear.g);
  CHECK(static_cast<uint8_t const*>(standard_colours::clear) + 2 == &standard_colours::clear.b);
  CHECK(static_cast<uint8_t const*>(standard_colours::clear) + 3 == &standard_colours::clear.a);

  // as_uints8()
  uint8_t rgba[4];
  colour{0x01, 0x02, 0x03, 0x04}.as_uint8s(rgba);
  CHECK(rgba[0] == 0x01);
  CHECK(rgba[1] == 0x02);
  CHECK(rgba[2] == 0x03);
  CHECK(rgba[3] == 0x04);

  // as_hsv()
  angle h; float s, v, a;
  CHECK_NOTHROW(standard_colours::red.as_hsv(h, s, v, a));
  CHECK(h.degrees() == approx(0.0));
  CHECK(s == approx(1.0));
  CHECK(v == approx(1.0));
  CHECK(a == approx(1.0));

  CHECK_NOTHROW(colour(207, 157, 71, 0).as_hsv(h, s, v, a));    // between yellow and magenta
  CHECK(h.degrees() == approx(37.941170));
  CHECK(s == approx(0.657));
  CHECK(v == approx(0.81176));
  CHECK(a == approx(0.0));

  CHECK_NOTHROW(colour(121, 171, 72, 128).as_hsv(h, s, v, a));  // between cyan and yellow
  CHECK(h.degrees() == approx(90.303));
  CHECK(s == approx(0.57895));
  CHECK(v == approx(0.6706));
  CHECK(a == approx(128.0 / 255.0));

  CHECK_NOTHROW(colour(67, 131, 231, 255).as_hsv(h, s, v, a));  // between magenta and cyan
  CHECK(h.degrees() == approx(216.585358));
  CHECK(s == approx(0.70996));
  CHECK(v == approx(0.90588));
  CHECK(a == approx(1.0));

  CHECK_NOTHROW(colour(100, 100, 100, 255).as_hsv(h, s, v, a));
  CHECK(is_nan(h));
  CHECK(s == approx(0.0));
  CHECK(v == approx(100.0 / 255.0));
  CHECK(a == approx(1.0));

  // hsv()
  CHECK(colour::hsv(0.0_deg, 1.0, 1.0, 1.0) == standard_colours::red);
  CHECK(colour::hsv(90.303_deg, 0.57895, 0.6706, 0.50196) == colour{121, 171, 72, 128});
  CHECK(colour::hsv(10.0_deg, 0.5, 0.5) == colour{128, 74, 64, 255});
  CHECK(colour::hsv(70.0_deg, 0.5, 0.5) == colour{117, 128, 64, 255});
  CHECK(colour::hsv(130.0_deg, 0.5, 0.5) == colour{64, 128, 74, 255});
  CHECK(colour::hsv(190.0_deg, 0.5, 0.5) == colour{64, 117, 128, 255});
  CHECK(colour::hsv(250.0_deg, 0.5, 0.5) == colour{74, 64, 128, 255});
  CHECK(colour::hsv(310.0_deg, 0.5, 0.5) == colour{128, 64, 117, 255});
  CHECK(colour::hsv(-50.0_deg, 0.5, 0.5) == colour{128, 64, 117, 255});
  CHECK(colour::hsv(670.0_deg, 0.5, 0.5) == colour{128, 64, 117, 255});
  CHECK(colour::hsv(nan<angle>(), 1.0, 1.0, 1.0) == standard_colours::white);
  CHECK(colour::hsv(nan<angle>(), 0.7, 0.0, 1.0) == standard_colours::black);

  // lerp_rgb()
  CHECK(colour::lerp_rgb(standard_colours::yellow, standard_colours::cyan, 0.5) == colour{127, 255, 127, 255});
  CHECK(colour::lerp_rgb(standard_colours::clear, standard_colours::white, 0.25) == colour{63, 63, 63, 63});

  // lerp_hsv()
  CHECK(colour::lerp_hsv(standard_colours::yellow, standard_colours::cyan, 0.5) == standard_colours::green);
  CHECK(colour::lerp_hsv(standard_colours::white, standard_colours::cyan, 0.5) == colour{128, 255, 255, 255});
  CHECK(colour::lerp_hsv(standard_colours::cyan, standard_colours::white, 0.5) == colour{128, 255, 255, 255});
  CHECK(colour::lerp_hsv(standard_colours::black, standard_colours::white, 0.5) == colour{128, 128, 128, 255});

  // read_string()
  CHECK(from_string<colour>("a72") == colour{0xaa, 0x77, 0x22, 0xff});
  CHECK(from_string<colour>("f03e") == colour{0xff, 0x00, 0x33, 0xee});
  CHECK(from_string<colour>("2f03ec") == colour{0x2f, 0x03, 0xec, 0xff});
  CHECK(from_string<colour>("25031983") == colour{0x25, 0x03, 0x19, 0x83});
  CHECK_THROWS_AS(from_string<colour>("g72"), string_conversion_error const&);
  CHECK_THROWS_AS(from_string<colour>("fg3e"), string_conversion_error const&);
  CHECK_THROWS_AS(from_string<colour>("2f0gec"), string_conversion_error const&);
  CHECK_THROWS_AS(from_string<colour>("25031g83"), string_conversion_error const&);
  CHECK_THROWS_AS(from_string<colour>("2503198383"), string_conversion_error const&);

  // format()
  CHECK(fmt::format("{}", standard_colours::cyan) == "00ffffff");
}
// LCOV_EXCL_STOP
