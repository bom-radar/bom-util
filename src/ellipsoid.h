/*------------------------------------------------------------------------------
 * Bureau of Meteorology C++ Utility Library
 *
 * Copyright 2014 Mark Curtis
 * Copyright 2016 Commonwealth of Australia, Bureau of Meteorology
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *----------------------------------------------------------------------------*/
#pragma once

#include "latlon.h"
#include "vec3.h"

namespace bom
{
  /// Reference ellipsoid for use with latitude and longitude coordinate systems
  class ellipsoid
  {
  public:
    /// Construct an ellipsoid based on the semi-major axis and inverse flattening parameters
    constexpr ellipsoid(double semi_major_axis, double invserse_flattening);

    ellipsoid(ellipsoid const& rhs) noexcept = default;
    ellipsoid(ellipsoid&& rhs) noexcept = default;

    auto operator=(ellipsoid&& rhs) noexcept -> ellipsoid& = default;
    auto operator=(ellipsoid const& rhs) noexcept -> ellipsoid& = default;

    ~ellipsoid() noexcept = default;

    auto semi_major_axis() const -> double      { return a_; }
    auto semi_minor_axis() const -> double      { return b_; }
    auto flattening() const -> double           { return f_; }
    auto inverse_flattening() const -> double   { return inv_f_; }
    auto eccentricity() const -> double         { return std::sqrt(e_sqr_); }
    auto eccentricity_squared() const -> double { return e_sqr_; }

    /// Determine the mean radius of the ellipsoid (IUGG R1)
    auto radius_mean() const -> double;

    /// Determine the authalic radius of the ellipsoid (IUGG R2)
    /** This is the radius of a sphere of equal area. */
    auto radius_authalic() const -> double;

    /// Determine the volumetric radius of the ellipsoid (IUGG R3)
    /** This is the radius of a sphere of equal volume. */
    auto radius_volumetric() const -> double;

    /// Distance from center of ellipsoid to point at given latitude
    auto radius_physical(angle lat) const -> double;

    /// Radius of curvature in north/south direction at given latitude
    auto radius_of_curvature_meridional(angle lat) const -> double;

    /// Radius of curvature in direction normal to meridian at given latitude (east/west @ equator)
    auto radius_of_curvature_normal(angle lat) const -> double;

    /// Mean radius of curvature at given latitude (ie: rad of curve averaged in all directions)
    auto radius_of_curvature(angle lat) const -> double;

    /// Convert latitude and longitude into earth-centered, earth-fixed cartesian coordinates
    auto latlon_to_ecefxyz(latlonalt pos) const -> vec3d;
    auto latlon_to_ecefxyz(latlon pos) const -> vec3d { return latlon_to_ecefxyz(latlonalt{pos, 0.0}); }

    /// Convert earth-centered, earth-fixed cartesian coordinates into latitude & longitude
    auto ecefxyz_to_latlon(vec3d pos) const -> latlonalt;

    /// Calculate position based on bearing and range from a reference point
    /** This calculation uses Vincenty's formula which is extremely accurate but also relatively expensive. */
    auto bearing_range_to_latlon(latlon pos, angle bearing, double range) const -> latlon;

    /// Calculate bearing and initial range based on two reference points
    /** The bearing returned is the bearing seen at pos1 looking toward pos2.  This calculation uses Vincenty's
     *  formula which is extremely accurate but also relatively expensive.  */
    auto latlon_to_bearing_range(latlon pos1, latlon pos2) const -> std::pair<angle, double>;

    /// Calculate position based on bearing and range from a reference point
    /** This version of the function uses the haversine formula, which relies on a spherical earth approximation.
     *  It is less accurate, but much faster than the Vincenty's method.  */
    auto bearing_range_to_latlon_haversine(latlon pos, angle bearing, double range) const -> latlon;

    /// Calculate initial bearing and range based on two reference points
    /** The bearing returned is the bearing seen at pos1 looking toward pos2.  This version of the function uses
     *  the haversine formula, which relies on a spherical earth approximation.  It is less accurate, but much
     *  faster than the Vincenty's method.  */
    auto latlon_to_bearing_range_haversine(latlon pos1, latlon pos2) const -> std::pair<angle, double>;

    /// Determine the first (if any) point of intersection between a ray and the ellipsoid surface
    /** Note: the direction vector must be normalized before calling this function */
    auto intersect_ray(vec3d start, vec3d direction, latlon& pos) const -> bool;
    auto intersect_ray(std::pair<vec3d, vec3d> sd, latlon& pos) const -> bool;

  private:
    // model parameters
    double a_;
    double inv_f_;

    // derived parameters
    double f_;
    double b_;
    double e_sqr_;
    double ep2_;
  };

  inline auto ellipsoid::intersect_ray(std::pair<vec3d, vec3d> sd, latlon& pos) const -> bool
  {
    return intersect_ray(sd.first, sd.second, pos);
  }

  constexpr ellipsoid::ellipsoid(double semi_major_axis, double inverse_flattening)
    : a_{semi_major_axis}
    , inv_f_{inverse_flattening}
    , f_{1.0 / inv_f_}
    , b_{a_ * (1.0 - f_)}
    , e_sqr_{f_ * (2.0 - f_)}
    , ep2_{(a_ * a_ - b_ * b_) / (b_ * b_)}
  { }

  /// The standard WGS84 ellipsoid as used in the GPS
  /** This ellipsoid should be used as the default for all geodetic coordinates unless explicitly stated. */
  constexpr ellipsoid wgs84{6378137.000, 298.257223563};
}
