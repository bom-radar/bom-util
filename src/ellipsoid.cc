/*------------------------------------------------------------------------------
 * Bureau of Meteorology C++ Utility Library
 *
 * Copyright 2014 Mark Curtis
 * Copyright 2016 Commonwealth of Australia, Bureau of Meteorology
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *----------------------------------------------------------------------------*/
#include "config.h"
#include "ellipsoid.h"
#include "trace.h"
#include "unit_test.h"

using namespace bom;

auto ellipsoid::radius_mean() const -> double
{
  return (2.0 * a_ + b_) / 3.0;
}

auto ellipsoid::radius_authalic() const -> double
{
  auto e = eccentricity();
  return std::sqrt((a_ * a_ * 0.5) + (b_ * b_ * 0.5) * (atanh(e) / e));
}

auto ellipsoid::radius_volumetric() const -> double
{
  return std::cbrt(a_ * a_ * b_);
}

auto ellipsoid::radius_physical(angle lat) const -> double
{
  auto a_cos = a_ * cos(lat);
  auto a2_cos = a_ * a_cos;
  auto b_sin = b_ * sin(lat);
  auto b2_sin = b_ * b_sin;
  return std::sqrt((a2_cos * a2_cos + b2_sin * b2_sin) / (a_cos * a_cos + b_sin * b_sin));
}

auto ellipsoid::radius_of_curvature_meridional(angle lat) const -> double
{
  auto ab = a_ * b_;
  auto a_cos = a_ * cos(lat);
  auto b_sin = b_ * sin(lat);
  return (ab * ab) / std::pow(a_cos * a_cos + b_sin * b_sin, 1.5);
}

auto ellipsoid::radius_of_curvature_normal(angle lat) const -> double
{
  auto a_cos = a_ * cos(lat);
  auto b_sin = b_ * sin(lat);
  return (a_ * a_) / std::sqrt(a_cos * a_cos + b_sin * b_sin);
}

auto ellipsoid::radius_of_curvature(angle lat) const -> double
{
  auto a_cos = a_ * cos(lat);
  auto b_sin = b_ * sin(lat);
  return (a_ * a_ * b_) / (a_cos * a_cos + b_sin * b_sin);
}

auto ellipsoid::latlon_to_ecefxyz(latlonalt pos) const -> vec3d
{
  auto sin_lat = sin(pos.lat);
  auto cos_lat = cos(pos.lat);
  auto nu = a_ / std::sqrt(1.0 - e_sqr_ * sin_lat * sin_lat);
  auto nu_plus_h = nu + pos.alt;

  return
  {
      nu_plus_h * cos_lat * cos(pos.lon)
    , nu_plus_h * cos_lat * sin(pos.lon)
    , ((1.0 - e_sqr_) * nu + pos.alt) * sin_lat
  };
}

auto ellipsoid::ecefxyz_to_latlon(vec3d pos) const -> latlonalt
{
  auto p = std::sqrt(pos.x * pos.x + pos.y * pos.y);
  angle theta = atan((pos.z * a_) / (p * b_));
  auto sin3 = sin(theta); sin3 = sin3 * sin3 * sin3;
  auto cos3 = cos(theta); cos3 = cos3 * cos3 * cos3;
  angle lat = atan((pos.z + ep2_ * b_ * sin3) / (p - e_sqr_ * a_ * cos3));
  auto sinlat = sin(lat);

  return latlonalt(
        lat
      , atan2(pos.y, pos.x)
      , (p / cos(lat)) - (a_ / std::sqrt(1.0 - e_sqr_ * sinlat * sinlat)));
}

auto ellipsoid::bearing_range_to_latlon(latlon pos, angle bearing, double range) const -> latlon
{
  // based on original open source script found at:
  // http://www.movable-type.co.uk/scripts/latlong-vincenty-direct.html
  // modified for use in ancilla

  /* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -  */
  /* Vincenty Direct Solution of Geodesics on the Ellipsoid (c) Chris Veness 2005-2012              */
  /*                                                                                                */
  /* from: Vincenty direct formula - T Vincenty, "Direct and Inverse Solutions of Geodesics on the  */
  /*       Ellipsoid with application of nested equations", Survey Review, vol XXII no 176, 1975    */
  /*       http://www.ngs.noaa.gov/PUBS_LIB/inverse.pdf                                             */
  /* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -  */

  auto sin_alpha_1 = sin(bearing);
  auto cos_alpha_1 = cos(bearing);

  auto tanU1 = (1.0 - f_) * tan(pos.lat);
  auto cosU1 = 1.0 / std::sqrt((1.0 + tanU1 * tanU1));
  auto sinU1 = tanU1 * cosU1;

  auto sigma1 = atan2(tanU1, cos_alpha_1);
  auto sinAlpha = cosU1 * sin_alpha_1;
  auto cosSqAlpha = 1.0 - sinAlpha * sinAlpha;
  auto uSq = cosSqAlpha * ep2_;
  auto A = 1.0 + uSq / 16384.0 * (4096.0 + uSq * (-768.0 + uSq * (320.0 - 175.0 * uSq)));
  auto B = uSq / 1024.0 * (256.0 + uSq * (-128.0 + uSq * (74.0 - 47.0 * uSq)));

  auto sigma = (range / (b_ * A)) * 1_rad;
  auto sigmaP = angle::two_pi * 1_rad;
  double cos2SigmaM, sinSigma, cosSigma;
  while (true)
  {
    cos2SigmaM = cos(2.0 * sigma1 + sigma);
    sinSigma = sin(sigma);
    cosSigma = cos(sigma);

    if ((sigma - sigmaP).abs() > 1e-12_rad)
      break;

    auto deltaSigma =
      B * sinSigma * (cos2SigmaM + B / 4 * (cosSigma * (-1.0 + 2.0 * cos2SigmaM * cos2SigmaM)
      - B / 6.0 * cos2SigmaM * (-3.0 + 4.0 * sinSigma * sinSigma)
      * (-3.0 + 4.0 * cos2SigmaM * cos2SigmaM)));
    sigmaP = sigma;
    sigma.set_radians(range / (b_ * A) + deltaSigma);
  }

  auto tmp = sinU1 * sinSigma - cosU1 * cosSigma * cos_alpha_1;
  auto lat2 = atan2(sinU1 * cosSigma + cosU1 * sinSigma * cos_alpha_1, (1-f_) * std::hypot(sinAlpha, tmp));
  auto lambda = atan2(sinSigma*sin_alpha_1, cosU1*cosSigma - sinU1*sinSigma*cos_alpha_1);
  auto C = f_/16*cosSqAlpha*(4+f_*(4-3*cosSqAlpha));
  auto L = lambda - (1-C) * f_ * sinAlpha *
      (sigma + 1_rad * C * sinSigma * (cos2SigmaM + C * cosSigma * (-1.0 + 2.0 * cos2SigmaM * cos2SigmaM)));
  auto lon2 = 1_rad * (std::fmod(pos.lon.radians() + L.radians() + (3 * angle::pi), angle::two_pi) - angle::pi);  // normalise to -180...+180

  return { lat2, lon2 };
}

/* Vincenty inverse formula - T Vincenty, "Direct and Inverse Solutions of Geodesics on the
 * Ellipsoid with application of nested equations", Survey Review, vol XXII no 176, 1975
 * http://www.ngs.noaa.gov/PUBS_LIB/inverse.pdf
 *
 * Implementation based on formulas found from the "Vincenty's Formulae" Wikipedia article.
 */
auto ellipsoid::latlon_to_bearing_range(latlon pos1, latlon pos2) const -> std::pair<angle, double>
{
  // initialization
  const double u1 = std::atan((1.0 - f_) * std::tan((double) pos1.lat.radians()));
  const double u2 = std::atan((1.0 - f_) * std::tan((double) pos2.lat.radians()));
  const double l = pos2.lon.radians() - pos1.lon.radians();

  // dependent initialization
  const double cos_u1 = std::cos(u1);
  const double cos_u2 = std::cos(u2);
  const double sin_u1 = std::sin(u1);
  const double sin_u2 = std::sin(u2);
  const double cu1su2 = cos_u1 * sin_u2;
  const double cu2su1 = cos_u2 * sin_u1;
  const double su1su2 = sin_u1 * sin_u2;
  const double cu1cu2 = cos_u1 * cos_u2;

  // iteration until convergence of lambda
  double lambda = l;
  for (size_t iters = 0; iters < 100; ++iters)
  {
    double cos_lambda = std::cos(lambda);
    double sin_lambda = std::sin(lambda);
    double sin_sigma  = std::hypot(cos_u2 * sin_lambda, cu1su2 - cu2su1 * cos_lambda);

    // check for coincident points
    if (sin_sigma == 0.0)
      return { 0.0_rad, 0.0 };

    double cos_sigma  = su1su2 + cu1cu2 * cos_lambda;
    double sigma      = std::atan2(sin_sigma, cos_sigma);
    double sin_alpha  = (cu1cu2 * sin_lambda) / sin_sigma;
    double csq_alpha  = 1.0 - sin_alpha * sin_alpha;
    double cos_2sigm  = cos_sigma - ((2.0 * su1su2) / csq_alpha);

    // check for equatorial line
    if (is_nan(cos_2sigm))
      cos_2sigm = 0.0;

    double c          = (f_ / 16.0) * csq_alpha * (4.0 + f_ * (4.0 - 3.0 * csq_alpha));
    double lambda_new = l + (1.0 - c) * f_ * sin_alpha * (sigma + c * sin_sigma *
                          (cos_2sigm + c * cos_sigma * (-1.0 + 2.0 * cos_2sigm)));

    // check for convergence to correct answer
    if (std::abs(lambda - lambda_new) < 1e-12)
    {
      double u_sqr  = csq_alpha * ep2_;
      double a      = 1.0 + (u_sqr / 16384.0) * (4096.0 + u_sqr *
                        (-768.0 + u_sqr * (320.0 - 175.0 * u_sqr)));
      double b      = (u_sqr / 1024.0) * (256.0 + u_sqr * (-128.0 + u_sqr * (74 - 47.0 * u_sqr)));
      double dsigma = b * sin_sigma * (cos_2sigm + 0.25 * b * (cos_sigma *
                        (-1.0 + 2.0 * cos_2sigm * cos_2sigm) - (1.0 / 6.0) * b * cos_2sigm *
                        (-3.0 + 4.0 * sin_sigma * sin_sigma) * (-3.0 + 4.0 * cos_2sigm * cos_2sigm)));
      double s      = b_ * a * (sigma - dsigma);
      double az_fwd = std::atan2(
                          (cos_u2 * std::sin(lambda_new))
                        , (cu1su2 - cu2su1 * std::cos(lambda_new)));
      return { az_fwd * 1_rad, s };
    }
    lambda = lambda_new;
  }

  // failed to converge - trace it and return the haversine solution
  //trace::debug("vincenty convergence failure: {} to {}", pos1, pos2);
  return latlon_to_bearing_range_haversine(pos1, pos2);
}

auto ellipsoid::bearing_range_to_latlon_haversine(latlon pos, angle bearing, double range) const -> latlon
{
  auto ronrad = (range / radius_of_curvature(pos.lat)) * 1_rad;
  auto srad = sin(ronrad);
  auto crad = cos(ronrad);
  auto slat = sin(pos.lat);
  auto clat = cos(pos.lat);

  auto lat = asin(slat * crad + clat * srad * cos(bearing));
  auto lon = pos.lon + atan2(sin(bearing) * srad * clat, crad - slat * sin(lat));

  return { lat, lon };
}

auto ellipsoid::latlon_to_bearing_range_haversine(latlon pos1, latlon pos2) const -> std::pair<angle, double>
{
  auto radius = radius_of_curvature(pos1.lat);

  auto dlat = pos2.lat - pos1.lat;
  auto dlon = pos2.lon - pos1.lon;

  auto clat1 = cos(pos1.lat);
  auto clat2 = cos(pos2.lat);

  // distance
  auto sdlat2 = sin(dlat * 0.5);
  auto sdlon2 = sin(dlon * 0.5);
  auto a = sdlat2 * sdlat2 + sdlon2 * sdlon2 * clat1 * clat2;
  auto r = radius * (2.0 * atan2(sqrt(a), sqrt(1.0 - a)).radians());

  // bearing
  auto y = sin(dlon) * clat2;
  auto x = clat1 * sin(pos2.lat) - sin(pos1.lat) * clat2 * cos(dlon);
  auto b = atan2(y,x);

  return { b, r };
}

auto ellipsoid::intersect_ray(vec3d start, vec3d direction, latlon& pos) const -> bool
{
  auto a2 = a_ * a_;
  auto b2 = b_ * b_;
  auto s2 = start.scale(start);
  auto d2 = direction.scale(direction);
  auto sd = start.scale(direction);
  auto v1 = (b2 * (sd.x + sd.y) + a2 * sd.z);
  auto v2 = (b2 * (d2.x + d2.y) + a2 * d2.z);
  auto sqrd = 4.0 * (v1 * v1 - v2 * (b2 * (-a2 + s2.x + s2.y) + a2 * s2.z));
  if (sqrd < 0.0)
    return false;

  auto root = std::sqrt(sqrd);
  auto t1 = -(1.0 / v2) * (b2 * (sd.x + sd.y) + a2 * sd.z + 0.5 * root);
  auto t2 = -(1.0 / v2) * (b2 * (sd.x + sd.y) + a2 * sd.z + 0.5 * -root);
  pos = ecefxyz_to_latlon(start + std::min(t1, t2) * direction);
  return true;
}

// LCOV_EXCL_START
TEST_CASE("ellipsoid")
{
  // these results sourced from WGS84 specification found at
  // http://earth-info.nga.mil/GandG/publications/tr8350.2/wgs84fin.pdf
  CHECK(wgs84.semi_major_axis() == approx(6378137.000));
  CHECK(wgs84.semi_minor_axis() == approx(6356752.3142));
  CHECK(wgs84.inverse_flattening() == approx(298.257223563));
  CHECK(wgs84.flattening() == approx(1.0 / 298.257223563));
  CHECK(wgs84.eccentricity() == approx(0.081819190842622));
  CHECK(wgs84.eccentricity_squared() == approx(.00669437999014));
  CHECK(wgs84.radius_mean() == approx(6371008.7714));
  CHECK(wgs84.radius_authalic() == approx(6371007.1809));
  CHECK(wgs84.radius_volumetric() == approx(6371000.7900));

  // results from https://rechneronline.de/earth-radius/
  CHECK(wgs84.radius_physical(-23.0_deg) == approx(6374895.0));
  CHECK(wgs84.radius_physical(0_deg) == approx(6378137.0));
  CHECK(wgs84.radius_physical(68.4_deg) == approx(6359671.0));

  // TODO radius_of_curvature_meridional
  // TODO radius_of_curvature_normal
  // TODO radius_of_curvature

  // results from http://www.oc.nps.edu/oc2902w/coord/llhxyz.htm
  auto xyz = wgs84.latlon_to_ecefxyz({-37.753743_deg, 145.314650_deg, 300.0});
  CHECK(xyz.x == approx(-4152109.));
  CHECK(xyz.y == approx(2873486.));
  CHECK(xyz.z == approx(-3884053.));

  // results from http://www.oc.nps.edu/oc2902w/coord/llhxyz.htm
  auto llh = wgs84.ecefxyz_to_latlon({-4129947.0, 2897061.0, -3889649.0});
  CHECK(llh.lat.degrees() == approx(-37.81957));
  CHECK(llh.lon.degrees() == approx(144.951237));
  CHECK(llh.alt == approx(9.92364));

  // results from http://www.movable-type.co.uk/scripts/latlong-vincenty.html
  auto br = wgs84.latlon_to_bearing_range({-37.753743_deg, 145.314650_deg}, {-37.819565_deg, 144.951237_deg});
  CHECK(br.first.degrees() == approx(-102.967214));
  CHECK(br.second == approx(32834.765));

  // TODO - verify this results independently
  br = wgs84.latlon_to_bearing_range_haversine({-37.753743_deg, 145.314650_deg}, {-37.819565_deg, 144.951237_deg});
  CHECK(br.first.degrees() == approx(-103.02));
  CHECK(br.second == approx(32772.6));

  // results from http://www.movable-type.co.uk/scripts/latlong-vincenty.html
  auto ll = wgs84.bearing_range_to_latlon({-37.753743_deg, 145.314650_deg}, -102.967214_deg, 32834.765);
  CHECK(ll.lat.degrees() == approx(-37.819565));
  CHECK(ll.lon.degrees() == approx(144.951237));

  ll = wgs84.bearing_range_to_latlon_haversine({-37.753743_deg, 145.314650_deg}, -102.967214_deg, 32834.765);
  CHECK(ll.lat.degrees() == approx(-37.819565));
  CHECK(ll.lon.degrees() == approx(144.951237));
}
// LCOV_EXCL_STOP
