/*------------------------------------------------------------------------------
 * Bureau of Meteorology C++ Utility Library
 *
 * Copyright 2014 Mark Curtis
 * Copyright 2016 Commonwealth of Australia, Bureau of Meteorology
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *----------------------------------------------------------------------------*/
#pragma once

#include "vec2.h"

namespace bom
{
  /// 2D axis aligned bounding box
  template <typename T>
  struct box2
  {
    using value_type = T;

    vec2<T> min, max;

    /// Expand the box to enclose a point
    auto expand(vec2<T> point) -> void
    {
      // can't optimize out the > tests as 'else ifs' because that fails for degenerate boxes such as the empty box
      if (point.x < min.x)
        min.x = point.x;
      if (point.x > max.x)
        max.x = point.x;
      if (point.y < min.y)
        min.y = point.y;
      if (point.y > max.y)
        max.y = point.y;
    }

    /// Expand the box to enclose another box
    auto expand(box2<T> box) -> void
    {
      if (box.min.x < min.x)
        min.x = box.min.x;
      if (box.max.x > max.x)
        max.x = box.max.x;
      if (box.min.y < min.y)
        min.y = box.min.y;
      if (box.max.y > max.y)
        max.y = box.max.y;
    }

    /// Is the passed point enclosed by the box?
    auto contains(vec2<T> point) const -> bool
    {
      return point.x >= min.x && point.x <= max.x && point.y >= min.y && point.y <= max.y;
    }

    /// Is the passed box fully enclosed by the box?
    auto contains(box2<T> box) const -> bool
    {
      return box.min.x >= min.x && box.max.x <= max.x && box.min.y >= min.y && box.max.y <= max.y;
    }

    /// Does the passed box have any intersetion with the box?
    auto intersects(box2<T> box) const -> bool
    {
      return !(box.max.x < min.x || max.x < box.min.x || box.max.y < min.y || max.y < box.min.y);
    }

    /// An empty box where min and max are set to their max and min values respectively
    /** This box may be used as a safe starting point when looping though a set of points and expanding the
     *  box to include each one.  Without this, you would need to initialize the box with the value of the
     *  first point and loop over the remaining points - which complicates the required logic. */
    static constexpr auto empty_box() -> box2
    {
      return
      {
          {std::numeric_limits<T>::max(), std::numeric_limits<T>::max()}
        , {std::numeric_limits<T>::lowest(), std::numeric_limits<T>::lowest()}
      };
    }
  };

  using box2i = box2<int>;
  using box2z = box2<size_t>;
  using box2f = box2<float>;
  using box2d = box2<double>;
}
