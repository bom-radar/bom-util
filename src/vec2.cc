/*------------------------------------------------------------------------------
 * Bureau of Meteorology C++ Utility Library
 *
 * Copyright 2014 Mark Curtis
 * Copyright 2016 Commonwealth of Australia, Bureau of Meteorology
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *----------------------------------------------------------------------------*/
#include "vec2.h"
#include "unit_test.h"

using namespace bom;

// sanity check that the default special member functions behave as we expect with regard to noexcept
static_assert(std::is_nothrow_default_constructible<vec2i>::value, "vec2i not nothrow default constructible");
static_assert(std::is_nothrow_copy_constructible<vec2i>::value, "vec2i not nothrow copy constructible");
static_assert(std::is_nothrow_move_constructible<vec2i>::value, "vec2i not nothrow move constructible");
static_assert(std::is_nothrow_copy_assignable<vec2i>::value, "vec2i not nothrow copy assignable");
static_assert(std::is_nothrow_move_assignable<vec2i>::value, "vec2i not nothrow move assignable");

// LCOV_EXCL_START
#include <sstream>
TEST_CASE("vec2")
{
  SUBCASE("basic")
  {
    vec2i a(10, 20);

    CHECK(a.x == 10);
    CHECK(a.y == 20);

    CHECK(a == vec2i(10, 20));
    CHECK(a != vec2i(11, 20));
    CHECK(a != vec2i(10, 22));

    CHECK((a += vec2i(1, 2)) == vec2i(11, 22));
    CHECK((a -= vec2i(1, 2)) == vec2i(10, 20));
    CHECK((a *= 2) == vec2i(20, 40));
    CHECK((a /= 10) == vec2i(2, 4));

    CHECK(vec2i(10, 20) + vec2i(1, 2) == vec2i(11, 22));
    CHECK(vec2i(10, 20) - vec2i(1, 2) == vec2i(9, 18));
    CHECK(vec2i(10, 20) * 2 == vec2i(20, 40));
    CHECK(vec2i(10, 20) / 10 == vec2i(1, 2));

    CHECK(vec2i(10, 20).dot(vec2i(1, 2)) == 50);
    CHECK(vec2i(10, 20).length_sqr() == 500);
    CHECK(std::abs(vec2i(10, 20).length() - 22.36) < 0.1);
    CHECK((vec2i(3, 4).angle(vec2i(-2, 1)) - 100.305_deg).abs() < 0.01_deg);
  }

  SUBCASE("serialization")
  {
    std::ostringstream oss;
    oss << vec2i(10, 20);
    CHECK(oss.str() == "10 20");

    vec2i v;
    std::istringstream iss("10 20");
    iss >> v;
    CHECK(v == vec2i(10, 20));
  }

  SUBCASE("format")
  {
    vec2i a{10, 20};
    CHECK(fmt::format("{}", a) == "10 20");
  }

  SUBCASE("mixed_mode")
  {
    vec2i a(10, 20);
    vec2f b(.1, .2);

    auto c = a + b;
    CHECK((std::is_same<decltype(c)::value_type, float>::value));
    CHECK(std::abs(c.x - 10.1) < 0.001);
    CHECK(std::abs(c.y - 20.2) < 0.001);

    c = a - b;
    CHECK(std::abs(c.x - 9.9) < 0.001);
    CHECK(std::abs(c.y - 19.8) < 0.001);

    c = a * 0.5;
    CHECK(std::abs(c.x - 5.0) < 0.001);
    CHECK(std::abs(c.y - 10.0) < 0.001);

    c = 0.5 * a;
    CHECK(std::abs(c.x - 5.0) < 0.001);
    CHECK(std::abs(c.y - 10.0) < 0.001);

    c = a / 0.5;
    CHECK(std::abs(c.x - 20.0) < 0.001);
    CHECK(std::abs(c.y - 40.0) < 0.001);
  }
}
// LCOV_EXCL_STOP
