/*------------------------------------------------------------------------------
 * Bureau of Meteorology C++ Utility Library
 *
 * Copyright 2014 Mark Curtis
 * Copyright 2016 Commonwealth of Australia, Bureau of Meteorology
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *----------------------------------------------------------------------------*/
#pragma once

#include <condition_variable>
#include <functional>
#include <list>
#include <mutex>
#include <thread>
#include <vector>

namespace bom
{
  /// Generic worker thread pool
  class thread_pool
  {
  public:
    /// Action to take upon normal completion of a task
    enum class task_action
    {
        finalize  // destroy task
      , repeat    // return task to end of queue for scheduling
    };

  public:
    thread_pool(size_t size, size_t cache_size = 64);
    thread_pool(thread_pool const&) = delete;
    thread_pool(thread_pool&&) = delete;
    auto operator=(thread_pool const&) -> thread_pool& = delete;
    auto operator=(thread_pool&&) noexcept -> thread_pool& = delete;
    ~thread_pool();

    auto size() const -> size_t { return threads_.size(); }
    auto enqueue(std::function<task_action()> job) -> void;

  private:
    using work_queue = std::list<std::function<task_action()>>;

  private:
    auto worker_main() -> void;

  private:
    size_t                    max_cache_; // maximum size of job object cache
    std::mutex                mut_;       // mutex to synchronize worker threads
    std::condition_variable   cv_;        // condition variable to wake workers
    bool                      shutdown_;  // flag to force all threads to exit
    std::vector<std::thread>  threads_;   // worker threads
    work_queue                work_;      // queued jobs
    work_queue                cache_;     // cache of job objects to save heap allocations
  };
}
