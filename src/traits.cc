/*------------------------------------------------------------------------------
 * Bureau of Meteorology C++ Utility Library
 *
 * Copyright 2014 Mark Curtis
 * Copyright 2016 Commonwealth of Australia, Bureau of Meteorology
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *----------------------------------------------------------------------------*/
#include "config.h"
#include "traits.h"
#include "unit_test.h"

#include <algorithm>

using namespace bom;

// LCOV_EXCL_START

// allow foo to convert to anything in an attempt to trick is_instance_of into giving us a bad result
template <typename T> struct bar { };
struct foo
{
  template <typename T>
  operator T() const { return T{}; }
};

TEST_CASE("traits")
{
  CHECK(all_of() == true);
  CHECK(all_of(false) == false);
  CHECK(all_of(true) == true);
  CHECK(all_of(true, true, true) == true);
  CHECK(all_of(true, false, true) == false);
  CHECK(all_of(true, true, false) == false);

  CHECK(min_of(10, 20, 5, 40) == 5);

  CHECK(max_of(10, 20, 5, 40) == 40);
  CHECK(max_of(10, 200, 5, 40) == 200);

  CHECK(is_sequential(100, 101, 102, 103, 104) == true);
  CHECK(is_sequential(100, 100, 102, 103, 104) == false);
  CHECK(is_sequential(100, 101, 102, 102, 104) == false);
  CHECK(is_sequential(100, 101, 102, 103, 103) == false);

  CHECK(is_index_sequence(100, 101, 102, 103, 104) == false);
  CHECK(is_index_sequence(0, 101, 102, 103, 104) == false);
  CHECK(is_index_sequence(0, 1, 2, 3, 4) == true);

  CHECK(type_one_of<int, bool, int, double>::value == true);
  CHECK(type_one_of<int, bool, short, double>::value == false);

  // doctest has link errors if using the trait directy in the check macro
  bool is_instance_of_false = is_instance_of<foo, bar>::value;
  CHECK(is_instance_of_false == false);

  bool is_instance_of_true = is_instance_of<bar<foo>, bar>::value;
  CHECK(is_instance_of_true == true);
}
// LCOV_EXCL_STOP
