/*------------------------------------------------------------------------------
 * Bureau of Meteorology C++ Utility Library
 *
 * Copyright 2016 Commonwealth of Australia, Bureau of Meteorology
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *----------------------------------------------------------------------------*/
#pragma once

#include "../file_utils.h"

// get rid of this if we wrap signals in our own enum
#include <signal.h>

// get rid of this if we wrap inotify flags in our own enum
#include <sys/inotify.h>

#include <functional>

namespace bom::sys
{
  /// Setup tracing for coloured output to a terminal
  auto setup_trace_coloured() -> void;

  /// Setup tracing for output to the systemd journal
  auto setup_trace_systemd() -> void;

  enum class service_state
  {
      ready
    , reloading
    , stopping
  };

  /// Wrapper for setting daemon states via sd_notify
  auto sd_notify_state(service_state state) -> void;

  /// Wrapper for setting custom daemon status string via sd_notify
  auto sd_notify_status(string status) -> void;

  /// Store file descriptors with the service manager to be retrieved after a restart
  //auto sd_store_fds(map<string, int> const& fdmap) -> void;
  auto sd_store_fds(vector<int> const& fdmap) -> void;

  /// Retrieve file descriptors which have been forwarded by the service manager
  //auto sd_retrieve_fds() -> map<string, int>;
  auto sd_retrieve_fds() -> vector<int>;

  /// Helper for the systemd watchdog timer
  /**
   * This class will detect the presence of SystemD and whether it is expecting this process to use the watchdog
   * system.  This enables the user to simply create an object of this type and then periodically call update()
   * without worrying about whether systemd is actually present.  If it is, then calling update() will cause a
   * watchdog keep-alive to be sent if (and only if) half of the watchdog timeout period has passed.
   *
   * If the user is expecting to perform a long operation and wishes to force a watchdog reset immediately rather
   * than waiting for the half way point, then they may call send_keepalive().
   *
   * Note that there is no advantage to only calling update() if enabled() returns true.  The update() function
   * already performs this check.  Simply add a call to 'update()' to your main loop and forget about it.
   */
  class sd_watchdog
  {
  public:
    sd_watchdog(timestamp now = timestamp::now());

    /// Whether the watchdog system is enabled
    auto enabled() const -> bool                { return period_ != duration::zero(); }

    /// Maximum time between keep-alives before systemd will terminate the process
    auto timeout() const -> duration            { return period_ * 2; }

    /// Time next keep-alive is due to be sent
    auto next_keepalive() const -> timestamp    { return next_; }

    /// Force a keep-alive to be sent now
    auto send_keepalive(timestamp now = timestamp::now()) -> void;

    /// Check and send a keep-alive if necessary
    auto update(timestamp now = timestamp::now()) -> void;

  private:
    duration  period_;
    timestamp next_;
  };

  /// Wrapper for linux signalfd API
  /**
   * Instanciating this class will initialize a signalfd instance and register all passed signals with it.  The
   * signals passed to the constructor will also be masked from the constructing thread so that they are only
   * reported via the signalfd mechanism.
   *
   * The file descriptor for the signalfd is set to non-blocking by default to facilitate the most likely use case
   * of wanting to poll on multiple file descriptors and signals at once in an asynchronous I/O environment.
   *
   * The original signal mask is NOT restored upon destruction of this class.  If it was, then any signals received
   * and processed via the signalfd mechasim would immediately be raised when the mask is unblocked. (I think).
   */
  class signalfd
  {
  public:
    signalfd(std::initializer_list<int> signals, bool non_blocking = true);

    /// Get a pollable file descriptor for the signalfd sysem
    /**
     * The file descriptor returned should be polled for the readable state to determine when there are signals
     * pending which will be returned by a call to read().  This descriptor is suitable for use with an edge
     * triggered polling mechanism (epoll) provided that read() is called repeatedly until it returns 0 between
     * successive polls.
     */
    auto fd() const -> file_desc_hnd const&     { return fd_; }

    /// Get the next available signal (or 0 if no signal has occured)
    auto read() -> int;

  private:
    file_desc_hnd fd_;
  };

  /// Wrapper for an eventfd handle used to break polling on asynchronous I/O
  class eventfd
  {
  public:
    eventfd(bool non_blocking = true);

    /// File descriptor which may be polled for readability
    auto fd() const -> file_desc_hnd const&       { return fd_; }

    /// Signal the event causing all functions currently polling on the descriptor to wake
    auto signal() -> void;

    /// Clear the event so that polling on the descriptor will block
    auto clear() -> void;

  private:
    file_desc_hnd fd_;
  };

  /// RAII wrapper for the inotify system
  /**
   * The file descriptor for the inotify instance is set to non-blocking by default to facilitate the most likely
   * use case of wanting to poll on multiple file descriptors once in an asynchronous I/O environment.
   */
  class inotify
  {
  public:
#if 0
    enum class event
    {
        access          = (1 << 0)
      , attrib          = (1 << 1)
      , close_write     = (1 << 2)
      , close_nowrite   = (1 << 3)
      , create          = (1 << 4)
      , deleted         = (1 << 5)
      , delete_self     = (1 << 6)
      , modify          = (1 << 7)
      , move_self       = (1 << 8)
      , moved_from      = (1 << 9)
      , moved_to        = (1 << 10)
      , open            = (1 << 11)
    };

    enum class option
    {
        dont_follow     = (
      , excl_unlink
      , mask_add
      , oneshot
      , onlydir
    };
#endif

    using callback_fn = std::function<void(inotify_event const&)>;

  public:
    inotify(bool non_blocking = true);

    auto fd() const -> file_desc_hnd const&     { return fd_; }

    auto add_watch(string const& path, uint32_t mask, callback_fn callback) -> int;

    auto remove_watch(int wd) -> void;

    auto dispatch_events() -> void;

  private:
    file_desc_hnd         fd_;
    map<int, callback_fn> watches_;
  };
}
