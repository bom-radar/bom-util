/*------------------------------------------------------------------------------
 * Bureau of Meteorology C++ Utility Library
 *
 * Copyright 2014 Mark Curtis
 * Copyright 2016 Commonwealth of Australia, Bureau of Meteorology
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *----------------------------------------------------------------------------*/
#pragma once

#include "array1.h"
#include "box2.h"
#include "string_utils.h"
#include "vec2.h"

namespace bom
{
  /// Definition of a 2D grid domain
  /** This class can be used to map between a grid's native coordinate system (which will often also be tied to a
   *  particular map_projection), and the indexes needed to address grid data stored in a 2d array.
   *
   *  The row and column coordinates of the grid do not need to be regularly spaced. */
  class grid_coordinates
  {
  public:
    /// Default construct an empty grid domain
    grid_coordinates();

    /// Construct a regular grid domain
    /** This constructor will initialize a domain of size cells.  The coordinates of the outside edge of cell 0,0
     *  is given by cell0_edge.  The spacing of subsequent cells is determined by cell_delta. */
    grid_coordinates(vec2z size, vec2d cell0_edge, vec2d cell_delta, string col_units, string row_units);

    /// Construct a grid domain by providing the native coordinates of column and row edges
    /** Note that this constructor accepts cell boundry coordinates, not cell centers.  This means that the arrays
     *  which are passed in should contain one more entry than the number of cells in the domain.  The first element
     *  of each array specifies the left/top edge of the first cell, while the final element specifies the
     *  right/bottom edge of the last cell.
     *
     *  This constructor will assess the passed arrays to determine whether the grid is regular in nature. */
    grid_coordinates(array1d col_edges, array1d row_edges, string col_units, string row_units, double regularity_constraint = 0.0001);

    grid_coordinates(grid_coordinates const&) = default;
    grid_coordinates(grid_coordinates&&) noexcept = default;

    auto operator=(grid_coordinates const&) -> grid_coordinates& = default;
    auto operator=(grid_coordinates&&) noexcept -> grid_coordinates& = default;

    /// Get the number of rows and columns in the grid domain
    auto size() const { return size_; }

    /// Get the native coordinates bounds of the grid domain
    auto bounds() const { return bounds_; }

    /// Get the units that the column coordinates are expressed in
    auto& col_units() const { return col_units_; }

    /// Get the units that the row coordinates are expressed in
    auto& row_units() const { return row_units_; }

    /// Get the maximum amount of irregularity in spacing of rows and columns allowed before grid is treated as irregular
    auto regularity_constraint() const { return regularity_constraint_; }

    /// Do the cell coordinates change monotonically for the whole domain in both directions?
    auto regular() const { return regular_; }

    /// Get the grid cell resolution in the native units
    /** NaN will be returned as the resolution corresponding to any axis which is not regular. */
    auto resolution() const { return resolution_; }

    /// Do the cell coordinates increase left to right and top to bottom?
    auto ascending() const { return ascending_; }

    /// Get the column leading edge coordinates
    /** Note the returned array contains size().x + 1 values.  The final value contains the coordinate of the trailing
     *  edge of the last column. */
    auto& col_edges() const { return cols_; }

    /// Get the row leading edge coordinates
    /** Note the returned array contains size().y + 1 values.  The final value contains the coordinate of the trailing
     *  edge of the last row. */
    auto& row_edges() const { return rows_; }

    /// Determine whether the passed native coordinates are within the grid domain
    auto contains(vec2d coordinate) const { return bounds_.contains(coordinate); }

    /// Get the index of the cell containing the passed native coordinates
    auto cell_containing(vec2d coordinate) const -> vec2z;

    /// Get the native coordinates of the centre of the cell at the passed row/column index
    /** The supplied index must be valid.  That is, 0 <= index.x < size().x && 0 <= index.y < size().y.  Violation
     *  of this precondition will invoke undefined behaviour. */
    auto center_of_cell(vec2z index) const -> vec2d;

    /// Get the native coordinate bounds of the cell at the passed row/column index
    auto bounds_of_cell(vec2z index) const -> box2d;

  private:
    vec2z         size_;
    box2d         bounds_;
    string        col_units_;
    string        row_units_;
    double        regularity_constraint_;
    bool          regular_;     // do the row and column coordinates advance regularly?
    vec2<double>  resolution_;  // grid cell resolution in native coordinates
    vec2<bool>    ascending_;   // do the row/column coordinates ascend along with the cell index?
    array1d       cols_;        // native coordinates of column edges (contains size_.x + 1 values)
    array1d       rows_;        // native coordinates of row edges (contains size_.y + 1 values)
  };
}
