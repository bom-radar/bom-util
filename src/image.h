/*------------------------------------------------------------------------------
 * Bureau of Meteorology C++ Utility Library
 *
 * Copyright 2014 Mark Curtis
 * Copyright 2016 Commonwealth of Australia, Bureau of Meteorology
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *----------------------------------------------------------------------------*/
#pragma once

#include "array2.h"
#include "colour.h"

namespace bom
{
  /// Graphical image stored in system memory
  /** \note Image data is stored from top to bottom, left to right.  That is, the first pixel in array represents the
   *        upper left corner of the image.  This is contrary to the OpenGL convention of having the image origin
   *        be the lower left corner.  We have chosen to deliberately use the upper-left origin convention for
   *        textures within BOM because it matches the convention used by our gridded products and is in general
   *        easier to reason about mentally when converting between texture coordinates and array indexes. */
  class image : public array2<colour>
  {
  public:
    using array2<colour>::array2;

    /// Overlay an image over this one at a specified position using alpha blending
    auto overlay(image const& img, vec2i position) -> void;
  };
}
