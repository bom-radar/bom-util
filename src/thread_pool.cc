/*------------------------------------------------------------------------------
 * Bureau of Meteorology C++ Utility Library
 *
 * Copyright 2014 Mark Curtis
 * Copyright 2016 Commonwealth of Australia, Bureau of Meteorology
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *----------------------------------------------------------------------------*/
#include "thread_pool.h"
#include "trace.h"

using namespace bom;

thread_pool::thread_pool(size_t size, size_t cache_size)
  : max_cache_{64}
  , shutdown_{false}
{
  if (size == 0)
    throw std::invalid_argument{"thread_pool invalid size"};

  // start the worker threads
  threads_.reserve(size);
  std::lock_guard<std::mutex> lock{mut_};
  for (size_t i = 0; i < size; ++i)
    threads_.emplace_back(&thread_pool::worker_main, this);
}

thread_pool::~thread_pool()
{
  // set the shutdown flag which will cause all worker thread to terminate
  {
    std::lock_guard<std::mutex> lock{mut_};
    shutdown_ = true;
  }

  // notify all the idle threads to wake up and terminate immediately
  cv_.notify_all();

  // wait for all threads to exit
  trace::log("thread_pool waiting for {} worker threads to exit", threads_.size());
  for (auto& t : threads_)
    t.join();
  trace::log("thread_pool all worker threads have terminated");
}

auto thread_pool::enqueue(std::function<task_action()> job) -> void
{
  // enqueue the work
  {
    std::lock_guard<std::mutex> lock{mut_};
    if (cache_.empty())
      work_.push_back(std::move(job));
    else
    {
      work_.splice(work_.end(), cache_, cache_.begin());
      work_.back() = std::move(job);
    }
  }
  
  // wake up a thread to do the work
  cv_.notify_one();
}

auto thread_pool::worker_main() -> void
{
  std::list<std::function<task_action()>> my_work;

  std::unique_lock<std::mutex> lock{mut_};
  while (!shutdown_)
  {
    // if there is no work to do then wait on the condition variable
    if (work_.empty())
    {
      cv_.wait(lock);
      continue;
    }
    my_work.splice(my_work.end(), work_, work_.begin());
    auto action = task_action::finalize;

    // perform the next work item on the queue
    lock.unlock();
    try
    {
      action = my_work.front()();
    }
    catch (std::exception& err)
    {
      trace::error("exception in worker thread: {}", format_exception(err));
    }
    catch (...)
    {
      trace::error("exception in worker thread: <unknown>");
    }
    lock.lock();

    // re-queue the job if desired
    if (action == task_action::repeat)
    {
      work_.splice(work_.end(), my_work);
    }
    // otherwise return the job object to the cache if room (and null it to release any argument resources)
    else if (cache_.size() < max_cache_)
    {
      cache_.splice(cache_.begin(), my_work);
      cache_.front() = nullptr;
    }
    else
    {
      my_work.clear();
    }
  }
}
