/*------------------------------------------------------------------------------
 * Bureau of Meteorology C++ Utility Library
 *
 * Copyright 2014 Mark Curtis
 * Copyright 2016 Commonwealth of Australia, Bureau of Meteorology
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *----------------------------------------------------------------------------*/
#include "vec3.h"
#include "unit_test.h"

using namespace bom;

// LCOV_EXCL_START
#include <sstream>
TEST_CASE("vec3")
{
  SUBCASE("basic")
  {
    vec3i a(10, 20, 30);

    CHECK(a.x == 10);
    CHECK(a.y == 20);
    CHECK(a.z == 30);

    CHECK(a == vec3i(10, 20, 30));
    CHECK(a != vec3i(11, 20, 30));
    CHECK(a != vec3i(10, 22, 30));
    CHECK(a != vec3i(10, 20, 33));

    CHECK((a += vec3i(1, 2, 3)) == vec3i(11, 22, 33));
    CHECK((a -= vec3i(1, 2, 3)) == vec3i(10, 20, 30));
    CHECK((a *= 2) == vec3i(20, 40, 60));
    CHECK((a /= 10) == vec3i(2, 4, 6));

    CHECK(vec3i(10, 20, 30) + vec3i(1, 2, 3) == vec3i(11, 22, 33));
    CHECK(vec3i(10, 20, 30) - vec3i(1, 2, 3) == vec3i(9, 18, 27));
    CHECK(vec3i(10, 20, 30) * 2 == vec3i(20, 40, 60));
    CHECK(vec3i(10, 20, 30) / 10 == vec3i(1, 2, 3));

    CHECK(vec3i(10, 20, 30).dot(vec3i(1, 2, 3)) == 140);
    CHECK(vec3i(10, 20, 30).cross(vec3i(3, 2, 1)) == vec3i(-40, 80, -40));
    CHECK(vec3i(10, 20, 30).length_sqr() == 1400);
    CHECK(std::abs(vec3i(10, 20, 30).length() -37.4) < 0.1);
    CHECK((vec3i(3, 4, -7).angle(vec3i(-2, 1, 3)) - 135.6_deg).abs() < 0.01_deg);
  }

  // serialization and unserialization
  SUBCASE("serialization")
  {
    std::ostringstream oss;
    oss << vec3i(10, 20, 30);
    CHECK(oss.str() == "10 20 30");

    vec3i v;
    std::istringstream iss("10 20 30");
    iss >> v;
    CHECK(v == vec3i(10, 20, 30));
  }

  // mixed mode arithmetic
  SUBCASE("mixed_mode")
  {
    vec3i a(10, 20, 30);
    vec3f b(.1, .2, .3);

    auto c = a + b;
    CHECK((std::is_same<decltype(c)::value_type, float>::value));
    CHECK(std::abs(c.x - 10.1) < 0.001);
    CHECK(std::abs(c.y - 20.2) < 0.001);
    CHECK(std::abs(c.z - 30.3) < 0.001);

    c = a - b;
    CHECK(std::abs(c.x - 9.9) < 0.001);
    CHECK(std::abs(c.y - 19.8) < 0.001);
    CHECK(std::abs(c.z - 29.7) < 0.001);

    c = a * 0.5;
    CHECK(std::abs(c.x - 5.0) < 0.001);
    CHECK(std::abs(c.y - 10.0) < 0.001);
    CHECK(std::abs(c.z - 15.0) < 0.001);

    c = 0.5 * a;
    CHECK(std::abs(c.x - 5.0) < 0.001);
    CHECK(std::abs(c.y - 10.0) < 0.001);
    CHECK(std::abs(c.z - 15.0) < 0.001);

    c = a / 0.5;
    CHECK(std::abs(c.x - 20.0) < 0.001);
    CHECK(std::abs(c.y - 40.0) < 0.001);
    CHECK(std::abs(c.z - 60.0) < 0.001);
  }
}
// LCOV_EXCL_STOP
