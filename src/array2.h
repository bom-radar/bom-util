/*------------------------------------------------------------------------------
 * Bureau of Meteorology C++ Utility Library
 *
 * Copyright 2014 Mark Curtis
 * Copyright 2016 Commonwealth of Australia, Bureau of Meteorology
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *----------------------------------------------------------------------------*/
#pragma once

#include "array1.h"
#include "vec2.h"

namespace bom
{
  template <typename T>
  class array2 : private array1<T>
  {
  private:
    using base = array1<T>;

  public:
    using value_type = typename base::value_type;
    using pointer = typename base::pointer;
    using const_pointer = typename base::const_pointer;
    using reference = typename base::reference;
    using const_reference = typename base::const_reference;
    using size_type = typename base::size_type;
    using difference_type = typename base::difference_type;
    using iterator = typename base::iterator;
    using const_iterator = typename base::const_iterator;
    using reverse_iterator = typename base::reverse_iterator;
    using const_reverse_iterator = typename base::const_reverse_iterator;

    using extents_type = vec2<size_type>;

  public:
    array2()
      : base{0}
      , extents_{0, 0}
    { }

    array2(extents_type extents)
      : base{extents.x * extents.y}
      , extents_{extents}
    { }

    array2(array2 const& rhs) = default;
    array2(array2&& rhs) noexcept = default;
    auto operator=(array2 const& rhs) -> array2& = default;
    auto operator=(array2&& rhs) noexcept -> array2& = default;

    auto resize(extents_type extents) -> void
    {
      base::resize(extents.x * extents.y);
      extents_ = extents;
    }

    auto extents() const noexcept -> extents_type                     { return extents_; }
    auto operator[](size_type i) noexcept -> pointer                  { return &base::operator[](i * extents_.x); }
    auto operator[](size_type i) const noexcept -> const_pointer      { return &base::operator[](i * extents_.x); }
    auto operator[](extents_type i) noexcept -> reference             { return base::operator[](i.y * extents_.x + i.x); }
    auto operator[](extents_type i) const noexcept -> const_reference { return base::operator[](i.y * extents_.x + i.x); }

    using base::size;
    using base::empty;
    using base::data;
    using base::begin;
    using base::end;
    using base::rbegin;
    using base::rend;
    using base::cbegin;
    using base::cend;
    using base::crbegin;
    using base::crend;
    using base::fill;

  private:
    extents_type extents_;
  };

  using array2i = array2<int>;
  using array2l = array2<long>;
  using array2z = array2<size_t>;
  using array2f = array2<float>;
  using array2d = array2<double>;
}
