/*------------------------------------------------------------------------------
 * Bureau of Meteorology C++ Utility Library
 *
 * Copyright 2014 Mark Curtis
 * Copyright 2016 Commonwealth of Australia, Bureau of Meteorology
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *----------------------------------------------------------------------------*/
#include "config.h"
#include "geometry_utils.h"
#include "unit_test.h"

#include <algorithm>
#include <limits>
#include <memory>

using namespace bom;

static bool is_convex(vec2f const& p1, vec2f const& p2, vec2f const& p3)
{
  return (p3.y - p1.y) * (p2.x - p1.x) - (p3.x - p1.x) * (p2.y - p1.y) > 0;
}

static bool in_cone(vec2f const& p1, vec2f const& p2, vec2f const& p3, vec2f const& p)
{
  if (is_convex(p1, p2, p3))
    return is_convex(p1, p2, p) && is_convex(p2, p3, p);
  else
    return is_convex(p1, p2, p) || is_convex(p2, p3, p);
}

static bool is_inside(vec2f const& p1, vec2f const& p2, vec2f const& p3, vec2f const& p)
{
  return !(is_convex(p1, p, p2) || is_convex(p2, p, p3) || is_convex(p3, p, p1));
}

//checks if two lines intersect
static bool lines_intersect(vec2f const& p11, vec2f const& p12, vec2f const& p21, vec2f const& p22)
{
  if (   (p11.x == p21.x && p11.y == p21.y)
      || (p11.x == p22.x && p11.y == p22.y)
      || (p12.x == p21.x && p12.y == p21.y)
      || (p12.x == p22.x && p12.y == p22.y))
    return false;
  vec2f o1{p12.y - p11.y, p11.x - p12.x};
  vec2f o2{p22.y - p21.y, p21.x - p22.x};
  if (   (p11 - p21).dot(o2) * (p12 - p21).dot(o2) > 0
      || (p21 - p11).dot(o1) * (p22 - p11).dot(o1) > 0)
    return false;
  return true;
}

auto polygon::winding() const -> point_winding
{
  if (points_.size() < 3)
    return point_winding::counter_clockwise;
  auto area = points_.back().x * points_[0].y - points_.back().y * points_[0].x;
  for (size_t i = 1; i < points_.size(); ++i)
    area += points_[i-1].x * points_[i].y - points_[i-1].y * points_[i].x;
  return area < 0 ? point_winding::clockwise : point_winding::counter_clockwise;
}

auto polygon::set_winding(point_winding dir) -> void
{
  if (winding() != dir)
    std::reverse(points_.begin(), points_.end());
}

auto polygon::remove_hole(polygon const& hole) -> bool
{
  // use the right-most point of the hole
  // TODO - maybe it would be nice to use the closest point to a point on the polygon?
  size_t hi = 0;
  for (size_t i = 1; i < hole.size(); i++)
    if (hole[i].x > hole[hi].x)
      hi = i;

  // find our best point at which to splice in the hole
  size_t pi = std::numeric_limits<size_t>::max();
  for (size_t i = 0; i < points_.size(); i++)
  {
    if (points_[i].x <= hole[hi].x)
      continue;
    if (!in_cone(points_[(i + points_.size() - 1) % points_.size()], points_[i], points_[(i + 1) % points_.size()], hole[hi])) 
      continue;
    if (   pi != std::numeric_limits<size_t>::max()
        && (points_[pi]-hole[hi]).normalize().x > (points_[i]-hole[hi]).normalize().x)
      continue;
    auto visible = true;
    for (size_t j = 0; j < points_.size(); ++j)
    {
      auto linep1 = points_[j];
      auto linep2 = points_[(j + 1) % points_.size()];
      if (lines_intersect(hole[hi], points_[i], linep1, linep2)) // TODO - this relies on x == x inside
      {
        visible = false;
        break;
      }
    }
    if (visible)
      pi = i;
  }

  // indicates that hole is not inside polygon
  if (pi == std::numeric_limits<size_t>::max())
    return false;

  // insert the hole points into the polygon (repeating both the poly point and hole point)
  points_.insert(points_.begin() + pi, hole.size() + 2, vec2f{0.0f, 0.0f});
  points_[pi] = points_[pi + hole.size() + 2];
  for (size_t i = 0; i <= hole.size(); ++i)
    points_[pi + 1 + i] = hole[(i + hi) % hole.size()];

  return true;
}

namespace
{
  struct ear_clip_vertex
  {
    vec2f  pos;
    bool   active;
    bool   ear;
    float  angle;
    size_t prev;
    size_t next;
  };
}

static void update_vertex(std::vector<ear_clip_vertex>& verts, size_t v)
{
  auto& v1 = verts[verts[v].prev];
  auto& v2 = verts[v];
  auto& v3 = verts[verts[v].next];

  auto convex = is_convex(v1.pos, v2.pos, v3.pos);

  auto vec1 = (v1.pos - v2.pos).normalize();
  auto vec3 = (v3.pos - v2.pos).normalize();
  v2.angle = vec1.x * vec3.x + vec1.y * vec3.y;

  if (convex)
  {
    v2.ear = true;
    for (size_t i = 0; i < verts.size(); i++)
    {
      if (verts[i].pos.x == v2.pos.x && verts[i].pos.y == v2.pos.y)
        continue;
      if (verts[i].pos.x == v1.pos.x && verts[i].pos.y == v1.pos.y)
        continue;
      if (verts[i].pos.x == v3.pos.x && verts[i].pos.y == v3.pos.y)
        continue;
      if (is_inside(v1.pos, v2.pos, v3.pos, verts[i].pos))
      {
        v2.ear = false;
        break;
      }
    }
  }
  else
    v2.ear = false;
}

auto polygon::triangulate(std::vector<uint16_t>& index, uint16_t base_index) -> bool
{
  // not a polygon
  if (points_.size() < 3)
    return false;

  // a simple triangle
  if (points_.size() == 3)
  {
    index.push_back(0 + base_index);
    index.push_back(1 + base_index);
    index.push_back(2 + base_index);
    return true;
  }

  // okay, setup for ear-clipping
  std::vector<ear_clip_vertex> verts(points_.size());
  for (size_t i = 0; i < points_.size(); ++i)
  {
    verts[i].pos = points_[i];
    verts[i].active = true;
    verts[i].ear = false;
    verts[i].next = (i == points_.size() - 1) ? 0 : i + 1;
    verts[i].prev = (i == 0) ? points_.size() - 1 : i - 1;
  }

  for (size_t i = 0; i < points_.size(); i++)
    update_vertex(verts, i);

  for (size_t i = 0; i < points_.size() - 3; ++i)
  {
    // find the most extruded ear
    size_t ear = std::numeric_limits<size_t>::max();
    for (size_t j = 0; j < points_.size(); ++j)
    {
      if (!verts[j].active || !verts[j].ear)
        continue;
      if (ear == std::numeric_limits<size_t>::max())
        ear = j;
      else if (verts[j].angle > verts[ear].angle)
        ear = j;
    }
    if (ear == std::numeric_limits<size_t>::max())
      return false;

    index.push_back(verts[ear].prev + base_index);
    index.push_back(ear + base_index);
    index.push_back(verts[ear].next + base_index);

    verts[ear].active = false;
    verts[verts[ear].prev].next = verts[ear].next;
    verts[verts[ear].next].prev = verts[ear].prev;

    if (i == points_.size() - 4)
      break;

    update_vertex(verts, verts[ear].prev);
    update_vertex(verts, verts[ear].next);
  }

  for (size_t i=0; i < points_.size(); ++i)
  {
    if (verts[i].active)
    {
      index.push_back(verts[i].prev + base_index);
      index.push_back(i + base_index);
      index.push_back(verts[i].next + base_index);
      break;
    }
  }

  return true;
}

// LCOV_EXCL_START
TEST_CASE("minimum_distance_to_line_segment_sqr")
{
  CHECK(minimum_distance_to_line_segment_sqr(vec2d{1.0, 2.0}, vec2d{1.0, 2.0}, vec2d{2.0, 3.0}) == approx(2.0));
  CHECK(minimum_distance_to_line_segment_sqr(vec2d{1.0, 2.0}, vec2d{1.0, 3.0}, vec2d{2.0, 2.5}) == approx(1.0));
  CHECK(minimum_distance_to_line_segment_sqr(vec2d{1.0, 2.0}, vec2d{1.0, 3.0}, vec2d{2.0, 2.5}) == approx(1.0));
}
// LCOV_EXCL_STOP
