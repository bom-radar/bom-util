/*------------------------------------------------------------------------------
 * Bureau of Meteorology C++ Utility Library
 *
 * Copyright 2014 Mark Curtis
 * Copyright 2016 Commonwealth of Australia, Bureau of Meteorology
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *----------------------------------------------------------------------------*/
#include "config.h"
#include "grid_coordinates.h"
#include "trace.h"
#include "unit_test.h"

using namespace bom;

grid_coordinates::grid_coordinates()
  : size_{0, 0}
  , bounds_(box2d::empty_box())
  , regularity_constraint_{0.0}
  , regular_{true}
  , resolution_{0.0, 0.0}
  , ascending_{true, true}
  , cols_{0}
  , rows_{0}
{ }

grid_coordinates::grid_coordinates(vec2z size, vec2d cell0_edge, vec2d cell_delta, string col_units, string row_units)
  : size_{size}
  , bounds_{box2d::empty_box()}
  , col_units_{std::move(col_units)}
  , row_units_{std::move(row_units)}
  , regularity_constraint_{0.0}
  , regular_{true}
  , resolution_{std::fabs(cell_delta.x), std::fabs(cell_delta.y)}
  , ascending_{cell_delta.x > 0.0, cell_delta.y > 0.0}
  , cols_{size_.x + 1}
  , rows_{size_.y + 1}
{
  for (size_t i = 0; i <= size_.x; ++i)
    cols_[i] = cell0_edge.x + i * cell_delta.x;
  for (size_t i = 0; i <= size_.y; ++i)
    rows_[i] = cell0_edge.y + i * cell_delta.y;

  bounds_.expand({cols_[0], rows_[0]});
  bounds_.expand({cols_[size_.x], rows_[size_.y]});
}

grid_coordinates::grid_coordinates(array1d col_edges, array1d row_edges, string col_units, string row_units, double regularity_constraint)
  : size_{col_edges.size() - 1, row_edges.size() - 1}
  , bounds_{box2d::empty_box()}
  , col_units_{std::move(col_units)}
  , row_units_{std::move(row_units)}
  , regularity_constraint_{regularity_constraint}
  , regular_{true}
  , cols_{std::move(col_edges)}
  , rows_{std::move(row_edges)}
{
  if (cols_.size() < 2 || rows_.size() < 2)
    throw std::logic_error{"attempt to initialize grid_coordinates with no cells"};

  resolution_.x = std::fabs(cols_[1] - cols_[0]);
  resolution_.y = std::fabs(rows_[1] - rows_[0]);

  ascending_.x = cols_[1] > cols_[0];
  ascending_.y = rows_[1] > rows_[0];

  bounds_.expand({cols_[0], rows_[0]});
  bounds_.expand({cols_[size_.x], rows_[size_.y]});

  vec2d d{cols_[1] - cols_[0], rows_[1] - rows_[0]};
  if (std::adjacent_find(cols_.begin(), cols_.end(), [&](double l, double r){ return std::abs(r - l - d.x) > regularity_constraint_; }) != cols_.end())
  {
    resolution_.x = nan<double>();
    regular_ = false;
  }
  if (std::adjacent_find(rows_.begin(), rows_.end(), [&](double l, double r){ return std::abs(r - l - d.y) > regularity_constraint_; }) != rows_.end())
  {
    resolution_.y = nan<double>();
    regular_ = false;
  }
}

auto grid_coordinates::cell_containing(vec2d coordinate) const -> vec2z
{
  // TODO - this function performs no error checking.  if coordinate is not contained by the grid then the
  //        grid cell returned will be (potentially) nonsense
  vec2z cell;
  if (regular_)
  {
    cell.x = (coordinate.x - cols_[0]) / (cols_[1] - cols_[0]);
    cell.y = (coordinate.y - rows_[0]) / (rows_[1] - rows_[0]);
  }
  else
  {
    if (ascending_.x)
      cell.x = std::upper_bound(cols_.begin(), cols_.end(), coordinate.x) - cols_.begin() - 1;
    else
      cell.x = std::upper_bound(cols_.begin(), cols_.end(), coordinate.x, [](double l, double r){ return r < l; }) - cols_.begin() - 1;

    if (ascending_.y)
      cell.y = std::upper_bound(rows_.begin(), rows_.end(), coordinate.y) - rows_.begin() - 1;
    else
      cell.y = std::upper_bound(rows_.begin(), rows_.end(), coordinate.y, [](double l, double r){ return r < l; }) - rows_.begin() - 1;
  }
  return cell;
}

auto grid_coordinates::center_of_cell(vec2z index) const -> vec2d
{
  return {0.5 * (cols_[index.x] + cols_[index.x + 1]), 0.5 * (rows_[index.y] + rows_[index.y + 1])};
}

auto grid_coordinates::bounds_of_cell(vec2z index) const -> box2d
{
  box2d ret{{cols_[index.x], rows_[index.y]}, {cols_[index.x + 1], rows_[index.y + 1]}};
  if (ret.max.x < ret.min.x)
    std::swap(ret.min.x, ret.max.x);
  if (ret.max.y < ret.min.y)
    std::swap(ret.min.y, ret.max.y);
  return ret;
}

// LCOV_EXCL_START
TEST_CASE("grid_coordinates")
{
  SUBCASE("default")
  {
    grid_coordinates coords{};
    CHECK(coords.size() == vec2z{0, 0});
    CHECK(coords.col_edges().size() == 0);
    CHECK(coords.row_edges().size() == 0);
  }

  SUBCASE("regular")
  {
    grid_coordinates coords{{10,20}, {-50.0, 0.0}, {10.0, -1.0}, "m", "m"};
    CHECK(coords.size() == vec2z{10,20});
    CHECK(coords.bounds().min.x == approx(-50.0));
    CHECK(coords.bounds().min.y == approx(-20.0));
    CHECK(coords.bounds().max.x == approx(50.0));
    CHECK(coords.bounds().max.y == approx(0.0));
    CHECK(coords.regularity_constraint() == approx(0.0));
    CHECK(coords.regular() == true);
    CHECK(coords.resolution().x == approx(10.0));
    CHECK(coords.resolution().y == approx(1.0));
    CHECK(coords.ascending() == vec2<bool>{true, false});
    REQUIRE(coords.col_edges().size() == 11);
    CHECK(coords.col_edges()[0] == approx(-50.0));
    CHECK(coords.col_edges()[1] == approx(-40.0));
    CHECK(coords.col_edges()[2] == approx(-30.0));
    REQUIRE(coords.row_edges().size() == 21);
    CHECK(coords.row_edges()[0] == approx(0.0));
    CHECK(coords.row_edges()[1] == approx(-1.0));
    CHECK(coords.row_edges()[2] == approx(-2.0));
    CHECK(coords.contains({0.0, -5.0}) == true);
    CHECK(coords.contains({-60.0, -5.0}) == false);
    CHECK(coords.cell_containing({-45.0, -0.5}) == vec2z{0,0});
    CHECK(coords.cell_containing({-35.0, -1.5}) == vec2z{1,1});
    CHECK(coords.cell_containing({49.5, -19.5}) == vec2z{9,19});
    CHECK(coords.center_of_cell({0,0}).x == approx(-45.0));
    CHECK(coords.center_of_cell({0,0}).y == approx(-0.5));
    CHECK(coords.bounds_of_cell({0,0}).min.x == approx(-50.0));
    CHECK(coords.bounds_of_cell({0,0}).min.y == approx(-1.0));
    CHECK(coords.bounds_of_cell({0,0}).max.x == approx(-40.0));
    CHECK(coords.bounds_of_cell({0,0}).max.y == approx(0.0));
  }

  SUBCASE("irregular")
  {
    array1d cols{6}, rows{5};

    cols[0] = 15.0; cols[1] = 12.0; cols[2] = 9.0; cols[3] = 7.0; cols[4] = 4.0; cols[5] = 1.0;
    rows[0] = 0.1; rows[1] = 0.2; rows[2] = 0.3; rows[3] = 0.7; rows[4] = 0.8;

    grid_coordinates coords{cols, rows, "km", "km"};
    CHECK(coords.size() == vec2z{5,4});
    CHECK(coords.bounds().min.x == approx(1.0));
    CHECK(coords.bounds().min.y == approx(0.1));
    CHECK(coords.bounds().max.x == approx(15.0));
    CHECK(coords.bounds().max.y == approx(0.8));
    CHECK(coords.regular() == false);
    CHECK(is_nan(coords.resolution().x));
    CHECK(is_nan(coords.resolution().y));
    CHECK(coords.ascending() == vec2<bool>{false, true});
    REQUIRE(coords.col_edges().size() == 6);
    CHECK(coords.col_edges()[0] == approx(15.0));
    CHECK(coords.col_edges()[1] == approx(12.0));
    CHECK(coords.col_edges()[2] == approx(9.0));
    REQUIRE(coords.row_edges().size() == 5);
    CHECK(coords.row_edges()[0] == approx(0.1));
    CHECK(coords.row_edges()[1] == approx(0.2));
    CHECK(coords.row_edges()[2] == approx(0.3));
    CHECK(coords.contains({6.0,0.2}) == true);
    CHECK(coords.contains({8.0,0.0}) == false);
    CHECK(coords.cell_containing({8.5,0.75}) == vec2z{2,3});
    CHECK(coords.center_of_cell({2, 0}).x == approx(8.0));
    CHECK(coords.center_of_cell({2, 0}).y == approx(0.15));
    CHECK(coords.bounds_of_cell({2, 2}).min.x == approx(7.0));
    CHECK(coords.bounds_of_cell({2, 2}).min.y == approx(0.3));
    CHECK(coords.bounds_of_cell({2, 2}).max.x == approx(9.0));
    CHECK(coords.bounds_of_cell({2, 2}).max.y == approx(0.7));

    grid_coordinates coords2{rows, cols, "km", "km"};
    CHECK(coords2.ascending() == vec2<bool>{true, false});
    CHECK(coords2.cell_containing({0.75,8.5}) == vec2z{3,2});

    CHECK_THROWS(grid_coordinates(array1d{1}, array1d{10}, "km", "km"));
    CHECK_THROWS(grid_coordinates(array1d{10}, array1d{1}, "nmiles", "nmiles"));
  }
}
// LCOV_EXCL_STOP
